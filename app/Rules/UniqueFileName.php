<?php

namespace App\Rules;

use App\Helpers\Facades\Tool;
use DB;
use Illuminate\Http\Request;
use Illuminate\Contracts\Validation\Rule;

class UniqueFileName implements Rule
{
    public $id;

    public $type = null;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id, $type = null) {
        $this->id = $id;
        $this->type = ! empty($type) ? $type : Tool::getConst('TYPE_ATTACH');
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $arr_file_name = [];
        foreach($value as $files) {
            array_push($arr_file_name, $files->getClientOriginalName());
        }
        $current_doc = DB::table('documents')->where('id', $this->id)->first();
        if (isset($current_doc) && $current_doc->status != Tool::getConst('DOCUMENT_PUBLISHED') && $this->type == Tool::getConst('TYPE_ATTACH')) {
            return true;
        } else {
            $type = $this->type;
            $allDocs = DB::table('documents')->where('code', $current_doc->code)->get();
            $arrFileNames = [];
            $allDocs->each(function ($doc) use (&$arrFileNames, $type) {
                $files = DB::table('files')->where('doc_id', $doc->id)->where('type', $type)->where('is_allowed', Tool::getConst('ACTIVE_ON'))->get();
                if ($files->count()) {
                    foreach($files as $file) {
                        $path = public_path($file->link . '/' . $file->file_name);
                        if (file_exists($path)) {
                            $arrFileNames[] = $file->file_name;
                        }
                    }
                }
            });
            $result = [];
            foreach($arrFileNames as $fileName){
                if (in_array($fileName, $arr_file_name)) {
                    array_push($result, $fileName);
                }
            }
            return count($result) == 0;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message() {
        switch($this->type) {
            case Tool::getConst('TYPE_ATTACH'):
                $message = trans('message.validate.file_dinh_kem_khong_duoc_trung_ten');
                break;
            case Tool::getConst('TYPE_BBDT'):
                $message = trans('message.validate.file_bien_ban_dao_tao_khong_duoc_trung_ten');
                break;
            case Tool::getConst('TYPE_PPTH'):
                $message = trans('message.validate.file_phan_phat_thu_hoi_khong_duoc_trung_ten');
                break;
            default:
                break;
        }
        return $message;
    }
}
