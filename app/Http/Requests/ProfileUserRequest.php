<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProfileUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');;
        switch ($this->method()) {
            case 'POST':
                {
                    return [
                        'full_name' => [
                            'max:255',
                        ],
                        'avatar' => 'image|mimes:jpeg,JPEG,png,PNG,jpg,JPG|max:2069',
                        'email' => [
                            'required',
                            'unique:users,email,' . null . ',id,status,1',
                            'regex: /[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}/',
                        ],
                    ];
                }
            case 'PATCH':
                {
                    return [
                        'full_name' => [
                            'max:255', 'required',
                        ],
                        'email' => [
                            'required',
                            'email' => 'unique:users,email,' . $id . ',id,status,1',
                            'regex: /[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}/',
                        ],
                    ];
                }
            case 'PUT':
                {
                    return [
                        'full_name' => [
                            'max:255',
                        ],
                        'avatar' => 'image|mimes:jpeg,JPEG,png,PNG,jpg,JPG|max:2069',
                        'email' => 'required|unique:users,email,' . $id . ',id,status,1|regex: /[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}/',
                    ];

                }
            default:
                break;
        }

    }

    /**
     * Return message if validation fails
     *
     * @return array
     */
    public function messages()
    {
        return [
            'full_name.required' => trans('message.vui_long_nhap_ho_ten'),
            'full_name.max' => trans('message.ho_ten_khong_vuot_qua_255_ki_tu'),
            'email.required' => trans('message.vui_long_nhap_email'),
            'email.regex' => trans('message.email_sai_dinh_dang_nhap_lai'),
            'email.unique' => trans('message.email_existed'),
            'avatar.mimes' => trans('message.anh_phai_thuoc_mot_trong_cac_dinh_dang_png_jpg_jpeg'),
            'avatar.image' => trans('message.anh_phai_thuoc_mot_trong_cac_dinh_dang_png_jpg_jpeg'),
            'avatar.max' => trans('message.dung_luong_anh_khong_duoc_vuot_qua_2MB')

        ];
    }
}
