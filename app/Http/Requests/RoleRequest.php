<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    return [
                        'name' => 'required|max:255|unique:roles,name,' . null . ',id,status,1',
                        'description' => 'nullable|max:255',
                    ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                        'name' => 'required|max:255|unique:roles,name,' . request()->route('id') . ',id,status,1',
                        'description' => 'nullable|max:255',
                    ];
                }
            default:
                break;
        }
    }

    public function messages()
    {
        return [
            'name.required' => trans('message.chua_nhap_ten_quyen_truy_cap'),
            'name.min' => trans('message.ten_quyen_truy_cap_phai_tu_4_ki_tu'),
            'name.max' => trans('message.ten_quyen_truy_cap_khong_duoc_qua_255_ki_tu'),
            'name.unique' => trans('message.ten_quyen_truy_cap_da_ton_tai'),
            'description.max' => trans('message.max_length_description'),
        ];
    }
}
