<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');
        switch ($this->method()) {
            case 'POST':
                {
                    return [
                        'title' => [
                            'required',
                            'max:255',
                            'unique:products,title,' . null . ',id,status,1',
                        ],
                    ];
                }
            case 'PATCH':
                {
                    return [
                        'title' => [
                            'required',
                            'max:255',
                            'unique:products,title,' . request()->route('id') . ',id,status,1',
                        ],
                    ];
                }
            case 'PUT':
                {
                    return [
                        'title' => [
                            'required',
                            'max:255',
                            'unique:products,title,' . request()->route('id') . ',id,status,1',
                        ],
                    ];
                }
            default:
                break;
        }

    }

    /**
     * Return message if validation fails
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.max' => 'Title không đươc vượt quá 255 kí tự',
            'title.required' => 'Title không được để trống',
            'title.unique' => 'Title đã tồn tại',
        ];
    }
}
