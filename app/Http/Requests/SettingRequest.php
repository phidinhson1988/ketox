<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    return [
                        'timeout' => 'required|integer|between:1,90',
                    ];
                }
            case 'PATCH':
            default:
                break;
        }
    }

    public function messages()
    {
        return [
            'timeout.required' => trans('message.chua_nhap_thoi_gian_truy_cap'),
            'timeout.integer' => trans('message.timeout_limit'),
            'timeout.between' => trans('message.timeout_between'),
        ];
    }
}
