<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');
        switch ($this->method()) {
            case 'POST':
                {
                    return [
                        'username' => [
                            'required',
                            'max:255',
                            'unique:users,username,' . null . ',id,status,1',
                        ],
                        'full_name' => [
                            'max:255', 'required',
                        ],
                        'email' => [
                            // 'required',
                            'nullable',
                            'unique:users,email,' . null . ',id,status,1',
                        ],
                        'role_id' => 'required|integer|not_in:0',
                    ];
                }
            case 'PATCH':
            case 'PUT':
                {
                    return [
                        'email' => [
                            // 'required',
                            'nullable',
                            'unique:users,email,' . $id . ',id,status,1',
                        ],
                        'role_id' => 'sometimes|required|integer|not_in:0',
                        'full_name' => [
                            'max:255', 'required',
                        ],
                    ];
                }
            default:
                break;
        }

    }

    /**
     * Return message if validation fails
     *
     * @return array
     */
    public function messages()
    {
        return [
            'username.required' => trans('message.vui_long_nhap_ten_dang_nhap'),
            'username.unique' => trans('message.ten_dang_nhap_da_ton_tai'),
            'role_id.not_in' => trans('message.vui_long_chon_quyen_han'),
            'role_id.integer' => trans('message.vui_long_chon_quyen_han'),
            'role_id.required' => trans('message.vui_long_chon_quyen_han'),
            'name.max' => trans('message.ho_ten_khong_vuot_qua_255_ki_tu'),
            'email.required' => trans('message.vui_long_nhap_email'),
            'email.regex' => trans('message.email_sai_dinh_dang_nhap_lai'),
            'email.unique' => trans('message.email_existed'),
            'avatar.mimes' => trans('message.anh_phai_thuoc_mot_trong_cac_dinh_dang_png_jpg_jpeg'),
            'avatar.image' => trans('message.anh_phai_thuoc_mot_trong_cac_dinh_dang_png_jpg_jpeg'),
            'avatar.max' => trans('message.dung_luong_anh_khong_duoc_vuot_qua_2MB'),
            'full_name.required' => trans('message.chua_nhap_ho_ten'),
        ];
    }
}
