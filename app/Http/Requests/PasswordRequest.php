<?php

namespace App\Http\Requests;

use App;
use Request;
use Illuminate\Foundation\Http\FormRequest;

class PasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                {

                }
                break;
            case 'PUT':
                {
                    return [
                        'old_password' => 'required',
                        'new_password' => 'required|min:6|max:20|different:old_password',
                        'password_confirmation' => 'required|min:6|max:20|same:new_password',
                    ];
                }
                break;
            case 'PATCH':
                {

                }
                break;
            default:
                break;
        }
    }

    public function messages()
    {

            return [

                'old_password.min' => trans('message.mat_khau_cu_tu_6_ki_tu_tro_len'),
                'old_password.max' => trans('message.mat_khau_cu_khong_qua_20_ki_tu'),
                'new_password.min' => trans('message.mat_khau_moi_phai_tu_6_ki_tu_tro_len'),
                'new_password.max' => trans('message.mat_khau_moi_khong_qua_20_ki_tu'),
                'password_confirmation.min' => trans('message.mat_khau_phai_tu_6_ki_tu_tro_len'),
                'old_password.required' => trans('message.chua_nhap_mat_khau_cu'),
                'new_password.required' => trans('message.chua_nhap_mat_khau_moi'),
                'password_confirmation.required' => trans('message.chua_xac_nhan_mat_khau_moi'),
                'password_confirmation.min' => trans('message.mat_khau_phai_tu_6_ki_tu_tro_len'),
                'password_confirmation.max' => trans('message.mat_khau_khong_qua_20_ki_tu'),
                'password_confirmation.same' => trans('message.phai_nhap_giong_mat_khau_moi'),
                'new_password.different' => trans('message.mat_khau_moi_phai_khac_mat_khau_cu'),
            ];

    }
}
