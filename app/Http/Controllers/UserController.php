<?php

/**
 * @package     DMS
 * @category    User Controller
 * @copyright   Copyright 2018 FPT
 * @author      SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Http\Controllers;

use App\Helpers\Facades\Tool;
use App\Http\Requests\PasswordRequest;
use App\Http\Requests\ProfileUserRequest;
use App\Models\RoleUser;
use App\Models\Role;
use App\Models\User;
use Dompdf\Exception;
use Illuminate\Http\Request;
use App\Helpers\Pagination;
use App\Http\Requests\UserRequest;
use Carbon\Carbon;
use Auth;
use App\Helpers\Module;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UserController extends Controller {
    /*
           * @description: xử lý dữ liệu hiển thị user,tìm kiếm user
           * @input:  hiển thị list không có input / tìm kiếm user
           * @output: array data
           * @author: KhoiDT3
           * @created at: 7/8/2018
     * *** */

    public function index(Request $request) {
        try {
            $limit = $request->get('limit') ? : Tool::getConst('DEFAULT_LIMIT');
            $q = $request->get('q', null);
            $role_id = $request->get('role_id', null);
            $data['limit'] = $limit;
            $append = compact('q', 'limit');
            $data['users'] = User::fetchAll($limit, $role_id, $q);

            $data['pagination'] = Pagination::init($data['users'])
                                ->limit(Tool::getConst('PAGGING_LIMIT'))
                                ->appends($append)
                                ->render();
            $data['roles'] = Role::showAllRoles();
            return view('user.index', $data, $append);
        } catch (\Exception $e) {
            abort(500);
        }
    }

    /*
           * @description: mở form thêm người dùng
           * @input:
           * @output: mở form thêm người dùng
           * @author: KhoiDT3
           * @created at: 7/8/2018
     * *** */

    public function create() {
        try {
            $data['roles'] = Role::showAllRoles();
            return view('user.create', $data);
        } catch (\Exception $e) {
            abort(500);
        }
    }

    /*
           * @description: mở màn hình password
           * @input:
           * @output: mở màn hình password
           * @author: KhoiDT3
           * @created at: 20/8/2018
     * *** */

    public function password() {
        return view('user.password');
    }

    /*
           * @description: cập nhập password
           * @input: password_old, password_new
           * @output: cập nhập mật khẩu thành công / lỗi
           * @author: KhoiDT3
           * @created at: 20/8/2018
     * *** */

    public function postChangePassword(PasswordRequest $request) {
        try {
            $user = Auth::user();
            if (!empty($user) && $user->status == true) {
                //Handle when update supplier
                if (Hash::check($request->old_password, $user->password)) {
                    User::passwordInstance($request, $user);
                    return redirect()->route('user.password')->with([
                                'level' => 'success',
                                'message' => trans('message.cap_nhat_mat_khau_thanh_cong')
                    ]);
                } else {
                    return redirect()->route('user.password')->with([
                                'level' => 'error',
                                'error' => trans('message.mat_khau_cu_sai_vui_long_thu_lai')
                    ]);
                }
            }
            abort(404);
        } catch (\Exception $e) {
            abort(500);
        }
    }

    /*
           * @description: them User người dùng
           * @input:  các trường dữ liệu bảng User
           * @output: array data
           * @author: KhoiDT3
           * @created at: 7/8/2018
     * *** */

    public function store(UserRequest $request) {
        DB::beginTransaction();
        try {
            User::storeInstance($request);
            DB::commit();
            return redirect()->route('user.index')->with([
                        'message' => trans('message.them_moi_tai_khoan_thanh_cong'),
                        'level' => 'success',
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            abort(500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

    }

    /*
           * @description: hiển thị màn hình edit
           * @input:
           * @output: hiển thị màn hình edit
           * @author: KhoiDT3
           * @created at: 7/8/2018
     * *** */

    public function edit($id) {
        $data['user'] = User::findOrFail($id);
        if (!empty($data['user']) && $data['user']->status == true) {
            $data['roles'] = Role::showAllRoles();
            //GET USER STAGE
            //GET USER ROLE
            $roleUser = RoleUser::where('user_id', $id)->first();
            if (!empty($roleUser)) {
                $data['roleUser'] = $roleUser->toArray();
            }
            return view('user.edit', $data);
        }
        abort(404);
    }

    /*
           * @description: update thông tin người dùng
           * @input:  name,phone,date,image,stage_id,department_id,role_id
           * @output: array data
           * @author: KhoiDT3
           * @created at: 7/8/2018
     * *** */

    public function resetPassword($id) {
        $user = User::find($id);
        if (!empty($user) && $user->status == true) {
            User::resetPass($user);
            return redirect()->route('user.index')->with([
                        'message' => trans('message.reset_mat_khau_nguoi_dung_thanh_cong'),
                        'level' => 'success',
            ]);
        }
        abort(404);
    }

    /*
           * @description: cập nhập tài khoản người dùng
           * @input:  name,phone,date,image,stage_id,department_id,role_id
           * @output: array data
           * @author: KhoiDT3
           * @created at: 7/8/2018
     * *** */

    public function update(Request $request, $id) {
        $user = User::findOrFail($id);
        if (!empty($user) && $user->status == true) {
            User::updateInstance($request, $user);

            return redirect()->route('user.index')->with([
                        'message' => trans('message.cap_nhat_tai_khoan_thanh_cong'),
                        'level' => 'success',
            ]);
        }
        abort(404);
    }

    /*
           * @description: xóa user người dùng
           * @input:  id
           * @output: xóa user người dùng
           * @author: KhoiDT3
           * @created at: 7/8/2018
     * *** */

    public function delete($id) {
        $statusCheck = true;
        $user = User::findOrFail($id);
        if (!empty($user) && $user->status == true) {
            if (Auth::user()->id == $user->id) {
                return back()->with(
                                [
                                    'level' => 'error',
                                    'error' => trans('message.khong_cho_phep_xoa_tai_khoan_dang_nhap_hien_tai')
                ]);
            } else {
                $user->status = Tool::getConst('ACTIVE_OFF');
                $user->update();

                //DELETE PTMS
                if (User::checkConnectAnotherDB()) {
                    $userFind = new User();
                    $userFind->setConnection('mysql2');
                    $userSync = $userFind->where('username', $user->username)->first();
                    if (!empty($userSync)) {
                        $userSync->status = Tool::getConst('ACTIVE_OFF');
                        $userSync->update();
                    }
                }

                return back()->with(['level' => 'success', 'message' => trans('message.xoa_nguoi_dung_thanh_cong')]);
                //DELETE DMS

            }
        }
        abort(404);
    }

    /*
           * @description: profile người dùng đăng nhập
           * @input:  id
           * @output: array profile người dùng đăng nhập
           * @author: KhoiDT3
           * @created at: 19/8/2018
     * *** */

    public function profile() {
        try {
            $roleUser = RoleUser::join('roles', 'roles.id', '=', 'role_user.role_id')
                    ->where('role_user.user_id', Auth::user()->id)
                    ->where('roles.status', true)
                    ->first();

            if (!empty($roleUser)) {
                $data['roleUser'] = $roleUser;
            }
            return view('user.profile', $data);
        } catch (\Exception $e) {
            abort(500);
        }
    }

    /*
           * @description: edit profile
           * @input:  id
           * @output: edit profile thành công
           * @author: KhoiDT3
           * @created at: 19/8/2018
     * *** */

    public function editProfile(Request $request, $id) {
        $user = User::findOrFail($id);
        if (!empty($user) && $user->status == true) {
            //Handle when update supplier
            User::profileInstance($request, $user);
            return redirect()->route('user.profile')->with([
                        'message' => trans('message.cap_nhat_tai_khoan_thanh_cong'),
                        'level' => 'success',
            ]);
        }
        abort(404);
    }

    public function sendNotification() {
        $user = User::findOrFail(3);
        $title = trans('message.thong_bao_moi');
        $content = trans('message.da_cap_nhat_thong_tin_nguoi_dung');

        Module::sendNotification($user, ['title' => $title, 'content' => $content]);

        return redirect()->route('home')->with([
                    'message' => trans('message.them_moi_thanh_cong'),
                    'level' => 'success',
        ]);
    }

}
