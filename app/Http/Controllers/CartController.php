<?php

/**
 * @package     CartController
 * @category    Produc Controller
 * @copyright   Copyright 2018 FPT
 * @author      SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Http\Controllers;

use App\Helpers\Facades\Tool;
use Illuminate\Http\Request;
use App\Helpers\Pagination;
use App\Models\Cart;
use DB;
use Carbon\Carbon;

class CartController extends Controller {
    
    /*
     * @description: Danh sách đơn hàng
     * @input: $request
     * @output: thông tin đơn hàng
     * @author: SonPD1
     * @created at: 17/8/2018
     * *** */
    public function index(Request $request) {
        $limit = $request->get('limit') ?: Tool::getConst('DEFAULT_LIMIT');
        $status = $request->get('status', null);
        $data['limit'] = $limit;
        $append = compact('status', 'limit');
        $data['carts'] = Cart::fetchAll($limit, $status);
        $data['pagination'] = Pagination::init($data['carts'])
            ->limit(Tool::getConst('PAGGING_LIMIT'))
            ->appends($append)
            ->render();
        return view('cart.index', $data, $append);
    }

    /*
     * @description: xóa đơn hàng
     * @input: id
     * @output: xóa đơn hàng
     * @author: SonPD1
     * @created at: 17/8/2018
     * *** */

    public function delete($id) {
        try {
            DB::table('carts')->where('id', $id)->update(['status' => 2, 'updated_at' => Carbon::now()]);
            return back()->with(['level' => 'success', 'message' => trans('message.cap_nhat_don_hang_thanh_cong')]);
        } catch (Exception $e) {
            abort(500);
        }
    }

}
