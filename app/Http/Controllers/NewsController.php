<?php

/**
 * @package     NewsController
 * @category    Produc Controller
 * @copyright   Copyright 2018 FPT
 * @author      SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Http\Controllers;

use App\Helpers\Facades\Tool;
use App\Helpers\Pagination;
use App\Http\Requests\NewsRequest;
use App\Models\News;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Auth;

class NewsController extends Controller {
    
    /*
     * @description: Danh sách tin tức
     * @input: $request
     * @output: thông tin tin tức
     * @author: SonPD1
     * @created at: 17/8/2018
     * *** */
    public function index(Request $request) {
        $limit = $request->get('limit') ?: Tool::getConst('DEFAULT_LIMIT');
        $q = $request->get('q', null);
        $type = $request->get('type', null);
        $data['limit'] = $limit;
        $data['type'] = $type;
        $append = compact('q', 'limit', 'type');
        $data['news'] = News::fetchAll($limit, $q, 'id', 'desc', $type);
        $data['pagination'] = Pagination::init($data['news'])
            ->limit(Tool::getConst('PAGGING_LIMIT'))
            ->appends($append)
            ->render();
        return view('news.index', $data, $append);
    }

    /*
     * @description: Create tin tức
     * @input: $request
     * @output: Create tin tức
     * @author: SonPD1
     * @created at: 17/8/2018
     * *** */
    public function create() {
        try {
            return view('news.create');
        } catch (Exception $e) {
            abort(500);
        }
    }
    
    /**
     * @description: Kiem tra slug
     * @input      :
     * @output     : view
     * @author     : SonPD1
     * @created_at : 15/04/2019
     */
    public function checkSlug(Request $request) {
        $status = true;
        $message = '';
        $slug = $request->get('slug', null);
        $newsId = $request->get('newsId', null);
        try {
            if ($slug) {
                $slug = trim(strip_tags($slug));
                $slug = Str::slug($slug);
                if ($newsId) {
                    $check = News::where('slug', $slug)->where('id', '<>', $newsId)->count();
                } else {
                    $check = News::where('slug', $slug)->count();
                }
                if ($check > 0) {
                    $status = false;
                    $message = trans('message.slug_da_ton_tai_thu_lai_voi_slug_khac');
                }
            }
        } catch (Exception $e) {
            $status = false;
            $message = trans('message.loi_server_thu_lai_sau');
        }
        return json_encode(['status' => $status, 'message' => $message, 'slug' => $slug]);
    }

    /*
     * @description: Create tin tức
     * @input: $request
     * @output: Create tin tức
     * @author: SonPD1
     * @created at: 17/8/2018
     * *** */
    public function store(NewsRequest $request) {
        try {
            News::storeInstance($request);
            return redirect()->route('news.index')->with([
                        'message' => trans('message.them_moi_tin_tuc_thanh_cong'),
                        'level' => 'success',
            ]);
        } catch (Exception $e) {
            abort(500);
        }
    }

    /*
     * @description: Edit tin tức
     * @input: $id
     * @output: Edit tin tức
     * @author: SonPD1
     * @created at: 17/8/2018
     * *** */
    public function edit($id) {
        $post = News::findOrFail($id);
        if (!empty($post)) {
            $post->start_date = $post->start_date ? Carbon::parse($post->start_date)->format('d-m-Y') : null;
            $post->end_date = $post->end_date ? Carbon::parse($post->end_date)->format('d-m-Y') : null;
            return view('news.edit', [
                'post' => $post,
            ]);
        }
        abort(404);
    }

    /*
     * @description: lưu tin tức
     * @input: $id, $request
     * @output: lưu tin tức
     * @author: SonPD1
     * @created at: 17/8/2018
     * *** */
    public function update(Request $request, $id) {
        $post = News::findOrFail($id);
        News::updateInstance($request, $post);
        return redirect()->route('news.index')->with([
                    'message' => trans('message.cap_nhat_tin_tuc_thanh_cong'),
                    'level' => 'success',
        ]);
    }

    /*
     * @description: xóa tin tức
     * @input: id
     * @output: xóa tin tức
     * @author: SonPD1
     * @created at: 17/8/2018
     * *** */

    public function delete(Request $request, $id) {
        try {
            $status = $request->get('status', 0);
            DB::table('posts')->where('id', $id)->update(['status' => $status]);
            $message = $status ? 'Active tin tức thành công!' : trans('message.xoa_tin_tuc_thanh_cong');
            return back()->with(['level' => 'success', 'message' => $message]);
        } catch (Exception $e) {
            abort(500);
        }
    }

}
