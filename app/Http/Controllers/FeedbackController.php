<?php

/**
 * @package     FeedbackController
 * @category    Feedback Controller
 * @copyright   Copyright 2018 FPT
 * @author      SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Http\Controllers;

use App\Helpers\Facades\Tool;
use App\Helpers\Pagination;
use App\Models\Feedback;
use App\Models\Contact;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Auth;

class FeedbackController extends Controller {
    
    /*
     * @description: Danh sách feedback
     * @input: $request
     * @output: thông tin tin tức
     * @author: SonPD1
     * @created at: 17/8/2018
     * *** */
    public function index(Request $request) {
        $limit = $request->get('limit') ?: Tool::getConst('DEFAULT_LIMIT');
        $q = $request->get('q', null);
        $data['limit'] = $limit;
        $append = compact('q', 'limit');
        $data['feedbacks'] = Feedback::fetchAll($limit, $q, 'id', 'desc');
        $data['pagination'] = Pagination::init($data['feedbacks'])
            ->limit(Tool::getConst('PAGGING_LIMIT'))
            ->appends($append)
            ->render();
        return view('feedback.index', $data, $append);
    }
    
    /*
     * @description: Danh sách mail
     * @input: $request
     * @output: thông tin tin tức
     * @author: SonPD1
     * @created at: 17/8/2018
     * *** */
    public function mail(Request $request) {
        $limit = $request->get('limit') ?: Tool::getConst('DEFAULT_LIMIT');
        $q = $request->get('q', null);
        $data['limit'] = $limit;
        $append = compact('q', 'limit');
        $data['mails'] = Contact::fetchAll($limit, $q, 'id', 'desc');
        $data['pagination'] = Pagination::init($data['mails'])
            ->limit(Tool::getConst('PAGGING_LIMIT'))
            ->appends($append)
            ->render();
        return view('feedback.mail', $data, $append);
    }
    
    /*
     * @description: export mail
     * @input: $request
     * @output: thông tin tin tức
     * @author: SonPD1
     * @created at: 17/8/2018
     * *** */
    public function exportMail(Request $request) {
        $mails = Contact::whereNotNull('email')->pluck('email')->toArray();
        // Export Excel
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Danh sách mail');
        $sheet->getStyle('A1')->getFont()->setSize(18)->setBold(true);
        $totalLine = 4;
        foreach ($mails as $mail) {
            $sheet->setCellValue('A' . $totalLine, $mail);
            $totalLine += 1;
        }

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: Attachment;filename="Danh-sach-mail.xlsx');
        header('Cache-Control: max-age=0');
        $writer->save('php://output');
    }

    /*
     * @description: Create tin tức
     * @input: $request
     * @output: Create tin tức
     * @author: SonPD1
     * @created at: 17/8/2018
     * *** */
    public function create() {
        try {
            $products = \App\Models\Product::where('status', 1)->get();
            $users = \App\Models\User::where('status', 1)->get();
            return view('feedback.create', ['products' => $products, 'users' => $users]);
        } catch (Exception $e) {
            abort(500);
        }
    }
    
    /*
     * @description: Create tin tức
     * @input: $request
     * @output: Create tin tức
     * @author: SonPD1
     * @created at: 17/8/2018
     * *** */
    public function store(Request $request) {
        try {
            Feedback::storeInstance($request);
            return redirect()->route('feedback.index')->with([
                        'message' => trans('message.them_moi_phan_hoi_thanh_cong'),
                        'level' => 'success',
            ]);
        } catch (Exception $e) {
            abort(500);
        }
    }

    /*
     * @description: Edit tin tức
     * @input: $id
     * @output: Edit tin tức
     * @author: SonPD1
     * @created at: 17/8/2018
     * *** */
    public function edit($id) {
        $post = Feedback::findOrFail($id);
        if (!empty($post)) {
            $products = \App\Models\Product::where('status', 1)->get();
            $users = \App\Models\User::where('status', 1)->get();
            return view('feedback.edit', [
                'post' => $post,
                'products' => $products,
                'users' => $users
            ]);
        }
        abort(404);
    }

    /*
     * @description: lưu tin tức
     * @input: $id, $request
     * @output: lưu tin tức
     * @author: SonPD1
     * @created at: 17/8/2018
     * *** */
    public function update(Request $request, $id) {
        $post = Feedback::findOrFail($id);
        Feedback::updateInstance($request, $post);
        return redirect()->route('feedback.index')->with([
                    'message' => trans('message.cap_nhat_phan_hoi_thanh_cong'),
                    'level' => 'success',
        ]);
    }

    /*
     * @description: xóa tin tức
     * @input: id
     * @output: xóa tin tức
     * @author: SonPD1
     * @created at: 17/8/2018
     * *** */

    public function delete(Request $request, $id) {
        try {
            $status = $request->get('status', 0);
            DB::table('feedbacks')->where('id', $id)->update(['status' => $status]);
            $message = $status ? 'Active phản hồi thành công!' : trans('message.xoa_phan_hoi_thanh_cong');
            return back()->with(['level' => 'success', 'message' => $message]);
        } catch (Exception $e) {
            abort(500);
        }
    }

}
