<?php

/**
 * @package     ProductController
 * @category    Produc Controller
 * @copyright   Copyright 2018 FPT
 * @author      SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Http\Controllers;

use App\Helpers\Facades\Tool;
use App\Http\Requests\ProductRequest;
use App\Helpers\Pagination;
use App\Models\Product;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Auth;

class ProductController extends Controller {
    /*
           * @description: Danh sách sản phẩm
           * @input: $request
           * @output: thông tin sản phẩm
           * @author: SonPD1
           * @created at: 17/8/2018
     * *** */

    public function index(Request $request) {
        $limit = $request->get('limit') ? : Tool::getConst('DEFAULT_LIMIT');
        $q = $request->get('q', null);
        $data['limit'] = $limit;
        $append = compact('q', 'limit');
        $data['products'] = Product::fetchAll($limit, $q);
        $data['pagination'] = Pagination::init($data['products'])
                ->limit(Tool::getConst('PAGGING_LIMIT'))
                ->appends($append)
                ->render();
        return view('product.index', $data, $append);
    }

    /*
           * @description: Create sản phẩm
           * @input: $request
           * @output: Create sản phẩm
           * @author: SonPD1
           * @created at: 17/8/2018
     * *** */

    public function create() {
        try {
            return view('product.create');
        } catch (Exception $e) {
            abort(500);
        }
    }

    /**
     * @description: Kiem tra slug
     * @input      :
     * @output     : view
     * @author     : SonPD1
     * @created_at : 15/04/2019
     */
    public function checkSlug(Request $request) {
        $status = true;
        $message = '';
        $slug = $request->get('slug', null);
        $productId = $request->get('productId', null);
        try {
            if ($slug) {
                $slug = trim(strip_tags($slug));
                $slug = Str::slug($slug);
                if ($productId) {
                    $check = Product::where('slug', $slug)->where('id', '<>', $productId)->count();
                } else {
                    $check = Product::where('slug', $slug)->count();
                }
                if ($check > 0) {
                    $status = false;
                    $message = trans('message.slug_da_ton_tai_thu_lai_voi_slug_khac');
                }
            }
        } catch (Exception $e) {
            $status = false;
            $message = trans('message.loi_server_thu_lai_sau');
        }
        return json_encode(['status' => $status, 'message' => $message, 'slug' => $slug]);
    }

    /**
     * @description: uploadFile
     * @input      :
     * @output     : view
     * @author     : SonPD1
     * @created_at : 15/04/2019
     */
    public function uploadFile(Request $request) {
        $error = '';
        $status = true;
        $path = '';
        $size = 0;
        $fileName = '';
        try {
            //Kiểm tra file
            if ($request->hasFile('file_upload')) {
                $file = $request->file_upload;
                $size = $file->getSize();
                $fileName = $file->getClientOriginalName();
                $fileName = Carbon::now()->timestamp . $fileName;
                $fileName = preg_replace('/\s+/', '-', $fileName);
                if (!file_exists(public_path('upload/draw'))) {
                    mkdir(public_path('upload/draw'), 0777, true);
                }
                $file->move('upload/draw/', $fileName);
                $path = asset('upload') . '/draw/' . $fileName;
            }
        } catch (Exception $e) {
            $error = trans('message.loi_server_thu_lai_sau');
            $status = false;
        }
        return json_encode(['status' => $status, 'path' => $path, 'error' => $error, 'fileName' => $fileName, 'size' => $size]);
    }

    /**
     * @description: uploadFile
     * @input      :
     * @output     : view
     * @author     : SonPD1
     * @created_at : 15/04/2019
     */
    public function cropFileImage(Request $request) {
        $error = '';
        $status = true;
        $path = '';
        $width = $request->get('w');
        $height = $request->get('h');
        $fileName = $request->get('fileName');
        $pathinfo = pathinfo($fileName);
        $extension = $pathinfo['extension'];
        $newFileName = $pathinfo['filename'] . '-' . $width . '-' . $height . '-' . $_GET['x'] . '-' . $_GET['y'] . '.' . $extension;
        $typeImage = 1;
        try {
            switch (strtolower($extension)) {
                case 'png':
                    $typeImage = 1;
                    break;
                case 'jpg':
                    $typeImage = 2;
                    break;
                case 'jpeg':
                    $typeImage = 2;
                    break;
                case 'gif':
                    $typeImage = 3;
                    break;
                default:
                    break;
            }
            if ($typeImage == 1) {
                $img_r = imagecreatefrompng($request->get('img'));
            } else if ($typeImage == 2) {
                $img_r = imagecreatefromjpeg($request->get('img'));
            } else {
                $img_r = imagecreatefromgif($request->get('img'));
            }
            $dst_r = ImageCreateTrueColor($width, $height);

            imagecopyresampled($dst_r, $img_r, 0, 0, $_GET['x'], $_GET['y'], $width, $height, $width, $height);
            if (!file_exists(public_path('upload/new'))) {
                mkdir(public_path('upload/new'), 0777, true);
            }
            $newPath = 'upload/new/' . $newFileName;
            $path = asset('upload') . '/new/' . $newFileName;
            imagejpeg($dst_r, $newPath);
            if ($typeImage == 1) {
                imagepng($dst_r, $newPath);
            } else if ($typeImage == 2) {
                imagejpeg($dst_r, $newPath);
            } else {
                imagegif($dst_r, $newPath);
            }
        } catch (Exception $e) {
            $error = trans('message.loi_server_thu_lai_sau');
            $status = false;
        }
        return json_encode(['status' => $status, 'path' => $path, 'nameFile' => $newFileName, 'error' => $error]);
    }

    /*
           * @description: Create sản phẩm
           * @input: $request
           * @output: Create sản phẩm
           * @author: SonPD1
           * @created at: 17/8/2018
     * *** */

    public function store(ProductRequest $request) {
        try {
            Product::storeInstance($request);
            return redirect()->route('product.index')->with([
                        'message' => trans('message.them_moi_san_pham_thanh_cong'),
                        'level' => 'success',
            ]);
        } catch (Exception $e) {
            abort(500);
        }
    }

    /*
           * @description: Edit sản phẩm
           * @input: $id
           * @output: Edit sản phẩm
           * @author: SonPD1
           * @created at: 17/8/2018
     * *** */

    public function edit($id) {
        $product = Product::findOrFail($id);
        if (!empty($product)) {
            return view('product.edit', [
                'product' => $product,
            ]);
        }
        abort(404);
    }

    /*
           * @description: lưu sản phẩm
           * @input: $id, $request
           * @output: lưu sản phẩm
           * @author: SonPD1
           * @created at: 17/8/2018
     * *** */

    public function update(Request $request, $id) {
        $product = Product::findOrFail($id);
        Product::updateInstance($request, $product);
        return redirect()->route('product.index')->with([
                    'message' => trans('message.cap_nhat_san_pham_thanh_cong'),
                    'level' => 'success',
        ]);
    }

    /*
           * @description: xóa sản phẩm
           * @input: id
           * @output: xóa sản phẩm
           * @author: SonPD1
           * @created at: 17/8/2018
     * *** */

    public function delete(Request $request, $id) {
        try {
            $status = $request->get('status', 0);
            DB::table('products')->where('id', $id)->update(['status' => $status]);
            $message = $status ? trans('message.active_san_pham_thanh_cong') : trans('message.xoa_san_pham_thanh_cong');
            return back()->with(['level' => 'success', 'message' => $message]);
        } catch (Exception $e) {
            abort(500);
        }
    }

}
