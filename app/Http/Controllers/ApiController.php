<?php

/**
 * @package     DMS
 * @category    Api Controller
 * @copyright   Copyright 2018 FPT
 * @author      SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Http\Controllers;

use App\Models\History;
use App\Models\Document;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Helpers\Facades\Tool;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ApiController extends Controller {

}
