<?php

/**
 * @package     DMS
 * @category    Role Controller
 * @copyright   Copyright 2018 FPT
 * @author      SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Http\Controllers;

use App\Helpers\Facades\Tool;
use App\Http\Requests\RoleRequest;
use App\Models\Permission;
use App\Models\PermissionRole;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Helpers\Pagination;
use DB;

class RoleController extends Controller
{
    /*
           * @description: xử lý dữ liệu hiển thị role,tìm kiếm role
           * @input:  hiển thị list không có input / tìm kiếm role $keywork
           * @output: array data
           * @author: KhoiDT3
           * @created at: 17/8/2018
     * *** */

    public function index(Request $request)
    {
        $limit = $request->get('limit') ?: Tool::getConst('DEFAULT_LIMIT');
        $q = $request->get('q', null);
        $data['limit'] = $limit;
        $append = compact('q', 'limit');
        $data['roles'] = Role::fetchAll($limit, $q);
        $data['pagination'] = Pagination::init($data['roles'])
            ->limit(Tool::getConst('PAGGING_LIMIT'))
            ->appends($append)
            ->render();
        return view('role.index', $data, $append);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            //GET DOC_TYPE

            //GET PERMISSION
            $permissions = Permission::orderBy("sort", "asc")->get()->toArray();
            if ($permissions) {
                $dataPermission = [];
                foreach ($permissions as $permission) {
                    if ($permission['parents']) {
                        $permission['parents'] = json_decode($permission['parents'], true);
                    }
                    if ($permission['childs']) {
                        $permission['childs'] = json_decode($permission['childs'], true);
                    }
                    $dataPermission[$permission['order']] = $permission;
                }
            }
            return view('role.create', ['permissions' => $dataPermission]);
        } catch (Exception $e) {
            abort(500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        try {
            Role::storeInstance($request);

            return redirect()->route('role.index')->with([
                'message' => trans('message.them_moi_quyen_thanh_cong'),
                'level' => 'success',
            ]);
        } catch (Exception $e) {
            abort(500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::findOrFail($id);
        if (!empty($role) && $role->status == true) {
            //GET PERMISSION
            $permissions = Permission::orderBy("sort", "asc")->get()->toArray();
            if ($permissions) {
                $dataPermission = [];
                foreach ($permissions as $permission) {
                    if ($permission['parents']) {
                        $permission['parents'] = json_decode($permission['parents'], true);
                    }
                    if ($permission['childs']) {
                        $permission['childs'] = json_decode($permission['childs'], true);
                    }
                    $dataPermission[$permission['order']] = $permission;
                }
                $rolePermissions = PermissionRole::where("permission_role.role_id", $id)
                    ->pluck("permission_role.permission_id", "permission_role.permission_id")
                    ->all();
            }
            return view('role.edit', [
                'role' => $role,
                'permissions' => $dataPermission,
                'rolePermissions' => $rolePermissions
            ]);
        }
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, $id)
    {
        $role = Role::findOrFail($id);
        if (!empty($role) && $role->status == true) {
            Role::updateInstance($request, $role);
            return redirect()->route('role.index')->with([
                'message' => trans('message.cap_nhat_quyen_thanh_cong'),
                'level' => 'success',
            ]);
        }
        abort(404);
    }

    /*
           * @description: xóa role
           * @input: id
           * @output: xóa quyền hạn
           * @author: KhoiDT3
           * @created at: 17/8/2018
     * *** */

    public function delete($id)
    {
        try {
            $role = DB::table('roles')->where('id', $id)->whereNotIn('name', ['root', 'Admin']);
            if ($role->count() > 0) {
                $role->update(['status' => Tool::getConst('ACTIVE_OFF')]);
                return back()->with(['level' => 'success', 'message' => trans('message.xoa_quyen_han_thanh_cong')]);
            } else {
                return back()->with(['level' => 'error', 'error' => trans('message.quyen_han_khong_the_xoa')]);
            }
        } catch (Exception $e) {
            abort(500);
        }
    }
}
