<?php

/**
 * @package     DMS
 * @category    Product Controller
 * @copyright   Copyright 2018 FPT
 * @author      SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Http\Controllers\Frontend;

use App\Helpers\Facades\Tool;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\News;
use App\Helpers\Pagination;
use Artisan;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Session;
use Auth;

class NewsController extends Controller {
    
    /**
     * @description: Màn hình tin tuc
     * @author     : SonPD1
     */
    public function index(Request $request) {
        try {
            $data = [];
            $order = $request->get('sort', 1);
            $q = $request->get('q', null);
            $limit = $request->get('limit', Tool::getConst('DEFAULT_LIMIT'));
            $query = News::where('status', 1)->where('type', 1);
            if (strlen(trim($q)) > 0) {
                $query = $query->where(function ($querySub) use ($q) {
                    $querySub->where('posts.title', 'LIKE', '%' . Tool::sanitizeQuery($q) . '%');
                });
            }
            $data['sortName'] = 'Mới nhất';
            switch ($order) {
                case 1:
                    $query->orderBy('id', 'desc');
                    $data['sortName'] = 'Mới nhất';
                    break;
                case 2:
                    $query->orderBy('id', 'asc');
                    $data['sortName'] = 'Cũ nhất';
                    break;
                default :
                    break;
            }
            $posts = $query->paginate($limit);
            $data['posts'] = $posts;
            $data['search'] = $q;
            $data['sort'] = $order;
            $data['limit'] = $limit;
            $pagination = Pagination::init($posts)->limit(Tool::getConst('PAGGING_LIMIT'))->appends(['sort' => $data['sort'], 'limit' => $limit, 'q' => $q])->render();
            $data['pagination'] = $pagination;
            return view('frontend.news.index', $data);
        } catch (Exception $e) {
            abort(500);
        }
    }
    
    /**
     * @description: Màn hình career
     * @author     : SonPD1
     */
    public function career(Request $request) {
        try {
            $data = [];
            $order = $request->get('sort', 1);
            $q = $request->get('q', null);
            $limit = $request->get('limit', Tool::getConst('DEFAULT_LIMIT'));
            $query = News::where('status', 1)->where('type', 2);
            if (strlen(trim($q)) > 0) {
                $select = $select->where(function ($query) use ($q) {
                    $query->where('posts.title', 'LIKE', '%' . Tool::sanitizeQuery($q) . '%');
                });
            }
            $data['sortName'] = 'Mới nhất';
            switch ($order) {
                case 1:
                    $query->orderBy('id', 'desc');
                    $data['sortName'] = 'Mới nhất';
                    break;
                case 2:
                    $query->orderBy('id', 'asc');
                    $data['sortName'] = 'Cũ nhất';
                    break;
                case 3:
                    $query->orderBy('price', 'desc');
                    $data['sortName'] = 'Giá từ cao đến thấp';
                    break;
                case 4:
                    $query->orderBy('price', 'asc');
                    $data['sortName'] = 'Giá từ thấp đến cao';
                    break;
                default :
                    break;
            }
            $posts = $query->paginate($limit);
            $data['posts'] = $posts;
            $data['search'] = $q;
            $data['sort'] = $order;
            $data['limit'] = $limit;
            $pagination = Pagination::init($posts)->limit(Tool::getConst('PAGGING_LIMIT'))->appends(['sort' => $data['sort'], 'limit' => $limit, 'q' => $q])->render();
            $data['pagination'] = $pagination;
            return view('frontend.news.career', $data);
        } catch (Exception $e) {
            abort(500);
        }
    }
    
    /**
     * @description: Màn hình detail
     * @author     : SonPD1
     */
    public function detail(Request $request, $slug) {
        try {
            if ($slug) {
                $post = News::where('slug', $slug)->where('status', 1)->first();
                if ($post) {
                    if ($post->is_require && !Auth::check()) {
                        return redirect()->route('login')->with([
                            'level' => 'error',
                            'error' => trans('message.ban_can_dang_nhap_de_doc_bai_viet_nay')
                        ]);
                    }
                    $others = News::where('status', 1)->where('id', '<>', $post->id)->where('type', 1)->orderBy('id', 'desc')->take(3)->get();
                    return view('frontend.news.detail', ['post' => $post, 'others' => $others]);
                }
            }
            abort(404);
        } catch (Exception $e) {
            abort(500);
        }
    }
    
    /**
     * @description: Màn hình detail career
     * @author     : SonPD1
     */
    public function detailCareer(Request $request, $slug) {
        try {
            if ($slug) {
                $post = News::where('slug', $slug)->where('status', 1)->first();
                if ($post) {
                    $others = News::where('status', 1)->where('id', '<>', $post->id)->where('type', 2)->orderBy('id', 'desc')->take(3)->get();
                    return view('frontend.news.detail-career', ['post' => $post, 'others' => $others]);
                }
            }
            abort(404);
        } catch (Exception $e) {
            abort(500);
        }
    }
}
