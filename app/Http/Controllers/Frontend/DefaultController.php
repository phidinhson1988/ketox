<?php

/**
 * @package     DMS
 * @category    Home Controller
 * @copyright   Copyright 2018 FPT
 * @author      SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Http\Controllers\Frontend;

use App\Helpers\Facades\Tool;
use App\Http\Controllers\Controller;
use App\Models\User;
use Artisan;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Session;
use Auth;

class DefaultController extends Controller {
    
    /**
     * @description: Màn hình home trang chu
     * @author     : SonPD1
     */
    public function index(Request $request) {
        // check update db:
        if (!\Illuminate\Support\Facades\Schema::hasColumn('contacts', 'type')) {
            $sql1 = "ALTER TABLE `contacts` ADD COLUMN `type` TINYINT(1) DEFAULT 1 NULL COMMENT '1: contact; 2: newletter' AFTER `status`;";
            DB::select($sql1);
        }
        if (!\Illuminate\Support\Facades\Schema::hasColumn('feedbacks', 'avatar')) {
            $sql11 = "ALTER TABLE `feedbacks` ADD COLUMN `avatar` VARCHAR(255) NULL AFTER `status`;";
            DB::select($sql11);
        }
        if (!\Illuminate\Support\Facades\Schema::hasColumn('products', 'sort')) {
            $sql2 = "ALTER TABLE `products` ADD COLUMN `sort` INT(11) DEFAULT 1 NULL AFTER `lang`;";
            DB::select($sql2);
        }
        if (!\Illuminate\Support\Facades\Schema::hasColumn('contacts', 'image')) {
            $query1 = 'ALTER TABLE `contacts` ADD COLUMN `image` VARCHAR(255) NULL AFTER `type`;';
            DB::select($query1);
        }
        if (!\Illuminate\Support\Facades\Schema::hasColumn('posts', 'is_require')) {
            $query2 = 'ALTER TABLE `posts` ADD COLUMN `is_require` TINYINT(1) DEFAULT 0 NULL AFTER `job`;';
            DB::select($query2);
        }
        try {
            $data = [];
            $language = 'vi';
            if (Session::has('locale')) {
                $language = Session::get('locale') ? Session::get('locale') : 'vi';
            }
            $data['language'] = $language;
            $data['settings'] = DB::table('settings')->pluck('content', 'type')->toArray();
            $data['services'] = DB::table('products')->where('status', 1)->orderBy('sort', 'asc')->orderBy('id', 'desc')->take(8)->get();
            $data['feedbacks'] = DB::table('feedbacks')
                    ->select('feedbacks.*', 'users.username', 'users.full_name', 'users.avatar as imageUser', 'users.job', 'city.city', 'products.slug as slugProduct', 'products.avatar as imageProduct')
                    ->join('users', 'users.id', '=', 'feedbacks.user_id')
                    ->leftJoin('city', 'users.city_id', '=', 'city.id')
                    ->join('products', 'products.id', '=', 'feedbacks.product_id')
                    ->where('feedbacks.status', 1)->orderBy('feedbacks.id', 'desc')->take(3)->get();
            return view('frontend.default.index', $data);
        } catch (Exception $e) {
            abort(500);
        }
    }
    /**
     * @description: Màn hình about
     * @author     : SonPD1
     */
    public function about(Request $request) {
        try {
            $data = [];
            $language = 'vi';
            if (Session::has('locale')) {
                $language = Session::get('locale') ? Session::get('locale') : 'vi';
            }
            if ($language == 'vi') {
                $about = DB::table('settings')->where('type', 'about-page')->first();
            } else {
                $about = DB::table('settings')->where('type', 'about-page-en')->first();
            }
            if ($about) {
                $dataAbout = json_decode($about->content, true);
                $data['careers'] = DB::table('posts')->whereIn('id', $dataAbout['careers'])->get();
                $data['dataAbout'] = $dataAbout;
                $data['about'] = $about;
                return view('frontend.default.about', $data);
            } else {
                abort(404);
            }
        } catch (Exception $e) {
            abort(500);
        }
    }
    /**
     * @description: Màn hình contact
     * @author     : SonPD1
     */
    public function contactUs(Request $request) {
        try {
            $data = [];
            $language = 'vi';
            if (Session::has('locale')) {
                $language = Session::get('locale') ? Session::get('locale') : 'vi';
            }
            if ($language == 'vi') {
                $contact = DB::table('settings')->where('type', 'contact-page')->first();
            } else {
                $contact = DB::table('settings')->where('type', 'contact-page-en')->first();
            }
            if ($contact) {
                $dataContact = json_decode($contact->content, true);
                $data['dataContact'] = $dataContact;
                $data['contact'] = $contact;
                return view('frontend.default.contact-us', $data);
            } else {
                abort(404);
            }
        } catch (Exception $e) {
            abort(500);
        }
    }
    /**
     * @description: Màn hình landingPage
     * @author     : SonPD1
     */
    public function landingPage(Request $request) {
        try {
            return view('frontend.default.landing');
        } catch (Exception $e) {
            abort(500);
        }
    }
    
public function subcribeEmail(Request $request){
    $email = $request->get('email');
    DB::table('contacts')->insert([
        'email' => $email,
        'status' => 0,
        'type' => 2,
    ]);
    return redirect()->route('frontend.home')->with([
        'level' => 'success',
        'message' => trans('message.ban_da_dang_ky_thanh_cong')
    ]);
}

        /**
     * @description: Màn hình landingPage
     * @author     : SonPD1
     */
    public function addContact(Request $request) {
        try {
            $routeNow = $request->get('routeNow', null);
            //Kiểm tra file
            $fileName = '';
            if ($request->hasFile('file_upload')) {
                $file = $request->file_upload;
                $fileName = $file->getClientOriginalName();
                $fileName = Carbon::now()->timestamp . $fileName;
                $fileName = preg_replace('/\s+/', '-', $fileName);
                if (!file_exists(public_path('upload/contacts'))) {
                    mkdir(public_path('upload/contacts'), 0777, true);
                }
                $file->move('upload/contacts/', $fileName);
            }
            DB::table('contacts')->insert([
                'name' => $request->get('name', null),
                'email' => $request->get('email', null),
                'subject' => $request->get('subject', null),
                'message' => $request->get('message', null),
                'created_at' => Carbon::now(),
                'image' => $fileName
            ]);
            return redirect(route($routeNow))->with([
                'message' => trans('message.luu_constact_thanh_cong'),
                'level' => 'success',
            ]);
        } catch (Exception $e) {
            abort(500);
        }
    }
    
    /**
     * @description: Màn hình landingPage
     * @author     : SonPD1
     */
    public function checkLogin(Request $request) {
        $userId = $request->get('userId', null);
        $url = $request->get('url', null);
        $status = false;
        if ($userId && Auth::user()->id) {
            $status = true;
        }
        session(['url.intended' => $url]);
        return response()->json(['status' => $status]);
    }
}
