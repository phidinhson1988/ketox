<?php

/**
 * @package     DMS
 * @category    User Controller
 * @copyright   Copyright 2018 FPT
 * @author      SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Http\Controllers\Frontend;

use App\Helpers\Facades\Tool;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Product;
use App\Models\Cart;
use Artisan;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Session;
use Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller {
    
    /**
     * @description: Màn hình user cp
     * @author     : SonPD1
     */
    public function index(Request $request) {
        try {
            return view('frontend.user.index');
        } catch (Exception $e) {
            abort(500);
        }
    }
    /**
     * @description: Màn hình profile
     * @author     : SonPD1
     */
    public function profile(Request $request) {
        try {
            $username = $request->get('username', null);
            if ($username) {
                $user = User::where('username', $username)->first();
            } else {
                $user = Auth::user();
            }
            if ($user) {
            $dataUser = $user->data ? json_decode($user->data, true) : [];
            $orders = Cart::where('user_id', $user->id)->orderBy('id', 'desc')->take(5)->get();
            if ($orders->count()) {
                foreach ($orders as $order) {
                    $products = Product::select(['products.*'])->join('cart_product', 'cart_product.product_id', '=', 'products.id')
                        ->where('cart_product.cart_id', $order->id)->get()->toArray();
                    $order->product = $products ? $products[0] : [];
                }
            }
            return view('frontend.user.profile', ['dataUser' => $dataUser, 'user' => $user, 'orders' => $orders]);
            } else {
                if(!Auth::check()) {
                    return redirect()->route('login');
                } else {
                    abort(404);
                }
            }
        } catch (Exception $e) {
            abort(500);
        }
    }
    
    /**
     * @description: Màn hình register
     * @author     : SonPD1
     */
    public function register(Request $request) {
        if(!Auth::check()) {
            $usernames = User::where('status', 1)->pluck('username')->toArray();
            $city = DB::table('city')->where('key_country', 'viet_nam')->pluck('city', 'id')->toArray();
            return view('frontend.user.register', ['city' => $city, 'usernames' => $usernames]);
        } else {
            return redirect()->route('frontend.user.profile', ['username' => Auth::user()->username]);
        }
    }    
    
    /**
     * @description: Màn hình register
     * @author     : SonPD1
     */
    public function report(Request $request) {
        if(Auth::check()) {
            $user = Auth::user();
            $dataUser = $user->data ? json_decode($user->data, true) : null;
            $city = DB::table('city')->where('key_country', 'viet_nam')->pluck('city', 'id')->toArray();
            return view('frontend.user.report', ['user' => $user, 'city' => $city, 'dataUser' => $dataUser]);
        } else {
            return redirect()->route('login');
        }
    } 
    /**
     * @description: Màn hình register
     * @author     : SonPD1
     */
    public function orderHistory(Request $request) {
        if(Auth::check()) {
            $userId = $request->get('user', null);
            if ($userId) {
                $user = User::find($userId);
                if (!$user) {
                    abort(404);
                }
            } else {
                $user = Auth::user();
            }
            $orders = Cart::where('user_id', $user->id)->get();
            if ($orders->count()) {
                foreach ($orders as $order) {
                    $products = Product::select(['products.*'])->join('cart_product', 'cart_product.product_id', '=', 'products.id')
                        ->where('cart_product.cart_id', $order->id)->get()->toArray();
                    $order->product = $products ? $products[0] : [];
                }
            }
            return view('frontend.user.history', ['user' => $user, 'orders' => $orders]);
        } else {
            return redirect()->route('login');
        }
    } 
    
    /**
     * @description: Màn hình register
     * @author     : SonPD1
     */
    public function done() {
        return view('frontend.user.done');
    }
    
    /**
     * @description: Màn hình register
     * @author     : SonPD1
     */
    public function saveRegister(Request $request) {
        $dataUser = $request->get('data-report', null);
        $infoUser = [
            'username' => $request->get('username'),
            'full_name' => $request->get('full_name', null),
            'email' => $request->get('email', null),
            'password' => Hash::make($request->get('password')),
            'birth_day' => $request->get('birth_day', null),
            'phone' => $request->get('phone', null),
            'avatar' => $request->get('avatar', null),
            'created_at' => Carbon::now(),
            'data' => $dataUser,
            'address' => $request->get('address', null),
            'gender' => $request->get('gender', null),
            'city_id' => $request->get('city', null),
            'image' => $request->get('image', null),
        ];
        DB::table('users')->insert($infoUser);
        if ($infoUser['avatar']) {
            Product::moveFileProduct($infoUser['avatar'], Tool::getConst('USER_UPLOAD'));
        }
        if ($infoUser['image']) {
            Product::moveFileProduct($infoUser['image'], Tool::getConst('USER_UPLOAD'));
        }
        if (Auth::attempt(['username' => $infoUser['username'], 'password' => $request->get('password'), 'status' => true])) {
            $timeout = Tool::getConst('TIME_OUT');
            $request->session()->put('timeout', $timeout);
            $permissions = [];
            $request->session()->put('permission', $permissions);
            return redirect()->route('frontend.user.done');
        } else {
            abort(500);
        }
    }
    
    /**
     * @description: Màn hình save report
     * @author     : SonPD1
     */
    public function saveReport(Request $request) {
        $user = User::find($request->get('userId'));
        $dataUser = $request->get('data-report', null);
        $infoUser = [
            'full_name' => $request->get('full_name', null),
            'email' => $request->get('email', null),
            'birth_day' => $request->get('birth_day', null),
            'phone' => $request->get('phone', null),
            'avatar' => $request->get('avatar', $user->avatar),
            'updated_at' => Carbon::now(),
            'data' => $dataUser,
            'address' => $request->get('address', null),
            'gender' => $request->get('gender', null),
            'city_id' => $request->get('city', null),
            'image' => $request->get('image', $user->image),
        ];
        $password = $request->get('password', null);
        if ($password) {
            $infoUser['password'] = Hash::make($password);
        }
        DB::table('users')->where('id', $user->id)->update($infoUser);
        if ($infoUser['avatar'] != $user->avatar) {
            Product::moveFileProduct($infoUser['avatar'], Tool::getConst('USER_UPLOAD'));
        }
        if ($infoUser['image'] != $user->image) {
            Product::moveFileProduct($infoUser['image'], Tool::getConst('USER_UPLOAD'));
        }
        return redirect()->back()->with([
            'message' => trans('message.cap_nhat_report_thanh_cong'),
            'level' => 'success',
        ]);
    }
    
    /**
     * @description: Màn hình save report
     * @author     : SonPD1
     */
    public function addFeedback(Request $request) {
        $user = User::find($request->get('userId', 0));
        $productId = $request->get('productId', null);
        $url = $request->get('urlPage', null);
        if (!$productId) {
            abort(500);
        } else {
            if (!$user) {
                session(['url.intended' => $url]);
                return redirect()->route('login');
            } else {
                DB::table('feedbacks')->insert([
                    'user_id' => $user->id,
                    'product_id' => $productId,
                    'content' => $request->get('description', null),
                    'created_at' => Carbon::now(),
                    'status' => 1,
                ]);
                return redirect()->back()->with([
                    'message' => trans('message.feedback_thanh_cong'),
                    'level' => 'success',
                ]);
            }
        }
    }
    
    /**
     * @description: Màn hình save report
     * @author     : SonPD1
     */
    public function changePass(Request $request) {
        $user = Auth::user();
        if ($user) {
            $password = $request->get('password', null);
            $infoUser = [];
            if ($password) {
                $infoUser['password'] = Hash::make($password);
            }
            DB::table('users')->where('id', $user->id)->update($infoUser);

            return redirect()->back()->with([
                'message' => trans('message.cap_nhat_password_thanh_cong'),
                'level' => 'success',
            ]);
        } else {
            abort(500);
        }
    }
}
