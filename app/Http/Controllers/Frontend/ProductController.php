<?php

/**
 * @package     DMS
 * @category    Product Controller
 * @copyright   Copyright 2018 FPT
 * @author      SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Http\Controllers\Frontend;

use App\Helpers\Facades\Tool;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Product;
use App\Helpers\Pagination;
use Artisan;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Session;

class ProductController extends Controller {
    
    /**
     * @description: Màn hình services
     * @author     : SonPD1
     */
    public function index(Request $request) {
        try {
            $data = [];
            $order = $request->get('sort', 1);
            $limit = $request->get('limit', Tool::getConst('DEFAULT_LIMIT'));
            $query = Product::where('status', 1);
            switch ($order) {
                case 1:
                    $query->orderBy('title', 'asc');
                    break;
                case 2:
                    $query->orderBy('title', 'desc');
                    break;
                case 3:
                    $query->orderBy('price', 'desc');
                    break;
                case 4:
                    $query->orderBy('price', 'asc');
                    break;
                case 5:
                    $query->orderBy('id', 'desc');
                    break;
                case 6:
                    $query->orderBy('id', 'asc');
                    break;
                default :
                    break;
            }
            $services = $query->paginate($limit);
            $data['services'] = $services;
            $data['sort'] = $order;
            $data['limit'] = $limit;
            $pagination = Pagination::init($services)->limit(Tool::getConst('PAGGING_LIMIT'))->appends(['sort' => $data['sort'], 'limit' => $limit])->render();
            $data['pagination'] = $pagination;
            return view('frontend.service.index', $data);
        } catch (Exception $e) {
            abort(500);
        }
    }
    
    public function carts(Request $request) {
        try {
            $data = [];
            $data['user'] = Auth::check() ? Auth::user() : null;
            $carts = Session::get('carts');
            $data['carts'] = $carts ? json_decode($carts, true) : null;
            return view('frontend.cart.index', $data);
        } catch (Exception $e) {
            abort(500);
        }
    }
    
    public function addCart(Request $request) {
        $productId = $request->get('productId', null);
        $product = Product::find($productId);
        if ($product) {
            $carts = Session::get('carts');
            $dataCart = [];
            if ($carts) {
                $dataCart = json_decode($carts, true);
            }
            $update = false;
            foreach ($dataCart as $key => $value) {
                if ($value['id'] == $product->id) {
                    $value['total'] = $value['total'] + 1;
                    $dataCart[$key] = $value;
                    $update = true;
                }
            }
            if (!$update) {
                $dataCart[] = [
                    'title' => $product->title,
                    'price' => $product->price,
                    'id' => $product->id,
                    'total' => 1,
                ];
            }
            Session::put('carts', json_encode($dataCart));
            return redirect()->route('frontend.carts')->with([
                'level' => 'success',
                'message' => trans('message.them_dich_vu_thanh_cong')
            ]);
        } else {
            abort(500);
        }
    }
    
    public function deleteCart(Request $request) {
        $indexCart = $request->get('id', null);
        if (!is_null($indexCart)) {
            $carts = Session::get('carts');
            if ($carts) {
                $carts = json_decode($carts, true);
                if (isset($carts[$indexCart])) {
                    unset($carts[$indexCart]);
                }
                $dataCart = array_values($carts);
                Session::put('carts', json_encode($dataCart));
            }
        }
        return redirect()->route('frontend.carts')->with([
            'level' => 'success',
            'message' => trans('message.xoa_thanh_cong')
        ]);
    }
    /**
     * @description: Màn hình detail service
     * @author     : SonPD1
     */
    public function detail(Request $request, $slug) {
        try {
            $user = Auth::user();
            if ($slug) {
                $product = Product::where('slug', $slug)->where('status', 1)->first();
                if ($product) {
                    $urlNow = $request->url();
                    $others = Product::where('status', 1)->where('id', '<>', $product->id)->orderBy('id', 'desc')->take(4)->get();
                    return view('frontend.service.detail', ['product' => $product, 'others' => $others, 'user' => $user, 'urlNow' => $urlNow]);
                }
            }
            abort(404);
        } catch (Exception $e) {
            abort(500);
        }
    }
    
    /**
     * @description: luu thông tin mua hàng
     * @author     : SonPD1
     */
    public function purchase(Request $request) {
        $status = true;
        try {
            $user = Auth::user();
            $productId = $request->get('id', null);
            $product = Product::find($productId);
            if ($product) {
                $carts = Session::get('carts');
                $dataCart = [];
                if ($carts) {
                    $dataCart = json_decode($carts, true);
                    $cartId = DB::table('carts')->insertGetId([
                        'user_id' => $user->id,
                        'created_time' => Carbon::now(),
                        'created_at' => Carbon::now(),
                        'status' => 1,
                    ]);
                    if ($cartId) {
                        foreach ($dataCart as $itemCart) {
                            DB::table('cart_product')->insert([
                                'cart_id' => $cartId,
                                'product_id' => $itemCart['id'],
                                'quantity' => $itemCart['total'],
                            ]);
                        }
                        Session::put('carts', null);
                    } else {
                        $status = false;
                    }
                }
            } else {
                $status = false;
            }
        } catch (Exception $e) {
            $status = false;
        }
        return redirect()->route('frontend.home')->with([
            'level' => 'success',
            'message' => trans('message.don_hang_cua_ban_da_gui_thanh_cong')
        ]);
    }
}
