<?php

/**
 * @package    PageController
 * @category    Produc Controller
 * @copyright   Copyright 2018 FPT
 * @author      SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Http\Controllers;

use App\Helpers\Facades\Tool;
use Illuminate\Http\Request;
use App\Helpers\Pagination;
use DB;

class PageController extends Controller {
    
    /*
     * @description: Danh sách Page
     * @input: $request
     * @output: thông tin page
     * @author: SonPD1
     * @created at: 17/8/2018
     * *** */
    public function index(Request $request) {
        
        return view('page.index');
    }

    /*
     * @description: Create page
     * @input: $request
     * @output: Create page
     * @author: SonPD1
     * @created at: 17/8/2018
     * *** */
    public function create() {
        try {
            
            return view('page.create');
        } catch (Exception $e) {
            abort(500);
        }
    }

    /*
     * @description: Create page
     * @input: $request
     * @output: Create page
     * @author: SonPD1
     * @created at: 17/8/2018
     * *** */
    public function store(RoleRequest $request) {
        try {
            return redirect()->route('page.index')->with([
                        'message' => 'Thêm mới page thành công!',
                        'level' => 'success',
            ]);
        } catch (Exception $e) {
            abort(500);
        }
    }

    /*
     * @description: Edit page
     * @input: $id
     * @output: Edit page
     * @author: SonPD1
     * @created at: 17/8/2018
     * *** */
    public function edit($id) {
        
        return view('role.edit');
    }

    /*
     * @description: lưu page
     * @input: $id, $request
     * @output: lưu page
     * @author: SonPD1
     * @created at: 17/8/2018
     * *** */
    public function update(RoleRequest $request, $id) {
        
        return redirect()->route('page.index')->with([
                    'message' => 'Cập nhật page thành công!',
                    'level' => 'success',
        ]);
    }

    /*
     * @description: xóa page
     * @input: id
     * @output: xóa page
     * @author: SonPD1
     * @created at: 17/8/2018
     * *** */

    public function delete($id) {
        try {
            DB::table('posts')->where('id', $id)->update(['status' => 0]);
            return back()->with(['level' => 'success', 'message' => 'Xóa page thành công!']);
        } catch (Exception $e) {
            abort(500);
        }
    }

}
