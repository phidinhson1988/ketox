<?php

/**
 * @package    DMS
 * @category    Controller
 * @copyright  Copyright 2018 FPT
 * @author     SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
