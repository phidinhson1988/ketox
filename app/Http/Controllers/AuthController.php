<?php

/**
 * @package     DMS
 * @category    Auth Controller
 * @copyright   Copyright 2018 FPT
 * @author      SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Http\Controllers;

use App\Helpers\Facades\Tool;
use App\Models\Setting;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;

class AuthController extends Controller
{

    /**
     * @description: Màn hình show login
     * @author     : HuyVL
     */
    public function index(Request $request)
    {
        $status = $request->get('status', null);
        return view('auth.login', ['status' => $status]);
    }

    /**
     * @description: Thao tác click login
     * @author     : HuyVL
     */
    public function postLogin(LoginRequest $request)
    {
        try {
            if (Auth::attempt(['username' => $request->username, 'password' => $request->password, 'status' => true])) {
                $timeout = Tool::getConst('TIME_OUT');
                $request->session()->put('timeout', $timeout);
                $permissions = Auth::user()->role->first();
                if ($permissions) {
                    $permissions = $permissions->permission()->pluck('name')->toArray();
                }
                $request->session()->put('permission', $permissions);
                if ($permissions && in_array('admin-controler', $permissions)) {
                    return redirect()->intended(route('home'));
                } else {
                    return redirect()->intended(route('frontend.user.profile', ['username' => Auth::user()->username]));
                }
            } else {
                return redirect(route('login'))->with([
                    'error' => trans('message.ten_dang_nhap_hoac_mat_khau_khong_dung_vui_long_thu_lai'),
                    'level' => 'error'
                ]);
            }
        } catch (Exception $e) {
            abort(500);
        }
    }

    /**
     * @description: Thao tác click logout
     * @author     : HuyVL
     */
    public function logout(Request $request)
    {
        try {
            $status = $request->get('status', null);
            if (Auth::check()) {
                $request->session()->forget('timeout');
                Auth::logout();
                return redirect(route('login', ['status' => $status]));
            } else {
                return view('auth.login', ['status' => $status]);
            }
        } catch (Exception $e) {
            abort(500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
