<?php

/**
 * @package     DMS
 * @category    Home Controller
 * @copyright   Copyright 2018 FPT
 * @author      SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Http\Controllers;

use App\Helpers\Facades\Tool;
use App\Models\User;
use Artisan;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Session;

class HomeController extends Controller {

    private function _clearTemporaryData() {
        Artisan::call('config:clear');
        Artisan::call('view:clear');
        Artisan::call('cache:clear');
    }
    
    /**
     * @description: Màn hình home trang chu
     * @author     : HuyVL
     */
    public function index(Request $request) {;
        return redirect(route('user.profile'));
        try {
            return view('index');
        } catch (Exception $e) {
            abort(500);
        }
    }
        
    /**
     * @description: Add role permission
     * @author     : HuyVL
     */
    public function addRole(Request $request) {
        $arr = \App\Helpers\Load::all();
        $data = [];
        DB::table('permission_role')->delete();
        DB::table('permissions')->delete();
        foreach ($arr as $item) {
            $data[] = [
                'name' => $item['permission_key'],
                'display_name' => $item['permission_name'],
                'order' => $item['order'],
                'level' => $item['level'],
                'parents' => json_encode($item['parents']),
                'childs' => json_encode($item['childs']),
                'sort' => $item['sort'],
            ];
        }
        DB::table('permissions')->insert($data);
        $permisstion = DB::table('permissions')->pluck('id')->toArray();
        $dataRelation = [];
        foreach ($permisstion as $item) {
            $dataRelation[] = [
                'permission_id' => $item,
                'role_id' => 10
            ];
        }
        DB::table('permission_role')->insert($dataRelation);
        dd('successfull!');
        try {
            return view('index');
        } catch (Exception $e) {
            abort(500);
        }
    }

    /**
     * @description: setLocale
     * @author     : HuyVL
     */
    public function setLocale($option = 'vi') {
        if (!Session::has('locale')) {
            Session::push('locale', null);
        }
        Session::put('locale', $option);
        return back();
    }
}
