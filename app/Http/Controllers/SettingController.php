<?php

/**
 * @package     DMS
 * @category    Setting Controller
 * @copyright   Copyright 2018 FPT
 * @author      SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Http\Controllers;

use App\Helper\ImportFile;
use App\Helpers\Facades\Tool;
use App\Helpers\Pagination;
use App\Http\Requests\SettingRequest;
use App\Models\IsoCode;
use App\Models\Document;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Support\Facades\File as FileUpload;
use Storage;

class SettingController extends Controller {

    /**
     * @description: Cấu hình hệ thống
     * @input      :
     * @output     : return view
     * @author     : SonPD1
     * @created_at : 22/08/2018
     */
    public function system() {
        $data = [];
        // banner vn
        $bannerSetting = DB::table('settings')->where('type', 'banner-home')->first();
        $banner = ['title' => '', 'image' => '', 'link' => '', 'description' => ''];
        if ($bannerSetting) {
            $banner = $bannerSetting->content ? json_decode($bannerSetting->content, true) : $banner;
        } else {
            DB::table('settings')->insert([
                'type' => 'banner-home',
                'content' => json_encode($banner)
            ]);
        }
        $data['banner'] = $banner;
        // banner en
        $bannerEnSetting = DB::table('settings')->where('type', 'banner-home-en')->first();
        $bannerEn = ['title' => '', 'image' => '', 'link' => '', 'description' => ''];
        if ($bannerEnSetting) {
            $bannerEn = $bannerEnSetting->content ? json_decode($bannerEnSetting->content, true) : $bannerEn;
        } else {
            DB::table('settings')->insert([
                'type' => 'banner-home-en',
                'content' => json_encode($bannerEn)
            ]);
        }
        $data['bannerEn'] = $bannerEn;
        // chat online
        $chatSetting = DB::table('settings')->where('type', 'chat-embed')->first();
        if (!$chatSetting) {
            $chatId = DB::table('settings')->insertGetId([
                'type' => 'banner-home-en',
                'content' => json_encode($bannerEn)
            ]);
            $chatSetting = DB::table('settings')->where('id', $chatId)->first();
        }
        $data['bannerEn'] = $bannerEn;
        $data['chat'] = $chatSetting;
        return view('setting.system', $data);
    }

    /**
     * @description: Cấu hình hệ thống
     * @input      :
     * @output     : return view
     * @author     : SonPD1
     * @created_at : 22/08/2018
     */
    public function aboutPage(Request $request) {
        $keyMenu = $request->get('key', 'about-page');
        $data = [];
        // banner vn
        $about = DB::table('settings')->where('type', $keyMenu)->first();
        $aboutData = ['image' => '', 'description_left' => '', 'description_right' => '', 'careers' => []];
        if ($about) {
            $aboutData = $about->content ? json_decode($about->content, true) : $aboutData;
        } else {
            DB::table('settings')->insert([
                'type' => $keyMenu,
                'content' => json_encode($aboutData)
            ]);
        }
        $data['about'] = $about;
        $data['aboutData'] = $aboutData;
        $data['key'] = $keyMenu;
        $data['careers'] = \App\Models\News::where('type', 2)->where('status', 1)->get();
        return view('setting.about-page', $data);
    }

    /**
     * @description: Cấu hình hệ thống
     * @input      :
     * @output     : return view
     * @author     : SonPD1
     * @created_at : 22/08/2018
     */
    public function contactPage(Request $request) {
        $keyMenu = $request->get('key', 'contact-page');
        $data = [];
        // banner vn
        $contact = DB::table('settings')->where('type', $keyMenu)->first();
        $contactData = ['map' => '', 'office' => '', 'facebook' => '', 'instagram' => '', 'mail' => '', 'phone' => ''];
        if ($contact) {
            $contactData = $contact->content ? json_decode($contact->content, true) : $contactData;
        } else {
            DB::table('settings')->insert([
                'type' => $keyMenu,
                'content' => json_encode($contactData)
            ]);
        }
        $data['contact'] = $contact;
        $data['contactData'] = $contactData;
        $data['key'] = $keyMenu;
        return view('setting.contact-page', $data);
    }

    public function postSystem(Request $request) {
        $keyMenu = $request->get('key-setting', null);
        $parameter = DB::table('settings')->where('type', $keyMenu)->first();
        $content = [];
        switch ($keyMenu) {
            case 'banner-home':
                $content = ['title' => '', 'image' => '', 'link' => '', 'description' => ''];
                $content = ($parameter && $parameter->content) ? json_decode($parameter->content, true) : $content;
                if ($request->hasFile('image')) {
                    $file = $request->file('image');
                    $fileName = $file->getClientOriginalName();
                    $name = md5(($fileName) . date('Y-m-d H:i:s')) . '.' . $file->getClientOriginalExtension();
                    $file->move(Tool::getConst('SETTING_UPLOAD'), $name);
                    $content['image'] = $name;
                }
                $content['title'] = $request->get('title');
                $content['link'] = $request->get('link');
                $content['description'] = $request->get('description');
                break;
            case 'banner-home-en':
                $content = ['title' => '', 'image' => '', 'link' => '', 'description' => ''];
                $content = ($parameter && $parameter->content) ? json_decode($parameter->content, true) : $content;
                if ($request->hasFile('image')) {
                    $file = $request->file('image');
                    $fileName = $file->getClientOriginalName();
                    $name = md5(($fileName) . date('Y-m-d H:i:s')) . '.' . $file->getClientOriginalExtension();
                    $file->move(Tool::getConst('SETTING_UPLOAD'), $name);
                    $content['image'] = $name;
                }
                $content['title'] = $request->get('title');
                $content['link'] = $request->get('link');
                $content['description'] = $request->get('description');
                break;
            case 'chat-embed':
                $content = null;
                $embedChat = $request->get('chat', null);
                if ($parameter) {
                    DB::table('settings')->where('type', $keyMenu)->update(['content' => $embedChat]);
                } else {
                    DB::table('settings')->insert([
                        'type' => $keyMenu,
                        'content' => $embedChat
                    ]);
                }
                break;
            default :
                break;
        }
        if ($content) {
            if ($parameter) {
                DB::table('settings')->where('type', $keyMenu)->update(['content' => json_encode($content)]);
            } else {
                DB::table('settings')->insert([
                    'type' => $keyMenu,
                    'content' => json_encode($content)
                ]);
            }
        }
        return redirect(route('setting.system'))->with([
                    'message' => trans('message.luu_cau_hinh_thanh_cong'),
                    'level' => 'success',
        ]);
    }

    public function saveAbout(Request $request) {
        $keyMenu = $request->get('key-setting', null);
        $parameter = DB::table('settings')->where('type', $keyMenu)->first();
        $content = [];
        switch ($keyMenu) {
            case 'about-page':
                $content = ['image' => '', 'description_left' => '', 'description_right' => '', 'careers' => []];
                $content = ($parameter && $parameter->content) ? json_decode($parameter->content, true) : $content;
                if ($request->hasFile('image')) {
                    $file = $request->file('image');
                    $fileName = $file->getClientOriginalName();
                    $name = md5(($fileName) . date('Y-m-d H:i:s')) . '.' . $file->getClientOriginalExtension();
                    $file->move(Tool::getConst('SETTING_UPLOAD'), $name);
                    $content['image'] = $name;
                }
                $content['careers'] = $request->get('careers');
                $content['description_left'] = $request->get('description_left');
                $content['description_right'] = $request->get('description_right');
                break;
            case 'about-page-en':
                $content = ['image' => '', 'description_left' => '', 'description_right' => '', 'careers' => []];
                $content = ($parameter && $parameter->content) ? json_decode($parameter->content, true) : $content;
                if ($request->hasFile('image')) {
                    $file = $request->file('image');
                    $fileName = $file->getClientOriginalName();
                    $name = md5(($fileName) . date('Y-m-d H:i:s')) . '.' . $file->getClientOriginalExtension();
                    $file->move(Tool::getConst('SETTING_UPLOAD'), $name);
                    $content['image'] = $name;
                }
                $content['careers'] = $request->get('careers');
                $content['description_left'] = $request->get('description_left');
                $content['description_right'] = $request->get('description_right');
                break;
            default :
                break;
        }
        if ($content) {
            if ($parameter) {
                DB::table('settings')->where('type', $keyMenu)->update(['content' => json_encode($content)]);
            } else {
                DB::table('settings')->insert([
                    'type' => $keyMenu,
                    'content' => json_encode($content)
                ]);
            }
        }
        return redirect(route('setting.aboutPage', ['key' => $keyMenu]))->with([
                    'message' => trans('message.luu_cau_hinh_thanh_cong'),
                    'level' => 'success',
        ]);
    }

    public function saveContactPage(Request $request) {
        $keyMenu = $request->get('key-setting', null);
        $parameter = DB::table('settings')->where('type', $keyMenu)->first();
        $content = [];
        switch ($keyMenu) {
            case 'contact-page':
                $content = ['map' => '', 'office' => '', 'facebook' => '', 'instagram' => '', 'mail' => '', 'phone' => ''];
                $content = ($parameter && $parameter->content) ? json_decode($parameter->content, true) : $content;
                $content['map'] = $request->get('map', null);
                $content['office'] = $request->get('office', null);
                $content['facebook'] = $request->get('facebook', null);
                $content['instagram'] = $request->get('instagram', null);
                $content['mail'] = $request->get('mail', null);
                $content['phone'] = $request->get('phone', null);
                break;
            case 'contact-page-en':
                $content = ['map' => '', 'office' => '', 'facebook' => '', 'instagram' => '', 'mail' => '', 'phone' => ''];
                $content = ($parameter && $parameter->content) ? json_decode($parameter->content, true) : $content;
                $content['map'] = $request->get('map', null);
                $content['office'] = $request->get('office', null);
                $content['facebook'] = $request->get('facebook', null);
                $content['instagram'] = $request->get('instagram', null);
                $content['mail'] = $request->get('mail', null);
                $content['phone'] = $request->get('phone', null);
                break;
            default :
                break;
        }
        if ($content) {
            if ($parameter) {
                DB::table('settings')->where('type', $keyMenu)->update(['content' => json_encode($content)]);
            } else {
                DB::table('settings')->insert([
                    'type' => $keyMenu,
                    'content' => json_encode($content)
                ]);
            }
        }
        return redirect(route('setting.contactPage', ['key' => $keyMenu]))->with([
                    'message' => trans('message.luu_cau_hinh_thanh_cong'),
                    'level' => 'success',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function menu(Request $request) {
        $data = [];
        $keyMenu = $request->get('key', 'home-menu-top');
        $parameter = DB::table('settings')->where('type', $keyMenu)->first();
        $multipleOrderTops = [];
        if ($parameter) {
            $multipleOrderTops = $parameter->content ? json_decode($parameter->content) : [];
        }
        $listOrderTop = [];
        $menuAll = [];
        foreach ($multipleOrderTops as $itemValueTop) {
            $listOrderTop[] = get_object_vars($itemValueTop);
            $menuAll[$itemValueTop->id] = $itemValueTop;
        }
        $data['treeOrderMenuTop'] = $this->orderToTree($listOrderTop);
        $data['menuAll'] = $menuAll;
        $data['keyMenu'] = $keyMenu;
        return view('setting.menu', $data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function socials(Request $request) {
        $data = [];
        $keyMenu = 'socials';
        $parameter = DB::table('settings')->where('type', $keyMenu)->first();
        $socials = ['line_1' => ['position' => '', 'class' => '', 'link' => '']];
        $max = 1;
        if ($parameter) {
            $socials = $parameter->content ? json_decode($parameter->content, true) : $socials;
        } else {
            DB::table('settings')->insert([
                'type' => $keyMenu,
                'content' => json_encode($socials)
            ]);
        }
        $data['socials'] = $socials;
        end($socials);
        $keyEnd = key($socials);
        $max = str_replace('line_', '', $keyEnd);
        $data['max'] = $max;
        return view('setting.socials', $data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addSocial(Request $request) {
        $dataSocial = $request->get('data-social', json_encode([]));
        DB::table('settings')->where('type', 'socials')->update(['content' => $dataSocial]);
        return redirect(route('setting.socials'))->with([
                    'message' => trans('message.luu_socials_thanh_cong'),
                    'level' => 'success',
        ]);
    }

    public function menuBottom(Request $request) {
        $data = [];
        $keyMenu = 'home-menu-bottom';
        $parameter = DB::table('settings')->where('type', $keyMenu)->first();
        $multipleOrderTops = [];
        if ($parameter) {
            $multipleOrderTops = $parameter->content ? json_decode($parameter->content) : [];
        }
        $listOrderTop = [];
        $menuAll = [];
        foreach ($multipleOrderTops as $itemValueTop) {
            $listOrderTop[] = get_object_vars($itemValueTop);
            $menuAll[$itemValueTop->id] = $itemValueTop;
        }
        $data['treeOrderMenuTop'] = $this->orderToTree($listOrderTop);
        $data['menuAll'] = $menuAll;
        $data['keyMenu'] = $keyMenu;
        return view('setting.menu', $data);
    }

    public function saveMenu(Request $request) {
        $arrayOrder = [];
        $sorder = $request->get('sorder', null);
        $keyMenu = $request->get('key', 'home-menu-top');
        if ($sorder) {
            $arrayOrder = json_decode($sorder, true);
        }
        $listValueParameter = [];
        $this->treeToOrder($listValueParameter, $arrayOrder, 0);

        $parameter = DB::table('settings')->where('type', $keyMenu)->first();
        if ($parameter && $parameter->content) {
            $valueOrder = json_decode($parameter->content);
            $arrayResult = [];
            foreach ($listValueParameter as $itemKey => $itemValue) {
                foreach ($valueOrder as $value) {
                    if ($value->id == $itemKey) {
                        $value->parent = $itemValue;
                        $arrayResult[] = $value;
                        break;
                    }
                }
            }
            DB::table('settings')->where('id', $parameter->id)->update(['content' => json_encode($arrayResult)]);
        }
        return redirect(route('setting.menu', ['key' => $keyMenu]))->with([
                    'message' => trans('message.sap_xep_menu_thanh_cong'),
                    'level' => 'success',
        ]);
    }

    public function addMenu(Request $request) {
        $title = $request->get('title', null);
        $link = $request->get('link', null);
        $id = $request->get('menu-id', null);
        $keyMenu = $request->get('key', 'home-menu-top');
        $parameter = DB::table('settings')->where('type', $keyMenu)->first();
        $listOrder = [];
        $message = trans('message.them_menu_thanh_cong');
        if ($parameter) {
            $valueParameter = $parameter->content ? json_decode($parameter->content) : [];
            foreach ($valueParameter as $itemValue) {
                $listOrder[] = get_object_vars($itemValue);
            }
            if (!$id) {
                $idMenu = $this->getOrderId($listOrder);
                $listOrder[] = [
                    "id" => ($idMenu) ? $idMenu : 1,
                    "title" => $title,
                    "url" => $link,
                    "parent" => 0
                ];
                DB::table('settings')->where('id', $parameter->id)->update(['content' => json_encode($listOrder)]);
            } else {
                $message = trans('message.cap_nhat_menu_thanh_cong');
                foreach ($valueParameter as $itemValue) {
                    if ($id == $itemValue->id) {
                        $itemValue->title = $title;
                        $itemValue->url = $link;
                    }
                }
                DB::table('settings')->where('id', $parameter->id)->update(['content' => json_encode($valueParameter)]);
            }
        } else {
            $listOrder[] = [
                "id" => 1,
                "title" => $title,
                "url" => $link,
                "parent" => 0
            ];
            DB::table('settings')->insert([
                'type' => $keyMenu,
                'content' => json_encode($listOrder)
            ]);
        }
        return redirect(route('setting.menu', ['key' => $keyMenu]))->with([
                    'message' => $message,
                    'level' => 'success',
        ]);
    }

    public function deleteMenu(Request $request) {
        $id = $request->get('id', null);
        $keyMenu = $request->get('key', 'home-menu-top');
        $parameter = DB::table('settings')->where('type', $keyMenu)->first();
        $listOrder = [];
        $message = trans('message.xoa_menu_thanh_cong');
        if ($parameter) {
            $valueParameter = $parameter->content ? json_decode($parameter->content) : [];
            $listValue = [];
            foreach ($valueParameter as $itemValue) {
                if ($itemValue->id != $id) {
                    if ($itemValue->parent == $id) {
                        $itemValue->parent = 0;
                    }
                    $listValue[] = $itemValue;
                }
            }
            DB::table('settings')->where('id', $parameter->id)->update(['content' => json_encode($listValue)]);
        }
        return redirect(route('setting.menu', ['key' => $keyMenu]))->with([
                    'message' => $message,
                    'level' => 'success',
        ]);
    }

    private function orderToTree(&$listOrder) {
        $map = array(
            0 => array('childrent' => array())
        );
        foreach ($listOrder as &$order) {
            $order['childrent'] = array();
            $map[$order['id']] = &$order;
        }
        foreach ($listOrder as &$order) {
            $map[$order['parent']]['childrent'][] = &$order;
        }
        return $map[0]['childrent'];
    }

    private function getOrderId($array) {
        if ($array) {
            for ($i = 0; $i < count($array) - 1; $i++) {
                for ($j = $i + 1; $j < count($array); $j++) {
                    if ($array[$i]['id'] < $array[$j]['id']) {
                        $th = $array[$i];
                        $array[$i] = $array[$j];
                        $array[$j] = $th;
                    }
                }
            }
            return $array[0]['id'] + 1;
        }
    }

    private function treeToOrder(&$result, $tree, $parent) {
        foreach ($tree as $key => $value) {
            $result[$value['id']] = $parent;
            if (isset($value['children']))
                $this->treeToOrder($result, $value['children'], $value['id']);
        }
    }
//
//    public function uploadEditor(Request $request) {
//        if (isset($_FILES['upload']['name'])) {
//            $file = $_FILES['upload']['tmp_name'];
//            $file_name = $_FILES['upload']['name'];
//            $file_name_array = explode(".", $file_name);
//            $extension = end($file_name_array);
//            $new_image_name = rand() . '.' . $extension;
//            chmod('upload', 0777);
//            $allowed_extension = array("jpg", "gif", "png");
//            if (in_array($extension, $allowed_extension)) {
//                move_uploaded_file($file, 'upload/' . $new_image_name);
//                $function_number = $request->get('CKEditorFuncNum', 0);
//                $url = asset('upload/' . $new_image_name);
//                $message = '';
//                echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($function_number, '$url', '$message');</script>";
//            }
//        }
//    }
    
    public function uploadEditor(Request $request) {
        // PHP Upload Script for CKEditor:  http://coursesweb.net/
        // HERE SET THE PATH TO THE FOLDER WITH IMAGES ON YOUR SERVER (RELATIVE TO THE ROOT OF YOUR WEBSITE ON SERVER)
        $upload_dir = Tool::getConst('CONTENT_UPLOAD');
        if (!file_exists($upload_dir)) {
            mkdir($upload_dir, 0777, true);
        }
        $re = '';
        //Kiểm tra file
        if ($request->hasFile('upload')) {
            $file = $request->upload;
            $size = $file->getSize();
            $fileName = $file->getClientOriginalName();
            $fileName = Carbon::now()->timestamp.$fileName;
            $fileName = preg_replace('/\s+/', '-', $fileName);
            $file->move($upload_dir, $fileName);
            $path = asset($upload_dir).'/'. $fileName;
        
            $CKEditorFuncNum = $_GET['CKEditorFuncNum'];
            $message = "Bạn đã upload file \"".$fileName."\" thành công!";
            $re = "window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$path', '$message')";
        }
        echo "<script>$re;</script>";
    }

    private function makeFriendlyTitle($string, $allowUnder = false) {
        static $charMap = array(
            "à" => "a", "ả" => "a", "ã" => "a", "á" => "a", "ạ" => "a", "ă" => "a", "ằ" => "a", "ẳ" => "a", "ẵ" => "a", "ắ" => "a", "ặ" => "a", "â" => "a", "ầ" => "a", "ẩ" => "a", "ẫ" => "a", "ấ" => "a", "ậ" => "a",
            "đ" => "d",
            "è" => "e", "ẻ" => "e", "ẽ" => "e", "é" => "e", "ẹ" => "e", "ê" => "e", "ề" => "e", "ể" => "e", "ễ" => "e", "ế" => "e", "ệ" => "e",
            "ì" => "i", "ỉ" => "i", "ĩ" => "i", "í" => "i", "ị" => "i",
            "ò" => "o", "ỏ" => "o", "õ" => "o", "ó" => "o", "ọ" => "o", "ô" => "o", "ồ" => "o", "ổ" => "o", "ỗ" => "o", "ố" => "o", "ộ" => "o", "ơ" => "o", "ờ" => "o", "ở" => "o", "ỡ" => "o", "ớ" => "o", "ợ" => "o",
            "ù" => "u", "ủ" => "u", "ũ" => "u", "ú" => "u", "ụ" => "u", "ư" => "u", "ừ" => "u", "ử" => "u", "ữ" => "u", "ứ" => "u", "ự" => "u",
            "ỳ" => "y", "ỷ" => "y", "ỹ" => "y", "ý" => "y", "ỵ" => "y",
            "À" => "A", "Ả" => "A", "Ã" => "A", "Á" => "A", "Ạ" => "A", "Ă" => "A", "Ằ" => "A", "Ẳ" => "A", "Ẵ" => "A", "Ắ" => "A", "Ặ" => "A", "Â" => "A", "Ầ" => "A", "Ẩ" => "A", "Ẫ" => "A", "Ấ" => "A", "Ậ" => "A",
            "Đ" => "D",
            "È" => "E", "Ẻ" => "E", "Ẽ" => "E", "É" => "E", "Ẹ" => "E", "Ê" => "E", "Ề" => "E", "Ể" => "E", "Ễ" => "E", "Ế" => "E", "Ệ" => "E",
            "Ì" => "I", "Ỉ" => "I", "Ĩ" => "I", "Í" => "I", "Ị" => "I",
            "Ò" => "O", "Ỏ" => "O", "Õ" => "O", "Ó" => "O", "Ọ" => "O", "Ô" => "O", "Ồ" => "O", "Ổ" => "O", "Ỗ" => "O", "Ố" => "O", "Ộ" => "O", "Ơ" => "O", "Ờ" => "O", "Ở" => "O", "Ỡ" => "O", "Ớ" => "O", "Ợ" => "O",
            "Ù" => "U", "Ủ" => "U", "Ũ" => "U", "Ú" => "U", "Ụ" => "U", "Ư" => "U", "Ừ" => "U", "Ử" => "U", "Ữ" => "U", "Ứ" => "U", "Ự" => "U",
            "Ỳ" => "Y", "Ỷ" => "Y", "Ỹ" => "Y", "Ý" => "Y", "Ỵ" => "Y"
        );

        $string = strtr($string, $charMap);

        $string = self::cleanUpSpecialChars($string, $allowUnder);
        return strtolower($string);
    }
    
    public static function cleanUpSpecialChars($string, $allowUnder = false) {
        $regExpression = "`\W`i";
        if ($allowUnder)
            $regExpression = "`[^a-zA-Z0-9-]`i";

        $string = preg_replace(array($regExpression, "`[-]+`",), "-", $string);
        return trim($string, "-");
    }

}
