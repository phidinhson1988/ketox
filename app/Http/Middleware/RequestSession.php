<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;
use Route;
use App\Models\RoleUser;

class RequestSession {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next) {

        $roleUser = RoleUser::where('user_id', Auth::user()->id)->first();
        if (!$roleUser) {
            return $next($request);
        }
        if (class_basename(Route::current()->getActionName()) != 'UserController@profile' && !Auth::user()->hasAdminRole(1)) {
            if (empty($roleUser->role()->first())) {
                return redirect()->route('user.profile')->with([
                            'warning' => 'Không có quyền hạn. Vui lòng liên hệ admin cập nhật quyền hạn !'
                ]);
            }
        }

        $permissions = Auth::user()->role->first();
        if ($permissions) {
            if ($request->session()->has('permission')) {
                $request->session()->forget('permission');
                $permissions = $permissions->permission()->pluck('name')->toArray();
                $request->session()->put('permission', $permissions);
            }
        }

        return $next($request);
    }

}
