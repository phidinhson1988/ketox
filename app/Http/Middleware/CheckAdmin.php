<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Foundation\Application;

class CheckAdmin
{
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $arrPermission = session()->has('permission') ? session()->get('permission') : [];
        if ($this->app->isDownForMaintenance()) {
            if (Auth::user()->hasRole('root') || in_array('setting-list', $arrPermission)) {
                return $next($request);
            } else {
                return abort(503);
            }
        }
        return $next($request);
    }
}
