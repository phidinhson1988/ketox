<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Foundation\Application;

class CheckForMaintenanceMode
{
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->app->isDownForMaintenance()) {
            if ($this->isCheckRequest($request)) {
                return $next($request);
            } else {
                return abort(503);
            }
        }
        return $next($request);
    }

    private function isCheckRequest($request)
    {
        return ($request->is('setting')
            or $request->is('login')
            or $request->is('logout')
            or $request->is('/')
        );
    }
}

