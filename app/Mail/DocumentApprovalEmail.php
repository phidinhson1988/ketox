<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DocumentApprovalEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $title = null;
    protected $body = null;

    public function __construct($title, $body)
    {
        $this->title = $title;
        $this->body = $body;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->from('admin@shi-v.com')
            ->subject('Mail phê duyệt')
            ->markdown('mails.check')
            ->with([
                'title' => $this->title,
                'body' => $this->body,
            ]);
        $attachments = $this->body['attachments'];
        if (count($attachments)) {
            foreach ($attachments as $attach) {
                $email->attach($attach->real_path);
            }
        }
        return $email;
    }
}
