<?php

namespace App\Providers;

use App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Session;
use App\Helpers\Facades\Tool;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $ranges = [10,20,30,50];
        View::composer('*', function ($view) use ($ranges) {
            $view->with([
                'default_range' => $ranges,
            ]);
        });
        
//        if (class_exists('Swift_Preferences')) {
//            if (!file_exists(storage_path().'/tmp')) {
//                mkdir(storage_path().'/tmp', 0777, true);
//            }
//            \Swift_Preferences::getInstance()->setTempDir(storage_path().'/
//            ');
//        } else {
//            \Log::warning('Class Swift_Preferences does not exists');
//        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
