<?php

/**
 * @package     DMS
 * @category    Role Model
 * @copyright   Copyright 2018 FPT
 * @author      SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Models;

use App\Helpers\Facades\Tool;
use Illuminate\Support\Facades\DB;
use Zizaco\Entrust\EntrustRole;
use Auth;

class Role extends EntrustRole
{
    protected $table = 'roles';

    protected $fillable = ['name,description,status'];
    public $timestamps = false;
    public $orderColumn = 'id';
    public $defaultSoft = 'DESC';
    public static $instance = null;

    public static function getInstance()
    {
        if (is_null(static::$instance)) {
            static:: $instance = new self;
        }
        return static:: $instance;
    }

    /*
              * @description: xử lý dữ liệu hiển thị role,tìm kiếm role
              * @input:  hiển thị list không có input / tìm kiếm role $keywork
              * @output: array data
              * @author: KhoiDT3
              * @created at: 17/8/2018
        * *** */
    public static function fetchAll($limit = null, $q = '')
    {
        $limit = $limit ? $limit : Tool::getConst('DEFAULT_LIMIT');
        $table = static::getInstance()->getTable();
        $select = static::select($table . '.*')
            ->from($table)
            ->where('status', '=', Tool::getConst('ROLES_ACTIVE'))
            ->where('name', '<>', 'root');

        if (strlen(trim($q)) > 0) {
            $select = $select->where(function ($query) use ($q, $table) {
                $query->where($table . '.name', 'LIKE', '%' . Tool::sanitizeQuery($q) . '%');
            });
        }
        $select = $select->orderBy('id', 'desc');
        return $select->paginate($limit);

    }

    /*
          * @description: hiển thị toàn bộ dữ liệu Roles
          * @input:
          * @output: array data
          * @author: KhoiDT3
          * @created at: 17/8/2018
    * *** */
    public static function showAllRoles()
    {
        $value = DB::table('roles')->where('status', '=', Tool::getConst('ROLES_ACTIVE'))->where('name', '<>', 'root')
            ->orderBy('name', 'asc')
            ->get();
        return $value;
    }

    public static function storeInstance($request)
    {
        $role = new Role();
        $role->name = $request->input('name');
        $role->description = $request->input('description');
        $role->status = true;
        $role->save();

        $permission = $request->get('permission', null);
        if ($permission && count($permission) > 0) {
            $role->permission()->detach();
            foreach ($permission as $key => $p) {
                $role->permission()->attach($p);
            }
        }

    }

    public static function updateInstance($request, $role)
    {
        $role->name = $request->input('name');
        $role->description = $request->input('description');
        $role->save();

        $permission = $request->get('permission', null);
        $permissionrole = PermissionRole::where('role_id', $role->id)->get();
        if (!empty($permissionrole)) {
            foreach ($permissionrole as $item) {
                $item->delete();
            }
        }
        if ($permission && count($permission) > 0) {
            $role->permission()->detach();
            foreach ($permission as $key => $p) {
                $role->permission()->attach($p);
            }
        }
    }

    public function user()
    {
        return $this->belongsToMany('App\Models\User', 'role_user', 'role_id', 'user_id');
    }

    public function permission()
    {
        return $this->belongsToMany('App\Models\Permission', 'permission_role', 'role_id', 'permission_id');
    }
}
