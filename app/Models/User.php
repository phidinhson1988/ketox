<?php

/**
 * @package     DMS
 * @category    User Model
 * @copyright   Copyright 2018 FPT
 * @author      SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Models;

use App\Helpers\Facades\Tool;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DB;

class User extends Authenticatable {

    use EntrustUserTrait;

use Notifiable;

    protected $table = 'users';
    protected $fillable = ['username,full_name,password,phone,day_of_birth,status,image,remember_token,created_at'];
    public $timestamps = false;
    public $orderColumn = 'id';
    public $defaultSoft = 'DESC';
    public $primaryKey = 'id';
    public static $instance = null;
    protected $tablelog = 'user_logs';

    public function role() {
        return $this->belongsToMany('App\Models\Role', 'role_user', 'user_id', 'role_id');
    }

    public static function getInstance() {
        if (is_null(static::$instance)) {
            static:: $instance = new self;
        }
        return static:: $instance;
    }

    public function hasAllStages() {
        $allStages = Stage::where('status', true)->count();
        $activeStages = $this->getActiveStages();
        return $activeStages->contains('all');
    }

    /*
         * @description: xử lý dữ liệu hiển thị user,tìm kiếm user
         * @input:  hiển thị list không có input / tìm kiếm user
         * @output: array data
         * @author: KhoiDT3
         * @created at: 7/8/2018
     * *** */

    public static function fetchAll($limit = null, $role_id = null, $q = '') {
        $limit = $limit ? $limit : Tool::getConst('DEFAULT_LIMIT');
        $table = static::getInstance()->getTable();
        $select = static::select($table . '.*', 'role_user.role_id')
                ->leftJoin('role_user', 'users.id', '=', 'role_user.user_id')
                ->where('users.status', '=', Tool::getConst('USER_ACTIVE'))
                ->where('users.username', '<>', Tool::getConst('SYSTEM_USER_ACCOUNT'))
                ->from($table);
        if ($role_id) {
            $select->where('role_user.role_id', '=', $role_id);
        }
        if (strlen(trim($q)) > 0) {
            $select->where($table . '.username', 'LIKE', '%' . Tool::sanitizeQuery($q) . '%');
        }
        $select = $select->orderBy('users.id', 'desc');
        return $select->paginate($limit);
    }

    /*
         * @description: resetPass
         * @input:  id User
         * @output: reset Pass thành công
         * @author: KhoiDT3
         * @created at: 21/8/2018
     * *** */

    public static function resetPass($user) {
        //RESET PASSWORD
        $user->password = Hash::make(Tool::getConst('HASH_DEFAULT'));
        $user->update();

        //RESET PTMS
        if (static::checkConnectAnotherDB()) {
            $userFind = new User();
            $userFind->setConnection('mysql2');
            $userSync = $userFind->where('username', $user->username)->first();
            if (!empty($userSync)) {
                $userSync->password = $user->password;
                $userSync->update();
            }
        }
    }

    /*
         * @description: checkUser
         * @input:
         * @output: check user đăng nhâp
         * @author: KhoiDT3
         * @created at: 21/8/2018
     * *** */

    public static function checkUser() {
        return self::where('id', Auth::user()->id)->first();
    }

    /*
         * @description: edit profile
         * @input:  id
         * @output: edit profile thành công
         * @author: KhoiDT3
         * @created at: 19/8/2018
     * *** */

    public static function profileInstance($request, $user) {
        //UPDATE DATABASE DMS
        $user->full_name = $request->get('full_name');
        $user->email = $request->get('email');
        $user->phone = $request->get('phone');
        $avatar = $request->get('avatar', null);
        if ($avatar && $avatar != $user->avatar) {
            Product::moveFileProduct($avatar, Tool::getConst('USER_UPLOAD'));
        }
        $user->avatar = $avatar;
        $user->save();
    }

    /*
         * @description: đổi mật khẩu
         * @input: password new
         * @output: cập nhập password thành công / lỗi
         * @author: KhoiDT3
         * @created at: 21/8/2018
     * *** */

    public static function passwordInstance($request, $user) {
        //CHANG PASSWORD DMS
        $user->password = Hash::make($request->new_password);
        $user->save();

        //CHANG PASSWORD PTMS
        if (static::checkConnectAnotherDB()) {
            $userFind = new User();
            $userFind->setConnection('mysql2');
            $userSync = $userFind->where('username', $user->username)->first();
            if (!empty($userSync)) {
                $userSync->password = $user->password;
                $userSync->update();
            }
        }
    }

    /**
     * @package     DMS
     * @category    Check user has administrator role
     * @author      TrungLH3
     *
     * @param       $depthLevel
     * @param       $manager
     *
     * @return      boolean
     */
    public function hasAdminRole($depthLevel = null) {
        $depth = $depthLevel ? $depthLevel : Tool::getConst('ACTIVE_OFF');

        if (Auth::check()) {
            $user = Auth::user();
            $role = $user->role()->get();
            return $role->filter(function ($r) use ($depth) {
                        if ($depth == Tool::getConst('ACTIVE_ON')) {
                            $condition = (strtolower($r->name) == 'admin') || (strtolower($r->name) == 'root');
                        } else {
                            $condition = (strtolower($r->name) == 'admin') || (strtolower($r->description) == 'admin');
                        }
                        return $condition;
                    })->count() > 0;
        }
        return false;
    }

    /**
     * @package     DMS
     * @category    Check user has manager role
     * @author      TrungLH3
     *
     * @param       void
     *
     * @return      boolean
     */
    public function hasManagerRole() {
        $role = $this->role()->get();
        return $role->filter(function ($r) {
                    return (strtolower($r->name) == 'manager') || (strtolower($r->description) == 'manager');
                })->count() > 0;
    }

    /**
     * @package     DMS
     * @category    Check connect PTMS server
     * @author      TrungLH3
     *
     * @param       void
     *
     * @return      boolean
     */
    public static function checkConnectAnotherDB() {
        $ptms_host = env('DB2_HOST') ?? null;
        $ptms_username = env('DB2_USERNAME') ?? null;
        $ptms_password = env('DB2_PASSWORD') ?? null;
        $status = false;
        try {
            if ($ptms_host && $ptms_username && $ptms_password) {
                $conn = @mysqli_connect($ptms_host, $ptms_username, $ptms_password);
                if ($conn) {
                    $status = true;
                }
            }
        } catch (Exception $e) {
            $status = false;
        }
        return $status;
    }

    /**
     * @package     DMS
     * @category    create user
     * @author      TrungLH3
     *
     * @param       void
     *
     * @return      boolean
     */
    public static function storeInstance($request) {
        //SAVE DATABASE DMS
        $user = new User();
        $user->username = $request->get('username');
        $user->password = Hash::make(Tool::getConst('HASH_DEFAULT'));
        $user->full_name = $request->get('full_name');
        $user->phone = $request->get('phone');
        $user->email = $request->get('email');
        $user->created_at = Carbon::now();
        $user->status = Tool::getConst('ACTIVE_ON');
        $avatar = $request->get('avatar', null);
        if ($avatar) {
            Product::moveFileProduct($avatar, Tool::getConst('USER_UPLOAD'));
        }
        $user->avatar = $avatar;
        $user->save();

        if ($request->get('role_id')) {
            $roleUser = new RoleUser();
            $roleUser->user_id = $user->id;
            $roleUser->role_id = $request->role_id;
            $roleUser->save();
        }
    }

    public static function updateInstance($request, $user) {
        //UPDATE DATABASE DMS
        $user->full_name = $request->get('full_name');
        $user->phone = $request->get('phone');
        $user->email = $request->get('email');
        $avatar = $request->get('avatar', null);
        if ($avatar && $avatar != $user->avatar) {
            Product::moveFileProduct($avatar, Tool::getConst('USER_UPLOAD'));
        }
        $user->avatar = $avatar;
        $user->save();

        if ($request->get('role_id')) {
            $UserRole = RoleUser::where('user_id', $user->id)->first();
            if (!empty($UserRole)) {
                $UserRole->delete();
            }
            $roleUser = new RoleUser();
            $roleUser->user_id = $user->id;
            $roleUser->role_id = $request->role_id;
            $roleUser->save();
        }

    }

}
