<?php

/**
 * @package     DMS
 * @category    Cart Model
 * @copyright   Copyright 2018 FPT
 * @author      SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Models;

use App\Helpers\Facades\Tool;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;

class Cart extends Model {

    protected $table = 'carts';
    public $timestamps = false;

    /*
           * @description: xử lý dữ liệu hiển thị đơn hàng
           * @input:  hiển thị list không có input / tìm kiếm đơn hàng $keywork
           * @output: array data
           * @author: SonPD1
           * @created at: 17/8/2018
     * *** */

    public static function fetchAll($limit = null, $status = '', $order = 'id', $sort = 'desc') {
        $limit = $limit ? $limit : Tool::getConst('DEFAULT_LIMIT');
        $select = Cart::select('carts.*', 'users.full_name', 'users.email', 'users.address', 'users.phone')
                ->join('users','users.id','=','carts.user_id');
        if ($status) {
            $select = $select->where('carts.status', $status);
        }
        $select = $select->orderBy('status', 'asc')->orderBy($order, $sort);
        return $select->paginate($limit);
    }

    public static function storeInstance($request) {
        //SAVE DATABASE DMS
        $cart = new Cart();
        $cart->title = $request->get('title');
        $cart->slug = \Illuminate\Support\Str::slug($cart->title);
        $cart->price = $request->get('price');
        $cart->description = $request->get('description');
        $cart->content = $request->get('content');
        $cart->created_by = Auth::user()->id;
        $cart->status = Tool::getConst('ACTIVE_ON');
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $fileName = $file->getClientOriginalName();
            $name = md5(($fileName) . date('Y-m-d H:i:s')) . '.' . $file->getClientOriginalExtension();
            $file->move(Tool::getConst('USER_UPLOAD'), $name);
            $cart->avatar = $name;
        }
        $cart->save();
    }

    public static function updateInstance($request, $cart) {
        $cart->title = $request->input('title');
        $cart->slug = \Illuminate\Support\Str::slug($cart->title);
        $cart->price = $request->input('price');
        $cart->description = $request->input('description');
        $cart->content = $request->input('content');
        if ($request->hasFile('avatar')) {
            $image = public_path(Tool::getConst('USER_UPLOAD') . $cart->avatar);
            if (strlen($cart->avatar) > 0 && file_exists($image)) {
                unlink($image);
            }
            $file = $request->file('avatar');
            $fileName = $file->getClientOriginalName();
            $name = md5(($fileName) . date('Y-m-d H:i:s')) . '.' . $file->getClientOriginalExtension();
            $file->move(Tool::getConst('USER_UPLOAD'), $name);
            $cart->avatar = $name;
        }
        $cart->save();
    }

}
