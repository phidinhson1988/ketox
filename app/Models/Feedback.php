<?php

/**
 * @package     DMS
 * @category    News Model
 * @copyright   Copyright 2018 FPT
 * @author      SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Models;

use App\Helpers\Facades\Tool;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;

class Feedback extends Model {

    protected $table = 'feedbacks';
    public $timestamps = false;

    /*
           * @description: xử lý dữ liệu hiển thị sản phẩm
           * @input:  hiển thị list không có input / tìm kiếm sản phẩm $keywork
           * @output: array data
           * @author: SonPD1
           * @created at: 17/8/2018
     * *** */

    public static function fetchAll($limit = null, $q = '', $order = 'id', $sort = 'desc') {
        $limit = $limit ? $limit : Tool::getConst('DEFAULT_LIMIT');
        $select = static::select('feedbacks.*');
        if (strlen(trim($q)) > 0) {
            $select = $select->join('products', 'products.id', '=', 'feedbacks.product_id')->where(function ($query) use ($q) {
                $query->where('products.title', 'LIKE', '%' . Tool::sanitizeQuery($q) . '%');
            });
        }
        $select = $select->orderBy('feedbacks.'.$order, $sort);
        return $select->paginate($limit);
    }

    public static function storeInstance($request) {
        //SAVE DATABASE DMS
        $productId = $request->get('product_id');
        $product = Product::find($productId);
        $post = new Feedback();
        $post->user_id = $request->get('user_id');
        $post->product_id = $productId;
        $post->content = $request->get('content');
        $post->created_at = Carbon::now();
        $post->status = Tool::getConst('ACTIVE_ON');
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $fileName = $file->getClientOriginalName();
            $name = md5(($fileName) . date('Y-m-d H:i:s')) . '.' . $file->getClientOriginalExtension();
            $file->move(Tool::getConst('POST_UPLOAD'), $name);
            $post->avatar = $name;
        }
        $post->save();
    }

    public static function updateInstance($request, $post) {
        $productId = $request->get('product_id');
        $product = Product::find($productId);
        $post->user_id = $request->get('user_id');
        $post->product_id = $productId;
        $post->content = $request->get('content');
        $post->created_at = Carbon::now();
        $post->status = Tool::getConst('ACTIVE_ON');
        if ($request->hasFile('avatar')) {
            $image = public_path(Tool::getConst('POST_UPLOAD') . $post->avatar);
            if (strlen($post->avatar) > 0 && file_exists($image)) {
                unlink($image);
            }
            $file = $request->file('avatar');
            $fileName = $file->getClientOriginalName();
            $name = md5(($fileName) . date('Y-m-d H:i:s')) . '.' . $file->getClientOriginalExtension();
            $file->move(Tool::getConst('POST_UPLOAD'), $name);
            $post->avatar = $name;
        }
        $post->save();
    }

}
