<?php

/**
 * @package     DMS
 * @category    News Model
 * @copyright   Copyright 2018 FPT
 * @author      SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Models;

use App\Helpers\Facades\Tool;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;

class News extends Model {

    protected $table = 'posts';
    public $timestamps = false;

    /*
           * @description: xử lý dữ liệu hiển thị sản phẩm
           * @input:  hiển thị list không có input / tìm kiếm sản phẩm $keywork
           * @output: array data
           * @author: SonPD1
           * @created at: 17/8/2018
     * *** */

    public static function fetchAll($limit = null, $q = '', $order = 'id', $sort = 'desc', $type = null) {
        $limit = $limit ? $limit : Tool::getConst('DEFAULT_LIMIT');
        $select = static::select('posts.*');
        if (strlen(trim($q)) > 0) {
            $select = $select->where(function ($query) use ($q) {
                $query->where('posts.title', 'LIKE', '%' . Tool::sanitizeQuery($q) . '%');
            });
        }
        if ($type) {
            $select->where('posts.type', $type);
        }
        $select = $select->orderBy($order, $sort);
        return $select->paginate($limit);
    }

    public static function storeInstance($request) {
        //SAVE DATABASE DMS
        $from = $request->get('from', null);
        $to = $request->get('to', null);
        $post = new News();
        $post->title = $request->get('title');
        $post->slug = \Illuminate\Support\Str::slug($post->title);
        $post->description = $request->get('description', null);
        $post->is_require = $request->get('is_require', 0);
        $post->content = $request->get('content', null);
        $post->lang = $request->get('lang', null);
        $post->meta_des = $request->get('meta_des', null);
        $post->meta_keyword = $request->get('meta_keyword', null);
        $post->type = $request->get('type', 1);
        $post->job = $request->get('job', null);
        $post->start_date = $from ? Carbon::parse($from)->startOfDay()->format('Y-m-d H:i:s') : null;
        $post->end_date = $to ? Carbon::parse($to)->endOfDay()->format('Y-m-d H:i:s') : null;
        $post->created_by = Auth::user()->id;
        $post->created_at = Carbon::now();
        $post->updated_at = Carbon::now();
        $post->status = Tool::getConst('ACTIVE_ON');
        $avatar = $request->get('avatar', null);
        if ($avatar) {
            Product::moveFileProduct($avatar, Tool::getConst('POST_UPLOAD'));
        }
        $post->avatar = $avatar;
        $post->save();
    }

    public static function updateInstance($request, $post) {
        $from = $request->get('from', null);
        $to = $request->get('to', null);
        $post->title = $request->input('title');
        $post->slug = \Illuminate\Support\Str::slug($post->title);
        $post->description = $request->get('description', null);
        $post->is_require = $request->get('is_require', 0);
        $post->meta_des = $request->get('meta_des', null);
        $post->lang = $request->get('lang', null);
        $post->type = $request->get('type', $post->type);
        $post->job = $request->get('job', $post->job);
        $post->start_date = $from ? Carbon::parse($from)->startOfDay()->format('Y-m-d H:i:s') : null;
        $post->end_date = $to ? Carbon::parse($to)->endOfDay()->format('Y-m-d H:i:s') : null;
        $post->meta_keyword = $request->get('meta_keyword', null);
        $post->content = $request->input('content', null);
        $post->created_at = $post->created_at ? $post->created_at : Carbon::now();
        $post->updated_at = Carbon::now();
        $avatar = $request->get('avatar', null);
        if ($avatar && $avatar != $post->avatar) {
            Product::moveFileProduct($avatar, Tool::getConst('POST_UPLOAD'));
        }
        $post->avatar = $avatar;
        $post->save();
    }

}
