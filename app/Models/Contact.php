<?php

/**
 * @package     DMS
 * @category    News Model
 * @copyright   Copyright 2018 FPT
 * @author      SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Models;

use App\Helpers\Facades\Tool;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;

class Contact extends Model {

    protected $table = 'contacts';
    public $timestamps = false;

    /*
           * @description: xử lý dữ liệu hiển thị sản phẩm
           * @input:  hiển thị list không có input / tìm kiếm sản phẩm $keywork
           * @output: array data
           * @author: SonPD1
           * @created at: 17/8/2018
     * *** */

    public static function fetchAll($limit = null, $q = '', $order = 'id', $sort = 'desc') {
        $limit = $limit ? $limit : Tool::getConst('DEFAULT_LIMIT');
        $select = static::select('contacts.*')->whereNotNull('email');
//        if (strlen(trim($q)) > 0) {
//            $select = $select->join('products', 'products.id', '=', 'contacts.product_id')->where(function ($query) use ($q) {
//                $query->where('products.title', 'LIKE', '%' . Tool::sanitizeQuery($q) . '%');
//            });
//        }
        $select = $select->orderBy('contacts.'.$order, $sort);
        return $select->paginate($limit);
    }

}
