<?php

/**
 * @package     DMS
 * @category    Product Model
 * @copyright   Copyright 2018 FPT
 * @author      SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Models;

use App\Helpers\Facades\Tool;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\File as FileUpload;

class Product extends Model {

    protected $table = 'products';
    public $timestamps = false;

    /*
           * @description: xử lý dữ liệu hiển thị sản phẩm
           * @input:  hiển thị list không có input / tìm kiếm sản phẩm $keywork
           * @output: array data
           * @author: SonPD1
           * @created at: 17/8/2018
     * *** */

    public static function fetchAll($limit = null, $q = '', $order = 'id', $sort = 'desc') {
        $limit = $limit ? $limit : Tool::getConst('DEFAULT_LIMIT');
        $select = static::select('products.*');
        if (strlen(trim($q)) > 0) {
            $select = $select->where(function ($query) use ($q) {
                $query->where('products.title', 'LIKE', '%' . Tool::sanitizeQuery($q) . '%');
            });
        }
        $select = $select->orderBy($order, $sort);
        return $select->paginate($limit);
    }
    
    /**
     * @package     CAPI
     * @category    move File product
     * @author      SonPD1
     * @param       $request
     * @return      boolean
     */
    public static function moveFileProduct($fileName, $path) {
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        $oldPath = public_path().'/upload/draw/'.$fileName;
        if (!file_exists($oldPath)) {
            $oldPath = public_path().'/upload/new/'.$fileName;
        }
        $newPath = $path.$fileName;
        FileUpload::move($oldPath, public_path().'/'.$newPath);
        return $newPath;
    }

    public static function storeInstance($request) {
        //SAVE DATABASE DMS
        $product = new Product();
        $product->title = $request->get('title');
        $product->slug = \Illuminate\Support\Str::slug($product->title);
        $product->price = $request->get('price');
        $product->description = $request->get('description');
        $product->lang = $request->get('lang');
        $product->content = $request->get('content');
        $product->created_by = Auth::user()->id;
        $product->created_at = Carbon::now();
        $product->updated_at = Carbon::now();
        $product->status = Tool::getConst('ACTIVE_ON');
        $product->sort = $request->get('sort', 1);
        $avatar = $request->get('avatar', null);
        if ($avatar) {
            Product::moveFileProduct($avatar, Tool::getConst('PRODUCT_UPLOAD'));
        }
        $product->avatar = $avatar;
        $product->save();
    }

    public static function updateInstance($request, $product) {
        $product->title = $request->get('title');
        $product->slug = \Illuminate\Support\Str::slug($product->title);
        $product->price = $request->get('price');
        $product->description = $request->get('description');
        $product->lang = $request->get('lang');
        $product->content = $request->get('content');
        $product->created_at = $product->created_at ? $product->created_at : Carbon::now();
        $product->updated_at = Carbon::now();
        $product->sort = $request->get('sort', 1);
        $avatar = $request->get('avatar', null);
        if ($avatar && $avatar != $product->avatar) {
            Product::moveFileProduct($avatar, Tool::getConst('PRODUCT_UPLOAD'));
        }
        $product->avatar = $avatar;
        $product->save();
    }

}
