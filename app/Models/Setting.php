<?php

/**
 * @package    DMS
 * @category    Setting Model
 * @copyright  Copyright 2018 FPT
 * @author     SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Models;

use App\Helpers\Common\CodeParsing;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\Facades\Tool;
use Illuminate\Support\Facades\File as FileUpload;
use Illuminate\Support\Facades\Auth;

class Setting extends Model {

    protected $table = 'settings';
    public $timestamps = false;

    public static function moveFile($path, $fileName) {
        $status = true;
        try {
            $directory = public_path(rtrim(Tool::getConst('USOL_UPLOAD_COMMON'), '/'));
            $destination = $directory . '/' . $fileName;
            if (!file_exists($directory)) {
                mkdir($directory, 0777, true);
            }
            if (file_exists($path)) {
                FileUpload::copy($path, $destination);
            }
        } catch (Exception $ex) {
            $status = false;
        }

        return $status;
    }
    public static function getDataMenu($data){
        $listOrder = [];
        $menuAll = [];
        foreach ($data as $itemValue) {
            $listOrder[] = get_object_vars($itemValue);
            $menuAll[$itemValue->id] = $itemValue;
        }
        $result = Setting::orderToTree($listOrder);
        return $result;
    }
    
    public static function orderToTree(&$listOrder) {
        $map = array(
            0 => array('childrent' => array())
        );
        foreach ($listOrder as &$order) {
            $order['childrent'] = array();
            $map[$order['id']] = &$order;
        }
        foreach ($listOrder as &$order) {
            $map[$order['parent']]['childrent'][] = &$order;
        }
        return $map[0]['childrent'];
    }
}