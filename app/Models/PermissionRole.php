<?php

/**
 * @package    DMS
 * @category    PermissionRole Model
 * @copyright  Copyright 2018 FPT
 * @author     SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionRole extends Model
{
    protected $table = 'permission_role';
    public $timestamps = false;
}
