<?php

/**
 * @package    DMS
 * @category    Permission Model
 * @copyright  Copyright 2018 FPT
 * @author     SonPD1 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Models;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
    protected $table = 'permissions';
    public $timestamps = false;

    public function role()
    {
        return $this->belongsToMany('App\Models\Role', 'permission_role', 'permission_id', 'role_id');
    }
}
