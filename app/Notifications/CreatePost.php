<?php

/**
 * @package    DMS - CreatePost Common
 * @copyright  Copyright 2018 FPT
 * @author     FSU2.C88
 */

namespace App\Notifications;

use Auth;
use App\Models\Post;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class CreatePost extends Notification
{

    use Queueable;

    protected $post;
    protected $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($post)
    {
        $user = User::where('id', Auth::id())->select('id', 'username', 'email', 'image')->first();
        $this->post = $post;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'post' => $this->post,
            'user' => $this->user,
        ];

    }

    public function toArray($notifiable)
    {
        return [];
    }

}
