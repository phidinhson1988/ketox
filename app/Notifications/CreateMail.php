<?php

namespace App\Notifications;

use App\Helpers\Facades\Tool;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\View;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CreateMail extends Notification
{
    use Queueable;

    protected $title;
    protected $post;
    protected $cc = array();
    protected $mailType = null;
    protected $specifiedAction = null;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($title, $post, $cc = null, $mailType = null, $specifiedAction = null)
    {
        $this->title = $title;
        $this->post = $post;
        $this->cc = $cc;
        $this->mailType = ($mailType || $mailType === 0) ? $mailType : Tool::getConst('APPROVE_MAIL');
        $this->specifiedAction = $specifiedAction;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            'mail',
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail = $this->_getMailByType($this->mailType);
        if (View::exists($mail)) {
            $sender = (new MailMessage())
                    ->subject('[DMS] - ' . $this->title)
                    ->markdown($mail, [
                        'title' => $this->title,
                        'body' => $this->post,
                        'action' => $this->specifiedAction,
                    ]);
            if (count($this->cc)) {
                $sender->cc($this->cc);
            }
            if (isset($this->post['attachments'])) {
                $attachments = $this->post['attachments'];
                if (count($attachments)) {
                    foreach($attachments as $attach) {
                        $sender->attach($attach->real_path);
                    }
                }
            }
            return $sender;
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    private function _getMailByType($mailType) {
        $mail = null;
        switch($mailType) {
            case Tool::getConst('CREATED_MAIL'):
                $mail = 'mails.create';
                break;
            case Tool::getConst('APPROVE_MAIL'):
                $mail = 'mails.check';
                break;
            case Tool::getConst('REJECT_MAIL'):
                $mail = 'mails.reject';
                break;
            case Tool::getConst('REVOKE_MAIL'):
                $mail = 'mails.revoke';
                break;
            case Tool::getConst('REVICE_MAIL'):
                $mail = 'mails.revice';
                break;
            case Tool::getConst('UPLOAD_MAIL'):
                $mail = 'mails.upload';
                break;
            case Tool::getConst('EXPIRED_MAIL'):
                $mail = 'mails.expired';
                break;
            case Tool::getConst('TEMPLATE_MAIL'):
                $mail = 'mails.template';
                break;
            default:
                $mail = 'mails.create';
                break;
        }
        return $mail;
    }
}
