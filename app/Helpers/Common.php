<?php

namespace App\Helper;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class Common {

    /**
     * @description: Sử lý hiển thị thời gian tact time
     * @input: $time
     * @output: $result
     * @author: SonPD1
     * @created_at: 010/09/2018
     */
    public static function formatTactTime($timestamp) {
        $time = '';
        if ($timestamp < 60) {
            $time = $timestamp . ' s';
        } else if ($timestamp >= 60 && $timestamp < 60 * 60) {
            $time = ($timestamp / 60) . ' m';
        } else {
            $time = ($timestamp / 60 / 60) . ' h';
        }
        return $time;
    }

    public static function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
        $sort_col = array();
        foreach ($arr as $key => $row) {
            $sort_col[$key] = $row[$col];
        }
        array_multisort($sort_col, $dir, $arr);
    }
}
