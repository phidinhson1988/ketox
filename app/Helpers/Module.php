<?php

namespace App\Helpers;

use Notification;
use App\Notifications\CreateMail;
use App\Notifications\CreatePost;
use Illuminate\Support\Facades\App;

class Module
{
    public static function sendNotification($user, $post)
    {
        Notification::send($user, new CreatePost($post));
    }

    public static function sendMail($user, array $cc, $title, array $post, int $mailType = null, $specifiedAction = null)
    {
        return true;
        $cc = array_filter($cc, function ($mail) {
            return ! empty($mail);
        });
        Notification::send($user, new CreateMail($title, $post, $cc, $mailType, $specifiedAction));
    }

    public static function checkServer()
    {
        return $status = App::isDownForMaintenance() ? 'down' : 'up';
    }
}
