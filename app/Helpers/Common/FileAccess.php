<?php

namespace App\Helpers\Common;
use Illuminate\Support\Facades\File;

class FileAccess {
    protected $data = null;
    public function listAll($path) {
        $data = $this->recursiveRead($path);
        $this->data = $this->getDataStructure($data);
        return $this;
    }
    public function toArray() {
        return $this->data;
    }
    public function toJson() {
        return json_encode($this->data);
    }
    public function getDataStructure($data) {
        $result = array();
        if (count($data) > 0) {
            array_walk_recursive($data, function ($item, $index) use (&$result) {
                if ($index == 'path') {
                    $result[] = [
                        'path' => rtrim(str_replace(basename($item), '', $item), DIRECTORY_SEPARATOR),
                        'file' => basename($item),
                    ];
                }
            });
        }
        return $result;
    }
    public function filter($condition = null) {
        if ($this->data) {
            $result = array();
            array_walk_recursive($this->data, function (&$item, $index) use ($condition, &$result) {
                $prefix = substr($item, 0, strlen($condition));
                if (! strpos($item, '\\') && !strcmp($prefix, $condition)) {
                    $result[] = [$index => $item];
                }
            });
            return $result;
        }
        return null;
    }
    public function recursiveRead($path) {
        $result = array();
        if (is_dir($path)) {
            $dataArray = array_values(array_diff(scandir($path), array('..', '.')));
            foreach($dataArray as $k => $v) {
                if (is_dir($path . DIRECTORY_SEPARATOR . $v)) {
                    $result[] = $this->recursiveRead($path . DIRECTORY_SEPARATOR . $v);
                } else {
                    $result[] = [
                        'path' => $path . DIRECTORY_SEPARATOR . $v,
                        'filename' => basename($path . DIRECTORY_SEPARATOR . $v),
                    ];
                }
                unset($dataArray[$k]);
            }
            return $result;
        }
    }
    public function moveBatch($destination = '') {
        $status = true;
        if (count($this->data) > 0) {
            array_walk($this->data, function ($item, $index) use ($status) {
                if (! File::move($item['path'].DIRECTORY_SEPARATOR.$item['filename'], $destination)) {
                    $status = false;
                }
            });
        }
        return $status ? 'OK' : 'Có lỗi xảy ra';
    }
}
