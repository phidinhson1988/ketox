<?php

/**
 * @package    DMS
 * @category    CodeParsing
 * @copyright  Copyright 2018 FPT
 * @author     TrungLH3 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Helpers\Common;

use App\Helpers\Facades\Tool;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class CodeParsing {

    // Sample document code: VHN-RCMP 18001020
    const PREFIX_LENGTH = 3;
    const FOLLOW_CODE_LENGTH = 4;
    const SUFFIX_LENGTH = 3;
    const FIRST_SEPARATOR = '-';
    const SECOND_SEPARATOR = ' ';
    const ISO_CODE_TABLE = 'iso_code';
    const DEPARTMENT_TABLE = 'departments';
    const STAGE_TABLE = 'stages';
    const PID_TABLE = 'types_pid';
    const DOCTYPE_TABLE = 'doc_types';
    const DOCUMENT_TABLE = 'documents';
    const FILE_TABLE = 'files';
    const ISO_CODE_COL = 'iso_code';
    const DOCUMENT_CODE_COL = 'code';
    const EMPTY_CHAR = '0';
    //USOL Server address
    const USOL_SERVER_HOST = 'http://10.17.74.11/dms/public';
    //USOL Database config
    const USOL_DB_HOST = '127.0.0.1';
    const USOL_DB_NAME = 'shiv_usol';
    const USOL_DB_USER = 'root';
    const USOL_DB_PASS = '';
    const USOL_DOCUMENT_TABLE = 'documents';
    const USOL_PID_TABLE = 'pids';
    const USOL_DOC_PID_TABLE = 'doc_pid';
    const USOL_PRODUCT_TABLE_COL = [
        'id' => 'id',
        'created_by' => 'createuserid',
        'created_at' => 'createdate',
        'updated_by' => 'updateuserid',
        'updated_at' => 'updatedate',
    ];

    protected static $instance = null;
    // List of allow document to convert
    public $result = array();
    //List of not allow document to convert
    public $excluded = array();
    public $data = null;
    public $resourcePath = null;
    protected $conn = null;
    protected $filePaths = [];

    private function __construct() {
        return false;
    }

    private static function _connect() {
        $instance = static::getInstance();
        $conn = mysqli_connect(static::USOL_DB_HOST, static::USOL_DB_USER, static::USOL_DB_PASS, static::USOL_DB_NAME);
        if ($conn) {
            $instance->conn = $conn;
        } else {
            throw new \Exception('Cannot connect to database!');
        }
    }

    //Resource from excel / csv file
    private static function setResource(array $resource) {
        $instance = static::getInstance();
        $instance->data = $resource;
        return $instance;
    }

    public static function getResult(array $resource = null) {
        $resultArr = [];
        if ($resource) {
            //If resource is available, set resource from exported data in excel file
            $instance = static::setResource($resource);
        } else {
            return static::getInstance();
            $instance = static::getInstance();
            //If no resource, system connect to usol database
            static::_connect();
            $query = 'SELECT ' . static::USOL_DOCUMENT_TABLE . '.*, group_concat(doc_pid.pid_id SEPARATOR ",") AS pids
                    FROM ' . static::USOL_DOCUMENT_TABLE . ' LEFT JOIN ' . static::USOL_DOC_PID_TABLE . '
                    ON ' . static::USOL_DOCUMENT_TABLE . '.id = ' . static::USOL_DOC_PID_TABLE . '.document_id
                    GROUP BY documents.id';
            $result = mysqli_query($instance->conn, $query);
            if (!is_bool($result)) {
                while ($row = mysqli_fetch_object($result)) {
                    $resultArr[] = $row;
                }
            }
            $instance->data = $resultArr;
        }
        return $instance;
    }

    public static function getExcludedResult() {
        $instance = static::getInstance();
        $result = array();
        if (sizeof($instance->excluded)) {
            $instance->excluded = collect($instance->excluded)->unique('code')->toArray();
            foreach ($instance->excluded as $excluded) {
                $result[] = [
                    'document' => $excluded['code'],
                    'pid' => $excluded['pid'][0],
                ];
            }
        }
        return $result;
    }

    //Chuyển đổi
    public static function convert(array $resource = []) {
        $instance = static::getInstance();
        $data = count($resource) ? $resource : $instance->data;
        if ($data) {
            foreach ($data as $k => $item) {
                static::parseItem($item);
            }
        }
        return $instance;
    }

    public static function extractPatternBackup($code, array $commonProperties = null) {
        $instance = static::getInstance();
        $name = null;
        $codePatterns = array_map('trim', explode(' ', $code));
        //prefix_length = 3, follow_code_length = 4, suffix_length = 3

        $firstPattern = current($codePatterns);
        if (strpos($firstPattern, static::FIRST_SEPARATOR)) {
            $firstSeparator = substr(trim($code), static::PREFIX_LENGTH + strlen(static::FIRST_SEPARATOR) + static::FOLLOW_CODE_LENGTH, 1);
        } else {
            $firstSeparator = substr(trim($code), static::PREFIX_LENGTH + static::FOLLOW_CODE_LENGTH, 1);
        }
        if ($firstSeparator == ' ') {
            array_shift($codePatterns);
            $followCode = current($codePatterns);
            //Nếu follow code có ký tự -
            $totalSuffixLength = static::SUFFIX_LENGTH + strlen(date('y'));
            if (strlen($followCode) == $totalSuffixLength) {
                if (strpos($followCode, '-')) {
                    $followCodePattern = explode('-', $followCode);
                    array_shift($followCodePattern);
                    array_shift($codePatterns);
                    $name = implode('-', $followCodePattern) . ' ' . join(' ', $codePatterns);
                } else {
                    array_shift($codePatterns);
                    $name = join(' ', $codePatterns);
                }
            } else {
                if (strpos($followCode, '-')) {
                    $patterns = explode('-', $followCode);
                    array_shift($patterns);
                    $name = join('-', $patterns);
                } else {
                    $name = substr($followCode, $totalSuffixLength);
                }
            }
        } elseif (preg_match('#[0-9]+#', $firstSeparator)) {
            $name = end($codePatterns);
        } else {
            //Push to excluded result
            $instance->excluded[] = [
                'code' => $code,
                'pid' => $commonProperties['id'],
            ];
        }
        return (object) [
                    'name' => $name,
                    'code' => rtrim(trim(str_replace([$name], null, $code)), '-'),
        ];
    }
    
    public static function explodeQrcode($rules, $code, &$index, &$data){
        $arrCode = [];
        if (isset($rules[$index])) {
            preg_match($rules[$index], $code, $arrCode);
            if (isset($arrCode[1])) {
                $data = [
                    'name' => (isset($arrCode[7])) ? trim(trim($arrCode[7], '-')) : null,
                    'code' => (isset($arrCode[1]) && isset($arrCode[3]) && isset($arrCode[5])) ? $arrCode[1] . '-' . $arrCode[3] . ' ' . $arrCode[5] : '',
                    'prefix' => (isset($arrCode[1]) && isset($arrCode[3])) ? $arrCode[1] . $arrCode[3] : '',
                ];
            } else {
                $index++;
                CodeParsing::explodeQrcode($rules, $code, $index, $data);
            }
        }
    }

    //Tách mã, lấy ra các item có pattern không hợp lệ
    //regex: ([a-zA-Z0-9]{3})(-|\s|_)([a-zA-Z0-9]{4})(-|\s|_)([0-9]{5})(.*?)(\.[0-9a-z]{1,})$
    //bug VMN-RDMQ 14074 9A1V22327---
    public static function extractPattern($code, array $commonProperties = null) {
        $rules = [
            "/([a-zA-Z]{3})([-|\s|_]{1,3})([a-zA-Z]{4})([-|\s|_]{1,3})([a-zA-Z0-9]{5})([-|\s|_]{1,3})(.*?)(\.[0-9A-Za-z]{1,})$/", // rule 1
            "/([a-zA-Z]{3})([-|\s|_]{1,3})([a-zA-Z]{4})([-|\s|_]{1,3})([a-zA-Z0-9]{6})([-|\s|_]{1,3})(.*?)(\.[0-9A-Za-z]{1,})$/", // rule 2
            "/([a-zA-Z]{3})([-|\s|_]{1,3})([a-zA-Z]{4})([-|\s|_]{0,3})([a-zA-Z0-9]{5,6})([-|\s|_]{1,3})(.*?)(\.[0-9A-Za-z]{1,})$/", // rule 3
            "/([a-zA-Z]{3})([-|\s|_]{1,3})([a-zA-Z]{4})([-|\s|_]{1,3})([a-zA-Z0-9]{5})([-|\s|_]{1,3})((.*?)(\.[0-9A-Za-z]{1,})?)$/", // rule 4
            "/([a-zA-Z]{3})([-|\s|_]{1,3})([a-zA-Z]{4})([-|\s|_]{1,3})([a-zA-Z0-9]{6})([-|\s|_]{1,3})((.*?)(\.[0-9A-Za-z]{1,})?)$/", // rule 5
            "/([a-zA-Z]{3})([-|\s|_]{1,3})([a-zA-Z]{4})([-|\s|_]{0,3})([a-zA-Z0-9]{5,6})([-|\s|_]{1,3})((.*?)(\.[0-9A-Za-z]{1,})?)$/" // rule 6
        ];
        $index = 0;
        $data = [];
        CodeParsing::explodeQrcode($rules, $code, $index, $data);
        $instance = static::getInstance();
        return [
            'name' => (isset($data['name'])) ? $data['name'] : null,
            'code' => (isset($data['code'])) ? $data['code'] : null,
            'prefix' => (isset($data['prefix'])) ? $data['prefix'] : null,
        ];
    }

    //Lấy danh sách Pid
    public static function getListPid($filename = null) {
        $result = [];
        $instance = static::getInstance();
        $data = collect($instance->data);
        $data->each(function ($d) use (&$result, $filename) {
            $files = collect($d['files']);
            if ($files->contains($filename)) {
                $result[] = $d['id'];
            }
        });
        return array_unique($result);
    }

    //Tạo instance
    public static function parseItem($item = null) {
        $status = [];
        $instance = static::getInstance();
        if ($item['id']) {
            //Get name of document
            $id = $item[static::USOL_PRODUCT_TABLE_COL['id']];
            $createdBy = $item[static::USOL_PRODUCT_TABLE_COL['created_by']];
            $createdAt = $item[static::USOL_PRODUCT_TABLE_COL['created_at']];
            $updatedBy = $item[static::USOL_PRODUCT_TABLE_COL['updated_by']];
            $updatedAt = $item[static::USOL_PRODUCT_TABLE_COL['updated_at']];
            $commonProperties = compact('id', 'createdBy', 'createdAt', 'updatedBy', 'updatedAt');
            // End lấy pid
            if (count($item['files'])) {
                foreach ($item['files'] as $file) {
                    //Nếu mã ko null
                    if (strlen(trim($file))) {
                        $components = static::extractPattern($file, $commonProperties);
                        $pidCode = static::getListPid($file);
                        //Lấy PID
                        $pid = [];
                        if (count($pidCode) > 0) {
                            foreach ($pidCode as $pcode) {
                                $pidCheck = DB::table(static::PID_TABLE)->where('code', trim($pcode))->where('status', true)->first();
                                if (!$pidCheck) {
                                    $commonGroup = DB::table('groups')->where('name', Tool::getConst('COMMON_GROUP_NAME'))->where('status', Tool::getConst('EXCLUDED_STATUS'))->first();
                                    if ($commonGroup) {
                                        $groupId = $commonGroup->id;
                                    } else {
                                        $groupId = DB::table('groups')->first()->id;
                                    }
                                    $pidAdded = DB::table(static::PID_TABLE)->insertGetId([
                                        'name' => $pcode,
                                        'code' => $pcode,
                                        'status' => Tool::getConst('ACTIVE_ON'),
                                        'group_id' => $groupId,
                                    ]);
                                } else {
                                    $pidAdded = $pidCheck->id;
                                }
                                array_push($pid, $pidAdded);
                            }
                        }
                        $pid = implode(',', $pid);
                        if ($components['prefix']) {
                            //Lấy các file ứng với tài liệu
                            $files = [
                                'file_name' => $file,
                                'doc_id' => null,
                                'upload_by' => Auth::check() ? Auth::id() : null,
                                'type' => Tool::getConst('TYPE_ATTACH'),
                                'link' => Tool::getConst('USOL_UPLOAD_COMMON'),
                                'created_at' => now(),
                                'revision' => null,
                                'status' => Tool::getConst('ACTIVE_ON'),
                                'type_file' => 1, //file upload
                            ];
                            //Check mã code có tồn tại so với ISO code
                            $isoCode = DB::table(static::ISO_CODE_TABLE)->where(static::ISO_CODE_COL, $components['prefix'])->first();
                            if (!empty($isoCode)) {
                                $instance->result[] = [
                                    'code' => $components['code'],
                                    'name' => $components['name'],
                                    'allow_print' => 1,
                                    'pid' => ',' . $pid . ',',
                                    'file' => $files,
                                    'exist_config' => true,
                                    'department_id' => $isoCode->department_id,
                                    'stage_id' => $isoCode->stage_id,
                                    'doc_type_id' => $isoCode->doc_type_id,
                                    'link' => Tool::getConst('USOL_UPLOAD_COMMON'),
                                    'is_manually' => false,
                                    'status' => Tool::getConst('DOCUMENT_PUBLISHED'),
                                    'frequency_report' => Tool::getConst('ACTIVE_OFF'),
                                    'relate_status' => Tool::getConst('ACTIVE_OFF'),
                                ];
                            } else {
                                $instance->excluded[] = [
                                    'code' => $file,
                                    'pid' => $pidCode,
                                ];
                            }
                        } else {
                            $instance->excluded[] = [
                                'code' => $file,
                                'pid' => $pidCode,
                            ];
                        }
                    }
                }
            }
        }
    }

    public static function getInstance() {
        if (is_null(static::$instance)) {
            static::$instance = new self;
        }
        return static::$instance;
    }

    public function generateDocumentCode($config) {
        $isoCode = $config->{static::ISO_CODE_COL};
        $instance = static::getInstance();
        $codeArr = DB::table(static::DOCUMENT_TABLE)->where(static::DOCUMENT_CODE_COL, 'LIKE', Tool::parseCode($isoCode) . date('y') . '%')->pluck(static::DOCUMENT_CODE_COL);
        $codeArr = $codeArr->map(function (&$code) use ($instance, $config) {
                    return substr($code, $instance->prefixLength + strlen(static::SECOND_SEPARATOR) + strlen(date('y')) + 1, $config->config);
                })->toArray();
        $codeArr = array_map(function ($code) {
            return intval($code);
        }, $codeArr);
        //Get max id
        if (isset($codeArr) && sizeof($codeArr)) {
            $lastDigit = max($codeArr) + 1;
        } else {
            $lastDigit = 1;
        }
        $year = date('y');
        $idCode = str_pad($lastDigit, $config->config, static::EMPTY_CHAR, STR_PAD_LEFT);
        $result = Tool::parseCode($isoCode . $year . $idCode);
        unset($codeArr);
        return $result;
    }

    public static function mergePid($pid, $oldPid) {
        $arrPid = $pid ? explode(',', trim($pid, ',')) : [];
        $arrOldPid = $pid ? explode(',', trim($pid, ',')) : [];
        $arrPid = array_merge($arrPid, $arrOldPid);
        $arrPid = array_unique($arrPid);
        return sizeof($arrPid) ? (',' . join(',', $arrPid) . ',') : null;
    }

    public static function insertOne($document, $resourcePath) {
        $status = true;
        $statusCreate = true;
        $statusSame = false;
        $itemSame = [];
        DB::beginTransaction();
        try {
            $checkDocument = \App\Models\Document::where('code', $document['code'])->first();
            if (empty($checkDocument)) {
                // if document doest not exist
                $idDoc = DB::table(static::DOCUMENT_TABLE)->insertGetId([
                    'code' => $document['code'],
                    'name' => $document['name'],
                    'allow_print' => 1,
                    'doc_type_id' => $document['doc_type_id'],
                    'stage_id' => $document['stage_id'],
                    'department_id' => $document['department_id'],
                    'created_at' => now(),
                    'updated_at' => now(),
                    'created_by' => Auth::check() ? Auth::id() : \App\Models\User::where('status', Tool::getConst('ACTIVE_ON'))->first(),
                    'allow_print' => $document['allow_print'],
                    'is_manually' => false,
                    'pid' => $document['pid'],
                    'status' => $document['status'],
                    'frequency_report' => Tool::getConst('ACTIVE_OFF'),
                    'relate_status' => Tool::getConst('ACTIVE_OFF'),
                    'link' => $document['link'],
                ]);
                if (!empty($idDoc)) {
                    //Lưu version
                    $idVersion = DB::table('versions')->insertGetId([
                        'doc_id' => $idDoc,
                        'version_no' => '00',
                        'created_by' => Auth::check() ? Auth::id() : \App\Models\User::where('status', Tool::getConst('ACTIVE_ON'))->first(),
                        'created_at' => now(),
                        'status' => $document['status'],
                        'notes' => null,
                    ]);
                    if (!empty($idVersion)) {
                        DB::table(static::DOCUMENT_TABLE)->where('id', $idDoc)->update(['version_publish' => $idVersion]);
                        $fileData = $document['file'];
                        $fileData['doc_id'] = $idDoc;
                        $fileData['revision'] = $idVersion;
                        $fileData['created_at'] = now();
                        $idFile = DB::table(static::FILE_TABLE)->insertGetId($fileData);
                        $responseMoveFile = static::moveFile($fileData, $resourcePath);
//                        if (empty($idFile) || $responseMoveFile['status']) {
//                            $status = false;
//                        }
                    } else {
                        $status = false;
                    }
                } else {
                    $status = false;
                }
            } else {
                $statusCreate = false;
                $pidOld = $checkDocument->pid;
//                $newPid = CodeParsing::mergePid($document['pid'], $pidOld);
                if ($document['name'] != $checkDocument->name) {
                    $fileOld = \App\Models\File::select('file_name')->where('doc_id', $checkDocument->id)->first();
                    $statusSame = true;
                    $itemSame = [
                        'key' => $document['code'],
                        'data' => [$document['file']['file_name'], $fileOld->file_name]
                    ];
                }
                //Nếu đã có document
                DB::table('documents')->where('code', $document['code'])->update([
                    'name' => $document['name'],
                    'doc_type_id' => $document['doc_type_id'],
                    'stage_id' => $document['stage_id'],
                    'department_id' => $document['department_id'],
                    'allow_print' => $document['allow_print'],
                    'is_manually' => false,
                    'pid' => null,
                    'status' => $document['status'],
                ]);
                //Lưu version
                $fileData = $document['file'];
                $fileData['doc_id'] = $checkDocument->id;
                DB::table(static::FILE_TABLE)->where('doc_id', $checkDocument->id)->update([
                    'file_name' => $fileData['file_name'],
                    'link' => $fileData['link'],
                ]);
                $responseMoveFile = static::moveFile($fileData, $resourcePath);
//                if (!$responseMoveFile['status']) {
//                    $status = false;
//                }
            }
        } catch (Exception $ex) {
            $status = false;
        }
        if ($status) {
            DB::commit();
        } else {
            DB::rollBack();
        }
        return ['status' => $status, 'statusCreate' => $statusCreate, 'statusSame' => $statusSame, 'itemSame' => $itemSame];
    }

    public function insert($resourcePath = null) {
        $status = true;
        $statusFile = true;
        $dataErrorFile = [];
        $dataErrorInsert = [];
        if (static::$instance) {
            $instance = static::getInstance();
            if ($resourcePath && is_dir($resourcePath)) {
                $instance->resourcePath = $resourcePath;
            }
            if (!empty($instance->result)) {
                foreach ($instance->result as $k => $document) {
                    $statusItem = true;
                    DB::beginTransaction();
                    try {
                        $checkDocument = DB::table(static::DOCUMENT_TABLE)->where('code', $document['code']);
                        if ($checkDocument->count() == 0) {
                            // if document doest not exist
                            $idDoc = DB::table(static::DOCUMENT_TABLE)->insertGetId([
                                'code' => $document['code'],
                                'name' => $document['name'],
                                'allow_print' => 1,
                                'doc_type_id' => $document['doc_type_id'],
                                'stage_id' => $document['stage_id'],
                                'department_id' => $document['department_id'],
                                'created_at' => now(),
                                'updated_at' => now(),
                                'created_by' => Auth::check() ? Auth::id() : \App\Models\User::where('status', Tool::getConst('ACTIVE_ON'))->first(),
                                'allow_print' => $document['allow_print'],
                                'is_manually' => false,
                                'pid' => $document['pid'],
                                'status' => $document['status'],
                                'frequency_report' => Tool::getConst('ACTIVE_OFF'),
                                'relate_status' => Tool::getConst('ACTIVE_OFF'),
                                'link' => $document['link'],
                            ]);

                            if (!empty($idDoc)) {
                                //Lưu version
                                $idVersion = DB::table('versions')->insertGetId([
                                    'doc_id' => $idDoc,
                                    'version_no' => '00',
                                    'created_by' => Auth::check() ? Auth::id() : \App\Models\User::where('status', Tool::getConst('ACTIVE_ON'))->first(),
                                    'created_at' => now(),
                                    'status' => $document['status'],
                                    'notes' => null,
                                ]);
                                if (!empty($idVersion)) {
                                    DB::table(static::DOCUMENT_TABLE)->where('id', $idDoc)->update(['version_publish' => $idVersion]);
                                    $fileData = $document['file'];
                                    $fileData['doc_id'] = $idDoc;
                                    $fileData['revision'] = $idVersion;
                                    $idFile = DB::table(static::FILE_TABLE)->insertGetId($fileData);
                                    $responseMoveFile = static::moveFile($fileData, $resourcePath);
                                    if (empty($idFile) || $responseMoveFile['status']) {
                                        $statusItem = false;
                                        if (!$responseMoveFile['statusFile']) {
                                            $dataErrorFile[] = [
                                                'document' => $document['name'],
                                                'pid' => $document['pid']
                                            ];
                                        }
                                    }
                                } else {
                                    $statusItem = false;
                                }
                            } else {
                                $statusItem = false;
                            }
                        } else {
                            //Nếu đã có document
                            $checkDocument->update([
                                'name' => $document['name'],
                                'doc_type_id' => $document['doc_type_id'],
                                'stage_id' => $document['stage_id'],
                                'department_id' => $document['department_id'],
                                'allow_print' => $document['allow_print'],
                                'is_manually' => false,
                                'pid' => $document['pid'],
                                'status' => $document['status'],
                            ]);
                            $doc = $checkDocument->first();
                            if (!empty($doc)) {
                                //Lưu version
                                $fileData = $document['file'];
                                $fileData['doc_id'] = $doc->id;
                                DB::table(static::FILE_TABLE)->where('doc_id', $doc->id)->update([
                                    'file_name' => $fileData['file_name'],
                                    'link' => $fileData['link'],
                                ]);
                                $responseMoveFile = static::moveFile($fileData, $resourcePath);
                                if (!$responseMoveFile['status']) {
                                    $status = false;
                                    $statusFile = $responseMoveFile['statusFile'];
                                }
                            } else {
                                $status = false;
                            }
                        }
                    } catch (Exception $ex) {
                        $status = false;
                    }
                    if ($status) {
                        DB::commit();
                    } else {
                        DB::rollBack();
                    }
                }
            } else {
                $status = false;
            }
        } else {
            $status = false;
        }
        return ['status' => $status, 'statusFile' => $statusFile];
    }

    public static function scanDirectory($dir, &$results = array()) {
        $directories = glob($dir . '/*', GLOB_ONLYDIR);
        foreach ($directories as $key => $value) {
            $results[] = $value;
            static::scanDirectory($value, $results);
        }
        return $results;
    }

    public static function getDirContents($dir, &$results = array()) {
        $files = array_diff(scandir($dir), ['.', '..']);
        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                $results[$path] = pathinfo($path, PATHINFO_FILENAME) . '.' . pathinfo($path, PATHINFO_EXTENSION);
            } else {
                static::getDirContents($path, $results);
            }
        }
        return $results;
    }

    public static function moveFile($data, $resourcePath = null) {
        $status = false;
        $statusFile = true;
        try {
            if ($data['link'] && $data['file_name']) {
                $instance = static::getInstance();
                if (empty($instance->filePaths)) {
                    $instance->filePaths = static::getDirContents($resourcePath);
                }
                if (in_array($data['file_name'], $instance->filePaths)) {
                    $path = collect($instance->filePaths)->filter(function ($item) use ($data) {
                                return $item == $data['file_name'];
                            })->keys()->first();
                    // check file
                    $code = $data['file_name'];
                    $fileNameNow = $data['file_name'];
                    $statusCheck = false;
                    $rule1 = "/(.*)(\.[0-9A-Za-z]{1,})$/";
                    $rule2 = "/(.*)(\.[0-9A-Za-z]{0,})$/";
                    $arrCode = [];
                    $checkStep = 1;
                    preg_match($rule1, $code, $arrCode);
                    if ($arrCode && isset($arrCode[1])) {
                        $pathFileCheck = $path;
                    } else {
                        $arrCode = [];
                        preg_match($rule2, $code, $arrCode);
                        if ($arrCode && isset($arrCode[1])) {
                            $pathFileCheck = $path . 'xls';
                            $fileNameNow = $fileNameNow . 'xls';
                            $checkStep = 2;
                        } else {
                            $pathFileCheck = $path . '.xls';
                            $fileNameNow = $fileNameNow . '.xls';
                            $checkStep = 3;
                        }
                    }
                    if (file_exists($pathFileCheck)) {
                        $statusCheck = true;
                    }
                    if (!$statusCheck && $checkStep != 1) {
                        $pathFileCheck = ($checkStep == 2) ? $path . 'xlsx' : $path . '.xlsx';
                        $fileNameNow = ($checkStep == 2) ? $fileNameNow . 'xlsx' : $fileNameNow . '.xlsx';
                        if (file_exists($pathFileCheck)) {
                            $statusCheck = true;
                        }
                    }

                    if ($statusCheck) {
                        $directory = public_path(rtrim(Tool::getConst('USOL_UPLOAD_COMMON'), '/'));
                        $destination = $directory . '/' . $fileNameNow;
                        if (!file_exists($directory)) {
                            mkdir($directory, 0777, true);
                        }
                        File::copy($path, $destination);
                        $status = true;
                    } else {
                        $statusFile = false;
                    }
                }
            }
        } catch (Exception $ex) {
            $status = false;
        }

        return ['status' => $status, 'statusFile' => $statusFile];
    }

}
