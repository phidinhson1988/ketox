<?php

namespace App\Helper;

use Excel;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ImportFile {

    /**
     * @description: Import file Excel and read file
     * @input: $fileExcel
     * @output: array data
     * @author: SonPD1
     * @created_at: 01/04/2018
     */
    public static function importExcelFile($fileExcel) {
        $result = ['data' => [], 'heading' => []];
        try {
            // Get data excel
            $dataImport = Excel::load($fileExcel)->get();
            $classArr = explode('\\', get_class($dataImport));
            if (end($classArr) === 'SheetCollection') {
                $data = $dataImport->all()[0];
            } else {
                $data = $dataImport;
            }
            $result['heading'] = $data->getHeading();
            if (!empty($data) && count($data) > 0) {
                foreach ($data->all() as $value) {
                    $statusRow = false;
                    foreach ($value as $valueItem) {
                        if ($valueItem) {
                            $statusRow = true;
                        }
                    }
                    $itemData = [];
                    if ($statusRow) {
                        $arrValue = $value->all();
                        foreach ($result['heading'] as $valueHeader) {
                            if ($valueHeader) {
                                $itemData[$valueHeader] = $arrValue[$valueHeader];
                            }
                        }
                    }
                    if ($itemData) {
                        $arrData[] = $itemData;
                    }
                }
            }
            if (!empty($arrData)) {
                $result['data'] = $arrData;
            }
            $result['status'] = true;
            $result['errors'] = '';
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['errors'] = 'Có lỗi khi import file.';
        }
        return $result;
    }

    /**
     * @description: Export file Excel
     * @input: $data, $heading, $fileName
     * @output: $result ( status và error )
     * @author: SonPD1
     * @created_at: 03/05/2018
     */
    public static function exportExcelFile($data, $heading, $fileName) {
        $status = true;
        $error = '';
        $ext = 'xlsx';
        try {
            if ($data || $heading) {
                $fileName = $fileName . '-' . Carbon::now()->timestamp;
                Excel::create($fileName, function($excel) use ($data, $heading) {
                    $excel->sheet('mySheet', function($sheet) use ($data, $heading) {
                        // build heading
                        foreach (range('A', 'Z') as $index => $char) {
                            if ($index <= count($heading) && isset($heading[$index])) {
                                $charHeading = $char . '1';
                                $sheet->cell($charHeading, function($cell) use ($heading, $index) {
                                    $cell->setValue($heading[$index]);
                                });
                            }
                        }
                        // build content
                        if (!empty($data)) {
                            foreach ($data as $indexRow => $valueRow) {
                                $stt = $indexRow + 2;
                                foreach (range('A', 'Z') as $index => $char) {
                                    if ($index <= count($valueRow) && isset($valueRow[$index])) {
                                        $charRow = $char . $stt;
                                        $sheet->cell($charRow, $valueRow[$index]);
                                    }
                                }
                            }
                        }
                    });
                })->export($ext);
            } else {
                $error = 'Chưa có dữ liệu export';
                $status = false;
            }
        } catch (Exception $ex) {
            $error = 'Có lỗi xảy ra. Thử lại sau';
            $status = false;
        }
        return [
            'status' => $status,
            'error' => $error,
        ];
    }

    /**
     * @description: Get data to save to db
     * @input: $data, $fillable (data fields are in db)
     * @output: array data
     * @author: SonPD1
     * @created_at: 01/04/2018
     */
    public static function mergeDataImport($data, $fillable) {
        $result = [];
        foreach ($fillable as $item) {
            if (isset($data[$item])) {
                $result[$item] = $data[$item];
            }
        }
        return $result;
    }

    /**
     * @description: Process File Import
     * @input: $data, $arrTypeInt (data fields are in db)
     * @output: array data
     * @author: SonPD1
     * @created_at: 01/04/2018
     */
    public static function processFileImport($data, $arrTypeInt = []) {
        $dataResult = [];
        $heading = [];
        $status = true;
        $error = '';
        if ($data) {
            foreach ($data[0] as $key => $value) {
                $value = trim(strip_tags($value));
                if ($value != '' && $value != null) {
                    $heading[$key] = str_slug($value, '_');
                }
            }
            if ($heading) {
                foreach ($data as $index => $itemData) {
                    $itemResult = [];
                    if ($index != 0) {
                        $statusRow = false;
                        foreach ($heading as $keyHeading => $valueItem) {
                            if ($valueItem) {
                                if ($arrTypeInt && in_array($valueItem, $arrTypeInt)) {
                                    $itemResult[$valueItem] = isset($itemData[$keyHeading]) ? (float) $itemData[$keyHeading] : null;
                                } else {
                                    $itemResult[$valueItem] = isset($itemData[$keyHeading]) ? trim(strip_tags($itemData[$keyHeading])) : null;
                                }
                                if (isset($itemData[$keyHeading]) && strlen(trim($itemData[$keyHeading]))) {
                                    $statusRow = true;
                                }
                            }
                        }
                        if ($itemResult && $statusRow) {
                            $dataResult[] = $itemResult;
                        }
                    }
                }
            } else {
                $status = false;
                $error = trans('message.khong_doc_duoc_du_lieu_import');
            }
        } else {
            $status = false;
            $error = trans('message.khong_doc_duoc_du_lieu_import');
        }
        return [
            "status" => $status,
            "data" => $dataResult,
            "heading" => $heading,
            "error" => $error,
        ];
    }

    /**
     * @description: Get response from validattor
     * @input: $validator
     * @output: object response
     * @author: SonPD1
     * @created_at: 14/08/2018
     */
    public static function createResponse($validator) {
        return (object) array(
                    'status' => $validator->fails() ? false : true,
                    'errors' => $validator->errors(),
                    'instance' => $validator,
        );
    }

    /**
     * @description: Save data to Database
     * @input: $data, $model
     * @output: boolean status, array errors
     * @author: SonPD1
     * @created_at: 14/08/2018
     */
    public static function saveToDatabase($data, $model) {
        try {
            $dataSave = [];
            if ($data) {
                foreach ($data as $key => $itemData) {
                    $dataSave[$key] = trim(strip_tags($itemData));
                }
            }
            $validator = Validator::make($dataSave, $model->rules, $model->message);
            $validator = ImportFile::createResponse($validator);
            if ($validator->status) {
                $item = $model->getItem($dataSave);
                if ($item) {
                    $item->update($dataSave);
                } else {
                    $item = $model;
                    $item->insert($dataSave);
                }
                return ['status' => true, 'errors' => []];
            } else {
                return ['status' => false, 'errors' => $validator->errors->toArray()];
            }
        } catch (Exception $ex) {
            return ['status' => false, 'errors' => ['Có lỗi xẩy ra!']];
        }
    }

    /**
     * @description: Merge Error vào danh sách dữ liệu import lên
     * @input: $data, $errors
     * @output: array
     * @author: SonPD1
     * @created_at: 14/08/2018
     */
    public static function mergeErrorToData($data, $errors) {
        $dataHeader = [];
        $dataContent = [];
        if ($data) {
            $index = 0;
            foreach ($data as $item) {
                $itemContent = [];
                foreach ($item as $key => $value) {
                    $itemHeader = ['data' => $key, 'status' => true];
                    $content = ['data' => $value, 'status' => true];
                    foreach ($errors as $error) {
                        // update error to header
                        if ($index == 0) {
                            if (isset($error['index']) && $error['index'] === ($index + 1) && $error['key'] == $key) {
                                $itemHeader = ['data' => $key, 'status' => false];
                            }
                        }
                        // update error to content
                        if (isset($error['index']) && $error['index'] === ($index + 2) && $error['key'] == $key) {
                            $content = ['data' => $value, 'status' => false];
                        }
                    }
                    if ($index == 0) {
                        $dataHeader[] = $itemHeader;
                    }
                    $itemContent[] = $content;
                }
                $dataContent[] = $itemContent;
                $index++;
            }
        }
        return [
            'dataHeader' => $dataHeader,
            'dataContent' => $dataContent,
        ];
    }

    /**
     * @description: Sắp xếp danh sách error theo hàng
     * @input: $errors
     * @output: array
     * @author: SonPD1
     * @created_at: 14/08/2018
     */
    public static function sortErrorMessage($errors) {
        $result = [];
        if ($errors) {
            foreach ($errors as $error) {
                $index = isset($error['index']) ? $error['index'] : 0;
                $result[$index][] = $error;
            }
        }
        return $result;
    }

    /**
     * @description: Sử lý file Excel usol_data
     * @input: $fileExcel
     * @output: array data
     * @author: SonPD1
     * @created_at: 03/10/2018
     */
    public static function processDataUsol($fileExcel = null) {
        $resultData = [];
        $status = true;
        try {
            if ($fileExcel) {
                $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($fileExcel);
                $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
                $reader->setReadDataOnly(true);
                $reader->setLoadSheetsOnly(null);
                $spreadsheet = $reader->load($fileExcel);
                $worksheet = $spreadsheet->getActiveSheet()->toArray();
                $response = \App\Helper\ImportFile::processFileImport($worksheet);
                if ($response['status'] && $response['data']) {
                    foreach ($response['data'] as $itemResponse) {
                        $itemResult = [];
                        $itemResult['files'] = [];
                        if ($itemResponse) {
                            foreach ($itemResponse as $key => $itemData) {
                                switch ($key) {
                                    case 'id':
                                        $itemResult['id'] = trim(strip_tags($itemData));
                                        break;
                                    case 'createuserid':
                                        $itemResult['createuserid'] = trim(strip_tags($itemData));
                                        break;
                                    case 'createdate':
                                        $itemResult['createdate'] = $itemData;
                                        break;
                                    case 'updateuserid':
                                        $itemResult['updateuserid'] = trim(strip_tags($itemData));
                                        break;
                                    case 'updatedate':
                                        $unixDate = ($itemData - 25569) * 86400;
                                        $itemResult['updatedate'] = ($unixDate > 0) ? gmdate("Y-m-d H:i:s", (int) $unixDate) : null;
                                        break;
                                    default :
                                        break;
                                }
                                if (in_array($key, ['file1', 'file2', 'file3', 'file4', 'file5', 'file6', 'file7', 'file8', 'file9', 'file10', 'file11', 'file12'])) {
                                    $fileName = trim(strip_tags($itemData));
                                    $itemResult['files'][] = $fileName;
                                }
                            }
                        }
                        $resultData[] = $itemResult;
                    }
                }
            }
        } catch (Exception $ex) {
            $status = false;
        }
        return [
            'status' => $status,
            'data' => $resultData,
        ];
    }

}

?>
