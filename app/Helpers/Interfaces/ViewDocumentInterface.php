<?php

/**
 * @package    DMS
 * @category    ConstCatchable
 * @copyright  Copyright 2018 FPT
 * @author     TrungLH3 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Helpers\Interfaces;
interface ViewDocumentInterface {
    public static function word2pdf($dir);
    public static function txt2pdf($dir);
    public static function excel2pdf($dir, $sheetIndex);
    public function convert();
}
