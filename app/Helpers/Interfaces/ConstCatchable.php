<?php

/**
 * @package    DMS
 * @category    ConstCatchable
 * @copyright  Copyright 2018 FPT
 * @author     TrungLH3 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Helpers\Interfaces;

interface ConstCatchable {
    //Get all consts
    public static function getAllConsts();
    //Get consts define by array
    public static function getConstByArray(array $keys);
    //Get one consts
    public static function getSpecifiedConst($name);
}
