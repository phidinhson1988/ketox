<?php

/**
 * @package    DMS
 * @category    BackupClean
 * @copyright  Copyright 2018 FPT
 * @author     TrungLH3 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Helpers\Backup;

use Spatie\Backup\Tasks\Cleanup\CleanupStrategy;
use Spatie\Backup\BackupDestination\BackupCollection;
use App\Helpers\Facades\Tool;

class BackupClean extends CleanupStrategy
{
    public function deleteOldBackups(BackupCollection $backups)
    {
        $path = realpath(config('filesystems.disks.local.root') . '/http---localhost');
        $keeping = Tool::getConfig('KEEPING_FILES');
        $total = $this->getTotalFiles($path);

        //Check number of keeping files
        while($total > $keeping) {
            $backup = $backups->oldest();
            $backup->delete();
            $total = $this->getTotalFiles($path);
        }

        //Remove previous backup
        $this->removePreviousBackup();

        //Move all backups to parent directory
        $this->moveToParentDirectory();
    }

    public function getTotalFiles($path)
    {
        $files = array_diff(scandir($path), array('.', '..'));
        return count($files);
    }

    public function moveToParentDirectory()
    {
        $path = realpath(config('filesystems.disks.local.root')) . BACKUP_ALL_FOLDER;
        if (file_exists($path)) {
            move_parent($path);
        }
    }

    public function removeTempDirectory()
    {
        if (file_exists(storage_path('laravel-backups'))) {
            if (file_exists(storage_path('laravel-backups/temp'))) {
                rmdir(storage_path('laravel-backups/temp'));
            } else {
                rmdir(storage_path('laravel-backups'));
            }
        }
    }

    public function removePreviousBackup()
    {
        $path = realpath(config('filesystems.disks.local.root'));
        $files = array_diff(scandir($path), array('.', '..'));
        $list = array();

        foreach($files as $file) {
            $patterns = explode('.', $file);
            $extension = end($patterns);
            if (strtolower($extension) == 'zip') {
                $list[] = $path . '/' . $file;
            }
        }

        $newArray = array();

        foreach($list as $file) {
            $newArray[$file] = filectime($file);
        }

        asort($newArray);
        $keeping = Tool::getConfig('KEEPING_FILES');
        $total = count($files);

        if ($keeping < $total) {
            $destroy = $total - $keeping;
            $i = 0;
            foreach($newArray as $file => $time) {
                if (++$i < $destroy) {
                    unlink($file);
                }
            }
        }
    }
}
