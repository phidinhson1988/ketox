<?php

/**
 * @package    DMS
 * @category    ViewDocs
 * @copyright  Copyright 2018 FPT
 * @author     TrungLH3 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Helpers;
use App\Helpers\Facades\Tool;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\IOFactory as WordIOFactory;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use App\Helpers\Interfaces\ViewDocumentInterface;

class ViewDocs implements ViewDocumentInterface {
	protected static $dir = null;
	protected static $instance = null;
	private $allowedExtension = array();

    public static function word2pdf($dirFileName){
    	Settings::loadConfig();
		Settings::setOutputEscapingEnabled(true);
		Settings::setPdfRenderer(Settings::PDF_RENDERER_DOMPDF, base_path() . '/vendor/barryvdh/laravel-dompdf');
		$ext = pathinfo($dirFileName, PATHINFO_EXTENSION);
		$filename = pathinfo($dirFileName, PATHINFO_FILENAME);
		if($ext == 'doc'){
			$writer = WordIOFactory::load($dirFileName,'MsDoc');
		}else{
			$writer = WordIOFactory::load($dirFileName);
		}
		// $phpWord = new \PhpOffice\PhpWord\PhpWord;
		// $writer = WordIOFactory::createWriter($phpWord, 'HTML');
		// $writer = WordIOFactory::load($dirFileName);
	    // Write documents
	    // $targetFile = public_path() . DIRECTORY_SEPARATOR . Tool::getConst('DOCUMENT_UPLOAD') . DIRECTORY_SEPARATOR . "Docs_Convert" . DIRECTORY_SEPARATOR . "{$filename}.pdf";
	    $targetFile = public_path() . DIRECTORY_SEPARATOR . Tool::getConst('DOCUMENT_UPLOAD') . DIRECTORY_SEPARATOR . "Docs_Convert" . DIRECTORY_SEPARATOR . "{$filename}.html";
        if($writer->save($targetFile, 'HTML') == 1){
        	return $targetFile;
        } else {
        	return 'error';
        }
	}

	public static function excel2pdf($dirFileName, $sheetIndex = 0){
		$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($dirFileName);
		$filename = pathinfo($dirFileName, PATHINFO_FILENAME);

		// $targetFile = public_path() . DIRECTORY_SEPARATOR . Tool::getConst('DOCUMENT_UPLOAD') . "Docs_Convert" . DIRECTORY_SEPARATOR . "{$filename}.pdf";
		$targetFile = public_path() . DIRECTORY_SEPARATOR . Tool::getConst('DOCUMENT_UPLOAD') . "Docs_Convert" . DIRECTORY_SEPARATOR . "{$filename}.html";
		$writer = new \PhpOffice\PhpSpreadsheet\Writer\Html($spreadsheet);
		mb_internal_encoding('UTF-8');
		// $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Dompdf');
		$writer->setSheetIndex($sheetIndex);
		$writer->setPreCalculateFormulas(false);
		$writer->save($targetFile);
		return asset(Tool::getLinkFromPath($targetFile));
	}

	public static function txt2pdf($dirFileName){
		$ext = pathinfo($dirFileName, PATHINFO_EXTENSION);
		$filename = pathinfo($dirFileName, PATHINFO_FILENAME);
		$templateHtml = <<<EOD
		<!DOCTYPE html>
		<html>
		<head>
			<meta charset="utf-8" />
		    <title>{HEAD_TITLE}</title>
		</head>
		<body>
			{BODY_CONTENT}
		</body>
		</html>
EOD;
		$fp = fopen( $dirFileName, 'r');
		$data_return = '';
		while( $line = fgets( $fp)){
		    $data_return .= htmlspecialchars( $line) . '<br />';
		}
		fclose($fp);
		$templateHtml = str_replace('{HEAD_TITLE}', $filename, $templateHtml);
		$templateHtml = str_replace('{BODY_CONTENT}', $data_return, $templateHtml);
		$targetFile = public_path() . DIRECTORY_SEPARATOR . Tool::getConst('DOCUMENT_UPLOAD') . "Docs_Convert" . DIRECTORY_SEPARATOR . "{$filename}.html";
		if (file_put_contents($targetFile, $templateHtml)) {
			return $targetFile;
		} else {
			return 'error';
		}
	}

	public static function getFile($path) {
		if (file_exists($path)) {
			static::$dir = $path;
		}
		if (is_null(static::$instance)) {
			static::$instance = new self;
		}
		return static::$instance;
	}

	public function convert() {
		$instance = static::$instance;
		$this->allowedExtension = '';
		if (is_null($instance)) {
			throw new \Exception('Cannot create '.__CLASS__.' from empty object!');
		}
		if (static::$dir) {
			$extension = pathinfo(static::$dir, PATHINFO_EXTENSION);
			if (in_array($extension, $this->allowedExtension)) {
				switch($extension) {
					case 'doc':
					case 'docx':
						$result = static::word2pdf(static::$dir);
						break;
					case 'xls':
					case 'xlsx':
						$result = static::excel2pdf(static::$dir);
						break;
					case 'txt':
						$result = static::txt2pdf(static::$dir);
						break;
					default:
						break;
				}
				return $result;
			}
			throw new \InvalidArgumentException('You have entered invalid file name extension!');
		}
	}
}
