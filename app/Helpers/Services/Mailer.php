<?php

/**
 * @package    DMS
 * @category    Mailer
 * @copyright  Copyright 2018 FPT
 * @author     TrungLH3 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Helpers\Services;
use App\Helpers\Facades\Tool;
use Illuminate\Support\Facades\Mail;
use App\Mail\DocumentRejectEmail as RejectEmail;
use App\Mail\DocumentApprovalEmail as ApproveEmail;
use App\Mail\DocumentCreatedEmail as CreateEmail;

class Mailer
{
	/**
     * List receiver email addresses
     *
     * @var array
     */
	protected $receivers = array();

	/**
     * Title of mail
     *
     * @var string
     */
	protected $title = null;

	/**
     * Body content of mail
     *
     * @var array
     */
	protected $body = array();

	/**
     * Format of mail
     *
     * @var array
     */
	protected $format = array();

	/**
     * Mail class reference to call
     *
     * @var object
     */
	protected $class = null;

	/**
     * Self instance of class
     *
     * @var object
     */
	protected static $instance = null;

	/**
     * @package     DMS
     * @category    Get singleton instance for this class
     * @author      TrungLH3
     * @param       void
     * @return      object
     */
	public static function getInstance() {
		if (! isset(static::$instance)) {
			static::$instance = new self;
		}
		return static::$instance;
	}

	/**
     * @package     DMS
     * @category    Init information to send mail
     * @author      TrungLH3
     * @param       array 		$receivers 			Receiver mail addresses
     * @param 		array 		$content 			Mail content
     * @param 		int 		$mailType			Type of mail
     * @return      this
     */
	public static function init(array $receivers, array $content = null, $mailType = null) {
		if (isset($content['title']) && isset($content['body'])) {
			$instance = static::getInstance();
			if (sizeof($receivers)) {
				$receiverKeys = array_keys($receivers);
				foreach($receiverKeys as $key) {
					if (! in_array($key, ['to', 'cc'], true)) {
						throw new \Exception('The email sender is not valid format!');
					}
				}
			}
			$instance->receivers = $receivers;
			$instance->title = $content['title'];
			$instance->body = $content['body'];
			$mail = static::getFormatMail($mailType);
			$instance->format = $mail->format;
			$instance->class = $mail->class;
			$statusFormat = static::validateEmailFormat($content);
			if ($statusFormat) {
				return $instance;
			}
		}
		return false;
	}

	/**
     * @package     DMS
     * @category    Send mail to user email
     * @author      TrungLH3
     * @param       void
     * @return      boolean
     */
	public function send() {
		$status = false;
		$instance = static::getInstance();
		if ($instance) {
			$cc = $instance->receivers['cc'] ?? [];
			$status = Mail::to($instance->receivers['to'])
						  ->cc($cc)
						  ->send($instance->class);
		}
		return $status;
	}

	/**
     * @package     DMS
     * @category    Validate email content format
     * @author      TrungLH3
     * @param       array 		$content 		Mail content
     * @return      boolean
     */
	public static function validateEmailFormat($content) {
		$statusFormat = true;
		$instance = static::getInstance();
		$errors = array();
		if (is_array($instance->body) && count($instance->body)) {
			array_walk($instance->body, function ($item, $index) use (&$statusFormat, $instance, &$errors) {
				if (! in_array($index, $instance->format)) {
					$statusFormat = false;
					$errors[] = $index;
				}
			});
		}
		if (! $statusFormat) {
			throw new \Exception('The content of email is not valid format. Field: `'. implode(', ', $errors) .'` is not neccessary!');
		} else {
                    if (sizeof($instance->format)) {
			foreach($instance->format as $field) {
				if (! in_array($field, array_keys($content['body']))) {
					$errors[] = $field;
				}
			}
			if (count($errors)) {
				$statusFormat = false;
				throw new \Exception('The content of email is missing required field: `'. implode(', ', $errors) .'` Please try again!');
			}
                    }
                }
		return $statusFormat;
	}

	/**
     * @package     DMS
     * @category    Get mail format base on mail type
     * @author      TrungLH3
     * @param       int 		$mailType		Mail type
     * @return      object
     */
	public static function getFormatMail($mailType) {
		$instance = static::getInstance();
		$format = array();
		$class = null;
		switch ($mailType) {
			case Tool::getConst('APPROVE_MAIL'):
				$format = [
					'document',
					'creator',
					'link',
					'attachments',
				];
				$class = new ApproveEmail($instance->title, $instance->body);
				break;
			case Tool::getConst('CREATED_MAIL'):
				$format = [
					'document',
					'creator',
					'link',
				];
				$class = new CreateEmail($instance->title, $instance->body);
				break;	
			case Tool::getConst('REJECT_MAIL'):
                $format = [
                    'document',
                    'creator',
                    'link',
                ];
                $class = new RejectEmail($instance->title, $instance->body);
				break;
			default:
				$format = [
					'document',
					'creator',
					'link',
					'attachments',
				];
				$class = new ApproveEmail($instance->title, $instance->body);
				break;
		}
		return (object) compact('format', 'class');
	}
}
