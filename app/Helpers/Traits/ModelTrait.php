<?php

namespace App\Helpers\Traits;
use App\Helpers\Facades\Tool;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Artisan;

trait ModelTrait {
    public static function checkEmpty($q) {
        return strlen(trim($q)) == 0;
    }
    public static function getInstance() {
        if (! isset(static::$instance)) {
            static::$instance = new self;
        }
        return static::$instance;
    }
    public static function getFilterResult($data = null, array $filter) {
        $table = static::getInstance()->getTable();
        if (count($filter) > 0) {
            foreach($filter as $col => $val) {
                if ($val) {
                    if ($col == 'stage') {
                        if (collect($filter['stage'])->contains('all')) {
                            continue;
                        } else {
                            if ($val) {
                                $data->where(function($queryStage) use ($val){
                                    foreach ($val as $indexStage => $stageItem) {
                                        if ($indexStage == 0) {
                                            $queryStage->where('documents.stage_ids', 'LIKE', "%," . $stageItem . ",%");
                                        } else {
                                            $queryStage->orWhere('documents.stage_ids', 'LIKE', "%," . $stageItem . ",%");
                                        }
                                    }
                                });
                            }
                        }

                    } else {
                        $data = is_array($val) ? $data->whereIn($table.'.'.$col.'_id', $val)
                                               : $data->where($table.'.'.$col.'_id', $val);
                    }
                }
            }
        }
        return $data;
    }
    public static function mapTableColumns($table, array $cols) {
        return array_map(function ($col) use ($table) {
            return $col = $table.".$col";
        }, $cols);
    }
    public static function getApproversByLevel($users) {
        $result = collect();
        if ($users->count() > 0) {
            $jobCloseIndex = 1;
            $users->each(function ($u) use ($result, &$jobCloseIndex) {
                if ($u->pivot->type == Tool::getConst('TYPE_TRAINING')) {
                    $role = 'Training';
                } elseif ($u->pivot->type == Tool::getConst('TYPE_JOBCLOSE')) {
                    $role = 'Job close '.$u->pivot->level;
                    $jobCloseIndex++;
                } elseif ($u->pivot->type == Tool::getConst('TYPE_APPROVE')) {
                    $role = 'Level '.$u->pivot->level;
                } else {
                    $role = null;
                }
                $result->push((object)[
                    'id' => $u->id,
                    'role' => $role,
                    'username' => '<a target="_blank" href="'.route('user.index', ['q' => urlencode($u->username)]).'">'.htmlentities($u->username).'</a>',
                    'status' => $u->pivot->status == Tool::getConst('IS_APPROVED')
                                ? '<label class="label p-xxs b-r-xs label-status label-primary">'.htmlentities(trans('message.da_phe_duyet')).'</label>'
                                : '<label class="label p-xxs b-r-xs label-status label-danger">'.htmlentities(trans('message.chua_phe_duyet')).'</label>',
                    'time' => $u->pivot->end_time ? Carbon::parse($u->pivot->end_time)->format('d/m/Y h:i A') : null,
                ]);
            });
        }
        return $result;
    }
    public static function filterByColumn($select, array $filter, array $condition = null) {
        if (count($filter) > 0) {
            foreach($filter as $col) {
                $select = $select->where($col, $condition[$col]);
            }
        }
        return $select;
    }
    public static function parseJsonColumn($column, $key = null) {
        $result = (object)json_decode($column, true) ?? (object)array();
        return $key ? $result->{$key} : $result;
    }
    public static function generateUuidColumn() {
        return (string) Str::uuid();
    }
    public static function clearCache() {
        set_time_limit(200);
        Artisan::call('route:clear');
        Artisan::call('config:clear');
    }
    public function scopeIsActive($query) {
        $query = $query->where('status', Tool::getConst('ACTIVE_ON'));
    }
}
