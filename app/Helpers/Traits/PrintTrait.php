<?php

namespace App\Helpers\Traits;

//Hook api using GuzzleHttp Client
use App\Models\File;
use App\Helpers\Facades\Tool;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Artisan;

trait PrintTrait {

    public function printDocument($request) {
        $fileRequest = $request->input('files');
        $printFiles = collect(array_filter($fileRequest, function ($file) {
            return ! empty($file['checked']);
        }))->map(function ($item) {
            foreach($item as $attr => $val) {
                $item[$attr] = intval($val);
            }
            return $item;
        })->toArray();
        return $this->processPrint($printFiles);
    }
    public function processPrint(array $files) {
        $status = true;
        if (! empty($files)) {
            foreach($files as $file) {
                $fileInstance = File::find($file['id']);
                $filePath = str_replace('/', DIRECTORY_SEPARATOR, public_path($fileInstance->link.'/'.$fileInstance->file_name));
                if (in_array(strtolower(pathinfo($filePath, PATHINFO_EXTENSION)), $this->getAllowedPrintDocument())) {
                    $fh = fopen($filePath, 'r');
                    $client = new GuzzleClient(['base_uri' => Tool::getConst('API_BASE')]);
                    $res = $client->request('POST', Tool::getConst('API_URL'), [
                        'multipart' => [
                            [
                                'name'     => 'myFile',
                                'contents' => $fh,
                            ], [
                                'name' => 'copies',
                                'contents' => $file['quantity']
                            ]
                        ]
                    ]);
                    // fclose($fh);
                    $status = $res->getStatusCode() == 200;
                }
            }
        }
        return $status;
    }
}
