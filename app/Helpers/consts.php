<?php

/**
 * @package    DMS
 * @category    DMS_Const
 * @copyright  Copyright 2018 FPT
 * @author     TrungLH3 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Helpers;
use App\Helpers\Interfaces\ConstCatchable;

if (! class_exists('DMS_Const')) {
    abstract class DMS_Const implements ConstCatchable {
        const CLASS_CONST = 'DMS_Const_Register';

        public final static function getConsts($key, $all = false) {
            if ($all === true) {
                return static::getAllConsts();
            }
            if (is_array($key)) {
                return static::getConstByArray($key);
            }
            if (! is_null(constant(self::CLASS_CONST.'::'.strtoupper($key))) && $all === false) {
                return static::getSpecifiedConst($key);
            }
            return 0;
        }
        public static function getAllConsts() {
            $rclass = new \ReflectionClass(self::CLASS_CONST);
            return $rclass->getConstants();
        }
        public static function getConstByArray(array $key) {
            $result = array();
            array_walk($key, function($el) use (&$result) {
                if (! is_null(constant(self::CLASS_CONST.'::'.strtoupper($el)))) {
                    $result = array_merge($result, [
                        $el => constant(self::CLASS_CONST.'::'.strtoupper($el)),
                    ]);
                }
            });
            return $result;
        }
        public static function getSpecifiedConst($name) {
            return constant(self::CLASS_CONST.'::'.strtoupper($name));
        }
    }
}
