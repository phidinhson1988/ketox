<?php

/**
 * @package    DMS
 * @category    ToolFactory
 * @copyright  Copyright 2018 FPT
 * @author     TrungLH3 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Helpers;

use App\Helpers\Facades\Tool;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class ToolFactory
{
    /**
     * Active link
     *
     * @param    $route     string    route parameter
     *
     * @return   string
     */
    public static function activeLink($route)
    {
        if (is_array($route)) {
            $class = array_filter($route, function ($r) {
                return trim($r) == Route::currentRouteName();
            }, ARRAY_FILTER_USE_BOTH) ? 'active' : null;
        } else {
            $class = Route::currentRouteName() == trim($route) ? 'active' : null;
        }
        return $class;
    }

    /**
     * Get image
     *
     * @param    $path     string     Path of image
     * @param    $mode     string     Directory store image
     *
     * @return   string
     */
    public static function getImage($path, $mode = null)
    {
        if (is_numeric(strpos($path, 'http')) || is_numeric(strpos($path, 'https'))) {
            return $path;
        }
        if (!empty($path) && file_exists(public_path(rtrim($mode, '/') . DIRECTORY_SEPARATOR . $path))) {
            return asset(rtrim($mode, '/') . '/' . $path);
        } else {
            return $mode == Tool::getConst('USER_UPLOAD')
                ? asset('upload/images/users/blank.png')
                : asset(Tool::getConst('BLANK_IMAGE'));
        }
    }

    /**
     * Get sortable column
     *
     * @param    $sortCol        string      Name of column to sort
     * @param    $displayCol     string      Display name of column
     *
     * @return   string
     */
    public function getSortableColumn($sortCol = null, $displayCol = null)
    {
        $template = '<a href="{link}">' . ucfirst(htmlentities($displayCol, ENT_COMPAT, 'UTF-8')) . ' <span class="pull-right">{icon}</span></a>';
        $sort = '<i class="fa fa-sort"></i>';
        $sortUp = '<i class="fa fa-sort-up"></i>';
        $sortDown = '<i class="fa fa-sort-down"></i>';
        $sortCol = strtolower($sortCol);
        $result = str_replace(array('{link}', '{icon}'), array(request()->fullUrlWithQuery(['sort' => $sortCol, 'order' => 'asc']), $sort), $template);
        if (request()->has('order') && !strcasecmp(request()->get('order'), 'asc')) {
            $link = request()->fullUrlWithQuery(array('sort' => $sortCol, 'order' => 'desc'));
            $icon = request()->get('sort') == $sortCol ? $sortUp : null;
        }
        if (request()->has('order') && !strcasecmp(request()->get('order'), 'desc')) {
            $link = request()->fullUrlWithQuery(array('sort' => $sortCol, 'order' => 'asc'));
            $icon = request()->get('sort') == $sortCol ? $sortDown : null;
        }
        if (request()->has('sort') && request()->isMethod('get')) {
            $result = str_replace(array('{link}', '{icon}'), array($link, $icon), $template);
        }
        return $result;
    }

    /**
     * Sanitize query
     *
     * @param    $query      string      Search query to satinize
     *
     * @return   string
     */
    public function sanitizeQuery($query)
    {
        $query = urldecode($query);
        if (strlen(trim($query)) >= 2 && (preg_match('#%+#', $query) || preg_match('#_+#', $query))) {
            return '\\' . $query;
        }
        return str_replace(['%', '_', '+', '\\', '(', ')'], ['\%', '\_', '\+', '\\\\', '\(', '\)'], trim($query));
    }

    /**
     * Get const base on key name
     *
     * @param    $key    string    Key name of const
     * @param    $all    boolean   All consts option
     *
     * @return   resources
     */
    public function getConst($key, $all = false)
    {
        return DMS_Const::getConsts($key, $all);
    }

    /**
     * Hight light content when search is completed
     *
     * @param    $content    string      Content to replace
     * @param    $keyword    string      Search keyword
     *
     * @return   string
     */
    public function highlight($content, $keyword)
    {
        $keyword = trim(urldecode($keyword));
        $patterns = explode(' ', $keyword);
        foreach ($patterns as $item) {
            $item = $this->sanitizeQuery($item);
            $content = preg_replace("#($item)#i", '<span style="background-color:' .
                Tool::getConst('SEARCH_HIGHLIGHT_COLOR') .
                ';color:#333;display:inline;">$1</span>', $content);
        }
        return $content;
    }

    /**
     * Highlight class error
     *
     * @param    $name       string
     * @param    $error      error bag
     * @param    $tab        tab has error
     *
     * @return   string
     */
    public function classError($name, $errors, $tab = null)
    {
        $condition = $tab ? ($errors->has($name) && session('tab') == $tab) : $errors->has($name);
        return $condition ? 'has-error' : null;
    }

    /**
     * Get page offset for list collection
     *
     * @param    $collection       collection
     *
     * @return   int
     */
    public function getPageOffset($collection)
    {
        return $collection->perPage() * ($collection->currentPage() - 1);
    }

    /**
     * Get document status
     *
     * @param    $status      string
     *
     * @return   array
     */
    public function getDocumentSetStatus($status)
    {
        $result = array();
        switch ($status) {
            case 0:
                $result = array(
                    'class' => 'bg-create-doc',
                    'text' => trans('message.tao_lap'),
                );
                break;
            case 1:
                $result = array(
                    'class' => 'blue-bg',
                    'text' => trans('message.cho_phe_duyet'),
                );
                break;
            case 2:
                $result = array(
                    'class' => 'yellow-bg',
                    'text' => trans('message.cho_dao_tao'),
                );
                break;
            case 3:
                $result = array(
                    'class' => 'lazur-bg',
                    'text' => trans('message.cho_jobclose'),
                );
                break;
            case 4:
                $result = array(
                    'class' => 'navy-bg',
                    'text' => trans('message.da_phat_hanh'),
                );
                break;
            case 5:
                $result = array(
                    'class' => 'red-bg',
                    'text' => trans('message.da_thu_hoi'),
                );
                break;
            case 6:
                $result = array(
                    'class' => 'info-bg',
                    'text' => trans('message.lien_quan'),
                );
                break;
            case 7:
                $result = array(
                    'class' => 'update-bg',
                    'text' => trans('message.cap_nhat'),
                );
                break;
            case 8:
                $result = array(
                    'class' => 'red-bg',
                    'text' => trans('message.thu_hoi'),
                );
                break;
            case 9:
                $result = array(
                    'class' => 'red-bg',
                    'text' => trans('message.version_cu'),
                );
                break;
            default:
                $result = array(
                    'class' => 'default',
                    'text' => 'N/A',
                );
                break;
        }
        return $result;
    }

    /**
     * Get document status
     *
     * @param    $status      string
     *
     * @return   array
     */
    public function getDocumentSetStatusTree($status)
    {
        $result = array();
        switch ($status) {
            case 0:
                $result = array(
                    'class' => 'bg-create-doc',
                    'text' => trans('message.tao_lap'),
                );
                break;
            case 1:
                $result = array(
                    'class' => 'blue-bg',
                    'text' => trans('message.cho_phe_duyet'),
                );
                break;
            case 2:
                $result = array(
                    'class' => 'yellow-bg',
                    'text' => trans('message.cho_dao_tao'),
                );
                break;
            case 3:
                $result = array(
                    'class' => 'lazur-bg',
                    'text' => trans('message.cho_jobclose'),
                );
                break;
            case 4:
                $result = array(
                    'class' => 'navy-bg',
                    'text' => trans('message.da_phat_hanh'),
                );
                break;
            case 5:
                $result = array(
                    'class' => 'red-bg',
                    'text' => trans('message.da_thu_hoi'),
                );
                break;
            case 6:
                $result = array(
                    'class' => 'info-bg',
                    'text' => trans('message.lien_quan'),
                );
                break;
            case 7:
                $result = array(
                    'class' => 'update-bg',
                    'text' => trans('message.cap_nhat'),
                );
                break;
            default:
                $result = array(
                    'class' => 'default',
                    'text' => 'N/A',
                );
                break;
        }
        return $result;
    }

    /**
     * Recursive render document structure
     *
     * @author   TrungLH3
     *
     * @param    $dir        string          Directory of document type
     * @param    $items      collection      Documents in same link
     *
     * @return   string
     */
    public function recursiveRender($dir, $items = null, $status = false)
    {
        $arr = explode('/', rtrim($dir, '/'));
        $result = '<ol class="dd-list">';
        $result .= $this->recursiveHtml($arr, 0, $items, last($arr), $status);
        $result .= '</ol>';
        return $result;
    }

    /**
     * Recursive make html template for document structure
     *
     * @author   TrungLH3
     *
     * @param    $arr        array          Data to render
     * @param    $index      int            Index of array
     * @param    $items      collection     Documents in same link
     * @param    $current    int            Last item index
     *
     * @return   string
     */
    public function recursiveHtml($arr, $index = 0, $items = null, $current, $status = false)
    {
        $docType = DB::table('doc_types')->where('id', $arr[$index])->first();
        $liClass = $index == 0 ? 'dd-item-parent' : 'dd-item';
        $html = '<li class="' . $liClass . '">
                    <div class="dd-handle">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-uppercase text-">' .
            htmlentities($docType->name) .
            '</label>
                        </div>
                    </div>';
        if (count($items) > 0 && $arr[$index] == $current) {
            $html .= '<div class="dd-item-list">';
            foreach ($items as $item) {
                $label = $status ? '<span id="doc-status-' . $item->id . '">
                                        <label class="bg-' .
                    Tool::getDocumentSetStatus($item->status)['class'] . ' p-xxs b-r-xs label-status badge text-center">' .
                    Tool::getDocumentSetStatus($item->status)['text'] .
                    '</label>
                                    </span>'
                    : null;
                $html .= '<div class="form-group row">
                                <label class="col-sm-3 col-form-label">' . htmlentities($item->code) . '</label>
                                <div class="col-sm-9">
                                    <button type="button" data-toggle="modal" data-target="#detail-doc-modal"
                                        class="btn btn-info btn-detail-doc" data-id="' . $item->id . '">
                                        <i class="fa fa-eye"></i> ' . trans('message.xem_chi_tiet') . '
                                    </button>
                                    <span class="pull-right">' . $label . '</span>
                                </div>
                            </div>';
            }
            $html .= '</div>';
        }
        if (isset($arr[$index + 1])) {
            $html .= '<ol class="dd-list">';
            $html .= $this->recursiveHtml($arr, $index + 1, $items, $current, $status);
            $html .= '</ol>';
        }
        $html .= '</li>';
        return $html;
    }

    /**
     * Check exist attachment
     *
     * @author   TrungLH3
     *
     * @param    $file            \App\Models\File          File instance
     * @param    $type            int                      Document type
     *
     * @return   object
     */
    public function checkExistAttachment($file, $type = null)
    {
        $type = $type ? $type : Tool::getConst('TYPE_ATTACH');
        $status = false;
        $path = null;
        $real_path = null;
        if ($file->link) {
            $dir = $file->link;
            $path = public_path($dir . DIRECTORY_SEPARATOR . $file->file_name);
            if (file_exists($path)) {
                $real_path = str_replace('/', DIRECTORY_SEPARATOR, $path);
                $path = asset(rtrim($dir, '/') . '/' . $file->file_name);
                $status = true;
            } else {
                $path = null;
            }
        }
        return (object)compact('status', 'path', 'real_path');
    }

    /**
     * Check exist attachment
     *
     * @author   TrungLH3
     *
     * @param    $doc             \App\Models\Document          Document instance
     * @param    $type            int                           Document type
     *
     * @return   string
     */
    public function makeSavePath($doc, $type = null)
    {
        $type = $type ? $type : Tool::getConst('TYPE_ATTACH');
        $root = rtrim(Tool::getConst('DOCUMENT_UPLOAD'), '/');
        $stage = $doc->stage;
        if ($doc->group) {
            //Nếu có group
            $groupDir = $doc->group->name ?? null;
        } else {
            //Ko có group
            if (!is_null($doc->pid)) {
                $pid = $doc->product_type->code ?? null;
                $groupDir = Tool::getConst('PID_DIR') . DIRECTORY_SEPARATOR . $pid;
            } else {
                //Nếu ko có group và ko có pid
                $groupDir = Tool::getConst('COMMON_DIR');
            }
        }
        $docType = $doc->doc_type->name ?? null;
        $dir = str_replace('/', '\\', $root) . DIRECTORY_SEPARATOR .
            Tool::convertDirectoryName(trim($stage->name)) . DIRECTORY_SEPARATOR .
            Tool::convertDirectoryName(trim($groupDir)) . DIRECTORY_SEPARATOR .
            Tool::convertDirectoryName(trim($docType)) . DIRECTORY_SEPARATOR . Tool::convertDirectoryName(trim($doc->code));
        switch ($type) {
            case Tool::getConst('TYPE_BBDT'):
                $dir .= DIRECTORY_SEPARATOR . Tool::getConst('BBDT_DIR');
                break;
            case Tool::getConst('TYPE_PPTH'):
                $dir .= DIRECTORY_SEPARATOR . Tool::getConst('PPTH_DIR');
                break;
            case Tool::getConst('TYPE_COMMENT'):
                $dir .= DIRECTORY_SEPARATOR . Tool::getConst('COMMENT_DIR');
                break;
            case Tool::getConst('TYPE_DTDK'):
                $dir .= DIRECTORY_SEPARATOR . Tool::getConst('DTDK_DIR');
                break;
            default :
                break;
        }
        return public_path($dir);
    }

    /**
     * Parse version number from int to float
     *
     * @author   TrungLH3
     *
     * @param    $ver            int          Version no.
     *
     * @return   string
     */
    public function parseVersion($ver)
    {
        if ($ver > 0) {
            $digit = $ver >= 10 ? intval($ver / 10) : "0";
            $sub = $ver >= 10 ? $ver % 10 : intval($ver);
            return $ver ? "$digit.$sub" : null;
        } else {
            return "0.0";
        }
    }

    /**
     * Check user can approve document
     *
     * @author   TrungLH3
     *
     * @param    int $docId Document id
     *
     * @return   boolean
     */
    public function canApprove($docId)
    {
        return auth()->check() && auth()->user()->canApproveDocument($docId);
    }

    /**
     * Check user can edit document
     *
     * @author   TrungLH3
     *
     * @param    int $docId Document id
     *
     * @return   boolean
     */
    public function canEdit($docId)
    {
        return auth()->check() && auth()->user()->canEditDocument($docId);
    }

    /**
     * Check user can revoke document
     *
     * @author   TrungLH3
     *
     * @param    int $docTypeId Document type id
     * @param    int $type      Action type
     *
     * @return   boolean
     */
    public function canAction($docTypeId, int $type = null)
    {
        return auth()->check() && auth()->user()->haveDocTypePermission($docTypeId, $type);
    }

    /**
     * Check user can approve involve document
     *
     * @author   TrungLH3
     *
     * @param    \App\Models\Document       $document
     *
     * @return   boolean
     */
    public function canApproveInvolve($doc) {
        return auth()->check() && auth()->user()->canApproveInvolve($doc);
    }

    /**
     * Get link from document path
     *
     * @author   TrungLH3
     *
     * @param    string $path Document path
     *
     * @return   string
     */
    public function getLinkFromPath($path)
    {
        return str_replace(DIRECTORY_SEPARATOR, '/', ltrim(str_replace(public_path(), '', $path), DIRECTORY_SEPARATOR));
    }

    /**
     * Get first pid in attributes pid
     *
     * @author   TrungLH3
     *
     * @param    string $pidStr Document PID
     *
     * @return   mixed
     */
    public function getListPid($pidStr, $mode = 'all')
    {
        if (!$pidStr) {
            return array();
        }
        $str = trim($pidStr, ',');
        $arr = strpos($str, ',') ? explode(',', $str) : array($str);
        $result = strtolower($mode) == 'all' ? $arr : current($arr);
        if ($result) {
            foreach ($result as $key => $value) {
                $result[$key] = $value ? (int) $value : $value;
            }
        }
        return $result;
    }

    /**
     * Parse document code
     *
     * @author   TrungLH3
     *
     * @param    string $code Document code
     *
     * @return   string
     */
    public function parseCode($code)
    {
        if (is_null($code)) {
            return null;
        }
        $firstCode = substr($code, 0, 3);
        $secondCode = substr($code, 3, 4);
        $suffix = substr($code, 7, strlen($code) - (strlen($firstCode) + strlen($secondCode)));
        return $firstCode . '-' . $secondCode . ' ' . $suffix;
    }

    public function getConfig($param)
    {
        $param = mb_strtoupper($param);
        if (in_array($param, array('KEEPING_FILES', 'DISK', 'FORMAT', 'FREQUENCY', 'AT', 'DAY', 'ON'))) {
            $configPath = base_path('app/Helpers/Backup/config.ini');
            if (file_exists($configPath)) {
                $file = file($configPath);
                $config = array();
                foreach ($file as $line) {
                    $info = explode('=', $line);
                    $config = array_merge($config, array($info[0] => trim(end($info))));
                }
                return $config[$param];
            } else {
                return NULL;
            }
        }
        throw new \Exception('Wrong config option!');
    }

    public function getDrives()
    {
        $letters = range('A', 'Z');
        $keys = range(1, count($letters));
        $drives = array();
        $letterArray = array_combine($keys, $letters);
        foreach ($letterArray as $drive) {
            try {
                $dir = scandir(realpath($drive . ':/'));
            } catch (\Exception $e) {
                $index = array_search($drive, $letterArray);
                unset($letterArray[$index]);
            }
        }
        foreach ($letterArray as $drive) {
            if (disk_free_space(realpath($drive . ':/')) > 0) {
                $drives[] = (object)array(
                    'disk' => $drive . ':/',
                    'free' => Tool::formatBytes(disk_free_space(realpath($drive . ':/'))),
                    'capacity' => Tool::formatBytes(disk_total_space(realpath($drive . ':/'))),
                    'used' => Tool::formatBytes(disk_total_space(realpath($drive . ':/'))
                        - disk_free_space(realpath($drive . ':/'))),
                    'selected' => Tool::getConfig('DISK') == $drive . ':/' ? 1 : 0,
                );
            }
        }
        return $drives;
    }

    public function formatBytes($bytes, $precision = 2)
    {
        $si_prefix = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $base = 1024;
        $class = min((int)log($bytes, $base), count($si_prefix) - 1);
        return sprintf('%1.2f', $bytes / pow($base, $class)) . ' ' . $si_prefix[$class];
    }

    public function saveConfig($config = array())
    {
        $lines = array();
        $configPath = base_path('app/Helpers/Backup/config.ini');
        $index = 0;
        $length = count($config) - 1;
        foreach ($config as $key => $value) {
            if ($index != $length) {
                $lines[] = $key . '=' . $value . "\n";
            } else {
                $lines[] = $key . '=' . $value;
            }
            $index++;
        }
        if (!file_put_contents($configPath, $lines)) {
            return false;
        }
        return true;
    }

    public function driveLetter($path)
    {
        return (preg_match('/^[A-Z]:/i', $path = realpath($path))) ? $path[0] . ':\\' : NULL;
    }

    public function convertDirectoryName($name = null)
    {
        if (!$name) {
            return null;
        }
        // $notAllowed = ['\\', '/', ':', '*', '?', '"', '<', '>', '|'];
        // return trim(str_replace($notAllowed, '', $name));
        return trim(str_slug($name));
    }

    public function removeSqlBackup()
    {
        $path = realpath(config('filesystems.disks.local.root') . '/databases');
        $files = array_diff(scandir($path), array('.', '..'));
        $list = array();

        foreach ($files as $file) {
            $patterns = explode('.', $file);
            $extension = end($patterns);
            if (in_array(strtolower($extension), array('zip', 'sql'))) {
                $list[] = $path . '/' . $file;
            }
        }

        $newArray = array();

        foreach ($list as $file) {
            $newArray[$file] = filectime($file);
        }

        asort($newArray);
        $keeping = Tool::getConfig('KEEPING_FILES');
        $total = count($files);

        if ($keeping < $total) {
            $destroy = $total - $keeping;
            $i = 0;
            foreach ($newArray as $file => $time) {
                if (++$i <= $destroy) {
                    unlink($file);
                }
            }
        }
    }

    public function move_parent($input)
    {
        // Get array of all source files
        $files = scandir($input);
        // Identify directories
        $source = realpath($input);
        $destination = realpath(dirname($input));
        $files = array_diff($files, array('.', '..'));
        // Cycle through all source files
        foreach ($files as $file) {
            if (copy($source . '/' . $file, $destination . '/' . $file)) {
                $delete[] = realpath($source . '/' . $file);
            }
        }
        // Delete all successfully-copied files
        foreach ($delete as $file) {
            unlink($file);
        }
        //Remove previous directory
        if (file_exists($source)) {
            rmdir($source);
        }
    }

    public function humanFile($bytes, $decimals = 2)
    {
        if ($bytes < 1024) {
            return $bytes . 'B';
        }

        $factor = floor(log($bytes, 1024));

        return sprintf("%.{$decimals}f ", $bytes / pow(1024, $factor)) . ['B', 'KB', 'MB', 'GB', 'TB', 'PB'][$factor];
    }

    public function getDateFormat($date_modify)
    {
        return Carbon::createFromTimestampMs($date_modify)->formatLocalized('%d %B %Y %H:%M');
    }

    public function compareStage($a, $b) {
        $diff = [];
        if (count($b)) {
            $diff = collect($b)->filter(function ($elem) use ($a) {
                return ! collect($a)->contains($elem);
            })->toArray();
        }
        return array_values($diff);
    }
}
