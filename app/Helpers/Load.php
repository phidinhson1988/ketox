<?php

namespace App\Helpers;

class Load
{
    public static function all()
    {
        $all = [
            //===QUANRLYSANPHAM===
            [ 'order'=> '32',   'sort'=>'1', 'level' => '1',   'permission_name' => 'quan_tri_he_thong',                   'permission_key' => 'admin-controler', 'parents' => [], 'childs' => [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]],
            [ 'order'=> '1',   'sort'=>'1', 'level' => '2',   'permission_name' => 'danh_sach_san_pham',                   'permission_key' => 'product-view', 'parents' => [32], 'childs' => [2,3,4,5]],
            [ 'order'=> '2',   'sort'=>'2', 'level' => '3',   'permission_name' => 'san_pham_danh_sach',                   'permission_key' => 'product-list', 'parents' => [1,32], 'childs' => []],
            [ 'order'=> '3',   'sort'=>'3', 'level' => '3',   'permission_name' => 'san_pham_them_moi',                    'permission_key' => 'product-create', 'parents' => [1,32], 'childs' => []],
            [ 'order'=> '4',   'sort'=>'4', 'level' => '3',   'permission_name' => 'san_pham_cap_nhat',                    'permission_key' => 'product-edit', 'parents' => [1,32], 'childs' => []],
            [ 'order'=> '5',   'sort'=>'5', 'level' => '3',   'permission_name' => 'san_pham_xoa',                         'permission_key' => 'product-delete', 'parents' => [1,32], 'childs' => []],
            
            //===QUANRLYNEW===
            [ 'order'=> '6',   'sort'=>'6', 'level' => '2',   'permission_name' => 'quan_ly_tin_tuc',                   'permission_key' => 'news-view', 'parents' => [32], 'childs' => [7,8,9,10]],
            [ 'order'=> '7',   'sort'=>'7', 'level' => '3',   'permission_name' => 'tin_tuc_danh_sach',                   'permission_key' => 'news-list', 'parents' => [6,32], 'childs' => []],
            [ 'order'=> '8',   'sort'=>'8', 'level' => '3',   'permission_name' => 'tin_tuc_them_moi',                    'permission_key' => 'news-create', 'parents' => [6,32], 'childs' => []],
            [ 'order'=> '9',   'sort'=>'9', 'level' => '3',   'permission_name' => 'tin_tuc_cap_nhat',                    'permission_key' => 'news-edit', 'parents' => [6,32], 'childs' => []],
            [ 'order'=> '10',   'sort'=>'10', 'level' => '3',   'permission_name' => 'tin_tuc_xoa',                         'permission_key' => 'news-delete', 'parents' => [6,32], 'childs' => []],
            
            //===QUANRLYPAGE===
//            [ 'order'=> '11',   'sort'=>'11', 'level' => '2',   'permission_name' => 'danh_sach_page',                   'permission_key' => 'page-view', 'parents' => [32], 'childs' => [12,13,14,15]],
//            [ 'order'=> '12',   'sort'=>'12', 'level' => '3',   'permission_name' => 'page_danh_sach',                   'permission_key' => 'page-list', 'parents' => [11,32], 'childs' => []],
//            [ 'order'=> '13',   'sort'=>'13', 'level' => '3',   'permission_name' => 'page_them_moi',                    'permission_key' => 'page-create', 'parents' => [11,32], 'childs' => []],
//            [ 'order'=> '14',   'sort'=>'14', 'level' => '3',   'permission_name' => 'page_cap_nhat',                    'permission_key' => 'page-edit', 'parents' => [11,32], 'childs' => []],
//            [ 'order'=> '15',   'sort'=>'15', 'level' => '3',   'permission_name' => 'page_xoa',                         'permission_key' => 'page-delete', 'parents' => [11,32], 'childs' => []],
            
            //===QUANRLYCART===
            [ 'order'=> '16',   'sort'=>'16', 'level' => '2',   'permission_name' => 'danh_sach_ban_hang',                   'permission_key' => 'cart-view', 'parents' => [32], 'childs' => [17,20]],
            [ 'order'=> '17',   'sort'=>'17', 'level' => '3',   'permission_name' => 'ban_hang_danh_sach',                   'permission_key' => 'cart-list', 'parents' => [16,32], 'childs' => []],
            [ 'order'=> '20',   'sort'=>'20', 'level' => '3',   'permission_name' => 'ban_hang_xoa',                         'permission_key' => 'cart-delete', 'parents' => [16,32], 'childs' => []],
            
            //===QUANRLYSETTING===
            [ 'order'=> '21',   'sort'=>'21', 'level' => '2',   'permission_name' => 'danh_sach_cau_hinh',                   'permission_key' => 'config-view', 'parents' => [32], 'childs' => []],

            //===QUANRLYTAIKHOAN===
            [ 'order'=> '22',   'sort'=>'22', 'level' => '2',   'permission_name' => 'quan_ly_nguoi_dung',                        'permission_key' => 'user-view', 'parents' => [32], 'childs' => [23,24,25,26]],
            [ 'order'=> '23',   'sort'=>'23', 'level' => '3',   'permission_name' => 'tai_khoan_danh_sach',                       'permission_key' => 'user-list', 'parents' => [22,32], 'childs' => []],
            [ 'order'=> '24',   'sort'=>'24', 'level' => '3',   'permission_name' => 'tai_khoan_them_moi',                        'permission_key' => 'user-create', 'parents' => [22,32], 'childs' => []],
            [ 'order'=> '25',   'sort'=>'25', 'level' => '3',   'permission_name' => 'tai_khoan_cap_nhat',                        'permission_key' => 'user-edit', 'parents' => [22,32], 'childs' => []],
            [ 'order'=> '26',   'sort'=>'26', 'level' => '3',   'permission_name' => 'tai_khoan_xoa',                             'permission_key' => 'user-delete', 'parents' => [22,32], 'childs' => []],

            //===QUANRLYPHANQUYEN===
            [ 'order'=> '27',   'sort'=>'27', 'level' => '2',   'permission_name' => 'quan_ly_phan_quyen',                        'permission_key' => 'role-view', 'parents' => [32], 'childs' => [28,29,30,31]],
            [ 'order'=> '28',   'sort'=>'28', 'level' => '3',   'permission_name' => 'phan_quyen_danh_sach',                      'permission_key' => 'role-list', 'parents' => [27,32], 'childs' => []],
            [ 'order'=> '29',   'sort'=>'29', 'level' => '3',   'permission_name' => 'phan_quyen_them_moi',                       'permission_key' => 'role-create', 'parents' => [27,32], 'childs' => []],
            [ 'order'=> '30',   'sort'=>'30', 'level' => '3',   'permission_name' => 'phan_quyen_cap_nhat',                       'permission_key' => 'role-edit', 'parents' => [27,32], 'childs' => []],
            [ 'order'=> '31',   'sort'=>'31', 'level' => '3',   'permission_name' => 'phan_quyen_xoa',                            'permission_key' => 'role-delete', 'parents' => [27,32], 'childs' => []],
            [ 'order'=> '33',   'sort'=>'33', 'level' => '1',   'permission_name' => 'Guest',                            'permission_key' => 'guest', 'parents' => [], 'childs' => []],
        ];

        return $all;

    }
}
