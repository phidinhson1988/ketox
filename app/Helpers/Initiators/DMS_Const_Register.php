<?php

/**
 * @package    DMS
 * @category    DMS_Const_Register
 * @copyright  Copyright 2018 FPT
 * @author     TrungLH3 FSU2.C88
 * @since       2018-08-10
 */

use App\Helpers\DMS_Const;

if (! class_exists('DMS_Const_Register')) {
    class DMS_Const_Register extends DMS_Const
    {
        //Khai báo các const của project tại đây
        /*
        Sử dụng:
            + Lấy một const cụ thể: get_const('TÊN_CONST');
            + Lấy một danh sách const theo mảng: get_const(['CONST_1', 'CONST_2', ...]);
            + Lấy tất cả const: get_const(null, true);
        */
        const ACTIVE_ON = 1;
        const ACTIVE_OFF = 0;
        const ACTIVE_PENDING = 2;
        const UNIT_PRICE = 'VND';
        const DEFAULT_LIMIT = 20;
        const PAGGING_LIMIT = 5;
        const DEFAULT_PAGE = 1;
        const VERSION_JS = '1.9';
        const RATIO_IMAGE_CARD = 1.5; // ratio Image (width 3/height 4)
        const RATIO_POSITION_CARD = "[0,0,300,200]"; // position Image
        const RATIO_IMAGE_NEWS = 1.5; // ratio Image (width 3/height 4)
        const RATIO_POSITION_NEWS = "[0,0,300,200]"; // position Image
        const RATIO_IMAGE_USER = 1; // ratio Image (width 1/height 1)
        const RATIO_POSITION_USER = "[0,0,400,400]"; // position Image
        const USER_BLANK = 'upload/users/blank.png';
        const IMAGE_BLACH = 'img/blank.png';
        const USER_UPLOAD = 'upload/images/users/';
        const SETTING_UPLOAD = 'upload/images/setting/';
        const CONTENT_UPLOAD = 'upload/images/content/';
        const USER_NOT_WORKING = 4;
        const LOCK_SCREEN = 1;
        const PRODUCT_UPLOAD = 'upload/products/avatar/';
        const POST_UPLOAD = 'upload/posts/avatar/';
        const BLANK_IMAGE = 'img/blank/img_preview.jpg';
        const ROLES_ACTIVE = 1;
        const TIME_OUT = 10000;
        
        const TEST_SYNC_ULSO = 1;
        const EXCEL_EXPORT = 'excel';
        // D:\Projects\Python\api_converst\tutorial\settings.py settin domain cho ALLOWED_HOSTS
        const API_URL = '/p2/api/printer';
        const PDF_EXPORT = 'pdf';
        const SEARCH_HIGHLIGHT_COLOR = 'yellow';
        const HASH_DEFAULT = '123456';
        const REQUEST_UPLOAD = 'upload/request/';
        const NO_DEPARTMENT = 'Không có phòng ban';
        const TEMPLATE_UPLOAD = 'upload/templates/';
        const DOCUMENT_UPLOAD = 'upload/documents/';
        const PDF_UPLOAD = 'upload/documents/Docs_Convert/';
        const COMMON_DIR = 'Common';
        const COMMENT_DIR = 'Comments';
        const ALLOWED_DOC_MIMES = 'doc,DOC,docx,DOCX,txt,TXT,xls,XLS,xlsx,XLSX,pdf,PDF';
        const DOC_EXT_FILTER = 0; //Cho phép chặn file upload theo extension hay không (1: Có, 0: Không)
        const DEFAULT_VERSION = '00';
        const BACKUP_ALL_FOLDER = '/http---localhost';
        const VIEW_TREE_BY_STAGE = 1;
        const VIEW_TREE_BY_GROUP = 2;
        const VIEW_TREE_BY_PID = 3;
        const COMMON_GROUP_NAME = 'COMMON_GROUP';
        const EXCLUDED_STATUS = 0;
        const FREQUENCY_REPORT = 'frequency_report';
        const UPLOAD_REPORT_DURATION = 3; //Số ngày upload đào tạo định kỳ sau khi revice
        const DEFAULT_TIME_BEFORE_YEAR_DEADLINE = 5; // Số ngày trước khi hạn upload tài liệu đào tạo định kỳ hàng năm

        //DB SETTINGS
        const TYPES_PID = 1;
        const STAGES_ACTION = 1;
        const DEFAULT_PREVIOUS_TIME = 1;
        const DEFAULT_PREVIOUS_CRONJOB = 100;
        const SETTING_UPLOAD_REPORT_DURATION = 3;
        const SETTING_DEFAULT_TIME_BEFORE_YEAR_DEADLINE = 4;

        //DB USER LOGS
        const USER_HISTORY_LOGIN = 1;
        const USER_HISTORY_LOGOUT = 2;
        const USER_HISTORY_NOT_RECORD = 0;

        const NOTIFI_NOT_READ = 1;
        const NOTIFI_READ = 2;

        // DB STAGES
        const STAGES_ACTIVE = 1;

        // DB DEPARTMENT
        const DEPARTMENT_ACTIVE = 1;

        // DB GROUPS
        const GROUP_ACTIVE = 1;

        // DB TYPES_PID
        const TYPES_PID_ACTIVE = 1;

        //DB DOCUMENT
        const APPROVE_OPTION = 1;
        const TRAIN_OPTION = 2;
        const JOBCLOSE_OPTION = 3;
        const REJECT_OPTION = 0;
        const REJECT_TO_CREATED = 0;
        const REJECT_TO_TRAINING = 1; //default

        // DB USERS
        const USER_ACTIVE = 1;
        const IS_APPROVED = 1;
        const NO_APPROVED = 0;

        //PERMISSION DOCTYPE
        const PERMISSION_CREATE = 0;
        const PERMISSION_REVOKE = 1;
        const PERMISSION_EDIT = 2;
        const PERMISSION_DOWNLOAD = 3;


        //SYSTEM
        const SYSTEM_USER_ACCOUNT = 'system';
        const TAC_TIME_DEFAULT = 1;
        const DEFAULT_INDEX_LENGTH = 3;
        // ratio Image
        const RATIO_USER_AVATAR = 1; // ratio Image (width 3/height 2)
        const RATIO_POSITION_USER_AVATAR = "[0,0,200,200]"; // position Image
        const RATIO_USER_IMAGE = 0.6; // ratio Image (width 1/height 2)
        const RATIO_POSITION_USER_IMAGE = "[0,0,300,500]"; // position Image
    }
}
