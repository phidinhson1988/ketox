<?php

/**
 * @package    DMS
 * @category    DMS_Table_Register
 * @copyright  Copyright 2018 FPT
 * @author     TrungLH3 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Helpers\Initiators;
if (! class_exists('DMS_Table_Register')) {
    class DMS_Table_Register {
        protected $table = [
            'groups' => [
                'NAME' => 'name',
                'DESC' => 'description',
                'STS' => 'status',
                'PARENT' => 'parent_id',
            ],
        ];
        protected static $instance = null;
        private function __construct() {
        }
        private static function getInstance() {
            if (! isset(static::$instance)) {
                static::$instance = new self;
            }
            return static::$instance;
        }
        public static function getTable($name) {
            $self = static::getInstance();
            return $self->table[$name];
        }
        public static function getColumn($table, $col) {
            $self = static::getInstance();
            if (in_array($col, array_keys(static::getTable($table)))) {
                return static::getTable($table)[$col];
            }
            return null;
        }
    }
}
