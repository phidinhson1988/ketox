<?php

/**
 * @package    DMS
 * @category    Pagination
 * @copyright  Copyright 2018 FPT
 * @author     TrungLH3 FSU2.C88
 * @since       2018-08-10
 */

namespace App\Helpers;

class Pagination
{
    protected $limit = 5;
    protected $data = NULL;
    protected $link = NULL;
    protected $query = '';
    public $config = array(
        'first'       => 'Previous',
        'last'        => 'Next',
        'ul'          => 'pagination justify-content-center',
        'li_active'   => 'page-item',
        'li_inactive' => 'page-item disabled',
        'li_current'  => 'page-item active',
    );
    protected static $instance;

    /**
     * @copyright  Copyright 2018 FPT
     * @author     : FSU2.C88
     *
     */

    public function setFirst($_first) {
        $this->config['first'] = $_first;
    }
    /**
     * @copyright  Copyright 2018 FPT
     * @author     : FSU2.C88
     *
     */
    public function setLast($_last) {
        $this->config['last'] = $_last;
    }
    /**
     * @copyright  Copyright 2018 FPT
     * @author     : FSU2.C88
     */
    public function setPrevious($_previous) {
        $this->config['previous'] = $_previous;
    }
    /**
     * @copyright  Copyright 2018 FPT
     * @author     : FSU2.C88
     *
     */
    public function setNext($_next) {
        $this->config['next'] = $_next;
    }
    /**
     * @copyright  Copyright 2018 FPT
     * @author     : FSU2.C88
     *
     */
    public function setUl($class) {
        $this->config['ul'] = $class;
    }
    /**
     * @copyright  Copyright 2018 FPT
     * @author     : FSU2.C88
     *
     */
    public function setLi_active($class) {
        $this->config['li_active'] = $class;
    }
   /**
     * @copyright  Copyright 2018 FPT
     * @author     : FSU2.C88
     *
     */
    public function setLi_inactive($class) {
        $this->config['li_inactive'] = $class;
    }
    /**
     * @copyright  Copyright 2018 FPT
     * @author     : FSU2.C88
     *
     */
    public function setLi_current($class) {
        $this->config['li_current'] = $class;
    }
    /**
     * @copyright  Copyright 2018 FPT
     * @author     : FSU2.C88
     *
     */
    public function setBaseConfig() {
        foreach($this->config as $key => $value) {
            if (! in_array($key, array('ul', 'li_active', 'li_inactive', 'li_current'))) {
                $this->config[$key] = ucfirst($key);
            }
        }
    }
    /**
     * @copyright  Copyright 2018 FPT
     * @author     : FSU2.C88
     *
     */
    private function __construct($_data, $config = NULL) {
        $this->data = $_data;
        $this->link = $_data->url('/');
        $this->limit += 2;
        if (isset($config)) {
            foreach($config as $key => $value) {
                $method = 'set' . ucfirst($key);
                $this->$method($value);
            }
        }
    }
    /**
     * @copyright  Copyright 2018 FPT
     * @author     : FSU2.C88
     *
     */
    public static function init($_input, $config = NULL) {
        if (is_object($_input) && ! isset(static::$instance)) {
            if (is_array($config)) {
                static::$instance = new self($_input, $config);
            } else {
                static::$instance = new self($_input);
            }
            return static::$instance;
        }
    }
   /**
     * @copyright  Copyright 2018 FPT
     * @author     : FSU2.C88
     *
     */
    public static function create($_input, $config = NULL) {
        if (is_object($_input)) {
            if (is_array($config)) {
                static::$instance = new self($_input, $config);
            } else {
                static::$instance = new self($_input);
            }
            return static::$instance;
        }
    }
    /**
     * @copyright  Copyright 2018 FPT
     * @author     : FSU2.C88
     *
     */
    public function limit($_limit = 5) {
        $_limit = (int) $_limit;
        if (is_numeric($_limit)) {
            $this->limit = $_limit + 2;
            return $this;
        }
    }
    /**
     * @copyright  Copyright 2018 FPT
     * @author     : FSU2.C88
     *
     */
    public function appends($_query = '', $_value = NULL) {
        $appends = '';
        if (empty($_query) && empty($_value)) {
            $appends .= '';
        } else {
            if (is_array($_query)) {
                foreach($_query as $key => $value) {
                    $appends .= '&' . "$key=$value";
                }
            } else {
                if (! empty($_value) && is_string($_query)) {
                    $appends .= '&' . "$_query=$_value";
                } else{
                    $appends .= '&' . "$_query=";
                }

            }
        }
        $this->link .= $appends;
        $this->query = $appends;
        return $this;
    }
    /**
     * @copyright  Copyright 2018 FPT
     * @author     : FSU2.C88
     *
     */
    public function render() {
        if ($this->data->hasPages()) {
            $half = floor($this->limit / 2);
            $html = '<ul class="' . $this->config['ul'] . '">';
            if ($this->data->currentPage() > 1) {
                $html .= '<li class="' . $this->config['li_active'] . '" style="margin-right: auto">
                            <a class="page-link" href="' . $this->link . '">' . $this->config['first'] . '</a>
                          </li>';
            } else {
                $html .= '<li class="' . $this->config['li_active'] . ' disabled" style="margin-right: auto">
                            <a class="page-link" href="' . $this->link . '">' . $this->config['first'] . '</a>
                          </li>';
            }

            if (array_key_exists('previous', $this->config)) {
                if ($this->data->currentPage() > 1) {
                    $html .= '<li class="' . $this->config['li_active'] . '">
                                <a class="page-link" href="' . $this->data->url($this->data->currentPage() - 1) . '">' .
                                $this->config['previous'] . '</a>
                              </li>';
                } else {
                    $html .= '<li class="' . $this->config['li_inactive'] . '">
                                <span>' . $this->config['previous']  .'</span>
                              </li>';
                }
            }

            if ($this->data->lastPage() > $this->limit - 2 && ($this->data->currentPage() > $half)) {
                $html .= '<li class="' . $this->config['li_inactive'] . '">
                            <span>...</span>
                          </li>';
            }

            for ($i = 1; $i <= $this->data->lastPage(); $i++) {
                $from = $this->data->currentPage() - $half;
                $to = $this->data->currentPage() + $half;

                if ($this->data->currentPage() < $half) {
                    $to += $half - $this->data->currentPage();
                }

                if ($this->data->lastPage() - $this->data->currentPage() < $half) {
                    $from -= $half - ($this->data->lastPage() - $this->data->currentPage()) - 1;
                }

                if ($from < $i && $i < $to) {
                    if ($this->data->currentPage() == $i) {
                        $html .= '<li class="' . $this->config['li_current'] . '">
                                    <a class="page-link" href="#">' . $i . '</a>
                                  </li>';
                    } else {
                        $html .= '<li class="' . $this->config['li_active'] . '">
                                    <a class="page-link" href="' . $this->data->url($i) . $this->query . '">' . $i . '</a>
                                  </li>';
                    }
                }
            }

            if ($this->data->lastPage() > $this->limit - 2 &&
                ($this->data->currentPage() < $this->data->lastPage() - $half + 1)) {
                $html .= '<li class="' . $this->config['li_inactive'] . '">
                            <span>...</span>
                          </li>';
            }

            if (array_key_exists('next', $this->config)) {
                if ($this->data->currentPage() < $this->data->lastPage()) {
                    $html .= '<li class="' . $this->config['li_active'] . '">
                                <a class="page-link" href="' . $this->data->url($this->data->currentPage() + 1) . '">' .
                                $this->config['next'] . '</a>
                              </li>';
                } else {
                    $html .= '<li class="' . $this->config['li_inactive'] . '">
                                <span>' . $this->config['next']  .'</span>
                              </li>';
                }
            }

            if ($this->data->currentPage() < $this->data->lastPage()) {
                $html .= '<li class="'. $this->config['li_active'] . '" style="margin-left: auto">
                            <a class="page-link" href="' . $this->data->url($this->data->lastPage()) .
                            $this->query .'">' . $this->config['last'] . '</a>
                          </li>';
            } else {
                $html .= '<li class="'. $this->config['li_active'] . ' disabled" style="margin-left: auto">
                            <a class="page-link" href="' . $this->data->url($this->data->lastPage()) .
                            $this->query .'">' . $this->config['last'] . '</a>
                          </li>';
            }

            $html .= '</ul>';
            return $html;
        }
    }
}
