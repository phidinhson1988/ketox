/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.5-10.1.30-MariaDB : Database - ketox
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ketox` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

/*Table structure for table `cart_product` */

DROP TABLE IF EXISTS `cart_product`;

CREATE TABLE `cart_product` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cart_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `cart_id` (`cart_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `cart_product` */

insert  into `cart_product`(`id`,`cart_id`,`product_id`,`quantity`) values (1,1,8,1),(2,2,7,1),(3,3,6,1),(4,4,4,1);

/*Table structure for table `carts` */

DROP TABLE IF EXISTS `carts`;

CREATE TABLE `carts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `status` (`status`),
  KEY `created_time` (`created_time`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `carts` */

insert  into `carts`(`id`,`user_id`,`created_time`,`created_at`,`updated_at`,`status`) values (1,1,'2019-08-17 15:45:07','2019-08-17 15:45:07','2019-08-17 15:48:06',2),(2,1,'2019-08-17 15:48:15','2019-08-17 15:48:15',NULL,1),(3,1,'2019-08-17 15:48:22','2019-08-17 15:48:22',NULL,1),(4,1,'2019-08-17 15:48:34','2019-08-17 15:48:34',NULL,1);

/*Table structure for table `city` */

DROP TABLE IF EXISTS `city`;

CREATE TABLE `city` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `key_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_country` (`key_country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `city` */

/*Table structure for table `contacts` */

DROP TABLE IF EXISTS `contacts`;

CREATE TABLE `contacts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '0' COMMENT '0: chưa sử lý; 1: đã sử lý',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `email` (`email`),
  KEY `subject` (`subject`),
  KEY `status` (`status`),
  KEY `created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `contacts` */

/*Table structure for table `feedbacks` */

DROP TABLE IF EXISTS `feedbacks`;

CREATE TABLE `feedbacks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `feedbacks` */

/*Table structure for table `permission_role` */

DROP TABLE IF EXISTS `permission_role`;

CREATE TABLE `permission_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(11) unsigned NOT NULL,
  `role_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_id` (`permission_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `permission_role` */

insert  into `permission_role`(`id`,`permission_id`,`role_id`) values (98,58,10),(99,59,10),(100,60,10),(101,61,10),(102,62,10),(103,63,10),(104,64,10),(105,65,10),(106,66,10),(107,67,10),(108,68,10),(109,69,10),(110,70,10),(111,71,10),(112,72,10),(113,73,10),(114,74,10),(115,75,10),(116,76,10),(117,77,10),(118,78,10),(119,79,10),(120,80,10),(121,81,10),(122,82,10),(123,83,10);

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `level` tinyint(4) DEFAULT NULL,
  `parents` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `childs` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`name`,`display_name`,`order`,`level`,`parents`,`childs`,`sort`) values (58,'admin-controler','quan_tri_he_thong',32,1,'[]','[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]',1),(59,'product-view','danh_sach_san_pham',1,2,'[32]','[2,3,4,5]',1),(60,'product-list','san_pham_danh_sach',2,3,'[1,32]','[]',2),(61,'product-create','san_pham_them_moi',3,3,'[1,32]','[]',3),(62,'product-edit','san_pham_cap_nhat',4,3,'[1,32]','[]',4),(63,'product-delete','san_pham_xoa',5,3,'[1,32]','[]',5),(64,'news-view','quan_ly_tin_tuc',6,2,'[32]','[7,8,9,10]',6),(65,'news-list','tin_tuc_danh_sach',7,3,'[6,32]','[]',7),(66,'news-create','tin_tuc_them_moi',8,3,'[6,32]','[]',8),(67,'news-edit','tin_tuc_cap_nhat',9,3,'[6,32]','[]',9),(68,'news-delete','tin_tuc_xoa',10,3,'[6,32]','[]',10),(69,'cart-view','danh_sach_ban_hang',16,2,'[32]','[17,20]',16),(70,'cart-list','ban_hang_danh_sach',17,3,'[16,32]','[]',17),(71,'cart-delete','ban_hang_xoa',20,3,'[16,32]','[]',20),(72,'config-view','danh_sach_cau_hinh',21,2,'[32]','[]',21),(73,'user-view','quan_ly_nguoi_dung',22,2,'[32]','[23,24,25,26]',22),(74,'user-list','tai_khoan_danh_sach',23,3,'[22,32]','[]',23),(75,'user-create','tai_khoan_them_moi',24,3,'[22,32]','[]',24),(76,'user-edit','tai_khoan_cap_nhat',25,3,'[22,32]','[]',25),(77,'user-delete','tai_khoan_xoa',26,3,'[22,32]','[]',26),(78,'role-view','quan_ly_phan_quyen',27,2,'[32]','[28,29,30,31]',27),(79,'role-list','phan_quyen_danh_sach',28,3,'[27,32]','[]',28),(80,'role-create','phan_quyen_them_moi',29,3,'[27,32]','[]',29),(81,'role-edit','phan_quyen_cap_nhat',30,3,'[27,32]','[]',30),(82,'role-delete','phan_quyen_xoa',31,3,'[27,32]','[]',31),(83,'guest','Guest',33,1,'[]','[]',33);

/*Table structure for table `posts` */

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `content` longtext COLLATE utf8_unicode_ci,
  `meta_des` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `type` tinyint(1) DEFAULT '1' COMMENT '1: post; 2: page',
  `lang` char(10) COLLATE utf8_unicode_ci DEFAULT 'vi',
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `job` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `slug` (`slug`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `posts` */

insert  into `posts`(`id`,`title`,`slug`,`status`,`avatar`,`description`,`content`,`meta_des`,`meta_keyword`,`created_at`,`updated_at`,`created_by`,`type`,`lang`,`start_date`,`end_date`,`job`) values (1,'News 1','news-1',1,'1566029019thumbnail-Talkshow-Background-6092-1565932732_180x108-162-108-10-0.jpg','Mô tả cho tin tức 1','<p><br />\r\nB&ecirc;n cạnh những nh&acirc;n vật ch&iacute;nh l&agrave; 290 t&acirc;n sinh vi&ecirc;n, kh&ocirc;ng thể kh&ocirc;ng nhắc tới c&aacute;c thầy c&ocirc; sẽ trực tiếp d&igrave;u dắt c&aacute;c em trong những năm học tới. Chủ nhiệm lớp PC 1505, c&ocirc; Th&uacute;y Hằng - giảng vi&ecirc;n m&ocirc;n Ngữ văn cho biết bản th&acirc;n c&oacute; ch&uacute;t lo lắng khi chưa bao giờ l&agrave;m gi&aacute;o vi&ecirc;n chủ nhiệm: &quot;Sau 1 tuần của kh&oacute;a định hướng, t&ocirc;i thấy rằng sự t&iacute;ch cực của gi&aacute;o vi&ecirc;n c&oacute; thể truyền qua cho c&aacute;c bạn. Đ&ocirc;i khi kỷ luật được tạo n&ecirc;n từ ch&iacute;nh sự niềm nở, t&ocirc;n trọng, cởi mở của gi&aacute;o vi&ecirc;n với sinh vi&ecirc;n. Thực tế, ở độ tuổi của c&aacute;c bạn hiện tại sẽ c&oacute; nhiều vấn đề về mặt giao tiếp, quan điểm sống, vốn sống. Nhưng t&ocirc;i tin rằng sự nhiệt t&acirc;m của gi&aacute;o vi&ecirc;n l&agrave; phương ph&aacute;p hiệu quả nhất để gi&uacute;p c&aacute;c bạn tiến bộ&quot;.</p>','bvcb','vcbvcbcv',NULL,NULL,1,1,'en',NULL,NULL,NULL),(2,'News 2','news-2',1,'1566028992gan-300-tan-sinh-vien-he-9-fpt-polytechnic-don-nam-hoc-moi-1566021230_500x300-444-296-36-3.jpg','Mô tả cho tin tức 2','<p><br />\r\nB&ecirc;n cạnh những nh&acirc;n vật ch&iacute;nh l&agrave; 290 t&acirc;n sinh vi&ecirc;n, kh&ocirc;ng thể kh&ocirc;ng nhắc tới c&aacute;c thầy c&ocirc; sẽ trực tiếp d&igrave;u dắt c&aacute;c em trong những năm học tới. Chủ nhiệm lớp PC 1505, c&ocirc; Th&uacute;y Hằng - giảng vi&ecirc;n m&ocirc;n Ngữ văn cho biết bản th&acirc;n c&oacute; ch&uacute;t lo lắng khi chưa bao giờ l&agrave;m gi&aacute;o vi&ecirc;n chủ nhiệm: &quot;Sau 1 tuần của kh&oacute;a định hướng, t&ocirc;i thấy rằng sự t&iacute;ch cực của gi&aacute;o vi&ecirc;n c&oacute; thể truyền qua cho c&aacute;c bạn. Đ&ocirc;i khi kỷ luật được tạo n&ecirc;n từ ch&iacute;nh sự niềm nở, t&ocirc;n trọng, cởi mở của gi&aacute;o vi&ecirc;n với sinh vi&ecirc;n. Thực tế, ở độ tuổi của c&aacute;c bạn hiện tại sẽ c&oacute; nhiều vấn đề về mặt giao tiếp, quan điểm sống, vốn sống. Nhưng t&ocirc;i tin rằng sự nhiệt t&acirc;m của gi&aacute;o vi&ecirc;n l&agrave; phương ph&aacute;p hiệu quả nhất để gi&uacute;p c&aacute;c bạn tiến bộ&quot;.<br />\r\n&nbsp;</p>',NULL,NULL,NULL,NULL,1,1,'vi',NULL,NULL,NULL),(3,'News 3','news-3',1,'1566028975fpt3-1566004020_240x144-216-144-0-0.jpg','Mô tả cho tin tức 3','<p>B&ecirc;n cạnh những nh&acirc;n vật ch&iacute;nh l&agrave; 290 t&acirc;n sinh vi&ecirc;n, kh&ocirc;ng thể kh&ocirc;ng nhắc tới c&aacute;c thầy c&ocirc; sẽ trực tiếp d&igrave;u dắt c&aacute;c em trong những năm học tới. Chủ nhiệm lớp PC 1505, c&ocirc; Th&uacute;y Hằng - giảng vi&ecirc;n m&ocirc;n Ngữ văn cho biết bản th&acirc;n c&oacute; ch&uacute;t lo lắng khi chưa bao giờ l&agrave;m gi&aacute;o vi&ecirc;n chủ nhiệm: &quot;Sau 1 tuần của kh&oacute;a định hướng, t&ocirc;i thấy rằng sự t&iacute;ch cực của gi&aacute;o vi&ecirc;n c&oacute; thể truyền qua cho c&aacute;c bạn. Đ&ocirc;i khi kỷ luật được tạo n&ecirc;n từ ch&iacute;nh sự niềm nở, t&ocirc;n trọng, cởi mở của gi&aacute;o vi&ecirc;n với sinh vi&ecirc;n. Thực tế, ở độ tuổi của c&aacute;c bạn hiện tại sẽ c&oacute; nhiều vấn đề về mặt giao tiếp, quan điểm sống, vốn sống. Nhưng t&ocirc;i tin rằng sự nhiệt t&acirc;m của gi&aacute;o vi&ecirc;n l&agrave; phương ph&aacute;p hiệu quả nhất để gi&uacute;p c&aacute;c bạn tiến bộ&quot;.<br />\r\n&nbsp;</p>',NULL,NULL,NULL,NULL,1,1,'vi',NULL,NULL,NULL),(4,'News 4','news-4',1,'15660289135bb5ac4b1d29fa77a338-1565939439_180x108-162-108-0-0.jpg','Mô tả cho tin tức 4','<p><br />\r\nB&ecirc;n cạnh những nh&acirc;n vật ch&iacute;nh l&agrave; 290 t&acirc;n sinh vi&ecirc;n, kh&ocirc;ng thể kh&ocirc;ng nhắc tới c&aacute;c thầy c&ocirc; sẽ trực tiếp d&igrave;u dắt c&aacute;c em trong những năm học tới. Chủ nhiệm lớp PC 1505, c&ocirc; Th&uacute;y Hằng - giảng vi&ecirc;n m&ocirc;n Ngữ văn cho biết bản th&acirc;n c&oacute; ch&uacute;t lo lắng khi chưa bao giờ l&agrave;m gi&aacute;o vi&ecirc;n chủ nhiệm: &quot;Sau 1 tuần của kh&oacute;a định hướng, t&ocirc;i thấy rằng sự t&iacute;ch cực của gi&aacute;o vi&ecirc;n c&oacute; thể truyền qua cho c&aacute;c bạn. Đ&ocirc;i khi kỷ luật được tạo n&ecirc;n từ ch&iacute;nh sự niềm nở, t&ocirc;n trọng, cởi mở của gi&aacute;o vi&ecirc;n với sinh vi&ecirc;n. Thực tế, ở độ tuổi của c&aacute;c bạn hiện tại sẽ c&oacute; nhiều vấn đề về mặt giao tiếp, quan điểm sống, vốn sống. Nhưng t&ocirc;i tin rằng sự nhiệt t&acirc;m của gi&aacute;o vi&ecirc;n l&agrave; phương ph&aacute;p hiệu quả nhất để gi&uacute;p c&aacute;c bạn tiến bộ&quot;.<br />\r\n&nbsp;</p>',NULL,NULL,NULL,NULL,1,1,'vi',NULL,NULL,NULL),(5,'Career 01','career-01',1,'156602894720190816152753-1565947732_180x108-162-108-0-0.png','Description for Career 01','<p><br />\r\nB&ecirc;n cạnh những nh&acirc;n vật ch&iacute;nh l&agrave; 290 t&acirc;n sinh vi&ecirc;n, kh&ocirc;ng thể kh&ocirc;ng nhắc tới c&aacute;c thầy c&ocirc; sẽ trực tiếp d&igrave;u dắt c&aacute;c em trong những năm học tới. Chủ nhiệm lớp PC 1505, c&ocirc; Th&uacute;y Hằng - giảng vi&ecirc;n m&ocirc;n Ngữ văn cho biết bản th&acirc;n c&oacute; ch&uacute;t lo lắng khi chưa bao giờ l&agrave;m gi&aacute;o vi&ecirc;n chủ nhiệm: &quot;Sau 1 tuần của kh&oacute;a định hướng, t&ocirc;i thấy rằng sự t&iacute;ch cực của gi&aacute;o vi&ecirc;n c&oacute; thể truyền qua cho c&aacute;c bạn. Đ&ocirc;i khi kỷ luật được tạo n&ecirc;n từ ch&iacute;nh sự niềm nở, t&ocirc;n trọng, cởi mở của gi&aacute;o vi&ecirc;n với sinh vi&ecirc;n. Thực tế, ở độ tuổi của c&aacute;c bạn hiện tại sẽ c&oacute; nhiều vấn đề về mặt giao tiếp, quan điểm sống, vốn sống. Nhưng t&ocirc;i tin rằng sự nhiệt t&acirc;m của gi&aacute;o vi&ecirc;n l&agrave; phương ph&aacute;p hiệu quả nhất để gi&uacute;p c&aacute;c bạn tiến bộ&quot;.<br />\r\n&nbsp;</p>',NULL,NULL,NULL,NULL,1,2,'vi','2019-08-01 00:00:00','2019-08-30 23:59:59','IT'),(6,'Career 02','career-02',1,'156602893213-1565999270_360x216-324-216-14-0.jpg','Description for Career 02','<p>Chỉ cần nhắc đến hoa th&ocirc;i l&agrave; người ta nghĩ ngay đến c&aacute;i đẹp. V&agrave; đ&uacute;ng vậy. Mỗi một lo&agrave;i hoa đều mang một vẻ đẹp ri&ecirc;ng m&agrave; kh&ocirc;ng thể đem sao s&aacute;nh với những lo&agrave;i hoa kh&aacute;c được. V&agrave; đương nhi&ecirc;n, những lo&agrave;i hoa đẹp ấy đều mang trong m&igrave;nh những &yacute; nghĩa, những biểu tượng ri&ecirc;ng v&agrave; được sử dụng trong nhiều trường hợp kh&aacute;c nhau.</p>\r\n\r\n<p><br />\r\nNhắc đến anh đ&agrave;o hầu như ai trong ch&uacute;ng ta đều biết đ&acirc;y l&agrave; lo&agrave;i hoa đến từ v&ugrave;ng đất Nhật Bản v&agrave; n&oacute; nở nhiều nhất v&agrave;o thời điểm đầu th&aacute;ng gi&ecirc;ng. Đ&acirc;y cũng ch&iacute;nh l&agrave; lo&agrave;i hoa mang biểu tượng cho đất nước mặt trời mọc. N&oacute; thường được xuất hiện trong c&aacute;c lễ hội trong v&agrave; ngo&agrave;i nước bởi m&agrave;u sắc của n&oacute; mang một vẻ đẹp v&ocirc; c&ugrave;ng dễ thương.</p>',NULL,NULL,NULL,NULL,1,2,'vi','2019-08-01 00:00:00','2019-08-30 23:59:59','Doctors');

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `price` float DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `content` longtext COLLATE utf8_unicode_ci,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `download` int(4) DEFAULT NULL,
  `views` int(4) DEFAULT NULL,
  `lang` char(10) COLLATE utf8_unicode_ci DEFAULT 'vi',
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `slug` (`slug`),
  KEY `status` (`status`),
  KEY `created_at` (`created_at`),
  KEY `download` (`download`),
  KEY `views` (`views`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `products` */

insert  into `products`(`id`,`title`,`slug`,`status`,`price`,`description`,`content`,`avatar`,`created_at`,`updated_at`,`created_by`,`download`,`views`,`lang`) values (1,'PACK 05','pack-05',1,432,'Description for PACK 05','<p><br />\r\nChỉ cần nhắc đến hoa th&ocirc;i l&agrave; người ta nghĩ ngay đến c&aacute;i đẹp. V&agrave; đ&uacute;ng vậy. Mỗi một lo&agrave;i hoa đều mang một vẻ đẹp ri&ecirc;ng m&agrave; kh&ocirc;ng thể đem sao s&aacute;nh với những lo&agrave;i hoa kh&aacute;c được. V&agrave; đương nhi&ecirc;n, những lo&agrave;i hoa đẹp ấy đều mang trong m&igrave;nh những &yacute; nghĩa, những biểu tượng ri&ecirc;ng v&agrave; được sử dụng trong nhiều trường hợp kh&aacute;c nhau.</p>\r\n\r\n<p><br />\r\nNhắc đến anh đ&agrave;o hầu như ai trong ch&uacute;ng ta đều biết đ&acirc;y l&agrave; lo&agrave;i hoa đến từ v&ugrave;ng đất Nhật Bản v&agrave; n&oacute; nở nhiều nhất v&agrave;o thời điểm đầu th&aacute;ng gi&ecirc;ng. Đ&acirc;y cũng ch&iacute;nh l&agrave; lo&agrave;i hoa mang biểu tượng cho đất nước mặt trời mọc. N&oacute; thường được xuất hiện trong c&aacute;c lễ hội trong v&agrave; ngo&agrave;i nước bởi m&agrave;u sắc của n&oacute; mang một vẻ đẹp v&ocirc; c&ugrave;ng dễ thương.<br />\r\n&nbsp;</p>','1566028386hinh-anh-hoa-dep-640-427-0-16.jpg','2019-07-13 15:39:53','2019-07-13 15:39:56',1,21,21,'vi'),(2,'PACK 04','pack-04',1,322,'Description for PACK 04','<p>Chỉ cần nhắc đến hoa th&ocirc;i l&agrave; người ta nghĩ ngay đến c&aacute;i đẹp. V&agrave; đ&uacute;ng vậy. Mỗi một lo&agrave;i hoa đều mang một vẻ đẹp ri&ecirc;ng m&agrave; kh&ocirc;ng thể đem sao s&aacute;nh với những lo&agrave;i hoa kh&aacute;c được. V&agrave; đương nhi&ecirc;n, những lo&agrave;i hoa đẹp ấy đều mang trong m&igrave;nh những &yacute; nghĩa, những biểu tượng ri&ecirc;ng v&agrave; được sử dụng trong nhiều trường hợp kh&aacute;c nhau.</p>\r\n\r\n<p><br />\r\nNhắc đến anh đ&agrave;o hầu như ai trong ch&uacute;ng ta đều biết đ&acirc;y l&agrave; lo&agrave;i hoa đến từ v&ugrave;ng đất Nhật Bản v&agrave; n&oacute; nở nhiều nhất v&agrave;o thời điểm đầu th&aacute;ng gi&ecirc;ng. Đ&acirc;y cũng ch&iacute;nh l&agrave; lo&agrave;i hoa mang biểu tượng cho đất nước mặt trời mọc. N&oacute; thường được xuất hiện trong c&aacute;c lễ hội trong v&agrave; ngo&agrave;i nước bởi m&agrave;u sắc của n&oacute; mang một vẻ đẹp v&ocirc; c&ugrave;ng dễ thương.</p>','1566028355hoa-dep-638-425-2-0.jpg',NULL,NULL,NULL,NULL,NULL,'vi'),(3,'PACK 03','pack-03',1,324,'Description for PACK 03','<p>Chỉ cần nhắc đến hoa th&ocirc;i l&agrave; người ta nghĩ ngay đến c&aacute;i đẹp. V&agrave; đ&uacute;ng vậy. Mỗi một lo&agrave;i hoa đều mang một vẻ đẹp ri&ecirc;ng m&agrave; kh&ocirc;ng thể đem sao s&aacute;nh với những lo&agrave;i hoa kh&aacute;c được. V&agrave; đương nhi&ecirc;n, những lo&agrave;i hoa đẹp ấy đều mang trong m&igrave;nh những &yacute; nghĩa, những biểu tượng ri&ecirc;ng v&agrave; được sử dụng trong nhiều trường hợp kh&aacute;c nhau.</p>\r\n\r\n<p><br />\r\nNhắc đến anh đ&agrave;o hầu như ai trong ch&uacute;ng ta đều biết đ&acirc;y l&agrave; lo&agrave;i hoa đến từ v&ugrave;ng đất Nhật Bản v&agrave; n&oacute; nở nhiều nhất v&agrave;o thời điểm đầu th&aacute;ng gi&ecirc;ng. Đ&acirc;y cũng ch&iacute;nh l&agrave; lo&agrave;i hoa mang biểu tượng cho đất nước mặt trời mọc. N&oacute; thường được xuất hiện trong c&aacute;c lễ hội trong v&agrave; ngo&agrave;i nước bởi m&agrave;u sắc của n&oacute; mang một vẻ đẹp v&ocirc; c&ugrave;ng dễ thương.</p>','1566028336images-(1)-274-183-0-0.jpg',NULL,NULL,NULL,NULL,NULL,'vi'),(4,'fdfPACK 02','fdfpack-02',1,54,'Description for PACK 02','<p><br />\r\nChỉ cần nhắc đến hoa th&ocirc;i l&agrave; người ta nghĩ ngay đến c&aacute;i đẹp. V&agrave; đ&uacute;ng vậy. Mỗi một lo&agrave;i hoa đều mang một vẻ đẹp ri&ecirc;ng m&agrave; kh&ocirc;ng thể đem sao s&aacute;nh với những lo&agrave;i hoa kh&aacute;c được. V&agrave; đương nhi&ecirc;n, những lo&agrave;i hoa đẹp ấy đều mang trong m&igrave;nh những &yacute; nghĩa, những biểu tượng ri&ecirc;ng v&agrave; được sử dụng trong nhiều trường hợp kh&aacute;c nhau.</p>\r\n\r\n<p><br />\r\nNhắc đến anh đ&agrave;o hầu như ai trong ch&uacute;ng ta đều biết đ&acirc;y l&agrave; lo&agrave;i hoa đến từ v&ugrave;ng đất Nhật Bản v&agrave; n&oacute; nở nhiều nhất v&agrave;o thời điểm đầu th&aacute;ng gi&ecirc;ng. Đ&acirc;y cũng ch&iacute;nh l&agrave; lo&agrave;i hoa mang biểu tượng cho đất nước mặt trời mọc. N&oacute; thường được xuất hiện trong c&aacute;c lễ hội trong v&agrave; ngo&agrave;i nước bởi m&agrave;u sắc của n&oacute; mang một vẻ đẹp v&ocirc; c&ugrave;ng dễ thương.</p>','1566028307images-272-181-6-0.jpg',NULL,NULL,1,NULL,NULL,'vi'),(5,'PACK 01','pack-01',1,453,'Description for PACK 01','<p><br />\r\nChỉ cần nhắc đến hoa th&ocirc;i l&agrave; người ta nghĩ ngay đến c&aacute;i đẹp. V&agrave; đ&uacute;ng vậy. Mỗi một lo&agrave;i hoa đều mang một vẻ đẹp ri&ecirc;ng m&agrave; kh&ocirc;ng thể đem sao s&aacute;nh với những lo&agrave;i hoa kh&aacute;c được. V&agrave; đương nhi&ecirc;n, những lo&agrave;i hoa đẹp ấy đều mang trong m&igrave;nh những &yacute; nghĩa, những biểu tượng ri&ecirc;ng v&agrave; được sử dụng trong nhiều trường hợp kh&aacute;c nhau.</p>\r\n\r\n<p><br />\r\nNhắc đến anh đ&agrave;o hầu như ai trong ch&uacute;ng ta đều biết đ&acirc;y l&agrave; lo&agrave;i hoa đến từ v&ugrave;ng đất Nhật Bản v&agrave; n&oacute; nở nhiều nhất v&agrave;o thời điểm đầu th&aacute;ng gi&ecirc;ng. Đ&acirc;y cũng ch&iacute;nh l&agrave; lo&agrave;i hoa mang biểu tượng cho đất nước mặt trời mọc. N&oacute; thường được xuất hiện trong c&aacute;c lễ hội trong v&agrave; ngo&agrave;i nước bởi m&agrave;u sắc của n&oacute; mang một vẻ đẹp v&ocirc; c&ugrave;ng dễ thương.</p>','1566028287images-(2)-275-183-0-0.jpg',NULL,NULL,1,NULL,NULL,'vi'),(6,'PACK 06','pack-06',1,455,'Description for PACK 06','<p>Chỉ cần nhắc đến hoa th&ocirc;i l&agrave; người ta nghĩ ngay đến c&aacute;i đẹp. V&agrave; đ&uacute;ng vậy. Mỗi một lo&agrave;i hoa đều mang một vẻ đẹp ri&ecirc;ng m&agrave; kh&ocirc;ng thể đem sao s&aacute;nh với những lo&agrave;i hoa kh&aacute;c được. V&agrave; đương nhi&ecirc;n, những lo&agrave;i hoa đẹp ấy đều mang trong m&igrave;nh những &yacute; nghĩa, những biểu tượng ri&ecirc;ng v&agrave; được sử dụng trong nhiều trường hợp kh&aacute;c nhau.<br />\r\n&nbsp;</p>\r\n\r\n<p>Nhắc đến anh đ&agrave;o hầu như ai trong ch&uacute;ng ta đều biết đ&acirc;y l&agrave; lo&agrave;i hoa đến từ v&ugrave;ng đất Nhật Bản v&agrave; n&oacute; nở nhiều nhất v&agrave;o thời điểm đầu th&aacute;ng gi&ecirc;ng. Đ&acirc;y cũng ch&iacute;nh l&agrave; lo&agrave;i hoa mang biểu tượng cho đất nước mặt trời mọc. N&oacute; thường được xuất hiện trong c&aacute;c lễ hội trong v&agrave; ngo&agrave;i nước bởi m&agrave;u sắc của n&oacute; mang một vẻ đẹp v&ocirc; c&ugrave;ng dễ thương.</p>','1566028253images-(3)-194-129-0-73.jpg',NULL,NULL,1,NULL,NULL,'vi'),(7,'PACK 07','pack-07',1,671,'Description for PACK 07','<p>Chỉ cần nhắc đến hoa th&ocirc;i l&agrave; người ta nghĩ ngay đến c&aacute;i đẹp. V&agrave; đ&uacute;ng vậy. Mỗi một lo&agrave;i hoa đều mang một vẻ đẹp ri&ecirc;ng m&agrave; kh&ocirc;ng thể đem sao s&aacute;nh với những lo&agrave;i hoa kh&aacute;c được. V&agrave; đương nhi&ecirc;n, những lo&agrave;i hoa đẹp ấy đều mang trong m&igrave;nh những &yacute; nghĩa, những biểu tượng ri&ecirc;ng v&agrave; được sử dụng trong nhiều trường hợp kh&aacute;c nhau.<br />\r\n&nbsp;</p>\r\n\r\n<p>Nhắc đến anh đ&agrave;o hầu như ai trong ch&uacute;ng ta đều biết đ&acirc;y l&agrave; lo&agrave;i hoa đến từ v&ugrave;ng đất Nhật Bản v&agrave; n&oacute; nở nhiều nhất v&agrave;o thời điểm đầu th&aacute;ng gi&ecirc;ng. Đ&acirc;y cũng ch&iacute;nh l&agrave; lo&agrave;i hoa mang biểu tượng cho đất nước mặt trời mọc. N&oacute; thường được xuất hiện trong c&aacute;c lễ hội trong v&agrave; ngo&agrave;i nước bởi m&agrave;u sắc của n&oacute; mang một vẻ đẹp v&ocirc; c&ugrave;ng dễ thương.</p>','1566028164images-(4)-259-173-0-21.jpg',NULL,NULL,1,NULL,NULL,'vi'),(8,'PACK 08','pack-08',1,234,'Description for PACK 08','<p>Chỉ cần nhắc đến hoa th&ocirc;i l&agrave; người ta nghĩ ngay đến c&aacute;i đẹp. V&agrave; đ&uacute;ng vậy. Mỗi một lo&agrave;i hoa đều mang một vẻ đẹp ri&ecirc;ng m&agrave; kh&ocirc;ng thể đem sao s&aacute;nh với những lo&agrave;i hoa kh&aacute;c được. V&agrave; đương nhi&ecirc;n, những lo&agrave;i hoa đẹp ấy đều mang trong m&igrave;nh những &yacute; nghĩa, những biểu tượng ri&ecirc;ng v&agrave; được sử dụng trong nhiều trường hợp kh&aacute;c nhau.<br />\r\n&nbsp;</p>\r\n\r\n<p>Nhắc đến anh đ&agrave;o hầu như ai trong ch&uacute;ng ta đều biết đ&acirc;y l&agrave; lo&agrave;i hoa đến từ v&ugrave;ng đất Nhật Bản v&agrave; n&oacute; nở nhiều nhất v&agrave;o thời điểm đầu th&aacute;ng gi&ecirc;ng. Đ&acirc;y cũng ch&iacute;nh l&agrave; lo&agrave;i hoa mang biểu tượng cho đất nước mặt trời mọc. N&oacute; thường được xuất hiện trong c&aacute;c lễ hội trong v&agrave; ngo&agrave;i nước bởi m&agrave;u sắc của n&oacute; mang một vẻ đẹp v&ocirc; c&ugrave;ng dễ thương.<br />\r\n&nbsp;</p>','1566027627images-2-259-173-0-21.jpg',NULL,NULL,1,NULL,NULL,'vi');

/*Table structure for table `role_user` */

DROP TABLE IF EXISTS `role_user`;

CREATE TABLE `role_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `role_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `role_user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `role_user_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `role_user` */

insert  into `role_user`(`id`,`user_id`,`role_id`) values (243,3,10),(244,2,11),(245,1,10),(246,4,11);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`description`,`status`) values (10,'Admin',NULL,1),(11,'Member',NULL,1),(12,'Biên tập viên','Người viết bài',1);

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `settings` */

insert  into `settings`(`id`,`type`,`content`) values (1,'lockscreen','[{\"timeout\":\"80\"}]'),(2,'tact-time',NULL),(4,'socials','{\"line_1\":{\"position\":\"\",\"class\":\"53454\",\"link\":\"/face\"},\"line_3\":{\"position\":\"\",\"class\":\"google\",\"link\":\"/go\"}}'),(6,'home-menu-top','[{\"id\":1,\"title\":\"Home\",\"url\":\"\\/\",\"parent\":0},{\"id\":2,\"title\":\"Services\",\"url\":\"\\/services\",\"parent\":0},{\"id\":3,\"title\":\"About\\/Contact\",\"url\":\"\\/about\",\"parent\":0},{\"id\":4,\"title\":\"About us\",\"url\":\"\\/about-us\",\"parent\":3},{\"id\":5,\"title\":\"Careers\",\"url\":\"\\/careers\",\"parent\":3},{\"id\":6,\"title\":\"Contact\",\"url\":\"\\/contact\",\"parent\":3},{\"id\":7,\"title\":\"News\",\"url\":\"\\/news\",\"parent\":0},{\"id\":8,\"title\":\"UserCP\",\"url\":\"\\/user\\/profile\",\"parent\":0}]'),(7,'home-menu-bottom','[{\"id\":1,\"title\":\"Home\",\"url\":\"\\/\",\"parent\":0},{\"id\":2,\"title\":\"Services\",\"url\":\"\\/services\",\"parent\":0},{\"id\":3,\"title\":\"About\\/Contact\",\"url\":\"\\/about\",\"parent\":0},{\"id\":4,\"title\":\"About us\",\"url\":\"\\/about-us\",\"parent\":3},{\"id\":5,\"title\":\"Careers\",\"url\":\"\\/careers\",\"parent\":3},{\"id\":6,\"title\":\"Contact\",\"url\":\"\\/contact\",\"parent\":3},{\"id\":7,\"title\":\"News\",\"url\":\"\\/news\",\"parent\":0},{\"id\":8,\"title\":\"UserCP\",\"url\":\"\\/user\\/profile\",\"parent\":0}]'),(8,'home-menu-top-en','[{\"id\":1,\"title\":\"Home\",\"url\":\"\\/\",\"parent\":0},{\"id\":2,\"title\":\"Services\",\"url\":\"\\/services\",\"parent\":0},{\"id\":3,\"title\":\"About\\/Contact\",\"url\":\"\\/about\",\"parent\":0},{\"id\":4,\"title\":\"About us\",\"url\":\"\\/about-us\",\"parent\":3},{\"id\":5,\"title\":\"Careers\",\"url\":\"\\/careers\",\"parent\":3},{\"id\":6,\"title\":\"Contact\",\"url\":\"\\/contact\",\"parent\":3},{\"id\":7,\"title\":\"News\",\"url\":\"\\/news\",\"parent\":0},{\"id\":8,\"title\":\"UserCP\",\"url\":\"\\/user\\/profile\",\"parent\":0}]'),(9,'home-menu-bottom-en','[{\"id\":1,\"title\":\"Home\",\"url\":\"\\/\",\"parent\":0},{\"id\":2,\"title\":\"Services\",\"url\":\"\\/services\",\"parent\":0},{\"id\":3,\"title\":\"About\\/Contact\",\"url\":\"\\/about\",\"parent\":0},{\"id\":4,\"title\":\"About us\",\"url\":\"\\/about-us\",\"parent\":3},{\"id\":5,\"title\":\"Careers\",\"url\":\"\\/careers\",\"parent\":3},{\"id\":6,\"title\":\"Contact\",\"url\":\"\\/contact\",\"parent\":3},{\"id\":7,\"title\":\"News\",\"url\":\"\\/news\",\"parent\":0},{\"id\":8,\"title\":\"UserCP\",\"url\":\"\\/user\\/profile\",\"parent\":0}]'),(11,'banner-home','{\"title\":\"What is ketox\",\"image\":\"f5652f589c624084169e4c236d70239c.jpg\",\"link\":\"\\/\",\"description\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.\"}'),(13,'banner-home-en','{\"title\":\"What is ketox\",\"image\":\"31ddf19d4104f6e7c2f63a58f0480fd2.jpg\",\"link\":\"\\/\",\"description\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.\"}'),(14,'about-page','{\"image\":\"634776cd2e0f476ca783d25c6f963bea.jpg\",\"description_left\":\"B\\u00ean c\\u1ea1nh nh\\u1eefng nh\\u00e2n v\\u1eadt ch\\u00ednh l\\u00e0 290 t\\u00e2n sinh vi\\u00ean, kh\\u00f4ng th\\u1ec3 kh\\u00f4ng nh\\u1eafc t\\u1edbi c\\u00e1c th\\u1ea7y c\\u00f4 s\\u1ebd tr\\u1ef1c ti\\u1ebfp d\\u00ecu d\\u1eaft c\\u00e1c em trong nh\\u1eefng n\\u0103m h\\u1ecdc t\\u1edbi. Ch\\u1ee7 nhi\\u1ec7m l\\u1edbp PC 1505, c\\u00f4 Th\\u00fay H\\u1eb1ng - gi\\u1ea3ng vi\\u00ean m\\u00f4n Ng\\u1eef v\\u0103n cho bi\\u1ebft b\\u1ea3n th\\u00e2n c\\u00f3 ch\\u00fat lo l\\u1eafng khi ch\\u01b0a bao gi\\u1edd l\\u00e0m gi\\u00e1o vi\\u00ean ch\\u1ee7 nhi\\u1ec7m: \\\"Sau 1 tu\\u1ea7n c\\u1ee7a kh\\u00f3a \\u0111\\u1ecbnh h\\u01b0\\u1edbng, t\\u00f4i th\\u1ea5y r\\u1eb1ng s\\u1ef1 t\\u00edch c\\u1ef1c c\\u1ee7a gi\\u00e1o vi\\u00ean c\\u00f3 th\\u1ec3 truy\\u1ec1n qua cho c\\u00e1c b\\u1ea1n. \\u0110\\u00f4i khi k\\u1ef7 lu\\u1eadt \\u0111\\u01b0\\u1ee3c t\\u1ea1o n\\u00ean t\\u1eeb ch\\u00ednh s\\u1ef1 ni\\u1ec1m n\\u1edf, t\\u00f4n tr\\u1ecdng, c\\u1edfi m\\u1edf c\\u1ee7a gi\\u00e1o vi\\u00ean v\\u1edbi sinh vi\\u00ean. Th\\u1ef1c t\\u1ebf, \\u1edf \\u0111\\u1ed9 tu\\u1ed5i c\\u1ee7a c\\u00e1c b\\u1ea1n hi\\u1ec7n t\\u1ea1i s\\u1ebd c\\u00f3 nhi\\u1ec1u v\\u1ea5n \\u0111\\u1ec1 v\\u1ec1 m\\u1eb7t giao ti\\u1ebfp, quan \\u0111i\\u1ec3m s\\u1ed1ng, v\\u1ed1n s\\u1ed1ng. Nh\\u01b0ng t\\u00f4i tin r\\u1eb1ng s\\u1ef1 nhi\\u1ec7t t\\u00e2m c\\u1ee7a gi\\u00e1o vi\\u00ean l\\u00e0 ph\\u01b0\\u01a1ng ph\\u00e1p hi\\u1ec7u qu\\u1ea3 nh\\u1ea5t \\u0111\\u1ec3 gi\\u00fap c\\u00e1c b\\u1ea1n ti\\u1ebfn b\\u1ed9\\\".\",\"description_right\":\"B\\u00ean c\\u1ea1nh nh\\u1eefng nh\\u00e2n v\\u1eadt ch\\u00ednh l\\u00e0 290 t\\u00e2n sinh vi\\u00ean, kh\\u00f4ng th\\u1ec3 kh\\u00f4ng nh\\u1eafc t\\u1edbi c\\u00e1c th\\u1ea7y c\\u00f4 s\\u1ebd tr\\u1ef1c ti\\u1ebfp d\\u00ecu d\\u1eaft c\\u00e1c em trong nh\\u1eefng n\\u0103m h\\u1ecdc t\\u1edbi. Ch\\u1ee7 nhi\\u1ec7m l\\u1edbp PC 1505, c\\u00f4 Th\\u00fay H\\u1eb1ng - gi\\u1ea3ng vi\\u00ean m\\u00f4n Ng\\u1eef v\\u0103n cho bi\\u1ebft b\\u1ea3n th\\u00e2n c\\u00f3 ch\\u00fat lo l\\u1eafng khi ch\\u01b0a bao gi\\u1edd l\\u00e0m gi\\u00e1o vi\\u00ean ch\\u1ee7 nhi\\u1ec7m: \\\"Sau 1 tu\\u1ea7n c\\u1ee7a kh\\u00f3a \\u0111\\u1ecbnh h\\u01b0\\u1edbng, t\\u00f4i th\\u1ea5y r\\u1eb1ng s\\u1ef1 t\\u00edch c\\u1ef1c c\\u1ee7a gi\\u00e1o vi\\u00ean c\\u00f3 th\\u1ec3 truy\\u1ec1n qua cho c\\u00e1c b\\u1ea1n. \\u0110\\u00f4i khi k\\u1ef7 lu\\u1eadt \\u0111\\u01b0\\u1ee3c t\\u1ea1o n\\u00ean t\\u1eeb ch\\u00ednh s\\u1ef1 ni\\u1ec1m n\\u1edf, t\\u00f4n tr\\u1ecdng, c\\u1edfi m\\u1edf c\\u1ee7a gi\\u00e1o vi\\u00ean v\\u1edbi sinh vi\\u00ean. Th\\u1ef1c t\\u1ebf, \\u1edf \\u0111\\u1ed9 tu\\u1ed5i c\\u1ee7a c\\u00e1c b\\u1ea1n hi\\u1ec7n t\\u1ea1i s\\u1ebd c\\u00f3 nhi\\u1ec1u v\\u1ea5n \\u0111\\u1ec1 v\\u1ec1 m\\u1eb7t giao ti\\u1ebfp, quan \\u0111i\\u1ec3m s\\u1ed1ng, v\\u1ed1n s\\u1ed1ng. Nh\\u01b0ng t\\u00f4i tin r\\u1eb1ng s\\u1ef1 nhi\\u1ec7t t\\u00e2m c\\u1ee7a gi\\u00e1o vi\\u00ean l\\u00e0 ph\\u01b0\\u01a1ng ph\\u00e1p hi\\u1ec7u qu\\u1ea3 nh\\u1ea5t \\u0111\\u1ec3 gi\\u00fap c\\u00e1c b\\u1ea1n ti\\u1ebfn b\\u1ed9111\\\".\",\"careers\":[\"5\",\"6\"]}'),(15,'about-page-en','{\"image\":\"22b82b7649f4d84e08549f123e8a41f8.jpg\",\"description_left\":\"B\\u00ean c\\u1ea1nh nh\\u1eefng nh\\u00e2n v\\u1eadt ch\\u00ednh l\\u00e0 290 t\\u00e2n sinh vi\\u00ean, kh\\u00f4ng th\\u1ec3 kh\\u00f4ng nh\\u1eafc t\\u1edbi c\\u00e1c th\\u1ea7y c\\u00f4 s\\u1ebd tr\\u1ef1c ti\\u1ebfp d\\u00ecu d\\u1eaft c\\u00e1c em trong nh\\u1eefng n\\u0103m h\\u1ecdc t\\u1edbi. Ch\\u1ee7 nhi\\u1ec7m l\\u1edbp PC 1505, c\\u00f4 Th\\u00fay H\\u1eb1ng - gi\\u1ea3ng vi\\u00ean m\\u00f4n Ng\\u1eef v\\u0103n cho bi\\u1ebft b\\u1ea3n th\\u00e2n c\\u00f3 ch\\u00fat lo l\\u1eafng khi ch\\u01b0a bao gi\\u1edd l\\u00e0m gi\\u00e1o vi\\u00ean ch\\u1ee7 nhi\\u1ec7m: \\\"Sau 1 tu\\u1ea7n c\\u1ee7a kh\\u00f3a \\u0111\\u1ecbnh h\\u01b0\\u1edbng, t\\u00f4i th\\u1ea5y r\\u1eb1ng s\\u1ef1 t\\u00edch c\\u1ef1c c\\u1ee7a gi\\u00e1o vi\\u00ean c\\u00f3 th\\u1ec3 truy\\u1ec1n qua cho c\\u00e1c b\\u1ea1n. \\u0110\\u00f4i khi k\\u1ef7 lu\\u1eadt \\u0111\\u01b0\\u1ee3c t\\u1ea1o n\\u00ean t\\u1eeb ch\\u00ednh s\\u1ef1 ni\\u1ec1m n\\u1edf, t\\u00f4n tr\\u1ecdng, c\\u1edfi m\\u1edf c\\u1ee7a gi\\u00e1o vi\\u00ean v\\u1edbi sinh vi\\u00ean. Th\\u1ef1c t\\u1ebf, \\u1edf \\u0111\\u1ed9 tu\\u1ed5i c\\u1ee7a c\\u00e1c b\\u1ea1n hi\\u1ec7n t\\u1ea1i s\\u1ebd c\\u00f3 nhi\\u1ec1u v\\u1ea5n \\u0111\\u1ec1 v\\u1ec1 m\\u1eb7t giao ti\\u1ebfp, quan \\u0111i\\u1ec3m s\\u1ed1ng, v\\u1ed1n s\\u1ed1ng. Nh\\u01b0ng t\\u00f4i tin r\\u1eb1ng s\\u1ef1 nhi\\u1ec7t t\\u00e2m c\\u1ee7a gi\\u00e1o vi\\u00ean l\\u00e0 ph\\u01b0\\u01a1ng ph\\u00e1p hi\\u1ec7u qu\\u1ea3 nh\\u1ea5t \\u0111\\u1ec3 gi\\u00fap c\\u00e1c b\\u1ea1n ti\\u1ebfn b\\u1ed9\\\".\",\"description_right\":\"B\\u00ean c\\u1ea1nh nh\\u1eefng nh\\u00e2n v\\u1eadt ch\\u00ednh l\\u00e0 290 t\\u00e2n sinh vi\\u00ean, kh\\u00f4ng th\\u1ec3 kh\\u00f4ng nh\\u1eafc t\\u1edbi c\\u00e1c th\\u1ea7y c\\u00f4 s\\u1ebd tr\\u1ef1c ti\\u1ebfp d\\u00ecu d\\u1eaft c\\u00e1c em trong nh\\u1eefng n\\u0103m h\\u1ecdc t\\u1edbi. Ch\\u1ee7 nhi\\u1ec7m l\\u1edbp PC 1505, c\\u00f4 Th\\u00fay H\\u1eb1ng - gi\\u1ea3ng vi\\u00ean m\\u00f4n Ng\\u1eef v\\u0103n cho bi\\u1ebft b\\u1ea3n th\\u00e2n c\\u00f3 ch\\u00fat lo l\\u1eafng khi ch\\u01b0a bao gi\\u1edd l\\u00e0m gi\\u00e1o vi\\u00ean ch\\u1ee7 nhi\\u1ec7m: \\\"Sau 1 tu\\u1ea7n c\\u1ee7a kh\\u00f3a \\u0111\\u1ecbnh h\\u01b0\\u1edbng, t\\u00f4i th\\u1ea5y r\\u1eb1ng s\\u1ef1 t\\u00edch c\\u1ef1c c\\u1ee7a gi\\u00e1o vi\\u00ean c\\u00f3 th\\u1ec3 truy\\u1ec1n qua cho c\\u00e1c b\\u1ea1n. \\u0110\\u00f4i khi k\\u1ef7 lu\\u1eadt \\u0111\\u01b0\\u1ee3c t\\u1ea1o n\\u00ean t\\u1eeb ch\\u00ednh s\\u1ef1 ni\\u1ec1m n\\u1edf, t\\u00f4n tr\\u1ecdng, c\\u1edfi m\\u1edf c\\u1ee7a gi\\u00e1o vi\\u00ean v\\u1edbi sinh vi\\u00ean. Th\\u1ef1c t\\u1ebf, \\u1edf \\u0111\\u1ed9 tu\\u1ed5i c\\u1ee7a c\\u00e1c b\\u1ea1n hi\\u1ec7n t\\u1ea1i s\\u1ebd c\\u00f3 nhi\\u1ec1u v\\u1ea5n \\u0111\\u1ec1 v\\u1ec1 m\\u1eb7t giao ti\\u1ebfp, quan \\u0111i\\u1ec3m s\\u1ed1ng, v\\u1ed1n s\\u1ed1ng. Nh\\u01b0ng t\\u00f4i tin r\\u1eb1ng s\\u1ef1 nhi\\u1ec7t t\\u00e2m c\\u1ee7a gi\\u00e1o vi\\u00ean l\\u00e0 ph\\u01b0\\u01a1ng ph\\u00e1p hi\\u1ec7u qu\\u1ea3 nh\\u1ea5t \\u0111\\u1ec3 gi\\u00fap c\\u00e1c b\\u1ea1n ti\\u1ebfn b\\u1ed9\\\".\",\"careers\":[\"5\",\"6\"]}'),(16,'contact-page','{\"map\":null,\"office\":\"15TH FLOOR CORNERSTONE BUILDING, 16 PHAN CHU TRINH, HANOI\",\"facebook\":\"www.facebook.com\\/ketoxvn\",\"instagram\":\"@ketoxvn copy\",\"mail\":\"questions@ketox.vn\",\"phone\":\"0933-653-869\"}'),(17,'contact-page-en','{\"map\":null,\"office\":\"15TH FLOOR CORNERSTONE BUILDING, 16 PHAN CHU TRINH, HANOI\",\"facebook\":\"www.facebook.com\\/ketoxvn\",\"instagram\":\"@ketoxvn copy\",\"mail\":\"questions@ketox.vn\",\"phone\":\"0933-653-869\"}'),(18,'banner-home-en','{\"title\":\"What is ketox\",\"image\":\"31ddf19d4104f6e7c2f63a58f0480fd2.jpg\",\"link\":\"\\/\",\"description\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.\"}'),(19,'banner-home-en','{\"title\":\"What is ketox\",\"image\":\"31ddf19d4104f6e7c2f63a58f0480fd2.jpg\",\"link\":\"\\/\",\"description\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.\"}'),(20,'banner-home-en','{\"title\":\"What is ketox\",\"image\":\"31ddf19d4104f6e7c2f63a58f0480fd2.jpg\",\"link\":\"\\/\",\"description\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.\"}'),(21,'banner-home-en','{\"title\":\"What is ketox\",\"image\":\"31ddf19d4104f6e7c2f63a58f0480fd2.jpg\",\"link\":\"\\/\",\"description\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.\"}'),(22,'banner-home-en','{\"title\":\"What is ketox\",\"image\":\"31ddf19d4104f6e7c2f63a58f0480fd2.jpg\",\"link\":\"\\/\",\"description\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.\"}');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birth_day` date DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data` longtext COLLATE utf8_unicode_ci,
  `address` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` tinyint(1) DEFAULT '1' COMMENT '1:male;2:female',
  `city_id` int(11) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `username` (`username`),
  KEY `full_name` (`full_name`),
  KEY `status` (`status`),
  KEY `created_at` (`created_at`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`username`,`full_name`,`email`,`password`,`birth_day`,`phone`,`status`,`avatar`,`remember_token`,`created_at`,`updated_at`,`data`,`address`,`gender`,`city_id`,`image`) values (1,'administrator','Admin','phidinhson1988@gmail.com','$2y$10$urXBe5GVMRh7V9GNHK6u3.x9vLM4bmVEvoxK1T7p6h4DKHaVdXPRW',NULL,'0966426437',1,'1566029660images-181-181-97-0.jpg','45mfLdcC2VP05mlmouqlCcVUiQoPSWzy47Su3O2H6z0dSKnflc3jtG4b8rB4','2019-07-11 06:46:00','2019-08-17 15:50:17','{\"weight_1\":\"12\",\"weight_2\":\"112\",\"waist_2\":\"22\",\"waist_1\":\"33\",\"belly_1\":\"44\",\"belly_2\":\"55\",\"from\":\"01-08-2019\",\"to\":\"29-08-2019\"}',NULL,1,NULL,NULL),(2,'lena','Lê Na','lena@gmail.com','$2y$10$j2nBqaTTy8.w8a6VD5q/YOq9NLsymHK8ISpnzqsFtMmIDW7.ULnbK',NULL,'09001001',1,'a756b08266b76e8164da80627fe4384f-178-178-0-0.jpg',NULL,'2019-07-13 15:29:34','2019-07-13 15:29:34',NULL,NULL,1,NULL,NULL),(3,'sonpd1','Phí Đình Sơn','son1@gmail.com','$2y$10$C3B3MP0lGYvsH4WRznc8f.4TKyzL7t0vqZ08DMXw3q7luPkwMHq0S',NULL,'09664264371',1,'15660295475bb5ac4b1d29fa77a338-1565939439_180x108-108-108-36-0.jpg',NULL,'2019-07-31 19:37:21','2019-07-31 19:37:21',NULL,NULL,1,NULL,NULL),(4,'tiendv','Đỗ Văn Tiền','tien@gmail.com','$2y$10$KAxB0UInrIM2wGfIYavrMu16ge41wC39rVKimpKlcOFQXRQQzXXmy',NULL,'09001001',1,'1566029811download-224-224-1-0.jpg',NULL,'2019-08-17 14:12:02','2019-08-17 14:15:46','{\"weight_1\":\"1\",\"waist_1\":\"2\",\"waist_2\":\"3\",\"weight_2\":\"4\",\"belly_1\":\"5\",\"belly_2\":\"3\",\"hips_1\":\"2\"}','sasasa',2,NULL,'1566025917download-(1)-170-284-0-0.jpg');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
