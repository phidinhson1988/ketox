ALTER TABLE `posts` CHANGE `image` `avatar` VARCHAR(255) CHARSET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `posts` ADD COLUMN `lang` CHAR(10) DEFAULT 'vi' NULL AFTER `type`; 
ALTER TABLE `products` ADD COLUMN `lang` CHAR(10) DEFAULT 'vi' NULL AFTER `views`; 

ALTER TABLE `posts` ADD COLUMN `start_date` TIMESTAMP NULL AFTER `lang`, ADD COLUMN `end_date` TIMESTAMP NULL AFTER `start_date`, ADD COLUMN `job` VARCHAR(255) NULL AFTER `end_date`;

CREATE TABLE `contacts`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `name` VARCHAR(255), `email` VARCHAR(255), `message` TEXT, `subject` VARCHAR(255), `created_at` TIMESTAMP, `status` TINYINT(1) DEFAULT 0 COMMENT '0: chưa sử lý; 1: đã sử lý', PRIMARY KEY (`id`), INDEX (`name`), INDEX (`email`), INDEX (`subject`), INDEX (`status`), INDEX (`created_at`) ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 

CREATE TABLE `city`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `city` VARCHAR(255), `country` VARCHAR(255), `key_country` VARCHAR(255), PRIMARY KEY (`id`), INDEX (`key_country`) ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 

ALTER TABLE `users` ADD COLUMN `gender` TINYINT(1) DEFAULT 1 NULL COMMENT '1: male; 2: female' AFTER `address`, ADD COLUMN `city_id` INT(11) NULL AFTER `gender`; 
/* update 15/08 */
ALTER TABLE `users` ADD COLUMN `image` VARCHAR(255) NULL AFTER `city_id`; 

ALTER TABLE `users` ADD COLUMN `job` VARCHAR(255) NULL AFTER `image`; 

/* update 06/11 */
ALTER TABLE `contacts` ADD COLUMN `type` TINYINT(1) DEFAULT 1 NULL COMMENT '1: contact; 2: newletter' AFTER `status`;

ALTER TABLE `products` ADD COLUMN `sort` INT(11) DEFAULT 1 NULL AFTER `lang`;