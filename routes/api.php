<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['as' => 'api.'], function () {
    Route::get('/get-document/{id?}', 'ApiController@getDocument')->name('getDocument');
    Route::get('/get-approve-detail/{id?}', 'ApiController@getApproveDetail')->name('getApproveDetail');
    Route::get('/get-comment-file/{cid?}', 'ApiController@getCommentFiles')->name('getCommentFile');
    Route::get('get-comments/{id?}/{revision?}', 'ApiController@getDocumentComments')->name('getComments');
    Route::post('iso-code/find', 'ApiController@findIsoCode')->name('findIsoCode');
    Route::post('/check-report', 'ApiController@checkReport')->name('checkReport');
    Route::post('/converst-excel', 'ApiController@converstExcel')->name('converstExcel');
    Route::get('/stop-converst-excel', 'ApiController@stopConverstExcel')->name('stopConverstExcel');
});

Route::get('/get-type-by-group/{group_id}', 'ApiController@getTypeByGroup');
Route::post('login', 'ApiController@login');
Route::post('signup', 'ApiController@register');

Route::group(['middleware' => 'auth:api'], function() {
    Route::group(['prefix' => 'mobile', 'as' => 'mobile.'], function () {
        Route::get('logout', 'ApiController@logout');
        Route::get('user', 'ApiController@user');
        Route::post('update-user', 'ApiController@updateUser');
        Route::post('/get-product-type', 'ApiController@getProductType')->name('getProductType');     
    });
});