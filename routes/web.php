<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();
Route::get('/login', 'AuthController@index')->name('login')->middleware('login');
Route::get('/logout', 'AuthController@logout')->name('logout');
Route::post('/login', 'AuthController@postLogin')->name('postLogin');
Route::get('/lang/{option?}', 'HomeController@setLocale')->name('locale');

Route::group(['middleware' => array('auth', 'locale', 'request', 'admin'), 'prefix' => 'admin'], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/addRole', 'HomeController@addRole')->name('addRole');

    //QUAN LY NGUOI DUNG
    Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
        Route::get('/', 'UserController@index')->name('index')->middleware('permission:user-list');
        Route::get('/create', 'UserController@create')->name('create')->middleware('permission:user-create');
        Route::get('/profile/', 'UserController@profile')->name('profile');
        Route::get('/password', 'UserController@password')->name('password');
        Route::get('/edit/{id?}', 'UserController@edit')->name('edit')->middleware('permission:user-edit');
        Route::post('/store', 'UserController@store')->name('store')->middleware('permission:user-create');
        Route::post('/update/{id}', 'UserController@update')->name('update')->middleware('permission:user-edit');
        Route::delete('/delete/{id?}', 'UserController@delete')->name('delete')->middleware('permission:user-delete');
        Route::post('/profile/{id}', 'UserController@editProfile')->name('editProfile');
        Route::put('/password', 'UserController@postChangePassword')->name('postChangePassword');

    });
    //QUAN LY PHAN QUYEN
    Route::group(['prefix' => 'role', 'as' => 'role.'], function () {
        Route::get('/', 'RoleController@index')->name('index')->middleware('permission:role-list');
        Route::get('/create', 'RoleController@create')->name('create')->middleware('permission:role-create');
        Route::get('/edit/{id}', 'RoleController@edit')->name('edit')->middleware('permission:role-edit');
        Route::delete('/delete/{id?}', 'RoleController@delete')->name('delete')->middleware('permission:role-list');
        Route::patch('/update/{id}', 'RoleController@update')->name('update');
        Route::post('/store', 'RoleController@store')->name('store');
    });
    //QUAN LY CAU HINH
    Route::group(['prefix' => 'setting', 'as' => 'setting.'], function () {
        Route::get('/', 'SettingController@system')->name('system')->middleware('permission:config-view');
        Route::post('/post-system', 'SettingController@postSystem')->name('postSystem')->middleware('permission:config-view');
        Route::get('/menu-top', 'SettingController@menu')->name('menu')->middleware('permission:config-view');
        Route::get('/menu-bottom', 'SettingController@menuBottom')->name('menuBottom')->middleware('permission:config-view');
        Route::post('/save-menu', 'SettingController@saveMenu')->name('saveMenu')->middleware('permission:config-view');
        Route::post('/add-menu', 'SettingController@addMenu')->name('addMenu')->middleware('permission:config-view');
        Route::get('/delete-menu', 'SettingController@deleteMenu')->name('deleteMenu')->middleware('permission:config-view');
        Route::get('/socials', 'SettingController@socials')->name('socials')->middleware('permission:config-view');
        Route::post('/add-socials', 'SettingController@addSocial')->name('addSocial')->middleware('permission:config-view');
        Route::get('/about-page', 'SettingController@aboutPage')->name('aboutPage');
        Route::post('/save-about', 'SettingController@saveAbout')->name('saveAbout');
        Route::get('/contact-page', 'SettingController@contactPage')->name('contactPage');
        Route::post('/save-contact-page', 'SettingController@saveContactPage')->name('saveContactPage');
        Route::post('/add-chat', 'SettingController@addChat')->name('addChat');
        Route::post('/upload-editor', 'SettingController@uploadEditor')->name('uploadEditor');
    });
    
    //QUAN LY SAN PHAM
    Route::group(['prefix' => 'product', 'as' => 'product.'], function () {
        Route::get('/', 'ProductController@index')->name('index')->middleware('permission:product-list');
        Route::get('/create', 'ProductController@create')->name('create')->middleware('permission:product-create');
        Route::get('/edit/{id}', 'ProductController@edit')->name('edit')->middleware('permission:product-edit');
        Route::delete('/delete/{id?}', 'ProductController@delete')->name('delete')->middleware('permission:product-delete');
        Route::post('/update/{id}', 'ProductController@update')->name('update')->middleware('permission:product-edit');
        Route::post('/store', 'ProductController@store')->name('store')->middleware('permission:product-create');
        Route::post('/check-slug', 'ProductController@checkSlug')->name('checkSlug');
        
    });
    //QUAN LY TIN TUC
    Route::group(['prefix' => 'news', 'as' => 'news.'], function () {
        Route::get('/', 'NewsController@index')->name('index')->middleware('permission:news-list');
        Route::get('/create', 'NewsController@create')->name('create')->middleware('permission:news-create');
        Route::get('/edit/{id}', 'NewsController@edit')->name('edit')->middleware('permission:news-edit');
        Route::delete('/delete/{id?}', 'NewsController@delete')->name('delete')->middleware('permission:news-delete');
        Route::post('/update/{id}', 'NewsController@update')->name('update')->middleware('permission:news-edit');
        Route::post('/store', 'NewsController@store')->name('store')->middleware('permission:news-create');
        Route::post('/check-slug', 'NewsController@checkSlug')->name('checkSlug');
    });
    
        //QUAN LY Feed back
    Route::group(['prefix' => 'feedback', 'as' => 'feedback.'], function () {
        Route::get('/', 'FeedbackController@index')->name('index');
        Route::get('/create', 'FeedbackController@create')->name('create');
        Route::get('/edit/{id}', 'FeedbackController@edit')->name('edit');
        Route::delete('/delete/{id?}', 'FeedbackController@delete')->name('delete');
        Route::post('/update/{id}', 'FeedbackController@update')->name('update');
        Route::post('/store', 'FeedbackController@store')->name('store');
    });
        //QUAN LY mail
    Route::group(['prefix' => 'mail', 'as' => 'mail.'], function () {
        Route::get('/', 'FeedbackController@mail')->name('index');
        Route::get('/export-mail', 'FeedbackController@exportMail')->name('export');
    });
    
    //QUAN LY PAGE
    Route::group(['prefix' => 'page', 'as' => 'page.'], function () {
        Route::get('/', 'PageController@index')->name('index')->middleware('permission:page-list');
        Route::get('/create', 'PageController@create')->name('create')->middleware('permission:page-create');
        Route::get('/edit/{id}', 'PageController@edit')->name('edit')->middleware('permission:page-edit');
        Route::delete('/delete/{id?}', 'PageController@delete')->name('delete')->middleware('permission:page-delete');
        Route::patch('/update/{id}', 'PageController@update')->name('update')->middleware('permission:page-edit');
        Route::post('/store', 'PageController@store')->name('store')->middleware('permission:page-create');
    });
    //QUAN LY BAN HANG
    Route::group(['prefix' => 'cart', 'as' => 'cart.'], function () {
        Route::get('/', 'CartController@index')->name('index')->middleware('permission:cart-list');
        Route::delete('/delete/{id?}', 'CartController@delete')->name('delete')->middleware('permission:cart-delete');
    });
});
Route::post('/product/upload-file', 'ProductController@uploadFile')->name('admin.uploadFile');
Route::get('/product/crop-file-image', 'ProductController@cropFileImage')->name('admin.cropFileImage');

Route::group(['prefix' => '/', 'namespace' => 'Frontend'], function () {
    Route::get('/', 'DefaultController@landingPage')->name('frontend.landingPage');
    Route::get('/home', 'DefaultController@index')->name('frontend.home');
    Route::get('/contact-us', 'DefaultController@contactUs')->name('frontend.default.contactUs');
    Route::post('/subcribe-email', 'DefaultController@subcribeEmail')->name('frontend.default.subcribeEmail');
    Route::get('/about', 'DefaultController@about')->name('frontend.default.about');
    Route::post('/about/add-contact', 'DefaultController@addContact')->name('frontend.default.addContact');
    Route::get('/search', 'ProductController@search')->name('frontend.search');
    Route::get('/services', 'ProductController@index')->name('frontend.service.index');
    Route::get('/service/purchase', 'ProductController@purchase')->name('frontend.service.purchase');
    Route::get('/service/{slug}', 'ProductController@detail')->name('frontend.service.detail');
    Route::get('/news', 'NewsController@index')->name('frontend.news.index');
    Route::get('/news/{slug}', 'NewsController@detail')->name('frontend.news.detail');
    Route::get('/careers', 'NewsController@career')->name('frontend.news.career');
    Route::get('/careers/{slug}', 'NewsController@detailCareer')->name('frontend.news.detailCareer');
    Route::get('/category/{slug}', 'ProductController@category')->name('frontend.category');
    Route::post('/save-email', 'DefaultController@saveEmail')->name('frontend.saveEmail');
    Route::get('/download-file', 'ProductController@download')->name('frontend.download');
    Route::get('/contributor/{slug}', 'ProductController@contributor')->name('frontend.contributor');
    Route::get('/carts', 'ProductController@carts')->name('frontend.carts');
    Route::get('/list-carts', 'ProductController@carts')->name('frontend.listcarts');
    Route::get('/delete-cart', 'ProductController@deleteCart')->name('frontend.deleteCart');
    Route::post('/add-cart', 'ProductController@addCart')->name('frontend.addCart');
    // User
    Route::get('/user/profile', 'UserController@profile')->name('frontend.user.profile');
    Route::post('/user/check-login', 'DefaultController@checkLogin')->name('frontend.user.checkLogin');
    Route::get('/user/register', 'UserController@register')->name('frontend.user.register');
    Route::post('/user/saveRegister', 'UserController@saveRegister')->name('frontend.user.saveRegister');
    Route::get('/user/done', 'UserController@done')->name('frontend.user.done');
    Route::get('/user/report', 'UserController@report')->name('frontend.user.report');
    Route::get('/user/order-history', 'UserController@orderHistory')->name('frontend.user.orderHistory');
    Route::post('/user/saveReport', 'UserController@saveReport')->name('frontend.user.saveReport');
    Route::post('/user/change-pass', 'UserController@changePass')->name('frontend.user.changePass');
    Route::post('/user/add-feedback', 'UserController@addFeedback')->name('frontend.user.addFeedback');
});