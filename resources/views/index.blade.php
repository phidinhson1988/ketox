@extends('layouts.master')
@section('title', trans('message.trang_chu'))
@section('head_stylesheet')
    <link href="{{ asset('css/plugins/c3/c3.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<?php use App\Helpers\Facades\Tool;?>
<style>
    .loading-spinner-page {
        display: none !important;
        opacity: 0;
    }
</style>
    <div class="wrapper wrapper-content test-rows">
        <h1>Updating</h1>
    </div>
@stop
@section('body_script')
    @includeif('partials.message')
@endsection
