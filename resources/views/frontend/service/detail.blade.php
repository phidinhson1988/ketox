@extends('frontend.layouts.app')
@section('title', $product->title)
@section('content')
<?php 
use App\Helpers\Facades\Tool;?>
<div class="container">
    <div class="banner-home">
        <div class="row">
            <div class="col-12">
                <a href="#"><img src="<?php echo Tool::getImage($product->avatar, Tool::getConst('PRODUCT_UPLOAD'));?>" /></a>
            </div>
        </div>
    </div>

    <div class="service-content">
        <div class="content-product">
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-3">
                    <h1>{{$product->title}}</h1>
                </div>
                <div class="col-sm-12 col-md-8 col-lg-9">
                    <div class="row">
                        <div class="col-sm-12 col-md-2 col-lg-1">
                            <h3>{{trans('message.thong_tin')}}:</h3>
                        </div>
                        <div class="col-sm-12 col-md-10 col-lg-11">
                            <div class="content-detail">
                                <?php echo $product->content;?>
                            </div>
                            <div class="price-detail clearfix">
                                <div class="clearfix pull-left" style="margin-bottom: 15px;">
                                    <span class="price">{{$product->price}}</span>
                                    <span class="unit"><?php echo Tool::getConst('UNIT_PRICE');?></span>
                                </div>
                                <div class="clearfix pull-right">
                                    <form action="{{route('frontend.addCart')}}" method="POST">
                                        @csrf
                                        <input type="hidden" name="productId" value="{{$product->id}}" />
                                        <a href="javascript:void(0)" id="feedback-product" class="purchase" data-target="#feedback-modal" data-toggle="modal">{{trans('message.phan_hoi')}}</a>
                                        <button type="submit" class="purchase">{{trans('message.mua_hang')}}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="services-other">
        <div class="row">
            <div class="col-sm-12 col-md-3 col-lg-2">
                <h2>{{trans('message.dich_vu_khac')}}:</h2>
            </div>
            <div class="col-sm-12 col-md-9 col-lg-10">
                <ul class="row">
                    <?php if ($others->count() > 0) {
                        foreach ($others as $productItem) {?>
                        <li class="col-sm-12 col-md-6 col-lg-3">
                            <a href="{{route('frontend.service.detail', ['slug' => $productItem->slug])}}" class="image-service">
                                <img src="<?php echo Tool::getImage($productItem->avatar, Tool::getConst('PRODUCT_UPLOAD'));?>" />
                            </a>
                            <h3><a href="{{route('frontend.service.detail', ['slug' => $productItem->slug])}}">{{$productItem->title}}</a></h3>
                            <div class="des">{{$productItem->description}}</div>
                        </li>
                    <?php }
                    }?>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="feedback-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="margin-top: 100px;">
        <div class="modal-content animated fadeIn">
            <form id="feedback-form" action="{{route('frontend.user.addFeedback')}}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="urlPage" value="{{$urlNow}}">
                <input type="hidden" name="userId" value="{{$user ? $user->id : ''}}">
                <input type="hidden" name="productId" value="{{$product->id}}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="position: absolute; top: 5px; right: 10px;"><span aria-hidden="true">&times;</span></button>
                    <h4 style="color: #7b7e87" class="modal-title">{{trans('message.phan_hoi_dich_vu')}}:</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-12 col-form-label" style="font-size: 16px; color: #7b7e87">{{ trans('message.dich_vu') }}: <strong>{{$product->title}}</strong></label>
                        <label class="col-sm-12 col-form-label" style="font-size: 16px; color: #7b7e87">{{ trans('message.noi_dung_feedback') }} </label>
                        <div class="col-sm-12">
                            <textarea rows="4" name="description" required class="form-control" style="font-size: 16px; color: #7b7e87"
                                      placeholder="{{ trans('message.nhap_noi_dung') }}"></textarea>
                            <span class="help-block"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" style="font-size: 16px;" class="btn btn-info">{{ trans('message.luu_lai') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
@stop
@section('body_script')
    @includeif('partials.message')
    <?php if ($user) {?>
    <script>
        var userId = "{{$user->id}}";
    </script>
    <?php } else { ?>
    <script>
        var userId = "";
    </script>
    <?php } ?>
    <script>
        var productId = "{{$product->id}}";
        var urlPage = "{{$urlNow}}";
        var purchaseUrl = "{{route('frontend.service.purchase')}}";
        var checkLoginUrl = "{{route('frontend.user.checkLogin')}}";
        var loginUrl = "{{route('login')}}";
        checkLoginWeb();
        function checkLoginWeb(){
            $.ajax({
                url: checkLoginUrl,
                method: 'post',
                data: {userId: userId, url: urlPage, _token: $('#_token').val()},
                success: function(response) {
                    if (response.status) {
                        return true;
                    } else {
                        return false;
                    }
                }, error: function(){
                    return false;
                }
            });
        }
    </script>
@endsection
