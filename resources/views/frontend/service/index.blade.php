@extends('frontend.layouts.app')
@section('title', trans('message.dich_vu'))
@section('content')
<?php 
use App\Helpers\Facades\Tool;?>

    <div class="container">
        <div class="services-content">
            <div class="row">
                <div class="col-12">
                    <div class="service-title clearfix">
                        <h1>{{trans('message.dich_vu')}}</h1>
                        <form action="" method="GET">
                            <div class="filter-box">
                                <label>{{trans('message.sap_xep_theo')}}:</label>
                                <select name="sort" class="form-control auto-submit">
    <!--                                <option value="1"{{($sort == 1) ? ' selected' : ''}}>Title asc</option>
                                    <option value="2"{{($sort == 2) ? ' selected' : ''}}>Title desc</option>-->
                                    <option value="3"{{($sort == 3) ? ' selected' : ''}}>Giá từ cao đến thấp</option>
                                    <option value="4"{{($sort == 4) ? ' selected' : ''}}>Giá từ thấp đến cao</option>
                                    <option value="5"{{($sort == 5) ? ' selected' : ''}}>Mới nhất</option>
                                    <option value="6"{{($sort == 6) ? ' selected' : ''}}>Cũ nhất</option>
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="list-service">
                        <?php if ($services->count() > 0) {?>
                            <ul class="row">
                                <?php foreach ($services as $index => $service) {?>
                                <li class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-5 col-md-6">
                                            <a href="{{route('frontend.service.detail', ['slug' => $service->slug])}}" class="image-service">
                                                <img src="<?php echo Tool::getImage($service->avatar, Tool::getConst('PRODUCT_UPLOAD'));?>" />
                                            </a>
                                        </div>
                                        <div class="col-sm-7 col-md-6">
                                            <h3><a href="{{route('frontend.service.detail', ['slug' => $service->slug])}}">{{$service->title}}</a></h3>
                                            <div class="des">{{$service->description}}</div>
                                            <a class="see-more" href="{{route('frontend.service.detail', ['slug' => $service->slug])}}">See more</a>
                                            <div class="price-detail clearfix">
                                                <div class="clearfix pull-left" style="margin-bottom: 15px;">
                                                    <span class="price">{{$service->price}}</span>
                                                    <span class="unit"><?php echo Tool::getConst('UNIT_PRICE');?></span>
                                                </div>
                                                <div class="clearfix pull-right">
                                                    <form action="{{route('frontend.addCart')}}" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="productId" value="{{$service->id}}" />
                                                        <button type="submit" class="purchase">{{trans('message.mua_hang')}}</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if ($services->count() > ($index + 1)) {?>
                                        <div class="col-12">
                                            <hr class="line" />
                                        </div>
                                        <?php } ?>
                                    </div>
                                </li>
                                <?php }?>
                            </ul>
                            <div class="row">
                                @includeif('partials.pagination', [
                                    'pagination' => $pagination,
                                    'data' => $services->toArray(),
                                ])
                            </div>
                        <?php } else {
                            echo '<h2 class="not-found">'.trans('message.khong_co_du_lieu').'!</h2>';
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('body_script')
    @includeif('partials.message')
    <script>
    $(document).ready(function () {
        $('.auto-submit').change(function(){
            $(this).parents('form').submit();
            return false;
        });
    });
    </script>
@endsection
