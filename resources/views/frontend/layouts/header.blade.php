<?php
use App\Helpers\Facades\Tool;
use \Illuminate\Http\Request;
$route = Request::capture()->url();
$baseUrl = URL::to('/');
$urlNow = str_replace($baseUrl, '', $route);
$language = 'vi';
if (Session::has('locale')) {
    $language = Session::get('locale') ? Session::get('locale') : 'vi';
}
$keyTop = (strtolower($language) == 'en') ? 'home-menu-top-en' : 'home-menu-top';
$menuTop = DB::table('settings')->where('type', $keyTop)->first();
$dataMenuTop = $menuTop ? App\Models\Setting::getDataMenu(json_decode($menuTop->content)) : [];
?>
<div class="container">
    <div class="header">
        <div class="row">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand logo-top" href="{{route('frontend.home')}}"><img src="{{ asset('/frontend/img/logo.png') }}" /></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarText">
                    <ul class="navbar-nav ml-auto">
                    <?php if ($dataMenuTop) {
                    foreach ($dataMenuTop as $itemFirst) {
                        $classLi = ($itemFirst['childrent']) ? 'dropdown' : '';
                        if (strpos($itemFirst['url'], 'http://') !== false || strpos($itemFirst['url'], 'https://') !== false || in_array($itemFirst['url'], ['javascript:;', 'javascript:void(0)'])) {
                            $itemFirst['url'] = $itemFirst['url'];
                        } else {
                            if ((!$urlNow && trim($itemFirst['url'], '/') == '') || ($urlNow && strpos($itemFirst['url'], $urlNow) !== false)) {
                                $classLi = $classLi.' active';
                            }
                            $itemFirst['url'] = trim($itemFirst['url'], '/');
                            $itemFirst['url'] = URL::to('/').'/'.$itemFirst['url'];
                        }
                        ?>
                        <li class="{{$classLi}}">
                            <a class="nav-link" href="{{$itemFirst['url']}}">{{$itemFirst['title']}}</a>
                            <?php if ($itemFirst['childrent']) {?>
                            <div class="dropdown-menu">
                                <?php foreach ($itemFirst['childrent'] as $itemSecond) {
                                    if (strpos($itemSecond['url'], 'http://') !== false || strpos($itemSecond['url'], 'https://') !== false || in_array($itemSecond['url'], ['javascript:;', 'javascript:void(0)'])) {
                                        $itemSecond['url'] = $itemSecond['url'];
                                    } else {
                                        $itemSecond['url'] = trim($itemSecond['url'], '/');
                                        $itemSecond['url'] = URL::to('/').'/'.$itemSecond['url'];
                                    }
                                    ?>
                                <a class="dropdown-item" href="{{$itemSecond['url']}}">{{$itemSecond['title']}}</a>
                                <?php }?>
                            </div>
                            <?php }?>
                        </li>
                        <?php }
                        }?>
                        <li class="nav-item" style="min-width: 125px; text-align: center">
                            <a class="cart-now" href="{{ route('frontend.listcarts') }}" style="display: inline-block;padding: 3px 10px;">
                                <img alt="image" src="{{ asset('img/icon-cart.png') }}">
                            </a>
<!--                            <a class="item-language{{(strtolower($language) == 'en') ? ' active-lang' : ''}}" href="{{ route('locale', ['option' => 'en']) }}">
                                <img alt="image" src="{{ asset('img/common/England.png') }}">
                            </a>
                            <a class="item-language{{(strtolower($language) == 'vi') ? ' active-lang' : ''}}" href="{{ route('locale', ['option' => 'vi']) }}">
                                <img alt="image" src="{{ asset('img/common/Vietnam.png') }}">
                            </a>-->
                        </li>
                        
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>