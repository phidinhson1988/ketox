<?php
use App\Helpers\Facades\Tool;
$language = 'vi';
if (Session::has('locale')) {
    $language = Session::get('locale') ? Session::get('locale') : 'vi';
}
$keyBottom = (strtolower($language) == 'en') ? 'home-menu-bottom-en' : 'home-menu-bottom';
$menuBottom = DB::table('settings')->where('type', $keyBottom)->first();
$dataMenuBottom = $menuBottom ? App\Models\Setting::getDataMenu(json_decode($menuBottom->content)) : [];
?>
<div class="container">
    <div class="footer">
        <a class="logo-bottom" href="{{URL::to('/')}}"><img src="{{ asset('/frontend/img/logo-bottom.png') }}" /></a>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-8 description-footer">
                        <p>OFFICE:  15F, CornerStone Building, 16 Phan Chu Trinh, Hoàn Kiếm.</p>
                        <p>Opening hours: 09:00 am - 04:00 pm</p>
                        <p>Hotline: +84 915 566 586</p>
                        <div class="social-bottom">
                            <a href="#" class="facebook-footer"></a>
                            <a href="#" class="instagram-footer"></a>
                            <a href="#" class="youtube-footer"></a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="footer-menu">
                            <ul>
                            <?php if ($dataMenuBottom) {
                            foreach ($dataMenuBottom as $itemFirst) {
                                if (strpos($itemFirst['url'], 'http://') !== false || strpos($itemFirst['url'], 'https://') !== false || in_array($itemFirst['url'], ['javascript:;', 'javascript:void(0)'])) {
                                    $itemFirst['url'] = $itemFirst['url'];
                                } else {
                                    $itemFirst['url'] = trim($itemFirst['url'], '/');
                                    $itemFirst['url'] = URL::to('/').'/'.$itemFirst['url'];
                                }
                                ?>
                                <li>
                                    <a href="{{$itemFirst['url']}}">{{$itemFirst['title']}}</a>
                                </li>
                                <?php }
                                }?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 subcribe-email">
                <form action="{{route('frontend.default.subcribeEmail')}}" method="POST">
                    @csrf
                    <p>Contact us</p>
                    <p>subcribe to join us</p>
                    <input type="email" name="email" value="" placeholder="Nhập địa chỉ email" required />
                    <button type="submit">Đăng ký</button>
                </form>
            </div>
        </div>
    </div>
</div>