<script type="text/javascript" src="{{ asset('/frontend/js/jquery-1.11.3.js') }}"></script>
<script type="text/javascript" src="{{ asset('/frontend/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>
<!--Template functions-->
@yield('script')
