@if ($paginator->hasPages())
    <ul class="pagination">
        @if ($paginator->onFirstPage())
            <li class="page-item active">
                <a class="page-link"><i class="fa fa-angle-left"></i></a>
            </li>
        @else
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->toArray()['first_page_url'] }}">
                    <i class="fa fa-angle-double-left kt-font-brand"></i>
                </a>
            </li>
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->previousPageUrl() }}">
                    <i class="fa fa-angle-left"></i>
                </a>
            </li>
        @endif

        @if($paginator->currentPage() > 3)
            <a class="page-link" href="{{ $paginator->url(1) }}">1</a>
        @endif
        @if($paginator->currentPage() > 4)
            <li class="page-item">
                <a class="page-link"><i class="fa fa-ellipsis-h"></i></a>
            </li>
        @endif

        @foreach(range(1, $paginator->lastPage()) as $i)
            @if($i >= $paginator->currentPage() - 2 && $i <= $paginator->currentPage() + 2)
                @if ($i == $paginator->currentPage())
                    <li class="page-item active">
                        <a class="page-link">{{ $i }}</a>
                    </li>
                @else
                    <a class="page-link" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                @endif
            @endif
        @endforeach
        @if($paginator->currentPage() < $paginator->lastPage() - 3)
            <li class="page-item">
                <a class="page-link"><i class="fa fa-ellipsis-h"></i></a>
            </li>
        @endif
        @if($paginator->currentPage() < $paginator->lastPage() - 2)
            <a class="page-link" href="{{ $paginator->url($paginator->lastPage()) }}">{{ $paginator->lastPage() }}</a>
        @endif

        @if ($paginator->hasMorePages())
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->nextPageUrl() }}"><i class="fa fa-angle-right"></i></a>
            </li>
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->toArray()['last_page_url'] }}"><i
                        class="fa fa-angle-double-right kt-font-brand"></i></a>
            </li>
        @else
            <li class="page-item active">
                <a class="page-link"><i class="fa fa-angle-right"></i></a>
            </li>
        @endif
    </ul>
@endif

