<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Ketox" />
    <meta name="description" content="Ketox website">
    <title>{{ config('app.name', 'Ketox') }} | @yield('title')</title>
    
    <meta property="og:url"           content="{{route('frontend.home')}}" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Ketox.vn" />
    <meta property="og:description"   content="Ketox.vn description" />
    <meta property="og:image"         content="{{asset('/frontend/img/logo.png')}}" />

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/frontend/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/frontend/css/font-web.css?v='.\App\Helpers\Facades\Tool::getConst('VERSION_JS')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/frontend/css/main.css?v='.\App\Helpers\Facades\Tool::getConst('VERSION_JS')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/frontend/font-awesome/css/font-awesome.css') }}">
    <link rel="shortcut icon" type="image/png" href="{{ asset('favicon.png') }}"/>
    <!-- END:Place Holder -->
</head>