<div class="col-xl-12 col-lg-12 col-md-12">
    <!-- Footer widget area 1 -->
    <div class="row">
        <?php if ($dataMenuBottom) {
        foreach ($dataMenuBottom as $itemFirst) {
        $index = 0;
        ?>
        <div class="col-lg-3">
            <div class="widget">
                <div class="widget-title">{{$itemFirst['title']}}</div>
                <?php if ($itemFirst['childrent']) {?>
                <ul class="list">
                    <?php foreach ($itemFirst['childrent'] as $itemSecond) {
                    if ($index < 5) {
                    if (strpos($itemSecond['url'], 'http://') !== false || strpos($itemSecond['url'], 'https://') !== false || in_array($itemSecond['url'], ['javascript:;', 'javascript:void(0)'])) {
                        $itemSecond['url'] = $itemSecond['url'];
                    } else {
                        $itemSecond['url'] = \App\MyConst::DOMAIN_FE.$itemSecond['url'];
                    }?>
                    <li>
                        <a href="{{$itemSecond['url']}}">{{$itemSecond['title']}}</a>
                    </li>
                    <?php $index++;}}?>
                </ul>
                <?php }?>
            </div>
        </div>
        <?php
        if (count($itemFirst['childrent']) > 5) {
        for ($i = $index; $i < count($itemFirst['childrent']); $i++) {
        $itemSecond = $itemFirst['childrent'][$i];
        if ($i%5 == 0) {
            echo '<div class="col-lg-3"><div class="widget"><div class="widget-title">&nbsp;</div><ul class="list">';
        }
        if (strpos($itemSecond['url'], 'http://') !== false || strpos($itemSecond['url'], 'https://') !== false || in_array($itemSecond['url'], ['javascript:;', 'javascript:void(0)'])) {
            $itemSecond['url'] = $itemSecond['url'];
        } else {
            $itemSecond['url'] = \App\MyConst::DOMAIN_FE.$itemSecond['url'];
        }?>
        <li>
            <a href="{{$itemSecond['url']}}">{{$itemSecond['title']}}</a>
        </li>
        <?php if ($i%5 == 4 || ($i == (count($itemFirst['childrent']) - 1))) {
            echo '</ul></div></div>';
        }
        }
        }
        } }?>
    </div>
    <!-- end: Footer widget area 1 -->
</div>
