@extends('frontend.layouts.app')
@section('title', trans('message.gio_hang'))
@section('content')
<?php
use App\Helpers\Facades\Tool; 
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
$age = ($user && $user->birth_day) ? ((int)Carbon::now()->format('Y') - (int)Carbon::parse($user->birth_day)->format('Y')) : null;
 ?>
<div class="container">
    <div class="usercp-page test-rows">
        <div class="row items-container">
            <div class="col-sm-12 box-profile item-child">
                <div class="row">
                    <div class="col-sm-12 col-md-3 box-info item-child">
                        <?php if ($user) {?>
                        <div class="avatar-user">
                            <img src="<?php echo Tool::getImage($user->avatar, Tool::getConst('USER_UPLOAD'));?>" />
                        </div>
                        <div class="name-user">
                            <p>{{trans('message.tuoi')}}: {{$age}}</p>
                            <p>{{$user->address}}</p>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="col-sm-12 col-md-9 box-detail item-child">
                        <div class="your-report" style="min-height: 750px;">
                            <h2>{{trans('message.danh_sach_hang_da_chon')}}</h2>
                            <table class="order-history">
                                <tbody>
                                    <tr>
                                        <th style="width: 100px">{{trans('message.stt')}}</th>
                                        <th>{{trans('message.dich_vu')}}</th>
                                        <th>{{trans('message.so_luong')}}</th>
                                        <th>{{trans('message.gia_ban')}}</th>
                                        <th>{{trans('message.tong_so')}}</th>
                                        <th>{{trans('message.thao_tac')}}</th>
                                    </tr>
                                    <?php if ($carts) {
                                        foreach ($carts as $index => $cart) {?>
                                        <tr>
                                            <th style="width: 100px">{{($index + 1)}}</th>
                                            <th>{{$cart['title']}}</th>
                                            <th>{{$cart['total']}}</th>
                                            <th>{{$cart['price']}} VND</th>
                                            <th>{{($cart['price'] * $cart['total'])}} VND</th>
                                            <th><a class="btn btn-danger" onclick="return confirm('Bạn có muốn xóa dịch vụ này không?')" href="{{route('frontend.deleteCart', ['id' => $index])}}">Xóa</a></th>
                                        </tr>
                                    <?php }} ?>
                                </tbody>
                            </table>
                            <a class="link-history" href="{{route('frontend.service.index')}}">{{trans('message.tiep_tuc_mua_hang')}}</a>
                            <?php if ($carts) {?>
                            <a class="link-history" href="{{route('frontend.service.purchase')}}" style="margin-left: 10px">{{trans('message.thanh_toan')}}</a>
                            <?php } ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('body_script')
@includeif('partials.message')
@endsection
