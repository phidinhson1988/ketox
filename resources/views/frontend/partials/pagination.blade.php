<div class="col-sm-12 col-md-3"></div>
<div class="col-sm-12 col-md-9">
    <div class="pagination-box">
        <nav>
            @if (isset($data) && $data['total'] > $limit)
                @isset($pagination)
                {!! $pagination !!}
                @endif
            @endif
        </nav>
    </div>
</div>