    <?php $ranges = [10,20,30,50];?>
<form action="{{ Request::fullUrl() }}" method="GET" class="form-horizontal" id="form-limit">
        <div class="input-group mb-0">
            <div class="input-group-addon"><strong>{{ trans('message.hien_thi') }}</strong></div>
            <select class="form-control" name="limit" onchange="$(this).closest('form').submit();">
                @foreach($ranges as $v)
                <option {{ collect(Request::get('limit'))->contains($v) ? 'selected' : null }} value="{{ $v }}">{{ $v }} {{ trans('message.phan_trang') }}</option>
                @endforeach
            </select>
        @isset($hidden)
            @foreach($hidden as $key => $value)
            <input type="hidden" name="{{ $key }}" value="{{ $value }}" />
            @endforeach
        @else
            {{--<input type="hidden" name="q" value="{{ Request::get('q', '') }}" />--}}
        @endisset
        </div>
    </form>
