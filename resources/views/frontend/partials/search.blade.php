<form action="{{ request()->fullUrl() }}" method="GET">
    <div class="input-group">
        @if (! empty($placeholder))
        <input placeholder="{{ $placeholder }}" type="text" name="q" value="{{ request()->get('q', null) }}" class="form-control" />
        @else
        <input type="text" name="q" value="{{ request()->get('q', null) }}" class="form-control" />
        @endif
        <span class="input-group-append">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i></button>
        </span>
    </div>
</form>
