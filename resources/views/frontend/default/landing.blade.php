<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Ketox" />
        <meta name="description" content="Ketox website">
        <title>Landing Page</title>

        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css" href="{{ asset('/frontend/css/bootstrap.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/frontend/css/font-web.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/frontend/css/login.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/frontend/font-awesome/css/font-awesome.css') }}">
        <link rel="shortcut icon" type="image/png" href="{{ asset('favicon.png') }}"/>
        <!-- END:Place Holder -->

    </head>
    <body class="landing-page">
        <div class="container-fluid">
            <div class="container">
                <div class="row ">
                    <div class="col-12 landing-page-logo">
                        <img src="{{ asset('/frontend/img/logo-landing.png') }}" />
                    </div>
                    <div class="col-sm-12 col-md-6 link-website" style="text-align: right">
                        <a href="{{route('frontend.home')}}">{{trans('message.khach')}}</a>
                    </div>
                    <div class="col-sm-12 col-md-6 link-website" style="text-align: left">
                        <a href="{{route('frontend.user.profile')}}">{{trans('message.thanh_vien')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
