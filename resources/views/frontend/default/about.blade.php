@extends('frontend.layouts.app')
@section('title', trans('message.gioi_thieu'))
@section('content')
<?php 
use App\Helpers\Facades\Tool;
use Carbon\Carbon;
?>
    <div class="container">
        <div class="about-content">
            <div class="row">
                <div class="col-sm-12 col-md-3">
                    <h2 class="about-title">{{trans('message.gioi_thieu')}}</h2>
                </div>
                <div class="col-sm-12 col-md-9">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="image-large">
                                <img src="<?php echo Tool::getImage($dataAbout['image'], Tool::getConst('SETTING_UPLOAD'));?>" />
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 des">
                            <?php echo $dataAbout['description_left'];?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 des">
                            <?php echo $dataAbout['description_right'];?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('body_script')
    @includeif('partials.message')
@endsection
