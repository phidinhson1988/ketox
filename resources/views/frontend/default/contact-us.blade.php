@extends('frontend.layouts.app')
@section('title', trans('message.lien_he'))
@section('content')
<?php

use App\Helpers\Facades\Tool;
use Carbon\Carbon;
?>
<style>
.google-maps {
    position: relative;
    padding-bottom: 500px; // This is the aspect ratio
    height: 0;
    overflow: hidden;
}
.google-maps iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100% !important;
    height: 500px !important;
}
</style>
<div class="container">
    <?php if ($dataContact['map']) { ?>
    <div class="about-map">
        <div class="row">
            <div class="col-12">
                <div class="google-maps">
                    <?php echo $dataContact['map'];?>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="about-content-page">
        <div class="row">
            <div class="col-sm-12 col-md-4 contact-left">
                <h1>Contact us</h1>
                <p class="address-office"><i class="fa fa-map-marker" aria-hidden="true"></i>{{trans('message.dia_chi_van_phong')}}:</p>
                <?php if ($dataContact['office']) { ?>
                    <p>{{$dataContact['office']}}</p>
                <?php } ?>
                <?php if ($dataContact['facebook']) { ?>
                    <p class="address-face"><i class="fa fa-facebook" aria-hidden="true"></i>{{$dataContact['facebook']}}</p>
                <?php } ?>
                <?php if ($dataContact['instagram']) { ?>
                    <p class="address-ketox"><i class="fa fa-instagram" aria-hidden="true"></i>{{$dataContact['instagram']}}</p>
                <?php } ?>
                <?php if ($dataContact['mail']) { ?>
                    <p class="address-mail"><i class="fa fa-envelope" aria-hidden="true"></i>{{$dataContact['mail']}}</p>
                <?php } ?>
                <?php if ($dataContact['phone']) { ?>
                    <p class="address-phone"><i class="fa fa-phone" aria-hidden="true"></i>{{$dataContact['phone']}}</p>
                <?php } ?>
            </div>
            <div class="col-sm-12 col-md-8 contact-right">
                <h3>{{trans('message.co_cau_hoi_nao_khong')}}</h3>
                <form action="{{route('frontend.default.addContact')}}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="routeNow" value="frontend.default.contactUs">
                    <table>
                        <tr>
                            <td class="title">{{trans('message.ho_ten')}}</td>
                            <td><input type="text" name="name" placeholder="{{trans('message.ho_ten')}}" required /></td>
                        </tr>
                        <tr>
                            <td class="title">{{trans('message.hom_thu')}}</td>
                            <td><input type="email" name="email" placeholder="{{trans('message.hom_thu')}}" required /></td>
                        </tr>
                        <tr>
                            <td class="title">{{trans('message.chu_de')}}</td>
                            <td><input type="text" name="subject" placeholder="{{trans('message.chu_de')}}" required /></td>
                        </tr>
                        <tr>
                            <td class="title">{{trans('message.noi_dung')}}</td>
                            <td><textarea name="message" rows="4" placeholder="{{trans('message.noi_dung')}}" required></textarea></td>
                        </tr>
                        <tr>
                            <td class="title">Ảnh</td>
                            <td><input style="padding-left: 0" type="file" accept="image/png,image/jpg,image/jpeg" name="file_upload" /></td>
                        </tr>
                    </table>
                    <button type="submit">{{trans('message.gui')}}</button>
                </form>
            </div>
        </div>
    </div>
</div>

@stop
@section('body_script')
@includeif('partials.message')
@endsection
