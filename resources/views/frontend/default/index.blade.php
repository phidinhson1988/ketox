@extends('frontend.layouts.app')
@section('title', 'Ketox')
@section('content')
<?php 
use App\Helpers\Facades\Tool;
$keyBanner = ($language == 'vi') ? 'banner-home' : 'banner-home-en';
if (isset($settings[$keyBanner]) && $settings[$keyBanner]) {
    $dataBanner = json_decode($settings[$keyBanner]);
    if (strpos($dataBanner->link, 'http://') !== false || strpos($dataBanner->link, 'https://') !== false || in_array($dataBanner->link, ['javascript:;', 'javascript:void(0)'])) {
        $dataBanner->link = $dataBanner->link;
    } else {
        $dataBanner->link = URL::to('/').'/'.$dataBanner->link;
    }
?>
    <div class="container">
        <div class="banner-home">
            <div class="row">
                <div class="col-12">
                    <a href="{{$dataBanner->link}}"><img src="<?php echo Tool::getImage($dataBanner->image, Tool::getConst('SETTING_UPLOAD'));?>" /></a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
    <div class="container">
        <div class="main-content">
            <div class="about-home">
                <div class="row">
                    <div class="col-sm-12 col-md-3 col-lg-2">
                        <h2>{{$dataBanner->title}}</h2>
                    </div>
                    <div class="col-sm-12 col-md-9 col-lg-10">
                        <p><?php echo $dataBanner->description;?></p>
                    </div>
                </div>
            </div>
            <div class="services-home">
                <div class="row">
                    <div class="col-sm-12 col-md-3 col-lg-2">
                        <h2>{{trans('message.dich_vu_cua_chung_toi')}}</h2>
                    </div>
                    <div class="col-sm-12 col-md-9 col-lg-10">
                        <ul class="row">
                            <?php if ($services) {
                            foreach ($services as $service) {?>
                                <li class="col-sm-12 col-md-6 col-lg-3">
                                    <a href="{{route('frontend.service.detail', ['slug' => $service->slug])}}" class="image-service">
                                        <img src="<?php echo Tool::getImage($service->avatar, Tool::getConst('PRODUCT_UPLOAD'));?>" />
                                    </a>
                                    <h3><a href="{{route('frontend.service.detail', ['slug' => $service->slug])}}">{{$service->title}}</a></h3>
                                    <div class="des">{{$service->description}}</div>
                                </li>
                            <?php } } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="customer-feedback">
                <div class="row">
                    <div class="col-sm-12 col-md-3 col-lg-2">
                        <h2>{{trans('message.phan_hoi_cua_khach_hang')}}</h2>
                    </div>
                    <div class="col-sm-12 col-md-9 col-lg-10">
                        <ul class="row">
                            <?php if ($feedbacks) {
                                foreach ($feedbacks as $feedback) {?>
                                    <li class="col-sm-12 col-md-6 col-lg-4" data="detail-{{$feedback->id}}">
                                        <a href="javascript:void(0)" class="image-customer tab-feedback">
                                            <img src="<?php echo Tool::getImage($feedback->avatar, Tool::getConst('POST_UPLOAD'));?>" />
                                        </a>
                                    </li>
                            <?php } } ?>
                        </ul>
                        <?php if ($feedbacks) {
                            foreach ($feedbacks as $index => $feedback) {
                                ?>
                            <div id="detail-{{$feedback->id}}" class="detail-customer-feedback row" style="display: {{($index == 0) ? 'flex' : 'none'}}">
                                <div class="col-sm-12 col-md-4">
                                    <div class="item-feedback">
                                        <a href="{{route('frontend.user.profile', ['username' => $feedback->username])}}" class="image-user">
                                            <img src="<?php echo Tool::getImage($feedback->imageUser, Tool::getConst('USER_UPLOAD'));?>" />
                                        </a>
                                        <div class="info-user">
                                            <p><a href="{{route('frontend.user.profile', ['username' => $feedback->username])}}">{{$feedback->full_name}}</a></p>
                                            <span>{{$feedback->job}}</span>
                                            <span>{{$feedback->city}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-8">
                                    <p>{!!$feedback->content!!}</p>
                                </div>
                            </div>
                        <?php } } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('body_script')
    @includeif('partials.message')
    <script>
    $('.tab-feedback').click(function(){
        var data = $(this).parent().attr('data');
        $('body').find('.detail-customer-feedback').hide();
        $('#'+data).show();
        return false;
    });
    </script>
@endsection
