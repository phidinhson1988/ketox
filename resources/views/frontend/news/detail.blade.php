@extends('frontend.layouts.app')
@section('title', $post->title)
@section('content')
<?php 
use App\Helpers\Facades\Tool;?>
<div class="container">
    <div class="image-large-news">
        <img src="<?php echo Tool::getImage($post->avatar, Tool::getConst('POST_UPLOAD'));?>" />
    </div>
    <div class="news-content" style="padding-top: 50px; min-height: 200px;">
        <div class="row">
            <div class="col-sm-12">
                <h1>{{$post->title}}</h1>
                <div class="main-news">
                    <?php echo $post->content;?>
                </div>
            </div>
        </div>
    </div>
    <div class="other-news">
        <h2>{{trans('message.tin_tuc_khac')}}:</h2>
        <ul class="row">
            <?php if ($others->count() > 0) {
                foreach ($others as $postItem) {?>
                <li class="col-sm-12 col-md-4">
                    <a href="{{route('frontend.news.detail', ['slug' => $postItem->slug])}}" class="image-article">
                        <img src="<?php echo Tool::getImage($postItem->avatar, Tool::getConst('POST_UPLOAD'));?>" />
                    </a>
                    <h3><a href="{{route('frontend.news.detail', ['slug' => $postItem->slug])}}">{{$postItem->title}}</a></h3>
                    <p class="des">{{$postItem->description}}</p>
                    <a href="{{route('frontend.news.detail', ['slug' => $postItem->slug])}}" class="read-more">{{trans('message.xem_tiep')}}</a>
                </li>
            <?php }
            }?>
        </ul>
    </div>
</div>

@stop
@section('body_script')
    @includeif('partials.message')
@endsection
