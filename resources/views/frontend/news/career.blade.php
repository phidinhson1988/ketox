@extends('frontend.layouts.app')
@section('title', trans('message.tuyen_dung'))
@section('content')
<?php 
use 
App\Helpers\Facades\Tool;
use Carbon\Carbon;
?>
<div class="container">
    <div class="career-content">
        <div class="row">
            <div class="col-sm-12">
                <h1>{{trans('message.tuyen_dung')}}</h1>
            </div>
            <div class="col-sm-12 col-md-4 filter-box">
                <form action="" method="GET">
                    <input type="hidden" name="sort" value="{{$sort}}"/>
                    <label>{{trans('message.sap_xep_theo')}}:</label>
                    <div class="filter-item btn-group">
                        <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{$sortName}}</button>
                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                            <a class="dropdown-item" href="{{route('frontend.news.career', ['sort' => 1, 'q' => $search])}}">Mới nhất</a>
                            <a class="dropdown-item" href="{{route('frontend.news.career', ['sort' => 2, 'q' => $search])}}">Cũ nhất</a>
                            <a class="dropdown-item" href="{{route('frontend.news.career', ['sort' => 3, 'q' => $search])}}">Giá từ cao đến thấp</a>
                            <a class="dropdown-item" href="{{route('frontend.news.career', ['sort' => 4, 'q' => $search])}}">Giá từ thấp đến cao</a>
                        </div>
                    </div>
                    <div class="filter-item btn-group">
                        <button id="btnGroupDrop2" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Places</button>
                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                          <a class="dropdown-item" href="#">Places</a>
                        </div>
                    </div>
                    <div class="filter-item btn-group">
                        <button id="btnGroupDrop3" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Latest</button>
                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                          <a class="dropdown-item" href="#">Latest</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-sm-12 col-md-8 list-career">
                <?php if ($posts->count() > 0) {?>
                    <ul>
                        <?php foreach ($posts as $post) {?>
                        <li>
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-6">
                                    <a href="{{route('frontend.news.detailCareer', ['slug' => $post->slug])}}" class="image-news">
                                        <img src="<?php echo Tool::getImage($post->avatar, Tool::getConst('POST_UPLOAD'));?>" alt="{{$post->title}}" />
                                    </a>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-6">
                                    <h3><a href="{{route('frontend.news.detailCareer', ['slug' => $post->slug])}}">{{$post->title}}</a></h3>
                                    <span class="posted">Posted {{$post->start_date ? Carbon::parse($post->start_date)->format('d M, Y') : ''}}</span>
                                    <p class="deadline">Deadline: {{$post->end_date ? Carbon::parse($post->end_date)->format('d/m/Y') : ''}}</p>
                                    <div class="des">{{$post->description}}</div>
                                    <a href="{{route('frontend.news.detailCareer', ['slug' => $post->slug])}}" class="link">{{trans('message.xem_tiep')}}</a>
                                </div>
                            </div>
                        </li>
                        <?php } ?>
                    </ul>
                    <div class="row">
                        @includeif('partials.pagination', [
                            'pagination' => $pagination,
                            'data' => $posts->toArray(),
                        ])
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

@stop
@section('body_script')
    @includeif('partials.message')
    <script>
    $(document).ready(function () {
        $('.auto-submit').change(function(){
            $(this).parents('form').submit();
            return false;
        });
    });
    </script>
@endsection
