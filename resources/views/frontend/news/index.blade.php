@extends('frontend.layouts.app')
@section('title', trans('message.tin_tuc'))
@section('facebook')
<div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v5.0"></script>
@endsection
@section('content')
<?php 
use App\Helpers\Facades\Tool;?>

    <div class="container">
        <div class="news-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="news-title clearfix">
                        <h1>{{trans('message.tin_tuc')}}</h1>
                        <?php if (!Auth::check()) {?>
                        <span class="title-require"><span class="icon-require"></span> <a href="{{route('login')}}">SIGN IN</a> để đọc các bài viết nổi bật</span>
                        <?php } ?>
                        <!--                        <div class="filter-box clearfix">
                            <form action="" method="GET">
                                <input type="hidden" name="sort" value="{{$sort}}"/>
                                <div class="filter-item" style="display: flex;">
                                    <label>{{trans('message.sap_xep_theo')}}:</label>
                                    <div class="filter-item btn-group">
                                        <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{$sortName}}</button>
                                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                          <a class="dropdown-item" href="{{route('frontend.news.index', ['sort' => 1, 'q' => $search])}}">Mới nhất</a>
                                          <a class="dropdown-item" href="{{route('frontend.news.index', ['sort' => 2, 'q' => $search])}}">Cũ nhất</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="filter-item">
                                    <input type="text" name="q" class="form-control" placeholder="TÌM KIẾM" value="{{$search}}" />
                                </div>
                            </form>
                        </div>-->
                    </div>
                </div>
                <div class="col-sm-12 col-md-12">
                    <div class="list-news">
                        <?php if ($posts->count() > 0) {?>
                        <ul class="row">
                            <?php foreach ($posts as $post) {?>
                            <li class="col-sm-6 col-md-4">
                                <a href="{{route('frontend.news.detail', ['slug' => $post->slug])}}" class="image-news">
                                    <img src="<?php echo Tool::getImage($post->avatar, Tool::getConst('POST_UPLOAD'));?>" />
                                    <?php if ($post->is_require) {?>
                                    <span class="icon-require"></span>
                                    <?php } ?>
                                </a>
                                <h3><a href="{{route('frontend.news.detail', ['slug' => $post->slug])}}">{{$post->title}}</a></h3>
                                <?php if ($post->description) {?>
                                <div class="des">{{$post->description}}</div>
                                <?php } ?>
                                <div class="bottom-news">
                                    <p>{{$post->created_at ? \Carbon\Carbon::parse($post->created_at)->format('d/m/Y') : ''}}</p>
                                    <div class="action">
                                        <div class="fb-like" data-href="https://www.facebook.com/ketoxvn" data-width="" data-layout="button" data-action="like" data-size="small" data-share="true"></div>
<!--                                        <a href="javascript:void(0)"><img src="{{asset('img/icon-share.png')}}" /></a>
                                        <a href="javascript:void(0)"><img src="{{asset('img/icon-heart.png')}}" /></a>-->
                                    </div>
                                </div>
                            </li>
                            <?php } ?>
                        </ul>
                        <?php } else { ?>
                        <h2 class="not-found">{{trans('message.khong_co_du_lieu')}}!</h2>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php if ($posts->count() > 0) {?>
            <div class="row">
                @includeif('partials.pagination', [
                    'pagination' => $pagination,
                    'data' => $posts->toArray(),
                ])
            </div>
            <?php }?>
        </div>
    </div>

@stop
@section('body_script')
    @includeif('partials.message')
    <script>
    $(document).ready(function () {
        $('.auto-submit').change(function(){
            $(this).parents('form').submit();
            return false;
        });
    });
    </script>
@endsection
