@extends('frontend.layouts.app')
@section('title', 'User CP')
@section('content')
<?php
use App\Helpers\Facades\Tool; 
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
$age = ($user->birth_day) ? ((int)Carbon::now()->format('Y') - (int)Carbon::parse($user->birth_day)->format('Y')) : null;
 ?>
<div class="container">
    <div class="usercp-page test-rows">
        <div class="row items-container">
            <div class="col-sm-12 box-profile item-child">
                <div class="row">
                    <div class="col-sm-12 col-md-3 box-info item-child">
                        <div class="avatar-user">
                            <img src="<?php echo Tool::getImage($user->avatar, Tool::getConst('USER_UPLOAD'));?>" />
                        </div>
                        <div class="name-user">
                            <p>{{trans('message.tuoi')}}: {{$age}}</p>
                            <p>{{$user->address}}</p>
                        </div>
                        <div class="action">
                        <?php if(Auth::check() && Auth::user()->id == $user->id) {?>
                            <a href="{{route('frontend.user.report')}}"><i class="icon-li"></i>{{trans('message.chinh_sua_profile')}}</a>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#change-pass-modal"><i class="icon-li"></i>{{trans('message.thay_doi_mat_khau')}}</a>
                            <a href="{{route('logout')}}"><i class="icon-li"></i>{{trans('message.dang_xuat')}}</a>
                        <?php } ?>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-5 box-meta item-child">
                        <h2>Info</h2>
                        <?php if ($user->image) {?>
                        <div class="image-user" style="display: block;width: 100%;text-align: center;padding-top: 20px;">
                                <img style="display: inline-block;max-width: 90%" src="<?php echo Tool::getImage($user->image, Tool::getConst('USER_UPLOAD'));?>" />
                            </div>
                        <?php } ?>
                        <table>
                            <tr>
                                <td class="title">{{trans('message.ho_va_ten')}}:</td>
                                <td>{{$user->full_name}}</td>
                            </tr>
                            <tr>
                                <td class="title">{{trans('message.hom_thu')}}:</td>
                                <td>{{$user->email}}</td>
                            </tr>
                            <tr>
                                <td class="title">{{trans('message.gioi_tinh')}}:</td>
                                <td>{{$user->gender == 1 ? trans('message.con_trai') : trans('message.con_gai')}}</td>
                            </tr>
                            <tr>
                                <td class="title">{{trans('message.ngay_sinh')}}:</td>
                                <td>{{$user->birth_day ? Carbon::parse($user->birth_day)->format('d/m/Y') : ''}}</td>
                            </tr>
                            <tr>
                                <td class="title">{{trans('message.dia_chi')}}:</td>
                                <td>{{$user->address}}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-12 col-md-4 box-detail item-child">
                        <div class="your-report">
                            <h2>{{trans('message.report_cua_ban')}}</h2>
                            <p>{{trans('message.tu_ngay')}}: {{(isset($dataUser['from'])) ? $dataUser['from'] : ''}} {{trans('message.den_ngay')}}: {{(isset($dataUser['to'])) ? $dataUser['to'] : ''}}</p>
                            <table>
                                <tbody>
                                    <tr>
                                        <td class="title">{{trans('message.can_nang')}}</td>
                                        <td style="text-align: right">{{(isset($dataUser['weight_1'])) ? $dataUser['weight_1'] : ''}} KG</td>
                                        <td style="text-align: right">{{(isset($dataUser['weight_2'])) ? $dataUser['weight_2'] : ''}} KG</td>
                                    </tr>
                                    <tr>
                                        <td class="title">{{trans('message.vong_eo')}}</td>
                                        <td style="text-align: right">{{(isset($dataUser['waist_1'])) ? $dataUser['waist_1'] : ''}} CM</td>
                                        <td style="text-align: right">{{(isset($dataUser['waist_2'])) ? $dataUser['waist_2'] : ''}} CM</td>
                                    </tr>
                                    <tr>
                                        <td class="title">{{trans('message.vong_eo')}}</td>
                                        <td style="text-align: right">{{(isset($dataUser['belly_1'])) ? $dataUser['belly_1'] : ''}} CM</td>
                                        <td style="text-align: right">{{(isset($dataUser['belly_2'])) ? $dataUser['belly_2'] : ''}} CM</td>
                                    </tr>
                                    <tr>
                                        <td class="title">{{trans('message.hong')}}</td>
                                        <td style="text-align: right">{{(isset($dataUser['hips_1'])) ? $dataUser['hips_1'] : ''}} CM</td>
                                        <td style="text-align: right">{{(isset($dataUser['hips_2'])) ? $dataUser['hips_2'] : ''}} CM</td>
                                    </tr>
                                    <tr>
                                        <td class="title">{{trans('message.dui')}}</td>
                                        <td style="text-align: right">{{(isset($dataUser['thigh_1'])) ? $dataUser['thigh_1'] : ''}} CM</td>
                                        <td style="text-align: right">{{(isset($dataUser['thigh_2'])) ? $dataUser['thigh_2'] : ''}} CM</td>
                                    </tr>
                                    <tr>
                                        <td class="title">{{trans('message.chieu_cao')}}</td>
                                        <td style="text-align: right">{{(isset($dataUser['height_1'])) ? $dataUser['height_1'] : ''}} CM</td>
                                        <td style="text-align: right">{{(isset($dataUser['height_2'])) ? $dataUser['height_2'] : ''}} CM</td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php if(Auth::check() && Auth::user()->id == $user->id) {?>
                            <a class="link" href="{{route('frontend.user.report')}}">{{trans('message.cap_nhat_thong_tin')}}</a>
                            <?php } ?>
                        </div>
                        <div class="order">
                            <h2>{{trans('message.lich_su_mua_hang')}}</h2>
                            <table>
                                <tbody>
                                    <?php if ($orders->count() > 0) {
                                        foreach ($orders as $order) {?>
                                    <tr>
                                        <td class="title">{{$order->product ? $order->product['title'] : ''}}</td>
                                        <td>{{$order->status == 1 ? trans('message.dang_thuc_thi') : trans('message.da_giao_hang')}}</td>
                                    </tr>
                                    <?php }} ?>
                                </tbody>
                            </table>
                            <a class="link" href="{{route('frontend.user.orderHistory', ['user'=> $user->id])}}">{{trans('message.xem_tiep')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="change-pass-modal" role="dialog">
    <div class="modal-dialog" style="margin-top: 100px;max-width: 500px; width: 95%;">
    <!-- Modal content-->
        <div class="modal-content">
            <form id="change-pass" name="change-pass" action="{{route('frontend.user.changePass')}}" enctype="multipart/form-data" method="post">
                <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                <div class="modal-header">
                    <button style="position: absolute;top: 10px;right: 10px;font-family: arial" type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title" id="detail-pallet-qrcode">{{trans('message.thay_doi_mat_khau')}}</h4>
                </div>
                <div class="modal-body">
                    <div class="clearfix">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>{{trans('message.mat_khau_moi')}}</label>
                                <input id="password" type="password" class="form-control" placeholder="" name="password" />
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>{{trans('message.xac_nhan_mat_khau')}}</label>
                                <input id="repassword" type="password" class="form-control" placeholder="" name="repassword" />
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="save-pass" class="btn btn-info">{{trans('message.luu_lai')}}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('message.dong_lai')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>


@stop
@section('body_script')
@includeif('partials.message')
<script type="text/javascript" src="{{asset('/frontend/js/matchHeight.js')}}"></script>
<script>
    var byRow = $('body').hasClass('test-rows');
    // apply matchHeight to each item container's items
    $('.items-container').each(function () {
        $(this).find('.item-child').matchHeight({
            byRow: byRow
        });
    });
    $('#save-pass').click(function(){
        var status = true;
        $('body').find('.has-error').removeClass('has-error');
        $('body').find('.help-block').text('').hide();
        var password = $('#password').val().trim();
        var repassword = $('#repassword').val().trim();
        if (password != '') {
            if (password != repassword) {
                status = false;
                $('#repassword').addClass('has-error');
                $('#repassword').parent().find('.help-block').text("{{trans('message.xac_nhan_mat_khau_khong_dung')}}").show();
            }
        } else {
            status = false;
            $('#password').addClass('has-error');
            $('#password').parent().find('.help-block').text("{{trans('message.mat_khau_duoc_de_trong')}}").show();
        }
        if (status) {
            $('#change-pass').submit();
        }
    });
</script>
@endsection
