@extends('frontend.layouts.app')
@section('title', trans('message.lich_su_mua_hang'))
@section('content')
<?php
use App\Helpers\Facades\Tool; 
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
$age = ($user->birth_day) ? ((int)Carbon::now()->format('Y') - (int)Carbon::parse($user->birth_day)->format('Y')) : null;
 ?>
<div class="container">
    <div class="usercp-page test-rows">
        <div class="row items-container">
            <div class="col-sm-12 box-profile item-child">
                <div class="row">
                    <div class="col-sm-12 col-md-3 box-info item-child">
                        <div class="avatar-user">
                            <img src="<?php echo Tool::getImage($user->avatar, Tool::getConst('USER_UPLOAD'));?>" />
                        </div>
                        <div class="name-user">
                            <p>{{trans('message.tuoi')}}: {{$age}}</p>
                            <p>{{$user->address}}</p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-9 box-detail item-child">
                        <div class="your-report" style="min-height: 750px;">
                            <h2>{{trans('message.lich_su_mua_hang')}}</h2>
                            <table class="order-history">
                                <tbody>
                                    <tr>
                                        <th style="width: 100px">{{trans('message.stt')}}</th>
                                        <th>{{trans('message.ngay_tao')}}</th>
                                        <th>{{trans('message.dich_vu')}}</th>
                                        <th>{{trans('message.so_luong')}}</th>
                                        <th>{{trans('message.gia_ban')}}</th>
                                        <th>{{trans('message.tong_so')}}</th>
                                        <th>{{trans('message.trang_thai')}}</th>
                                    </tr>
                                    <?php if ($orders->count() > 0) {
                                        foreach ($orders as $index => $order) {?>
                                        <tr>
                                            <th style="width: 100px">{{($index + 1)}}</th>
                                            <th>{{$order->created_time ? Carbon::parse($order->created_time)->format('d/m/Y') : ''}}</th>
                                            <th>{{$order->product ? $order->product['title'] : ''}}</th>
                                            <th>1</th>
                                            <th>{{$order->product ? $order->product['price'] : ''}}</th>
                                            <th>{{$order->product ? $order->product['price'] : ''}}</th>
                                            <th>{{$order->status == 1 ? trans('message.dang_thuc_thi') : trans('message.da_giao_hang')}}</th>
                                        </tr>
                                    <?php }} ?>
                                </tbody>
                            </table>
                            <a class="link-history" href="#">{{trans('message.xem_lai_cac_muc_truoc')}}</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('body_script')
@includeif('partials.message')
@endsection
