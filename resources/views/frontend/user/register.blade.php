@extends('frontend.layouts.app')
@section('title', trans('message.dang_ky'))
@section('content')
<style>
    .table-wrapper-scroll-y {
        display: block;
        max-height: 500px;
        overflow-y: auto;
        -ms-overflow-style: -ms-autohiding-scrollbar;
    }
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.Jcrop.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/datapicker/datepicker3.css') }}">
<?php

use App\Helpers\Facades\Tool;
use Carbon\Carbon;
?>
<div class="container">
    <form id="form-data-register" action="{{route('frontend.user.saveRegister')}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" id="result-report" name="data-report" value="">
        <!--Step 1-->
        <div id="report-page1" class="report-page" style="display: block">
            <div class="row ">
                <div class="col-sm-12 col-md-12 col-lg-6">
                    <div class="sidebar-left">
                        <div class="bg_sidebar"></div>
                        <div class="sidebar-content">
                            <a href="javascript:void(0)" class="link active next-step1"><i class="icon-li"></i>{{trans('message.buoc_1_tao_thong_tin_co_ban')}}</a>
                            <a href="javascript:void(0)" class="link next-step2"><i class="icon-li"></i>{{trans('message.buoc_2_dien_thong_tin_ca_nhan')}}</a>
                            <a href="javascript:void(0)" class="link next-step3"><i class="icon-li"></i>{{trans('message.buoc_3_xac_nhan')}}</a>
                            <a href="javascript:void(0)" class="submit-link button-submit-form" data="1">{{trans('message.luu_lai_va_thoat_ra')}}</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6">
                    <div class="create-profile">
                        <h1>“{{trans('message.title_chao_mung_dang_ky')}}”</h1>
                        <div class="row">
                            <div class="col-sm-12 col-md-7 col-lg-7">
                                <div class="form-group">
                                    <label>{{trans('message.ten_dang_nhap')}}</label>
                                    <input type="text" id="username" class="form-control" placeholder="{{trans('message.nhap_tai_day')}}" name="username" />
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>{{trans('message.mat_khau')}}</label>
                                    <input id="password" type="password" class="form-control" placeholder="" name="password" />
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>{{trans('message.ho_va_ten')}}</label>
                                    <input id="full_name" type="text" class="form-control" placeholder="{{trans('message.nhap_tai_day')}}" name="full_name" />
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group gender-check clearfix">
                                    <input id="select-gender" type="hidden" name="gender" value="1" />
                                    <label>{{trans('message.gioi_tinh')}}</label>
                                    <span id="gender-male" class="active">{{trans('message.con_trai')}}</span>
                                    <span id="gender-female">{{trans('message.con_gai')}}</span>
                                </div>
                                <div class="form-group">
                                    <label>{{trans('message.dia_chi')}}</label>
                                    <input type="text" class="form-control" placeholder="{{trans('message.nhap_tai_day')}}" name="address" />
                                </div>
                                <div class="form-group">
                                    <label>{{trans('message.hom_thu')}}</label>
                                    <input id="email" type="text" class="form-control" placeholder="{{trans('message.nhap_tai_day')}}" name="email" />
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-5 col-lg-5">
                                <a href="javascript:void(0)" id="upload-avatar" class="upload-image"><i class="fa fa-camera" aria-hidden="true"></i> {{trans('message.upload_anh_dai_dien')}}</a>
                                <div id="image-detail">
                                    <input type="hidden" id="image-detail-user-input" name="avatar" />
                                    <img style="width: 100%" id="image-detail-user" src="" />
                                </div>

                                <div class="form-group">
                                    <label>{{trans('message.thanh_pho')}}</label>
                                    <input type="hidden" name="city" id="input-city" value=""/>
                                    <div class="filter-item btn-group" style="width: 100%">
                                        <button id="btnGroupDrop1" style="width: 100%" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{trans('message.chon_thanh_pho')}}</button>
                                        <?php if ($city) {?>
                                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                            <?php foreach ($city as $keyCity => $itemValue) {
                                                echo '<a class="dropdown-item select-city" data="'.$keyCity.'" href="javascript:void(0)">'.$itemValue.'</a>';
                                            }?>
                                        </div>
                                        <?php }?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>{{trans('message.so_dien_thoai')}}</label>
                                    <input type="text" class="form-control" placeholder="{{trans('message.nhap_tai_day')}}" name="phone" />
                                </div>
                                <div class="form-group">
                                    <label>{{trans('message.ngay_sinh')}}</label>
                                    <input type="date" class="form-control" placeholder="DD/MM/YYYY" name="birth_day" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="save-report clearfix">
                        <button type="button" class="pull-left" id="back-page">{{trans('message.tro_lai')}}</button>
                        <button type="button" class="pull-right active next-step2" id="next-page">{{trans('message.tiep_tuc')}}</button>
                    </div>
                </div>
            </div>
        </div>
        <!--Step 2-->
        <div id="report-page2" class="report-page" style="display: none">
            <div class="row ">
                <div class="col-sm-12 col-md-12 col-lg-6">
                    <div class="sidebar-left">
                        <div class="bg_sidebar"></div>
                        <div class="sidebar-content">
                            <a href="javascript:void(0)" class="link next-step1"><i class="icon-li"></i>{{trans('message.buoc_1_tao_thong_tin_co_ban')}}</a>
                            <a href="javascript:void(0)" class="link active next-step2"><i class="icon-li"></i>{{trans('message.buoc_2_dien_thong_tin_ca_nhan')}}</a>
                            <a href="javascript:void(0)" class="link next-step3"><i class="icon-li"></i>{{trans('message.buoc_3_xac_nhan')}}</a>
                            <a href="javascript:void(0)" class="submit-link button-submit-form" data="2">{{trans('message.luu_lai_va_thoat_ra')}}</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6">
                    <div class="report-page-right">
                        <p class="id-user">ID: <span></span></p>
                        <h1>{{trans('message.thong_tin_ca_nhan')}}</h1>
                        <div class="content-report">
                            <table>
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text-center">{{trans('message.truoc_day')}}</th>
                                        <th class="text-center">{{trans('message.sau_nay')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{trans('message.ngay')}}</td>
                                        <td>
                                            <div class="input-group input-daterange">
                                                <input type="text" readonly class="form-control get-data" data="from" name="from" placeholder="dd-mm-yyyy" value=""/>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-group input-daterange">
                                                <input type="text" readonly class="form-control get-data" data="to" name="to" placeholder="dd-mm-yyyy" value=""/>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{trans('message.can_nang')}}</td>
                                        <td>
                                            <input type="text" class="get-data" data="weight_1" name="data[]" placeholder="---" />
                                            <span>KG</span>
                                        </td>
                                        <td>
                                            <input type="text" class="get-data" data="weight_2" name="data[]" placeholder="---" />
                                            <span>KG</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{trans('message.vong_eo')}}</td>
                                        <td>
                                            <input type="text" class="get-data" data="waist_1" name="data[]" placeholder="---" />
                                            <span>CM</span>
                                        </td>
                                        <td>
                                            <input type="text" class="get-data" data="waist_2" name="data[]" placeholder="---" />
                                            <span>CM</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{trans('message.bung')}}</td>
                                        <td>
                                            <input type="text" class="get-data" data="belly_1" name="data[]" placeholder="---" />
                                            <span>CM</span>
                                        </td>
                                        <td>
                                            <input type="text" class="get-data" data="belly_2" name="data[]" placeholder="---" />
                                            <span>CM</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{trans('message.hong')}}</td>
                                        <td>
                                            <input type="text" class="get-data" data="hips_1" name="data[]" placeholder="---" />
                                            <span>CM</span>
                                        </td>
                                        <td>
                                            <input type="text" class="get-data" data="hips_2" name="data[]" placeholder="---" />
                                            <span>CM</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{trans('message.dui')}}</td>
                                        <td>
                                            <input type="text" class="get-data" data="thigh_1" name="data[]" placeholder="---" />
                                            <span>CM</span>
                                        </td>
                                        <td>
                                            <input type="text" class="get-data" data="thigh_2" name="data[]" placeholder="---" />
                                            <span>CM</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{trans('message.chieu_cao')}}</td>
                                        <td>
                                            <input type="text" class="get-data" data="height_1" name="data[]" placeholder="---" />
                                            <span>CM</span>
                                        </td>
                                        <td>
                                            <input type="text" class="get-data" data="height_2" name="data[]" placeholder="---" />
                                            <span>CM</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="save-report clearfix">
                        <button type="button" class="pull-left next-step1" id="back-page">{{trans('message.tro_lai')}}</button>
                        <button type="button" class="pull-right active next-step3" id="next-page">{{trans('message.tiep_tuc')}}</button>
                    </div>
                </div>
            </div>
        </div>

        <!--Step 3-->
        <div id="report-page3" class="report-page" style="display: none">
            <div class="row ">
                <div class="col-sm-12 col-md-12 col-lg-6">
                    <div class="sidebar-left" style="height: 787px;">
                        <div class="bg_sidebar"></div>
                        <div class="image-customer">
                            <input type="hidden" id="image-other-input" name="image" />
                            <img id="image-other" src="{{asset('upload/images/users/blank.png')}}" />
                        </div>
                    </div>
                    <div class="update-avatar clearfix">
                        <a href="javascript:void(0)" class="save-page button-submit-form" data="3"><i class="icon-li"></i>{{trans('message.luu_lai_va_thoat_ra')}}</a>
                        <a href="javascript:void(0)" class="upload-image" id="upload-image"><i class="fa fa-camera" aria-hidden="true"></i> {{trans('message.upload_anh_khac')}}</a>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6">
                    <div class="report-page-right profile3">
                        <p class="id-user2">ID: <span></span></p>
                        <h1>{{trans('message.thong_tin_ca_nhan')}}</h1>
                        <div class="content-report">
                            <table>
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text-center">{{trans('message.truoc_day')}}</th>
                                        <th class="text-center">{{trans('message.sau_nay')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{trans('message.ngay')}}</td>
                                        <td>
                                            <div class="input-group input-daterange">
                                                <input type="text" readonly class="form-control get-data" data="from" name="from" placeholder="dd-mm-yyyy" value=""/>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-group input-daterange">
                                                <input type="text" readonly class="form-control get-data" data="to" name="to" placeholder="dd-mm-yyyy" value=""/>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{trans('message.can_nang')}}</td>
                                        <td>
                                            <input type="text" class="get-data" data="weight_1" name="data[]" placeholder="---" />
                                            <span>KG</span>
                                        </td>
                                        <td>
                                            <input type="text" class="get-data" data="weight_2" name="data[]" placeholder="---" />
                                            <span>KG</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{trans('message.vong_eo')}}</td>
                                        <td>
                                            <input type="text" class="get-data" data="waist_1" name="data[]" placeholder="---" />
                                            <span>CM</span>
                                        </td>
                                        <td>
                                            <input type="text" class="get-data" data="waist_2" name="data[]" placeholder="---" />
                                            <span>CM</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{trans('message.bung')}}</td>
                                        <td>
                                            <input type="text" class="get-data" data="belly_1" name="data[]" placeholder="---" />
                                            <span>CM</span>
                                        </td>
                                        <td>
                                            <input type="text" class="get-data" data="belly_2" name="data[]" placeholder="---" />
                                            <span>CM</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{trans('message.hong')}}</td>
                                        <td>
                                            <input type="text" class="get-data" data="hips_1" name="data[]" placeholder="---" />
                                            <span>CM</span>
                                        </td>
                                        <td>
                                            <input type="text" class="get-data" data="hips_2" name="data[]" placeholder="---" />
                                            <span>CM</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{trans('message.dui')}}</td>
                                        <td>
                                            <input type="text" class="get-data" data="thigh_1" name="data[]" placeholder="---" />
                                            <span>CM</span>
                                        </td>
                                        <td>
                                            <input type="text" class="get-data" data="thigh_2" name="data[]" placeholder="---" />
                                            <span>CM</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{trans('message.chieu_cao')}}</td>
                                        <td>
                                            <input type="text" class="get-data" data="height_1" name="data[]" placeholder="---" />
                                            <span>CM</span>
                                        </td>
                                        <td>
                                            <input type="text" class="get-data" data="height_2" name="data[]" placeholder="---" />
                                            <span>CM</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="save-report clearfix">
                        <button type="button" class="pull-left next-step2" id="back-page">{{trans('message.tro_lai')}}</button>
                        <button type="button" class="pull-right active button-submit-form" id="next-page">{{trans('message.tiep_tuc')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="modal fade" id="upload-file-product" role="dialog">
    <div class="modal-dialog" style="margin-top: 100px;max-width: 1000px; width: 95%;">
    <!-- Modal content-->
        <div class="modal-content">
            <form id="uploadfileimage" name="uploadfileimage" action="" enctype="multipart/form-data" method="post">
                <div class="modal-header">
                    <button style="position: absolute;top: 10px;right: 10px;font-family: arial" type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title" id="detail-pallet-qrcode">{{trans('message.upload_file_anh')}}</h4>
                </div>
                <div class="modal-body">
                    <div class="clearfix">
                        <div class="col-sm-12">
                            <div id="box-input-upload">
                                <input type-file="image" type="file" name="file_upload" class="custom-file-input" id="upload_file_image">
                                <label class="custom-file-label">{{trans('message.chon_file')}}</label>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="show-image col-12" style="margin-top: 30px;width: 100%;overflow: hidden;overflow-x: scroll;">
                            <img src="{{ asset('upload') }}/no-image.gif" id="cropbox" class="img" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="crop-image" class="btn btn-info">{{trans('message.cat_anh')}}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('message.dong_lai')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">

@stop
@section('body_script')
@includeif('partials.message')
<script src="{{ asset('js/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="{{asset('js/common.js?v='.Tool::getConst('VERSION_JS'))}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.Jcrop.min.js?v='.Tool::getConst('VERSION_JS')) }}"></script>
<script>
var usernames = <?php echo json_encode($usernames); ?>;
var dataCity = <?php echo json_encode($city); ?>;
$(document).ready(function() {
    $('.auto-submit').change(function() {
        $(this).parents('form').submit();
        return false;
    });
});
$('.input-daterange').datepicker({
    keyboardNavigation: false,
    forceParse: false,
    autoclose: true,
    format: 'dd-mm-yyyy'
});
</script>
<script>
    var uploadFileAttact = '{{route('admin.uploadFile')}}';
    var urlCropFileImage = '{{route('admin.cropFileImage')}}';
    var RATIO_USER_AVATAR = <?php echo Tool::getConst('RATIO_USER_AVATAR');?>;
    var RATIO_POSITION_USER_AVATAR = <?php echo Tool::getConst('RATIO_POSITION_USER_AVATAR');?>;
    var RATIO_USER_IMAGE = <?php echo Tool::getConst('RATIO_USER_IMAGE');?>;
    var RATIO_POSITION_USER_IMAGE = <?php echo Tool::getConst('RATIO_POSITION_USER_IMAGE');?>;
    var mesageError = {
        loi: "{{trans('message.loi')}}",
        ban_chua_nhap_du_du_lieu: "{{trans('message.ban_chua_nhap_du_du_lieu')}}",
        ten_dang_nhap_da_ton_tai: "{{trans('message.ten_dang_nhap_da_ton_tai')}}",
        ten_dang_nhap_khong_duoc_de_trong: "{{trans('message.ten_dang_nhap_khong_duoc_de_trong')}}",
        mat_khau_khong_duoc_de_trong: "{{trans('message.mat_khau_khong_duoc_de_trong')}}",
        ho_va_ten_khong_duoc_de_trong: "{{trans('message.ho_va_ten_khong_duoc_de_trong')}}",
        hom_thu_khong_dung: "{{trans('message.hom_thu_khong_dung')}}",
        ten_dang_nhap_da_ton_tai: "{{trans('message.ten_dang_nhap_da_ton_tai')}}",
        co_loi_xay_ra_thu_lai_sau: "{{trans('message.co_loi_xay_ra_thu_lai_sau')}}",
        dinh_dang_file_anh_khong_dung: "{{trans('message.dinh_dang_file_anh_khong_dung')}}",
    };
</script>
<script type="text/javascript" src="{{asset('js/dms/register.js?v='.Tool::getConst('VERSION_JS'))}}"></script>
@endsection
