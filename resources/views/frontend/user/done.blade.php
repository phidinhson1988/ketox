@extends('frontend.layouts.app')
@section('title', 'Register')
@section('content')
<style>
    .table-wrapper-scroll-y {
        display: block;
        max-height: 500px;
        overflow-y: auto;
        -ms-overflow-style: -ms-autohiding-scrollbar;
    }
</style>
<div class="container">
    <div id="report-page4" class="report-page">
        <div class="col-sm-12">
            <div class="complete-profile">
                <img src="{{ asset('/frontend/img/image-complete.png') }}" />
                <p>{{trans('message.chu_y_ban_nho_cap_nhat_report')}}.</p>
            </div>
        </div>
    </div>
</div>
@stop
@section('body_script')
@includeif('partials.message')
@endsection
