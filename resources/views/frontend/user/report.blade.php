@extends('frontend.layouts.app')
@section('title', trans('message.cap_nhat_thong_tin_ca_nhan'))
@section('content')
<style>
    .table-wrapper-scroll-y {
        display: block;
        max-height: 500px;
        overflow-y: auto;
        -ms-overflow-style: -ms-autohiding-scrollbar;
    }
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.Jcrop.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/datapicker/datepicker3.css') }}">
<?php

use App\Helpers\Facades\Tool;
use Carbon\Carbon;
?>
<div class="container">
    <form id="form-data-register" action="{{route('frontend.user.saveReport')}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" id="result-report" name="data-report" value="{{$user->data}}">
        <input type="hidden" name="userId" value="{{$user->id}}">
        <!--Step 1-->
        <div class="report-page">
            <div class="row ">
                <div class="col-sm-12 col-md-12 col-lg-6">
                    <div class="sidebar-left" style="height: 787px;">
                        <div class="bg_sidebar"></div>
                        <div class="image-customer">
                            <input type="hidden" id="image-other-input" data="<?php echo Tool::getImage($user->image, Tool::getConst('USER_UPLOAD'));?>" value="{{$user->image}}" name="image" />
                            <img id="image-other" data="{{$user->image}}" src="<?php echo Tool::getImage($user->image, Tool::getConst('USER_UPLOAD'));?>" />
                        </div>
                    </div>
                    <div class="update-avatar clearfix">
                        <a href="<?php echo Tool::getImage($user->image, Tool::getConst('USER_UPLOAD'));?>" class="save-page" target="_blank"><i class="icon-li"></i>{{trans('message.xem_anh')}}</a>
                        <a href="javascript:void(0)" class="upload-image" id="upload-image"><i class="fa fa-camera" aria-hidden="true"></i> {{trans('message.upload_anh_khac')}}</a>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6">
                    <div class="report-page-right your-report">
                        <p class="id-user">ID: {{$user->username}}</p>
                        <h1>{{trans('message.thong_tin_ca_nhan')}}</h1>
                        <div class="content-report clearfix">
                            <div id="box-content-1" class="clearfix" style="display: none; height: 488px;">
                                <div class="row" style="margin-bottom: 10px;">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{trans('message.mat_khau_moi')}}</label>
                                            <input id="password" type="password" class="form-control" placeholder="" name="password" />
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{trans('message.xac_nhan_mat_khau')}}</label>
                                            <input id="repassword" type="password" class="form-control" placeholder="" name="repassword" />
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group" style="margin-bottom: 20px;">
                                            <label>{{trans('message.ho_va_ten')}}</label>
                                            <input id="full_name" type="text" class="form-control" placeholder="{{trans('message.nhap_tai_day')}}" value="{{$user->full_name}}" name="full_name" />
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="form-group gender-check clearfix" style="margin-bottom: 20px;">
                                            <input id="select-gender" type="hidden" name="gender" value="{{$user->gender}}" />
                                            <label>{{trans('message.gioi_tinh')}}</label>
                                            <span id="gender-male" class="{{($user->gender == 1) ? 'active' : ''}}">{{trans('message.con_trai')}}</span>
                                            <span id="gender-female" class="{{($user->gender == 2) ? 'active' : ''}}">{{trans('message.con_gai')}}</span>
                                        </div>
                                        <div class="form-group" style="margin-bottom: 20px;">
                                            <label>{{trans('message.dia_chi')}}</label>
                                            <input type="text" class="form-control" placeholder="{{trans('message.nhap_tai_day')}}" value="{{$user->address}}" name="address" />
                                        </div>
                                        <div class="form-group" style="margin-bottom: 22px;">
                                            <label>{{trans('message.hom_thu')}}</label>
                                            <input id="email" type="text" class="form-control" placeholder="{{trans('message.nhap_tai_day')}}" value="{{$user->email}}" name="email" />
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="box-content-2" class="clearfix" style="display: none; height: 488px;">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group clearfix">
                                            <a style="float: left;" href="javascript:void(0)" id="upload-avatar" class="upload-image"><i class="fa fa-camera" aria-hidden="true"></i> {{trans('message.upload_anh_dai_dien')}}</a>
                                            <div id="image-detail" style="height: 156px;text-align: center;margin-bottom: 10px;">
                                                <input type="hidden" id="image-detail-user-input" data="<?php echo Tool::getImage($user->avatar, Tool::getConst('USER_UPLOAD'));?>" name="avatar" value="{{$user->avatar}}" />
                                                <img style="height: 100%; width: auto;display: inline-block" id="image-detail-user" data="{{$user->avatar}}" src="<?php echo Tool::getImage($user->avatar, Tool::getConst('USER_UPLOAD'));?>" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('message.thanh_pho')}}</label>
                                            <input type="hidden" name="city" id="input-city" value="{{$user->city_id}}"/>
                                            <div class="filter-item btn-group" style="width: 100%">
                                                <button id="btnGroupDrop1" style="width: 100%" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{($user->city_id) ? $city[$user->city_id] : 'Select your city'}}</button>
                                                <?php if ($city) {?>
                                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                    <?php foreach ($city as $keyCity => $itemValue) {
                                                        echo '<a class="dropdown-item select-city" data="'.$keyCity.'" href="javascript:void(0)">'.$itemValue.'</a>';
                                                    }?>
                                                </div>
                                                <?php }?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('message.so_dien_thoai')}}</label>
                                            <input type="text" class="form-control" placeholder="{{trans('message.nhap_tai_day')}}" name="phone" value="{{$user->phone}}" />
                                        </div>
                                        <div class="form-group" style="margin-bottom: 20px;">
                                            <label>{{trans('message.ngay_sinh')}}</label>
                                            <input type="date" class="form-control" placeholder="DD/MM/YYYY" name="birth_day" value="{{($user->birth_day) ? Carbon::parse($user->birth_day)->format('d/m/Y') : null}}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="box-content-3" class="clearfix" style="display: block; height: 488px;">
                                <table>
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th class="text-center">{{trans('message.truoc_day')}}</th>
                                            <th class="text-center">{{trans('message.sau_nay')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{trans('message.ngay')}}</td>
                                            <td>
                                                <div class="input-group input-daterange">
                                                    <input type="text" readonly class="form-control get-data" data="from" name="from" placeholder="dd-mm-yyyy" value="{{(isset($dataUser['from'])) ? $dataUser['from'] : ''}}"/>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="input-group input-daterange">
                                                    <input type="text" readonly class="form-control get-data" data="to" name="to" placeholder="dd-mm-yyyy" value="{{(isset($dataUser['to'])) ? $dataUser['to'] : ''}}"/>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{trans('message.can_nang')}}</td>
                                            <td>
                                                <input type="text" class="get-data" data="weight_1" name="data[]" value="{{(isset($dataUser['weight_1'])) ? $dataUser['weight_1'] : ''}}" placeholder="---" />
                                                <span>KG</span>
                                            </td>
                                            <td>
                                                <input type="text" class="get-data" data="weight_2" name="data[]" value="{{(isset($dataUser['weight_2'])) ? $dataUser['weight_2'] : ''}}" placeholder="---" />
                                                <span>KG</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{trans('message.vong_eo')}}</td>
                                            <td>
                                                <input type="text" class="get-data" data="waist_1" name="data[]" value="{{(isset($dataUser['waist_1'])) ? $dataUser['waist_1'] : ''}}" placeholder="---" />
                                                <span>CM</span>
                                            </td>
                                            <td>
                                                <input type="text" class="get-data" data="waist_2" name="data[]" value="{{(isset($dataUser['waist_2'])) ? $dataUser['waist_2'] : ''}}" placeholder="---" />
                                                <span>CM</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{trans('message.bung')}}</td>
                                            <td>
                                                <input type="text" class="get-data" data="belly_1" name="data[]" value="{{(isset($dataUser['belly_1'])) ? $dataUser['belly_1'] : ''}}" placeholder="---" />
                                                <span>CM</span>
                                            </td>
                                            <td>
                                                <input type="text" class="get-data" data="belly_2" name="data[]" value="{{(isset($dataUser['belly_2'])) ? $dataUser['belly_2'] : ''}}" placeholder="---" />
                                                <span>CM</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{trans('message.hong')}}</td>
                                            <td>
                                                <input type="text" class="get-data" data="hips_1" name="data[]" value="{{(isset($dataUser['hips_1'])) ? $dataUser['hips_1'] : ''}}" placeholder="---" />
                                                <span>CM</span>
                                            </td>
                                            <td>
                                                <input type="text" class="get-data" data="hips_2" name="data[]" value="{{(isset($dataUser['hips_2'])) ? $dataUser['hips_2'] : ''}}" placeholder="---" />
                                                <span>CM</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{trans('message.dui')}}</td>
                                            <td>
                                                <input type="text" class="get-data" data="thigh_1" name="data[]" value="{{(isset($dataUser['thigh_1'])) ? $dataUser['thigh_1'] : ''}}" placeholder="---" />
                                                <span>CM</span>
                                            </td>
                                            <td>
                                                <input type="text" class="get-data" data="thigh_2" name="data[]" value="{{(isset($dataUser['thigh_2'])) ? $dataUser['thigh_2'] : ''}}" placeholder="---" />
                                                <span>CM</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{trans('message.chieu_cao')}}</td>
                                            <td>
                                                <input type="text" class="get-data" data="height_1" name="data[]" value="{{(isset($dataUser['height_1'])) ? $dataUser['height_1'] : ''}}" placeholder="---" />
                                                <span>CM</span>
                                            </td>
                                            <td>
                                                <input type="text" class="get-data" data="height_2" name="data[]" value="{{(isset($dataUser['height_2'])) ? $dataUser['height_2'] : ''}}" placeholder="---" />
                                                <span>CM</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="pre-next clearfix">
                                <button id="prev-page" data="2" type="button" class="pull-left">{{trans('message.tro_lai')}}</button>
                                <button id="next-page" data="3" type="button" class="pull-right">{{trans('message.tiep_tuc')}}</button>
                            </div>
                        </div>
                    </div>
                    <div class="save-report clearfix">
                        <!--<button type="button" class="pull-left" id="back-page">Share report</button>-->
                        <button type="button" class="pull-right active" id="submit-report">{{trans('message.cap_nhat_thong_tin')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="modal fade" id="upload-file-product" role="dialog">
    <div class="modal-dialog" style="margin-top: 100px;max-width: 1000px; width: 95%;">
    <!-- Modal content-->
        <div class="modal-content">
            <form id="uploadfileimage" name="uploadfileimage" action="" enctype="multipart/form-data" method="post">
                <div class="modal-header">
                    <button style="position: absolute;top: 10px;right: 10px;font-family: arial" type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title" id="detail-pallet-qrcode">{{trans('message.upload_file_anh')}}</h4>
                </div>
                <div class="modal-body">
                    <div class="clearfix">
                        <div class="col-sm-12">
                            <div id="box-input-upload">
                                <input type-file="image" type="file" name="file_upload" class="custom-file-input" id="upload_file_image">
                                <label class="custom-file-label">{{trans('message.chon_file')}}</label>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="show-image col-12" style="margin-top: 30px;width: 100%;overflow: hidden;overflow-x: scroll;">
                            <img src="{{ asset('upload') }}/no-image.gif" id="cropbox" class="img" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="crop-image" class="btn btn-info">{{trans('message.cat_anh')}}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('message.dong_lai')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">

@stop
@section('body_script')
@includeif('partials.message')
<script src="{{ asset('js/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="{{asset('js/common.js?v='.Tool::getConst('VERSION_JS'))}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.Jcrop.min.js?v='.Tool::getConst('VERSION_JS')) }}"></script>
<script>
var dataCity = <?php echo json_encode($city); ?>;
$(document).ready(function() {
    $('.auto-submit').change(function() {
        $(this).parents('form').submit();
        return false;
    });
});
$('.input-daterange').datepicker({
    keyboardNavigation: false,
    forceParse: false,
    autoclose: true,
    format: 'dd-mm-yyyy'
});
</script>
<?php if ($user->data) {?>
    <script>var dataReport = <?php echo $user->data;?>;</script>
<?php } else {?>
    <script>var dataReport = {};</script>
<?php }?>
<script>
    var uploadFileAttact = '{{route('admin.uploadFile')}}';
    var urlCropFileImage = '{{route('admin.cropFileImage')}}';
    var RATIO_USER_AVATAR = <?php echo Tool::getConst('RATIO_USER_AVATAR');?>;
    var RATIO_POSITION_USER_AVATAR = <?php echo Tool::getConst('RATIO_POSITION_USER_AVATAR');?>;
    var RATIO_USER_IMAGE = <?php echo Tool::getConst('RATIO_USER_IMAGE');?>;
    var RATIO_POSITION_USER_IMAGE = <?php echo Tool::getConst('RATIO_POSITION_USER_IMAGE');?>;
    var mesageError = {
        loi: "{{trans('message.loi')}}",
        ban_chua_nhap_du_du_lieu: "{{trans('message.ban_chua_nhap_du_du_lieu')}}",
        ten_dang_nhap_da_ton_tai: "{{trans('message.ten_dang_nhap_da_ton_tai')}}",
        ten_dang_nhap_khong_duoc_de_trong: "{{trans('message.ten_dang_nhap_khong_duoc_de_trong')}}",
        mat_khau_khong_duoc_de_trong: "{{trans('message.mat_khau_khong_duoc_de_trong')}}",
        ho_va_ten_khong_duoc_de_trong: "{{trans('message.ho_va_ten_khong_duoc_de_trong')}}",
        hom_thu_khong_dung: "{{trans('message.hom_thu_khong_dung')}}",
        ten_dang_nhap_da_ton_tai: "{{trans('message.ten_dang_nhap_da_ton_tai')}}",
        co_loi_xay_ra_thu_lai_sau: "{{trans('message.co_loi_xay_ra_thu_lai_sau')}}",
        dinh_dang_file_anh_khong_dung: "{{trans('message.dinh_dang_file_anh_khong_dung')}}",
        xac_nhan_mat_khau_khong_dung: "{{trans('message.xac_nhan_mat_khau_khong_dung')}}",
    };
</script>
<script type="text/javascript" src="{{asset('js/dms/report.js?v='.Tool::getConst('VERSION_JS'))}}"></script>
@endsection
