@extends('layouts.master')
@section('title', trans('message.lich_su_truy_cap'))
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}"><i class="fa fa-home"></i> {{ trans('message.trang_chu') }}</a>
                </li>
                <li class="breadcrumb-item active">
                    <a href="{{route('user.log')}}"><strong>{{ trans('message.lich_su_truy_cap') }}</strong></a>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h3><i class="fa fa-users"></i> {{ trans('message.lich_su_truy_cap') }}</h3>
                    </div>
                    <div class="ibox-content">

                        <form name="form1" action="{{route('user.log')}}" method="GET">
                            <div class="row">
                                <div class="col-lg-3 col-sm-6">
                                    <div class="input-group">
                                        @include('partials.limit')
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="input-group input-daterange auto-submit" id="datepicker">
                                        <span class="input-group-addon"><strong>{{ trans('message.tu') }}</strong></span>
                                        <input type="text" class="form-control" name="from" value="{{ Request::get('from') ?: \Carbon\Carbon::now()->subMonth(1)->format('d-m-Y') }}"/>
                                        <span class="input-group-addon"><strong>{{ trans('message.den') }}</strong></span>
                                        <input type="text" class="form-control" name="to" value="{{ Request::get('to') ?: \Carbon\Carbon::now()->format('d-m-Y') }}" />
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div class="input-group">
                                        <input
                                                placeholder="{{ trans('message.nhap_ten_dang_nhap') }}"
                                                type="text" name="q" class="form-control"
                                                value="{{Request::get('q') ?:null}}">
                                        <span class="input-group-append">
                                        <button type="submit" class="btn btn-sm btn-primary"><i
                                                    class="fa fa-search"></i>
                                    </button> </span></div>
                                </div>

                            </div>
                        </form>

                        <br>
                        @if(isset($userlog) && count($userlog) > 0)
                        <div class="table-responsive">

                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>{{ trans('message.ma_nhan_vien') }}</th>
                                    <th>{{ trans('message.ten_dang_nhap') }}</th>
                                    <th>{{ trans('message.ho_va_ten') }}</th>
                                    <th>{{ trans('message.thoi_gian') }}</th>
                                    <th width="5%" class="text-center">{{ trans('message.hanh_dong') }}</th>
                                </tr>
                                </thead>
                                <tbody>

                                    @foreach($userlog as $value)
                                        <tr>
                                            <td>{{$value->qr_code}}</td>
                                            <td>{{$value->username}}</td>
                                            <td>{{$value->name}}</td>
                                            <td>{{ \Carbon\Carbon::parse($value->time)->format('d-m-Y H:i:s')}}</td>
                                            <td class="text-center">
                                                @if($value->action == 1)
                                                    <label class="bg-success p-xxs b-r-xs" style="width: 120px">{{ trans('message.log_in') }}</label>
                                                @elseif($value->action == 2)
                                                    <label class="bg-danger p-xxs b-r-xs" style="width: 120px">{{ trans('message.log_out') }}</label>
                                                @else
                                                    <label class="bg-warning p-xxs b-r-xs" style="width: 120px">{{ trans('message.not_record') }}</label>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            @includeif('partials.pagination', [
                               'pagination'  => $pagination,
                               'data'   => $userlog->toArray(),
                               'hidden' => [
                                             'from' => Request::get('from', Carbon\Carbon::now()->subMonth(1)->format('d-m-Y')),
                                             'to'   => Request::get('to', Carbon\Carbon::now()->format('d-m-Y')),
                                             ],
                           ])
                        </div>
                        @else
                            @includeIf('partials.empty');
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop



