@extends('layouts.master')
@section('title', trans('message.thay_doi_mat_khau'))
@section('content')
<?php use App\Helpers\Facades\Tool;?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}"><i class="fa fa-home fa-fw"></i> {{ trans('message.trang_chu') }}</a>
                </li>
                <li class="breadcrumb-item active">
                    <a>{{ trans('message.thay_doi_mat_khau') }}</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                {!! Form::open(['method' => 'PUT', 'action' => 'UserController@postChangePassword', 'class' => 'form-horizontal']) !!}
                <div class="ibox ">
                    <div class="ibox-title">
                        <h3><i class="fa fa-key"></i> {{ trans('message.thay_doi_mat_khau') }}</h3>
                    </div>
                    <div class="ibox-content">
                        {{ csrf_field() }}
                        <div class="form-group row {{ Tool::classError('old_password', $errors) }}">
                            <label class="col-sm-2 col-form-label">{{ trans('message.mat_khau_cu') }} <span class="require"></span></label>
                            <div class="col-sm-6">
                                <input type="password" name="old_password" value="{{ old('old_password') }}" class="form-control" placeholder="{{ trans('message.nhap_mat_khau_cu') }}">
                                @if ($errors->has('old_password'))
                                    <span class="help-block">{{ $errors->first('old_password') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row  {{ Tool::classError('new_password', $errors) }}">
                            <label class="col-sm-2 col-form-label">{{ trans('message.mat_khau_moi') }} <span class="require"></span></label>
                            <div class="col-sm-6">
                                <input type="password" name="new_password" placeholder="{{ trans('message.nhap_mat_khau_moi') }}" class="form-control" value="{{ old('new_password') }}"/>
                                @if ($errors->has('new_password'))
                                    <span class="help-block">{{ $errors->first('new_password') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row  {{ Tool::classError('password_confirmation', $errors) }}">
                            <label class="col-sm-2 col-form-label">{{ trans('message.xac_nhan_mat_khau') }} <span class="require"></span></label>
                            <div class="col-sm-6">
                                <input type="password" name="password_confirmation" placeholder="{{ trans('message.xac_nhan_mat_khau_moi') }}" class="form-control" value="{{ old('password_confirmation') }}"/>
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row">
                            <div class="col-sm-8 offset-2">

                                <button class="btn btn-primary btn-lg" type="button" title="{{ trans('mat_khau') }}"
                                        onclick="event.preventDefault(); commonFunc.confirm(this, '{{ trans('message.mat_khau') }}', 2, 'vi', 'false');"
                                        ><i class="fa fa-save fa-fw"></i> {{ trans('message.luu_lai') }}</button>
                                <a href="{{route('user.password')}}"><button class="btn btn-danger btn-lg" type="button"><i class="fa fa-undo fa-fw"></i> {{ trans('message.nhap_lai') }}</button></a>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('body_script')
    @includeif('partials.message')
@endsection

