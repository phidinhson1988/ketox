@extends('layouts.master')
@section('title', trans('message.them_moi_tai_khoan'))
@section('head_stylesheet')
    <style>
        .table-wrapper-scroll-y {
            display: block;
            max-height: 500px;
            overflow-y: auto;
            -ms-overflow-style: -ms-autohiding-scrollbar;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.Jcrop.min.css') }}"/>
@endsection
@section('content')
<?php use App\Helpers\Facades\Tool;?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}"><i class="fa fa-home fa-fw"></i> {{ trans('message.trang_chu') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('user.index') }}"> {{ trans('message.danh_sach_tai_khoan') }}</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>{{ trans('message.them_moi_tai_khoan') }}</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h3><i class="fa fa-users"></i> {{ trans('message.them_moi_tai_khoan') }}</h3>
                    </div>
                    <div class="ibox-content">
                        <form id="form-user" name="customFile6" action="{{route('user.store')}}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group row {{ Tool::classError('username', $errors) }}">
                                        <label class="col-sm-4 col-form-label">{{ trans('message.ten_dang_nhap') }} <span
                                                    class="require"></span></label>
                                        <div class="col-sm-8"><input type="text" name="username"
                                                                     value="{{ old('username') }}"
                                                                     placeholder="{{ trans('message.nhap_ten_dang_nhap') }}"
                                                                     class="form-control">
                                            @if ($errors->has('username'))
                                                <span class="help-block">{{ $errors->first('username') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row {{ Tool::classError('password',$errors) }}">
                                        <label class="col-sm-4 col-form-label">{{ trans('message.mat_khau') }} <span
                                                    class="require"></span></label>
                                        <input type="hidden" name="password" value="123456">
                                        <label class="col-sm-8 col-form-label text-danger">123456</label>
                                    </div>
                                    <div class="form-group row {{ Tool::classError('full_name',$errors) }}">
                                        <label class="col-sm-4 col-form-label">{{ trans('message.ho_va_ten') }} <span
                                                    class="require"></span></label>
                                        <div class="col-sm-8">
                                            <input type="text" name="full_name" value="{{ old('full_name') }}" placeholder="{{ trans('message.nhap_ho_va_ten') }}" class="form-control">
                                            @if ($errors->has('full_name'))
                                                <span class="help-block">{{ $errors->first('full_name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row {{ Tool::classError('phone',$errors) }}">
                                        <label class="col-sm-4 col-form-label">{{ trans('message.dien_thoai') }}</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="phone" value="{{ old('phone') }}" placeholder="" class="form-control">
                                            @if ($errors->has('phone'))
                                                <span class="help-block">{{ $errors->first('phone') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row {{ Tool::classError('email',$errors) }}">
                                        <label class="col-sm-4 col-form-label">{{ trans('message.hom_thu') }}</label>
                                        <div class="col-sm-8"><input type="text" name="email" value="{{ old('email') }}"
                                                                     placeholder="{{ trans('message.nhap_dia_chi_mail') }}"
                                                                     class="form-control">
                                            @if ($errors->has('email'))
                                                <span class="help-block">{{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row {{ Tool::classError('role_id', $errors) }}">
                                        <label class="col-sm-4 col-form-label">{{ trans('message.phan_quyen') }} <span
                                                    class="require"></span></label>
                                        <div class="col-sm-8">
                                            <select id="role_id" class="form-control m-b select2" name="role_id">
                                                <option value="0">{{ trans('message.chon_quyen_han') }}</option>
                                                @if(isset($roles) && count($roles) >0)
                                                    @foreach($roles as $value )
                                                        <option value="{{$value->id}}" {{ ( old('role_id') == $value->id) ? 'selected' : '' }}>{{$value->name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            @if ($errors->has('role_id'))
                                                <span class="help-block">{{ $errors->first('role_id') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group row">
                                        <input id="image-file" type="hidden" name="avatar" />
                                        <label class="col-sm-2 col-form-label">{{ trans('message.anh_dai_dien') }}</label>
                                        <div class="col-sm-6 box-upload-file">
                                            <div class="custom-file" style="margin-bottom: 20px;">
                                                <input type-file="image" data-name="customFile6" type="file" name="file_upload" class="custom-file-input" id="customFile6">
                                                <label class="custom-file-label" for="customFile6">{{ trans('message.chon_file') }}</label>
                                                <div class="invalid-feedback" style="font-size: 14px;"></div>
                                            </div>
                                            <ul class="list-show-image" style="width: 100%;padding-top:0"></ul>
                                        </div>
                                        <br/>
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row">
                                <div class="col-sm-8 offset-2">
                                    <button class="btn btn-primary btn-lg" type="submit"><i
                                                class="fa fa-save fa-fw"></i>
                                        {{ trans('message.luu_lai') }}
                                    </button>
                                    <a href="{{ route('user.create') }}">
                                        <button class="btn btn-danger btn-lg" type="button"><i
                                                    class="fa fa-undo fa-fw"></i>
                                            {{ trans('message.nhap_lai') }}
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="viewDetail" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title" id="detail-pallet-qrcode">{{ trans('message.cat_anh') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 show-image">
                            <img src="{{ asset('upload') }}/no-image.gif" id="cropbox" class="img" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="crop-image" class="btn btn-info">{{ trans('message.cat_anh') }}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('message.dong_lai') }}</button>
                </div>

            </div>
        </div>
    </div>
    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
@stop
@section('body_script')
    <script type="text/javascript" src="{{ asset('js/jquery.Jcrop.min.js?v='.Tool::getConst('VERSION_JS')) }}"></script>
    <script>
        var statusImage = false;
        $('.input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "d-m-yyyy",
        });

        $("input.img-choice").change(function () {
            var self = $(this);
            self.parent().parent().removeClass('has-error');
            self.parent().parent().parent().find('.help-block').hide();
            var size = parseInt(this.files[0].size);
            if (size > 2097152) {    // 15728640 byte ~ 15MB
                var largestImgCapacity = "{{ trans('message.dung_luong_anh_khong_duoc_vuot_qua_2MB') }}";
                self.parent().parent().addClass('has-error');
                self.parent().parent().parent().find('.help-block').text(largestImgCapacity).show();
                statusImage = false;
                return false;
            }
            var extension = $(this).val().split('.').pop().toLowerCase();
            if (extension == 'jpg' || extension == 'png' || extension == 'jpeg') {
                readURL(this);
            } else {
                var formatImg = "{{ trans('message.anh_phai_thuoc_mot_trong_cac_dinh_dang_png_jpg_jpeg') }}";
                statusImage = false;
                self.parent().parent().addClass('has-error');
                self.parent().parent().parent().find('.help-block').text(formatImg).show();
            }
            return false;
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var self = $(input);
                var reader = new FileReader();
                reader.onload = e => {
                    self.closest('div.form-group').next('div.form-group').find('img.img-preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
                statusImage = true;
            } else {
                statusImage = false;
            }
        }
        $("#form-user").submit(function(){
            if (document.getElementById("image-file").files.length > 0 && !statusImage) {
                return false;
            }
            return true;
        });
    </script>
    <script>
        var uploadFileAttact = '{{route('admin.uploadFile')}}';
        var urlCropFileImage = '{{route('admin.cropFileImage')}}';
        var idTarget = '';
        var RATIO_IMAGE_CARD = <?php echo Tool::getConst('RATIO_IMAGE_USER');?>;
        var RATIO_POSITION_CARD = <?php echo Tool::getConst('RATIO_POSITION_USER');?>;
    </script>
    <script type="text/javascript" src="{{asset('js/dms/product.js?v='.Tool::getConst('VERSION_JS'))}}"></script>
@endsection

