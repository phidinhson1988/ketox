@extends('layouts.master')
@section('title', trans('message.thong_tin_tai_khoan'))
@section('head_stylesheet')
    <style>
        .table-wrapper-scroll-y {
            display: block;
            max-height: 500px;
            overflow-y: auto;
            -ms-overflow-style: -ms-autohiding-scrollbar;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.Jcrop.min.css') }}"/>
@endsection
@section('content')
<?php use App\Helpers\Facades\Tool;?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}"><i class="fa fa-home"></i> {{ trans('message.trang_chu') }}</a>
                </li>
                <li class="breadcrumb-item active">
                    <a>{{ trans('message.thong_tin_tai_khoan') }}</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h3><i class="fa fa-user"></i> {{ trans('message.thong_tin_tai_khoan') }}</h3>
                    </div>
                    <div class="ibox-content">

                        <form id="form-user" name="customFile6" action="{{ route('user.editProfile', Auth::user()->id) }}" method="POST" enctype="multipart/form-data">
                        <div class="row">

                            <div class="col-sm-6"><h3 class="m-t-none m-b"></h3>
                                {{ csrf_field() }}
                                <div class="form-group row {{ Tool::classError('full_name', $errors) }}">
                                    <label class="col-sm-4 col-form-label"> {{trans('message.ho_va_ten') }} <span
                                                class="require"></span></label>
                                    <div class="col-sm-8"><input type="text" name="full_name" class="form-control"
                                                                 placeholder="{{ trans('message.nhap_ho_va_ten') }}"
                                                                 value="{{isset(Auth::user()->full_name) ? Auth::user()->full_name:''}}">
                                        @if ($errors->has('full_name'))
                                            <span class="help-block">{{ $errors->first('full_name') }}</span>
                                        @endif
                                    </div>

                                </div>
                                <div class="form-group row {{ Tool::classError('name', $errors) }}">
                                    <label class="col-sm-4 col-form-label">{{ trans('message.dien_thoai') }}</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="phone" class="form-control" placeholder="" value="{{isset(Auth::user()->phone) ? Auth::user()->phone:''}}">
                                        @if ($errors->has('phone'))
                                            <span class="help-block">{{ $errors->first('phone') }}</span>
                                        @endif
                                    </div>

                                </div>
                                <div class="form-group row {{ Tool::classError('email', $errors) }}">
                                    <label class="col-sm-4 col-form-label">{{ trans('message.hom_thu') }} <span
                                                class="require"></span></label>
                                    <div class="col-sm-8"><input type="text" class="form-control" name="email"
                                                                 placeholder="{{ trans('message.nhap_dia_chi_mail') }}"
                                                                 value="{{ old('email', isset(Auth::user()->email) ? Auth::user()->email:'' ) }}">
                                        @if ($errors->has('email'))
                                            <span class="help-block">{{ $errors->first('email') }}</span>
                                        @endif

                                    </div>
                                </div>
                                <div class="form-group row"><label
                                            class="col-sm-4 col-form-label">{{ trans('message.phan_quyen') }}</label>
                                    <div class="col-sm-8" style="padding-top: 4px;">
                                        @if(!empty($roleUser->role->name))
                                            <label class="label label-primary text-uppercase">{{isset($roleUser) ? $roleUser->role->name:''}}</label>
                                        @else
                                            <label class="label label-primary text-uppercase">{{ trans('message.khong_co_quyen_han') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h3 class="m-t-none m-b"></h3>
                                <div class="form-group row">
                                    <input id="image-file" type="hidden" name="avatar" value="{{Auth::user()->avatar}}" />
                                    <label class="col-sm-2 col-form-label">{{ trans('message.anh_dai_dien') }}</label>
                                    <div class="col-sm-6 box-upload-file">
                                        <div class="custom-file" style="margin-bottom: 20px;">
                                            <input type-file="image" data-name="customFile6" type="file" name="file_upload" class="custom-file-input" id="customFile6">
                                            <label class="custom-file-label" for="customFile6">{{ trans('message.chon_file') }}</label>
                                            <div class="invalid-feedback" style="font-size: 14px;"></div>
                                        </div>
                                        <ul class="list-show-image" style="width: 100%;padding-top:0">
                                            <?php if (Auth::user()->avatar) {
                                                $cardImage = Tool::getImage(Auth::user()->avatar, Tool::getConst('USER_UPLOAD')); ?>
                                                <li class="col-12">
                                                    <span class="delete-image">x</span>
                                                    <img class="new-image-crop" data="{{Auth::user()->avatar}}" src="{{$cardImage}}">
                                                    <input class="customFile6" id="customFile6-image-hero" type="hidden" name="" data="{{$cardImage}}" value="{{Auth::user()->avatar}}">
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <br/>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button class="btn btn-primary btn-lg" type="submit">
                                            <i class="fa fa-fw fa-save"></i> {{ trans('message.luu_thay_doi') }}
                                        </button>
                                        <a href="{{route('user.profile')}}">
                                            <button class="btn btn-danger btn-lg" type="button">
                                                <i class="fa fa-fw fa-undo"></i> {{ trans('message.nhap_lai') }}
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="viewDetail" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title" id="detail-pallet-qrcode">{{ trans('message.cat_anh') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 show-image">
                            <img src="{{ asset('upload') }}/no-image.gif" id="cropbox" class="img" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="crop-image" class="btn btn-info">{{ trans('message.cat_anh') }}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('message.dong_lai') }}</button>
                </div>

            </div>
        </div>
    </div>
    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
@stop
@section('body_script')
    @includeif('partials.message')
    <script type="text/javascript" src="{{ asset('js/jquery.Jcrop.min.js?v='.Tool::getConst('VERSION_JS')) }}"></script>
    <script>
        var uploadFileAttact = '{{route('admin.uploadFile')}}';
        var urlCropFileImage = '{{route('admin.cropFileImage')}}';
        var idTarget = '';
        var RATIO_IMAGE_CARD = <?php echo Tool::getConst('RATIO_IMAGE_USER');?>;
        var RATIO_POSITION_CARD = <?php echo Tool::getConst('RATIO_POSITION_USER');?>;
    </script>
    <script type="text/javascript" src="{{asset('js/dms/product.js?v='.Tool::getConst('VERSION_JS'))}}"></script>
@endsection

