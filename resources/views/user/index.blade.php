@extends('layouts.master')
@section('title', trans('message.danh_sach_tai_khoan'))
@section('content')
<?php use App\Helpers\Facades\Tool; ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2></h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('home') }}"><i class="fa fa-home fa-fw"></i> {{ trans('message.trang_chu') }}</a>
            </li>
            <li class="breadcrumb-item active">
                <a href="{{route('user.index')}}"><strong>{{ trans('message.danh_sach_tai_khoan') }}</strong></a>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h3><i class="fa fa-users fa-fw"></i> {{ trans('message.danh_sach_tai_khoan') }}</h3>
                    @if (Entrust::can('user-create'))
                    <a href="{{ route('user.create') }}" class="btn btn-primary pull-right"><i
                            class="fa fa-plus-square fa-fw"></i> {{ trans('message.them_moi') }}</a>
                    @endif
                </div>
                <div class="ibox-content">
                    <form name="form1" action="{{ route('user.index') }}" method="GET">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6">
                                @include('partials.limit')
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <select id="role_id" class="form-control select2" name="role_id"
                                        onchange="$(this).closest('form').submit();">
                                    <option value="0"> {{ trans('message.chon_nhom_quyen_han') }}</option>
                                    @foreach($roles as $value)
                                    <option @if( isset($_GET['role_id']) && $_GET['role_id'] ==  $value->id ) selected
                                             @endif value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="input-group" {{ Tool::classError('name_search', $errors) }}>
                                     <input
                                        placeholder="{{ trans('message.nhap_ten_dang_nhap') }}"
                                        type="text" name="q" class="form-control"
                                        value="{{ urldecode(Request::get('q')) ?: null }}">
                                    <span class="input-group-append"> <button type="submit"
                                                                              class="btn btn-sm btn-primary"><i
                                                class="fa fa-search"></i>

                                            @if ($errors->has('name_search'))
                                            <span class="help-block">{{ $errors->first('name_search') }}</span>
                                            @endif

                                        </button> </span>
                                </div>
                            </div>
                        </div>
                    </form>
                    <br>
                    @if(isset($users) && $users->count() > 0)
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">{{ trans('message.hinh_anh') }}</th>
                                    <th class="text-center">{{ trans('message.ten_dang_nhap') }}</th>
                                    <th class="text-center">{{ trans('message.ten') }}</th>
                                    <th class="text-center">{{ trans('message.hom_thu') }}</th>
                                    <th class="text-center">{{ trans('message.dien_thoai') }}</th>
                                    <th class="text-center">{{ trans('message.quyen') }}</th>
                                    <th width="5%" class="text-center">{{ trans('message.thao_tac') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td class="feed-element text-center">
                                        <img alt="image" class="img-circle" width="48px" height="48px"
                                             src="{{ Tool::getImage($user->avatar, \App\Helpers\Facades\Tool::getConst('USER_UPLOAD')) }}">
                                    </td>
                                    <td class="text-center">{{$user->username}}</td>
                                    <td class="text-center">{{$user->full_name}}</td>
                                    <td class="text-center">{{$user->email}}</td>
                                    <td class="text-center">{{$user->phone}}</td>
                                    <td class="text-center">
                                        @if(!empty($user->role()->where('status', \App\Helpers\Facades\Tool::getConst('ACTIVE_ON'))->first()))
                                        {{ $user->role()->first()->name }}
                                        @endif
                                    </td>

                                    <td class="text-center no-wrap">

                                        <form method="post" action="{{ route('user.delete', $user->id ) }}"
                                              accept-charset="UTF-8">

                                            <input name="_method" type="hidden" value="delete">
                                            <input name="user_id" type="hidden" value="{{$user->id}}">
                                            <a title="Chi tiết" href="{{route('frontend.user.profile', ['username' => $user->username])}}" target="_blank" class="btn btn-white btn-sm"><i class="fa fa-eye"></i></a>
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_token" id="token"
                                                   value="{{ csrf_token() }}">
                                            @if (Entrust::can('user-edit'))
                                            <a title="{{ trans('message.sua') }}"
                                               href="{{ route('user.edit',['id'=>$user->id]) }}"
                                               class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                                            @endif
                                            @if (Entrust::can('user-reset'))
                                            <a title="Reset password"
                                               href="{{route('user.resetPassword',['id'=>$user->id])}}"
                                               class="btn btn-warning btn-sm resetPassword"><i class="fa fa-undo"></i></a>
                                            @endif
                                            @if (Entrust::can('user-delete'))
                                            @if(!$user->hasRole('root') || !$user->username == 'administrator')
                                            <button type="button" title="{{ trans('message.xoa') }}"
                                                    class="btn btn-danger btn-sm delete-button"><i
                                                    class="fa fa-trash-o"></i></button>
                                            @endif
                                            @endif
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        @includeif('partials.pagination', [
                        'pagination'  => $pagination,
                        'data'   => $users->toArray(),
                        ])
                    </div>
                    @else
                    @includeIf('partials.empty');
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('body_script')
@includeif('partials.message')
<script>
            $(".resetPassword").click(function () {
    var self = $(this);
            swal({
            buttons: {
            confirm: {
            text: 'OK',
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true,
            },
                    cancel: {
                    text: 'Cancel',
                            value: null,
                            visible: true,
                            className: "",
                            closeModal: true,
                            buttons: true,
                    },
            },
                    dangerMode: true,
                    title: "{{ trans('message.canh_bao') }}",
                    text: "{{ trans('message.ban_co_muon_reset_mat_khau_cua_nguoi_dung_khong') }}",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '{{ trans('message.co') }}',
                    cancelButtonText: '{{ trans('message.khong') }}'
            }).then(function (isConfirm) {
    if (isConfirm.value) {
    var url = self.attr("href");
            window.location.href = url;
            return true;
    } else {
    return false;
    }
    }).catch(swal.noop);
            return false;
    });
            $(".delete-button").click(function () {
    var self = $(this);
            swal({
            buttons: {
            confirm: {
            text: 'OK',
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true,
            },
                    cancel: {
                    text: 'Cancel',
                            value: null,
                            visible: true,
                            className: "",
                            closeModal: true,
                            buttons: true,
                    },
            },
                    dangerMode: true,
                    title: "{{ trans('message.canh_bao') }}",
                    text: "{{ trans('message.ban_co_muon_xoa_tai_khoan_nay_khong') }}",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '{{ trans('message.co') }}',
                    cancelButtonText: '{{ trans('message.khong') }}'
            }).then(function (isConfirm) {
    if (isConfirm.value) {
    self.parent().submit();
            return true;
    } else {
    return false;
    }
    }).catch(swal.noop);
            return false;
    });
</script>
@endsection
