@extends('layouts.master')
@section('title',trans('message.quan_ly_menu'))
@section('head_stylesheet')
    <link href="{{ asset('js/plugins/jquery-nestable/nestable.css') }}" rel="stylesheet" type="text/css" />
<style>
    .table-wrapper-scroll-y {
        display: block;
        max-height: 500px;
        overflow-y: auto;
        -ms-overflow-style: -ms-autohiding-scrollbar;
    }
    #nestable-menu-top {
        max-width: 1000px;
    }
#nestable-output-menu-top {
    float: none;
    margin: 0 auto;
}
#nestable-menu-top span.dd-handle {
    width: 100%;
    display: block;
    padding-right: 65px;
    padding-left: 30px;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
    font-size: 20px;
    font-family: aria;
    height: 50px;
    margin: 10px 0;
    line-height: 34px;
}
#nestable-menu-top li.dd-item button {
    position: absolute;
    top: 10px;
    left: 5px;
}
 #nestable-menu-top .options {
    position: absolute;
    top: 6px;
    right: 6px;
}
</style>
@endsection
@section('content')
<?php use App\Helpers\Facades\Tool;
$titlePage = '';
switch ($keyMenu) {
    case 'home-menu-top':
        $titlePage = trans('message.quan_ly_menu_top');
        break;
    case 'home-menu-top-en':
        $titlePage = trans('message.quan_ly_menu_top_en');
        break;
    case 'home-menu-bottom':
        $titlePage = trans('message.quan_ly_menu_bottom');
        break;
    case 'home-menu-bottom-en':
        $titlePage = trans('message.quan_ly_menu_bottom_en');
        break;
    default :
        break;
 }
?>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">
                    <strong>{{$titlePage}}</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h3><i class="fa fa-unlock-alt"></i> {{$titlePage}}</h3>
                    </div>
                    <div class="ibox-content">
                        <div class="page-content" style="background-color: #fff;padding: 15px;">
                            <div class="row">
                                <div class="col-sm-12 col-xs-12 clearfix web-content-form" style="padding: 0 5px;">
                                    <div id="menu-top-form" data="manage-menu-top" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 content-landing-page" style="display: block;">
                                        <div class="panel panel-blue">

                                            <div class="panel-body">
                                                <textarea id="nestable-output-menu-top" style="display: none;"></textarea>
                                                <div id="nestable-menu-top" class="dd">
                                                    <ol class="dd-list">
                                                        <?php 
                                                            if ($treeOrderMenuTop) {
                                                                foreach ($treeOrderMenuTop as $multipleMenuTop) { ?>
                                                                <li class="dd-item" data-id="<?php echo $multipleMenuTop['id'];?>">
                                                                    <span class="dd-handle"><?php echo $multipleMenuTop['title'];?></span>
                                                                    <div class="options" data="{{$multipleMenuTop['id']}}">
                                                                        <span style="width: 45px;height: 38px;text-align: center;padding-left: 8px;font-size: 24px;" class="btn btn-default btn-xs edit-menu"><i class="fa fa-edit"></i></span>
                                                                        <span style="width: 45px;height: 38px;text-align: center;padding-left: 8px;font-size: 24px;line-height: 32px;" class="btn btn-default btn-xs delete-menu"><i class="fa fa-times"></i></span>
                                                                    </div>
                                                                    <?php 
                                                                        if ($multipleMenuTop['childrent']) {
                                                                            echo '<ol class="dd-list">';
                                                                            foreach ($multipleMenuTop['childrent'] as $subMenuTop1) { ?>
                                                                                <li class="dd-item" data-id="<?php echo $subMenuTop1['id'];?>">
                                                                                    <span class="dd-handle"><?php echo $subMenuTop1['title'];?></span>
                                                                                    <div class="options" data="{{$subMenuTop1['id']}}">
                                                                                        <span style="width: 45px;height: 38px;text-align: center;padding-left: 8px;font-size: 24px;" class="btn btn-default btn-xs edit-menu"><i class="fa fa-edit"></i></span>
                                                                                        <span style="width: 45px;height: 38px;text-align: center;padding-left: 8px;font-size: 24px;line-height: 32px;" class="btn btn-default btn-xs delete-menu"><i class="fa fa-times"></i></span>
                                                                                    </div>
                                                                                    <?php 
                                                                                        if ($subMenuTop1['childrent']) {
                                                                                            echo '<ol class="dd-list">';
                                                                                            foreach ($subMenuTop1['childrent'] as $subMenuTop2) { ?>
                                                                                                <li class="dd-item" data-id="<?php echo $subMenuTop2['id'];?>">
                                                                                                    <span class="dd-handle"><?php echo $subMenuTop2['title'];?></span>
                                                                                                    <div class="options" data="{{$subMenuTop2['id']}}">
                                                                                                        <span style="width: 45px;height: 38px;text-align: center;padding-left: 8px;font-size: 24px;" class="btn btn-default btn-xs edit-menu"><i class="fa fa-edit"></i></span>
                                                                                                        <span style="width: 45px;height: 38px;text-align: center;padding-left: 8px;font-size: 24px;line-height: 32px;" class="btn btn-default btn-xs delete-menu"><i class="fa fa-times"></i></span>
                                                                                                    </div>
                                                                                                    <?php 
                                                                                                        if ($subMenuTop2['childrent']) {
                                                                                                            echo '<ol class="dd-list">';
                                                                                                            foreach ($subMenuTop2['childrent'] as $subMenuTop3) { ?>
                                                                                                                <li class="dd-item" data-id="<?php echo $subMenuTop3['id'];?>">
                                                                                                                    <span class="dd-handle"><?php echo $subMenuTop3['title'];?></span>
                                                                                                                    <div class="options" data="{{$subMenuTop3['id']}}">
                                                                                                                        <span style="width: 45px;height: 38px;text-align: center;padding-left: 8px;font-size: 24px;" class="btn btn-default btn-xs edit-menu"><i class="fa fa-edit"></i></span>
                                                                                                                        <span style="width: 45px;height: 38px;text-align: center;padding-left: 8px;font-size: 24px;line-height: 32px;" class="btn btn-default btn-xs delete-menu"><i class="fa fa-times"></i></span>
                                                                                                                    </div>
                                                                                                                    <?php 
                                                                                                                        if ($subMenuTop3['childrent']) {
                                                                                                                            echo '<ol class="dd-list">';
                                                                                                                            foreach ($subMenuTop3['childrent'] as $subMenuTop4) { ?>
                                                                                                                                <li class="dd-item" data-id="<?php echo $subMenuTop4['id'];?>">
                                                                                                                                    <span class="dd-handle"><?php echo $subMenuTop4['title'];?></span>
                                                                                                                                    <div class="options" data="{{$subMenuTop4['id']}}">
                                                                                                                                        <span style="width: 45px;height: 38px;text-align: center;padding-left: 8px;font-size: 24px;" class="btn btn-default btn-xs edit-menu"><i class="fa fa-edit"></i></span>
                                                                                                                                        <span style="width: 45px;height: 38px;text-align: center;padding-left: 8px;font-size: 24px;line-height: 32px;" class="btn btn-default btn-xs delete-menu"><i class="fa fa-times"></i></span>
                                                                                                                                    </div>
                                                                                                                                </li>
                                                                                                                            <?php }
                                                                                                                            echo '</ol>';
                                                                                                                        }
                                                                                                                    ?>
                                                                                                                </li>
                                                                                                            <?php }
                                                                                                            echo '</ol>';
                                                                                                        }
                                                                                                    ?>
                                                                                                </li>
                                                                                            <?php }
                                                                                            echo '</ol>';
                                                                                        }
                                                                                    ?>
                                                                                </li>
                                                                            <?php }
                                                                            echo '</ol>';
                                                                        }
                                                                    ?>
                                                                </li>
                                                        <?php }
                                                            }
                                                        ?>
                                                    </ol>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 20px">
                                <form id="save-form-menu" action="{{route('setting.saveMenu')}}" method="POST">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="sorder" id="sorder-input" value="">
                                    <input type="hidden" name="key" value="{{$keyMenu}}">
                                    <div class="col-sm-12">
                                        <div class="btn-group">
                                            <button id="add-form-menu" type="button" class="btn btn-success">
                                                <i class="la la-check"></i>
                                                <span class="kt-hidden-mobile">{{trans('message.tao_moi')}}</span>
                                            </button>
                                        </div>
                                        <div class="btn-group">
                                            <button id="save-button-menu" type="button" class="btn btn-info">
                                                <i class="la la-check"></i>
                                                <span class="kt-hidden-mobile">{{trans('message.luu_thay_doi')}}</span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="menu-top-modal" role="dialog">
        <div class="modal-dialog" style="max-width: 800px; width: 80%;margin-top: 100px">
        {{--popup chi tiet pallet linh kien--}}
        <!-- Modal content-->
            <div class="modal-content">
                <form id="add-item-menu" action="{{route('setting.addMenu')}}" method="POST">
                    <input id="menu-id" type="hidden" name="menu-id" value="" />
                    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" id="_token" name="key" value="{{ $keyMenu }}">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" style="position: absolute;top: 10px; right: 10px;"></button>
                        <h4 class="modal-title" id="detail-pallet-qrcode">{{trans('message.chi_tiet')}}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" style="margin-bottom: 15px;">
                            <label class="col-4 col-form-label" style="font-size: 14px;font-weight: 500;color: #464457">{{trans('message.tieu_de')}}</label>
                            <div class="col-8">
                                <input id="menu-title" class="form-control" placeholder="{{trans('message.nhap_tieu_de')}} ..." type="text" name="title" value="">
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 15px;">
                            <label class="col-4 col-form-label" style="font-size: 14px;font-weight: 500;color: #464457">Link</label>
                            <div class="col-8">
                                <input id="menu-link" class="form-control" placeholder="Link ..." type="text" name="link" value="">
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="add-menu" class="btn btn-info">{{trans('message.luu_lai')}}</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('message.dong_lai')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
@section('body_script')
    <script src="{{ asset('js/plugins/jquery-nestable/jquery.nestable.js') }}" type="text/javascript"></script>
<script>
    var modeOrder = 'create';
    var keyMenu = '{{$keyMenu}}';
    var urlDeleteMenu = '{{route('setting.deleteMenu')}}';
    var menuAll = <?php echo json_encode($menuAll);?>;
    $(document).ready(function () {
        var updateOutput = function (e) {
            var list = e.length ? e : $(e.target),
                    output = list.data('output');
            if (window.JSON) {
                output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
            } else {
                output.val('JSON browser support required for this demo.');
            }
        };
        $('#nestable-menu-top').nestable({
            group: 1
        }).on('change', updateOutput);
        updateOutput($('#nestable-menu-top').data('output', $('#nestable-output-menu-top')));
    });
</script>
<script>
    $('body').on('click', '.edit-menu', function(){
        var idMenu = $(this).parent().attr('data');
        var menuNow = menuAll[idMenu];
        $('#menu-title').val(menuNow.title);
        $('#menu-link').val(menuNow.url);
        $('#menu-id').val(idMenu);
        $('#menu-top-modal').modal();
    });
    $('body').on('click', '.delete-menu', function(){
        var idMenu = $(this).parent().attr('data');
        var urlDelete = urlDeleteMenu + '?id=' + idMenu+'&key='+keyMenu;
        window.location.href = urlDelete;
    });
    $('#add-form-menu').click(function(){
        $('#menu-title').val('');
        $('#menu-link').val('');
        $('#menu-id').val('');
        $('#menu-top-modal').modal();
    });
    $('#save-button-menu').click(function(){
        var sorder = $("#nestable-output-menu-top").val();
        $('#sorder-input').val(sorder);
        $('#save-form-menu').submit();
    });
    $('#add-menu').click(function(){
        var id = $('#menu-id').val();
        var title = $('#menu-title').val();
        var link = $('#menu-link').val();
        var status = true;
        if (!title || title == '') {
            $('#menu-title').addClass('has-error');
            $('#menu-title').parent().find('.invalid-feedback').text("{{trans('message.tieu_de_khong_duoc_de_trong')}}").show();
            status = false;
        }
        if (!link || link == '') {
            $('#menu-link').addClass('has-error');
            $('#menu-link').parent().find('.invalid-feedback').text("{{trans('message.link_khong_duoc_de_trong')}}").show();
            status = false;
        }
        if (status) {
            $('#add-item-menu').submit();
        }
        return false;
    });
</script>
@endsection