@extends('layouts.master')
@section('title', trans('message.cau_hinh_trang_gioi_thieu'))
@section('head_stylesheet')
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/switchery/switchery.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}">
<style>
    .show_screen {
        display: none;
    }

    .table-wrapper-scroll-y {
        display: block;
        max-height: 200px;
        overflow-y: auto;
        -ms-overflow-style: -ms-autohiding-scrollbar;
    }
</style>
@endsection
@section('content')
<?php

 use App\Helpers\Facades\Tool; ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2></h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="javascript:void(0)"> {{ trans('message.quan_ly_cau_hinh') }}</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>{{ trans('message.cau_hinh_trang_gioi_thieu') }}</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row items-container">
        <div class="col-12">
            <form action="{{route('setting.saveAbout')}}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="key-setting" value="{{$key}}" />
                <div class="ibox item-child">
                    <div class="ibox-title">
                        <h3><i class="fa fa-gear fa-fw"></i> {{($key == 'about-page') ? 'About Page' : 'About Page EN'}}</h3>
                    </div>
                    <div class="ibox-content">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">{{ trans('message.career') }} <span class="require"></span></label>
                            <div class="col-sm-10">
                                <select class="form-control select2" name="careers[]" multiple>
                                    <?php if ($careers) {
                                        foreach ($careers as $career) {?>
                                    <option value="{{$career->id}}" {{(in_array($career->id, $aboutData['careers'])) ? 'selected' : ''}}>{{$career->title}}</option>
                                        <?php }}?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">{{ trans('message.mo_ta_trai') }} </label>
                            <div class="col-sm-10">
                                <textarea id="messageArea1" rows="4" name="description_left" class="form-control ckeditor"
                                          placeholder="{{ trans('message.nhap_mo_ta') }}">{{ old('description_left', $aboutData['description_left']) }}</textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">{{ trans('message.mo_ta_phai') }} </label>
                            <div class="col-sm-10">
                                <textarea id="messageArea2" rows="4" name="description_right" class="form-control ckeditor"
                                          placeholder="{{ trans('message.nhap_mo_ta') }}">{{ old('description_right', $aboutData['description_right']) }}</textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">{{ trans('message.anh_dai_dien') }}</label>
                            <div class="col-sm-8 col-form-label">
                                <input type="file" class="img-choice" name="image" accept="image/*" />
                            </div>
                            <br/>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 offset-2">
                                <img id="blah" alt="blank-user" src="{{ Tool::getImage($aboutData['image'], Tool::getConst('SETTING_UPLOAD')) }}" class="img-thumbnail img-preview" style="max-height: 200px; width: auto">
                            </div>
                            <div style="padding-left: 17px;">
                                    <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group row">
                            <div class="col-sm-10 offset-2">
                                <button class="btn btn-primary btn-lg" type="submit">
                                    <i class="fa fa-save fa-fw"></i> {{ trans('message.luu_lai') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
</div>
<input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
@stop
@section('body_script')
@includeif('partials.message')
<script type="text/javascript" src="{{asset('js/plugins/ckeditor/ckeditor.js?v='.\App\Helpers\Facades\Tool::getConst('VERSION_JS'))}}"></script>
<!-- Switchery -->
<script src="{{ asset('js/plugins/switchery/switchery.js') }}"></script>
<script>

// get test settings
var byRow = $('body').hasClass('test-rows');

// apply matchHeight to each item container's items
$('.items-container').each(function() {
    $(this).find('.item-child').matchHeight({
        byRow: byRow
    });
});
</script>
<script>
    $("input.img-choice").change(function () {
        var size = parseInt(this.files[0].size);
        if (size > 2097152) {    // 15728640 byte ~ 15MB
            $(this).val('');
            swal("File upload", "{{ trans('message.dung_luong_anh_khong_duoc_vuot_qua_2MB') }}", "error").catch(swal.noop);
            return false;
        }
        var extension = $(this).val().split('.').pop().toLowerCase();
        if (extension == 'jpg' || extension == 'png' || extension == 'jpeg') {
            readURL(this);
        } else {
            $(this).val('');
            swal("{{ trans('message.chu_y') }}!", "{{ trans('message.anh_phai_thuoc_mot_trong_cac_dinh_dang_png_jpg_jpeg') }}", "warning");
        }
        return false;
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var self = $(input);
            var reader = new FileReader();
            reader.onload = e => {
                self.closest('div.form-group').next('div.form-group').find('img.img-preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<script type="text/javascript">
    var returnMsg = {
        thanh_cong: "{{ trans('message.thanh_cong') }}",
        that_bai: "{{ trans('message.that_bai') }}",
        cap_nhat_thanh_cong: "{{ trans('message.cap_nhat_thanh_cong') }}",
        co_loi_xay_ra_thu_lai_sau: "{{ trans('message.co_loi_xay_ra_thu_lai_sau') }}",
    };
</script>
    <script type="text/javascript">
        CKEDITOR.replace( 'messageArea1',
            {
             height: 500,
            filebrowserImageUploadUrl: "{{route('setting.uploadEditor', ['_token' => csrf_token()])}}"
             });
        CKEDITOR.replace( 'messageArea2',
            {
             height: 500,
            filebrowserImageUploadUrl: "{{route('setting.uploadEditor', ['_token' => csrf_token()])}}"
             });
   </script> 
   <script>
    $.fn.modal.Constructor.prototype.enforceFocus = function() {
	        var $modalElement = this.$element;
	        $(document).on('focusin.modal',function(e) {
	                var $parent = $(e.target.parentNode);
	                if ($modalElement[0] !== e.target
	                                && !$modalElement.has(e.target).length
	                                && $(e.target).parentsUntil('*[role="dialog"]').length === 0) {
	                        $modalElement.focus();
	                }
	        });
	};
</script>
@endsection

