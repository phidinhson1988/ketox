@extends('layouts.master')
@section('title',trans('message.quan_ly_socials'))
@section('content')
<?php use App\Helpers\Facades\Tool;?>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">
                    <strong>{{trans('message.quan_ly_socials')}}</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h3><i class="fa fa-unlock-alt"></i> {{trans('message.quan_ly_socials')}}</h3>
                    </div>
                    <div class="ibox-content">
                        <div class="page-content" style="background-color: #fff;padding: 15px;">
                            <div class="row">
                                <div class="col-sm-12 col-xs-12 clearfix web-content-form" style="padding: 0 5px;">
                                    <form id="add-form-social" action="{{route('setting.addSocial')}}" method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" id="data-social" name="data-social" value="">
                                        <div class="kt-portlet__body table-responsive">
                                            <table class="table table-striped- table-hover table-checkable" id="kt_table_1">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 20%">{{trans('message.vi_tri')}}</th>
                                                        <th style="width: 20%">{{trans('message.loai_social')}}</th>
                                                        <th>Link</th>
                                                        <th width="80px" class="text-center">{{trans('message.thao_tac')}}</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="content-socials">
                                                </tbody>
                                            </table>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-sm-12">
                                    <div class="btn-group">
                                        <button id="add-social" data="{{$max ? $max : 0}}" type="button" class="btn btn-success">
                                            <i class="la la-check"></i>
                                            <span class="kt-hidden-mobile">{{trans('message.tao_moi')}}</span>
                                        </button>
                                    </div>
                                    <div class="btn-group">
                                        <button id="save-button-social" type="button" class="btn btn-info">
                                            <i class="la la-check"></i>
                                            <span class="kt-hidden-mobile">{{trans('message.luu_thay_doi')}}</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('body_script')
    <script>
    var socials = <?php echo json_encode($socials)?>;
    function showSocial(){
        var html = '';
        if (Object.keys(socials).length > 0) {
            Object.keys(socials).forEach(function(keySocial){
                html +='<tr data="'+keySocial+'">';
                    html +='<td>';
                        html +='<select class="form-control position-social" name="position">';
                            html +='<option value="top" '+(socials[keySocial].position == 'top' ? 'selected' : '')+'>Top</option>';
                            html +='<option value="bottom" '+(socials[keySocial].position == 'bottom' ? 'selected' : '')+'>Bottom</option>';
                        html +='</select>';
                    html +='</td>';
                    html +='<td>';
                        html +='<select class="form-control class-social" name="class">';
                            html +='<option value="face" '+(socials[keySocial].class == 'face' ? 'selected' : '')+'>Facebook</option>';
                            html +='<option value="instagram" '+(socials[keySocial].class == 'instagram' ? 'selected' : '')+'>Instagram</option>';
                            html +='<option value="google" '+(socials[keySocial].class == 'google' ? 'selected' : '')+'>Google</option>';
                        html +='</select>';
                    html +='</td>';
                    html +='<td><input id="link-'+keySocial+'" class="form-control link-social" placeholder="Link ..." type="text" name="link[]" value="'+socials[keySocial].link+'" /><div class="invalid-feedback" style="font-size: 12px;"></div></td>';
                    html +='<td class="text-center">';
                    html +='<button type="button" title="xóa" class="btn btn-danger btn-sm delete-social" data="'+keySocial+'">';
                        html +='<i class="fa fa-trash-o"></i>';
                    html +='</button>';
                    html +='</td>';
                html +='</tr>';
            });
        }
        $('#content-socials').html(html);
    }
    $('#add-social').click(function(){
        var indexNow = $(this).attr('data');
        indexNext = parseInt(indexNow) + 1;
        var keySocial = 'line_'+indexNext;
        socials[keySocial] = {
            position: '',
            class: '',
            link: '',
        };
        $('#add-social').attr('data', indexNext);
        showSocial();
    });
    $('body').on('click', '.delete-social', function(){
        var keyDelete = $(this).attr('data');
        delete socials[keyDelete];
        if (Object.keys(socials).length <= 0) {
            $('#add-social').attr('data', 0);
        }
        showSocial();
    });
    $(document).ready(function () {
        showSocial();
        
        $('body').on('keyup keypress paste change', '.position-social', function(){
            var keySocial = $(this).parent().parent().attr('data');
            socials[keySocial].position = $(this).val();
        });
        $('body').on('keyup keypress paste change', '.class-social', function(){
            var keySocial = $(this).parent().parent().attr('data');
            socials[keySocial].class = $(this).val();
        });
        $('body').on('keyup keypress paste change', '.link-social', function(){
            var keySocial = $(this).parent().parent().attr('data');
            socials[keySocial].link = $(this).val();
        });
        $('#save-button-social').click(function(){
            $('body').find('.has-error').removeClass('has-error');
            $('body').find('.invalid-feedback').hide();
            var status = true;
            if (Object.keys(socials).length > 0) {
                Object.keys(socials).forEach(function(keySocial){
                    if (!socials[keySocial].link || socials[keySocial].link == '') {
                        $('#link-'+keySocial).addClass('has-error');
                        $('#link-'+keySocial).parent().find('.invalid-feedback').text("{{trans('message.link_khong_duoc_de_trong')}}").show();
                        status = false;
                    }
                    if (!socials[keySocial].class || socials[keySocial].class == '') {
                        $('#class-'+keySocial).addClass('has-error');
                        $('#class-'+keySocial).parent().find('.invalid-feedback').text("{{trans('message.loai_social_khong_duoc_de_trong')}}").show();
                        status = false;
                    }
                });
            }
            if (status) {
                $('#data-social').val(JSON.stringify(socials));
                $('#add-form-social').submit();
            } else {
                swal.fire("Error!", "{{trans('message.ban_chua_nhap_du_du_lieu')}}", "error");
            }
        });
    });
</script>
@endsection