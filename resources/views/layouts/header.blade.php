<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/animate.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/select2/select2.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/iCheck/custom.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/chosen/chosen.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/sweetalert/sweetalert.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/datapicker/datepicker3.css') }}">


<script>
    window.DMS = {
        csrf: '{{ csrf_token() }}',
        url: '{{ asset('/') }}',
        lang: "{{ app()->getLocale() }}"
    };
</script>


