<?php use App\Helpers\Facades\Tool;?>
<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0;">
    <div class="navbar-header">
        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary" href="javascript:void(0);"><i
                    class="fa fa-bars fa-2x"></i></a>
    </div>
    <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">
            <?php
                $language = 'vi';
                if (Session::has('locale')) {
                    $language = Session::get('locale') ? Session::get('locale') : 'vi';
                }
            ?>
            <a title="{{ trans('message.chon_ngon_ngu') }}" class="dropdown-toggle count-info" data-toggle="dropdown"
               href="javascript:void(0);">
                @if (strtolower($language) == 'en')
                    <img src="{{ asset('img/common/England.png') }}"/>
                @else
                    <img src="{{ asset('img/common/Vietnam.png') }}"/>
                @endif 
            </a>
            <ul class="dropdown-menu lang-choice">
                <li>
                    <a title="{{ trans('message.tieng_anh') }}" href="{{ route('locale', ['option' => 'en']) }}"
                       class="dropdown-item lang-option-top">
                        <img alt="image"
                             src="{{ asset('img/common/England.png') }}"><span> {{ trans('message.tieng_anh') }}</span>
                    </a>
                </li>
                <li class="dropdown-divider lang-divider"></li>
                <li>
                    <a title="{{ trans('message.tieng_viet') }}" href="{{ route('locale', ['option' => 'vi']) }}"
                       class="dropdown-item lang-option-down">
                        <img alt="image"
                             src="{{ asset('img/common/Vietnam.png') }}"><span> {{ trans('message.tieng_viet') }}</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i> {{ trans('message.dang_xuat') }}</a>
        </li>
    </ul>
</nav>
