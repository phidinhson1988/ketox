<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}"/>
    <title>@yield('title') | Ketox</title>
    @includeif('layouts.header')
    @yield('head_stylesheet')
    @yield('head_script')
</head>
<body>
<div id="wrapper">
    @includeif('layouts.sidebar')
    <div id="page-wrapper" class="gray-bg" style="z-index: 10">
        <div class="loading-spinner-page" style="display: block"><i class="img-loading-spinner"></i></div>
        <div class="row border-bottom">
            @includeif('layouts.navbar')
        </div>
        @yield('content')
        <div class="footer">
            @includeif('layouts.footer')
        </div>
    </div>
</div>
@includeif('layouts.script')
<script>
    $(window).on('load', function () {
        $('body .loading-spinner-page').hide();
    });
</script>
@yield('body_script')
</body>
</html>
