<!-- Mainly scripts -->
<script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
@yield('top-script')
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- Custom and plugin javascript -->
<script src="{{ asset('js/inspinia.js') }}"></script>
<!-- Automatic page load progressbar -->
<script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>
<!-- Date range use moment.js same as full calendar plugin -->
<script src="{{ asset('js/plugins/fullcalendar/moment.min.js') }}"></script>
<!-- Data picker -->
<script src="{{ asset('js/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- Chosen -->
<script src="{{ asset('js/plugins/chosen/chosen.jquery.js') }}"></script>
<!-- Sweetalert -->
<script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>
<!-- Sweetalert -->
<script src="{{ asset('js/matchHeight.js') }}"></script>
<!-- Setup -->
<script src="{{ asset('js/plugins/idle-timer/idle-timer.min.js') }}"></script>
<!-- Date range picker -->
<script src="{{ asset('js/plugins/daterangepicker/daterangepicker.js') }}"></script>

<script>
    var LogOut = "<?php echo route("login") ?>";
    $('.select2').select2();
    $('.select2.stage-filter').select2({
        placeholder: '-- {{ trans("message.chon_cong_doan") }} --'
    })
    $(".chosen-select").chosen();
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        $('.input-daterange').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: 'dd-mm-yyyy'
        });
    });
    $('.stage-filter').on('change', function () {
        var val = $(this).val();
        $('input[name=hidden_stage]').val(val);
        $(this).closest('form').submit();
    });
    var messages = {
        noNews: "{{ trans('message.khong_co_tin_moi') }}",
        canh_bao: "{{ trans('message.canh_bao') }}",
        xoa: "{{ trans('message.xoa') }}",
        dung_hoat_dong: "{{ trans('message.dung_hoat_dong') }}",
        cap_nhat: "{{ trans('message.cap_nhat') }}",
        ban_co_muon: "{{ trans('message.ban_co_muon') }}",
        nay_khong: "{{ trans('message.nay_khong') }}",
    };
</script>
<script src="{{ asset('js/common.js?v='.\App\Helpers\Facades\Tool::getConst('VERSION_JS')) }}"></script>

