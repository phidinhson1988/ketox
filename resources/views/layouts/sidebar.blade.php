<?php
use App\Helpers\Facades\Tool;
use \Illuminate\Support\Facades\Request;
$arrPermission = session()->has('permission') ? session()->get('permission') : [];
?>
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element text-center">
                    <span class="text-center">
                        <img alt="image" class="img-thumbnail"
                             src="{{ Tool::getImage(Auth::user()->avatar, Tool::getConst('USER_UPLOAD')) }}"
                             style="width: 60px;height: 60px"/>
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void(0);">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">{{ Auth::user()->username }}
                                    <span class="text-muted text-xs"><b class="caret"></b></span>
                                </strong>
                            </span>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a class="dropdown-item"
                               href="{{ route('frontend.home') }}" target="_blank">{{ trans('message.di_toi_trang_web') }}</a></li>
                        <li><a class="dropdown-item"
                               href="{{ route('user.profile') }}">{{ trans('message.chinh_sua_profile') }}</a></li>
                        <li><a class="dropdown-item"
                               href="{{ route('user.password') }}">{{ trans('message.doi_mat_khau') }}</a></li>
                        <li class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="{{ route('logout') }}">{{ trans('message.dang_xuat') }}</a>
                        </li>
                    </ul>
                </div>
                <div class="logo-element">
                    @if (auth()->check())
                        <img alt="image" class="img-thumbnail logo-thumbnail"
                             src="{{ Tool::getImage(Auth::user()->avatar, Tool::getConst('USER_UPLOAD')) }}"/>
                    @endif
                </div>
            </li>
            
            @if(in_array('product-view', $arrPermission))
                <li class="{{ Tool::activeLink(['product.index', 'product.create', 'product.edit']) }}">
                    <a href="{{ route('product.index') }}"><i class="fa fa-product-hunt fa-fw"></i> <span class="nav-label">{{ trans('message.danh_sach_san_pham') }}</span></a>
                </li>
            @endif   
                        
            @if(in_array('cart-view', $arrPermission))
                <li class="{{ Tool::activeLink(['cart.index']) }}">
                    <a href="{{ route('cart.index') }}"><i class="fa fa-shopping-cart fa-fw"></i> <span class="nav-label">{{ trans('message.danh_sach_ban_hang') }}</span></a>
                </li>
            @endif  
            
            @if(in_array('news-view', $arrPermission))
                <li class="{{ Tool::activeLink(['news.index', 'news.create', 'news.edit']) }}">
                    <a href="{{ route('news.index') }}"><i class="fa fa-newspaper-o fa-fw"></i> <span class="nav-label">{{ trans('message.quan_ly_tin_tuc') }}</span></a>
                </li>
            @endif  
            
                <li class="{{ Tool::activeLink(['mail.index']) }}">
                    <a href="{{ route('mail.index') }}"><i class="fa fa-envelope-o fa-fw"></i> <span class="nav-label">{{ trans('message.danh_sach_mail_lien_he') }}</span></a>
                </li>
            
                <li class="{{ Tool::activeLink(['feedback.index', 'feedback.create', 'feedback.edit']) }}">
                    <a href="{{ route('feedback.index') }}"><i class="fa fa-newspaper-o fa-fw"></i> <span class="nav-label">{{ trans('message.quan_ly_phan_hoi_khach_hang') }}</span></a>
                </li>
            
            @if(in_array('news-view', $arrPermission))
<!--                <li class="{{ Tool::activeLink(['page.index', 'page.create', 'page.edit']) }}">
                    <a href="{{ route('page.index') }}"><i class="fa fa-file-text fa-fw"></i> <span class="nav-label">Quản lý page</span></a>
                </li>-->
            @endif
            @if(in_array('user-view', $arrPermission))
                <li class="{{ Tool::activeLink(['user.index', 'user.create', 'user.edit']) }}">
                    <a href="{{ route('user.index') }}"><i class="fa fa-users fa-fw"></i> <span class="nav-label">{{ trans('message.quan_ly_nguoi_dung') }}</span></a>
                </li>
            @endif
            
            @if(in_array('role-view', $arrPermission))
                <li class="{{ Tool::activeLink(['role.index', 'role.create', 'role.edit']) }}">
                    <a href="{{ route('role.index') }}"><i class="fa fa-users fa-fw"></i> <span class="nav-label">{{ trans('message.quan_ly_phan_quyen') }}</span></a>
                </li>
            @endif
            
            @if (in_array('config-view', $arrPermission))
                <li class="{{ Tool::activeLink(['setting.system', 'setting.menu', 'setting.aboutPage', 'setting.contactPage', 'setting.menuBottom', 'setting.socials']) }}">
                    <a href="javascript:void(0)"><i class="fa fa-plug fa-fw"></i> <span
                                class="nav-label">{{ trans('message.quan_ly_cau_hinh') }}</span> <span
                                class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                            <li class="<?php echo (Request::get('key', null) == 'about-page') ? 'active' : '';?>">
                                <a href="{{ route('setting.aboutPage', ['key' => 'about-page']) }}">{{ trans('message.quan_ly_about_page') }}</a>
                            </li>
                            <li class="<?php echo (Request::get('key', null) == 'about-page-en') ? 'active' : '';?>">
                                <a href="{{ route('setting.aboutPage', ['key' => 'about-page-en']) }}">{{ trans('message.quan_ly_about_page_en') }}</a>
                            </li>
                            <li class="<?php echo (Request::get('key', null) == 'contact-page') ? 'active' : '';?>">
                                <a href="{{ route('setting.contactPage', ['key' => 'contact-page']) }}">{{ trans('message.quan_ly_contact_page') }}</a>
                            </li>
                            <li class="<?php echo (Request::get('key', null) == 'contact-page-en') ? 'active' : '';?>">
                                <a href="{{ route('setting.contactPage', ['key' => 'contact-page-en']) }}">{{ trans('message.quan_ly_contact_page_en') }}</a>
                            </li>
                            <li class="<?php echo (Request::get('key', null) == 'home-menu-top') ? 'active' : '';?>">
                                <a href="{{ route('setting.menu', ['key' => 'home-menu-top']) }}">{{ trans('message.quan_ly_menu_top') }}</a>
                            </li>
                            <li class="<?php echo (Request::get('key', null) == 'home-menu-top-en') ? 'active' : '';?>">
                                <a href="{{ route('setting.menu', ['key' => 'home-menu-top-en']) }}">{{ trans('message.quan_ly_menu_top_en') }}</a>
                            </li>
                            <li class="<?php echo (Request::get('key', null) == 'home-menu-bottom') ? 'active' : '';?>">
                                <a href="{{ route('setting.menu', ['key' => 'home-menu-bottom']) }}">{{ trans('message.quan_ly_menu_bottom') }}</a>
                            </li>
                            <li class="<?php echo (Request::get('key', null) == 'home-menu-bottom-en') ? 'active' : '';?>">
                                <a href="{{ route('setting.menu', ['key' => 'home-menu-bottom-en']) }}">{{ trans('message.quan_ly_menu_bottom_en') }}</a>
                            </li>
                            <li class="{{ Tool::activeLink('setting.socials') }}"><a
                                        href="{{ route('setting.socials') }}">{{ trans('message.quan_ly_socials') }}</a>
                            </li>
                            <li class="{{ Tool::activeLink('setting.system') }}"><a
                                        href="{{ route('setting.system') }}">{{ trans('message.cau_hinh_chung') }}</a>
                            </li>
                    </ul>
                </li>
            @endif
        </ul>
    </div>
</nav>
