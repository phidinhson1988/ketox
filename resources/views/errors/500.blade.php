<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ trans('message.error') }} 500 | DMS</title>

    @includeif('layouts.header')

</head>

<body class="gray-bg">


<div class="middle-box text-center animated fadeInDown">
    <h1>500</h1>
    <h3 class="font-bold">{{ trans('message.loi_server') }}!</h3>

    <div class="error-desc">
        {{ trans('message.sorry_error_server') }}: <br/><a href="{{route('home')}}" class="btn btn-primary m-t">{{ trans('message.trang_chu') }}</a>
    </div>
</div>

<!-- Mainly scripts -->
@includeif('layouts.script')

</body>

</html>
