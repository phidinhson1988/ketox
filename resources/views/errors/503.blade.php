<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ trans('message.error') }} 503 | DMS</title>

    @includeif('layouts.header')

</head>

<body class="gray-bg">


<div class="middle-box text-center animated fadeInDown">
    <h1>503</h1>
    <h3 class="font-bold">{{ trans('message.loi_server') }}!</h3>

    <div class="error-desc">
        {{ trans('message.sorry_server_maintain') }} <br/>
    </div>
</div>

<!-- Mainly scripts -->
@includeif('layouts.script')

</body>

</html>
