<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ trans('message.error') }} 404 | DMS</title>

    @includeif('layouts.header')

</head>

<body class="gray-bg">


<div class="middle-box text-center animated fadeInDown">
    <h1>404</h1>
    <h3 class="font-bold">{{ trans('message.khong_tim_thay_trang_ban_yeu_cau') }}</h3>

    <div class="error-desc">
        {{ trans('message.not_found') }} <br/><a href="{{route('home')}}" class="btn btn-primary m-t">{{ trans('message.trang_chu') }}</a>
    </div>
</div>

<!-- Mainly scripts -->
@includeif('layouts.script')

</body>

</html>
