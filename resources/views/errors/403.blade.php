<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ trans('message.error') }} 403 | Ketox</title>

    @includeif('layouts.header')

</head>

<body class="gray-bg">


<div class="middle-box text-center animated fadeInDown">
    <h1>403</h1>
    <h3 class="font-bold">{{ trans('message.loi_quyen_truy_cap') }}!</h3>

    <div class="error-desc">
        {{ trans('message.not_have_access') }}: <br/><a href="{{route('home')}}" class="btn btn-primary m-t">{{ trans('message.trang_chu') }}</a>
    </div>
</div>

<!-- Mainly scripts -->
@includeif('layouts.script')

</body>

</html>
