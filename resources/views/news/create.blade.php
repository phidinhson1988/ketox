@extends('layouts.master')
@section('title', trans('message.them_moi_tin_tuc'))
@section('head_stylesheet')
    <style>
        .table-wrapper-scroll-y {
            display: block;
            max-height: 500px;
            overflow-y: auto;
            -ms-overflow-style: -ms-autohiding-scrollbar;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.Jcrop.min.css') }}"/>
@endsection
@section('content')
<?php use App\Helpers\Facades\Tool;?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('news.index') }}">{{trans('message.danh_sach_tin_tuc')}}</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>{{trans('message.them_moi_tin_tuc')}}</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h3><i class="fa fa-unlock-alt"></i> {{trans('message.them_moi_tin_tuc')}}</h3>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <form id="form-news" name="customFile6" action="{{route('news.store')}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group row {{ Tool::classError('title', $errors) }}">
                                    <label class="col-sm-2 col-form-label">{{trans('message.tieu_de')}} <span class="require"></span></label>
                                    <div class="col-sm-10">
                                        <input id="title" type="text" name="title" required class="form-control" value="{{ old('title') }}"
                                               placeholder="{{trans('message.nhap_tieu_de')}} ...">
                                        @if ($errors->has('title'))
                                            <span class="help-block">{{ $errors->first('title') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">{{trans('message.slug')}} <span class="require"></span></label>
                                    <div class="col-sm-10">
                                        <input id="slug" type="text" name="slug" class="form-control" required value="" placeholder="{{trans('message.nhap_slug')}} ...">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">{{trans('message.the_loai')}}</label>
                                    <div class="col-sm-10">
                                        <select id="select-type" name="type" class="form-control" style="width: 50%;">
                                            <option value="1">{{ trans('message.tin_tuc') }}</option>
                                            <option value="2">{{ trans('message.career') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row show-career" style="display: none">
                                    <label class="col-sm-2 col-form-label">{{trans('message.cong_viec')}}</label>
                                    <div class="col-sm-10">
                                        <input id="job" type="text" name="job" class="form-control" value="" placeholder="{{trans('message.nhap_cong_viec')}} ...">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group row show-career" style="display: none">
                                    <label class="col-sm-2 col-form-label">{{trans('message.thoi_gian')}}</label>
                                    <div class="col-sm-10">
                                        <div class="input-group input-daterange" id="datepicker">
                                            <span class="input-group-addon"><strong>{{ trans('message.tu_ngay') }}</strong></span>
                                            <input type="text" readonly class="form-control" name="from" value=""/>
                                            <span class="input-group-addon"><strong>{{ trans('message.den_ngay') }}</strong></span>
                                            <input type="text" readonly class="form-control" name="to" value="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row {{ Tool::classError('description', $errors) }}">
                                    <label class="col-sm-2 col-form-label">{{ trans('message.mo_ta') }} </label>
                                    <div class="col-sm-10">
                                        <textarea rows="4" name="description" class="form-control"
                                                  placeholder="{{ trans('message.nhap_mo_ta') }}">{{ old('description') }}</textarea>
                                        @if ($errors->has('description'))
                                            <span class="help-block">{{ $errors->first('description') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row {{ Tool::classError('content', $errors) }}">
                                    <label class="col-sm-2 col-form-label">{{ trans('message.noi_dung_chinh') }} </label>
                                    <div class="col-sm-10">
                                        <textarea id="messageArea" rows="7" class="form-control ckeditor" name="content"></textarea>
                                        @if ($errors->has('content'))
                                            <span class="help-block">{{ $errors->first('content') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">{{ trans('message.ngon_ngu') }} </label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="lang" style="max-width: 200px">
                                            <option value="vi">{{ trans('message.tieng_viet') }}</option>
                                            <option value="en">{{ trans('message.tieng_anh') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">{{ trans('message.yeu_cau_dang_nhap') }} </label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="is_require" style="max-width: 200px">
                                            <option value="0">{{ trans('message.khong') }}</option>
                                            <option value="1" {{old('is_require') == 1 ? 'selected' : ''}}>{{ trans('message.co') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <input id="image-file" type="hidden" name="avatar" />
                                    <label class="col-sm-2 col-form-label">{{ trans('message.anh_dai_dien') }}</label>
                                    <div class="col-sm-6 box-upload-file">
                                        <div class="custom-file" style="margin-bottom: 20px;">
                                            <input type-file="image" data-name="customFile6" type="file" name="file_upload" class="custom-file-input" id="customFile6">
                                            <label class="custom-file-label" for="customFile6">{{ trans('message.chon_file') }}</label>
                                            <div class="invalid-feedback" style="font-size: 14px;"></div>
                                        </div>
                                        <ul class="list-show-image" style="width: 100%;padding-top:0"></ul>
                                    </div>
                                    <br/>
                                </div>
                                <div class="form-group row {{ Tool::classError('meta_des', $errors) }}">
                                    <label class="col-sm-2 col-form-label">{{ trans('message.meta_description') }}</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="meta_des" class="form-control" value="{{ old('meta_des') }}"
                                               placeholder="{{ trans('message.nhap_meta_description') }} ...">
                                        @if ($errors->has('meta_des'))
                                            <span class="help-block">{{ $errors->first('meta_des') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row {{ Tool::classError('meta_keyword', $errors) }}">
                                    <label class="col-sm-2 col-form-label">{{ trans('message.meta_keyword') }}</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="meta_keyword" class="form-control" value="{{ old('meta_keyword') }}"
                                               placeholder="{{ trans('message.nhap_meta_keyword') }} ...">
                                        @if ($errors->has('meta_keyword'))
                                            <span class="help-block">{{ $errors->first('meta_keyword') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="hr-line-dashed"></div>
                                <div class="form-group row">
                                    <div class="col-sm-8 offset-2">
                                        <button class="btn btn-primary btn-lg" type="submit"><i
                                                    class="fa fa-save fa-fw"></i> {{ trans('message.luu_lai') }}</button>
                                        <a href="{{ route('news.create') }}">
                                            <button class="btn btn-danger btn-lg" type="button"><i
                                                        class="fa fa-undo fa-fw"></i> {{ trans('message.nhap_lai') }}</button>
                                        </a>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="modal fade" id="viewDetail" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title" id="detail-pallet-qrcode">{{ trans('message.cat_anh') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 show-image">
                            <img src="{{ asset('upload') }}/no-image.gif" id="cropbox" class="img" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="crop-image" class="btn btn-info">{{ trans('message.cat_anh') }}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('message.dong_lai') }}</button>
                </div>

            </div>
        </div>
    </div>
    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
@stop
@section('body_script')
    <script type="text/javascript" src="{{asset('js/plugins/ckeditor/ckeditor.js?v='.\App\Helpers\Facades\Tool::getConst('VERSION_JS'))}}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.Jcrop.min.js?v='.Tool::getConst('VERSION_JS')) }}"></script>
    <script type="text/javascript">
        CKEDITOR.replace( 'messageArea',
            {
             height: 500,
            filebrowserImageUploadUrl: "{{route('setting.uploadEditor', ['_token' => csrf_token()])}}"
             })
   </script> 
    <script>
        $('#select-type').change(function(){
            var type = $(this).val();
            if (type == 1) {
                $('body').find('.show-career').hide();
            } else {
                $('body').find('.show-career').show();
            }
            return false;
        });
    </script>
    <script>
        var urlCheckSlug = '{{route('news.checkSlug')}}';
        var uploadFileAttact = '{{route('admin.uploadFile')}}';
        var urlCropFileImage = '{{route('admin.cropFileImage')}}';
        var idTarget = '';
        var RATIO_IMAGE_CARD = <?php echo Tool::getConst('RATIO_IMAGE_NEWS');?>;
        var RATIO_POSITION_CARD = <?php echo Tool::getConst('RATIO_POSITION_NEWS');?>;
    </script>
        <script>
    $.fn.modal.Constructor.prototype.enforceFocus = function() {
	        var $modalElement = this.$element;
	        $(document).on('focusin.modal',function(e) {
	                var $parent = $(e.target.parentNode);
	                if ($modalElement[0] !== e.target
	                                && !$modalElement.has(e.target).length
	                                && $(e.target).parentsUntil('*[role="dialog"]').length === 0) {
	                        $modalElement.focus();
	                }
	        });
	};
</script>
    <script type="text/javascript" src="{{asset('js/dms/product.js?v='.Tool::getConst('VERSION_JS'))}}"></script>
@endsection