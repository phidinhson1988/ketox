@extends('layouts.master')
@section('title', trans('message.danh_sach_tin_tuc'))
@section('content')
<?php use App\Helpers\Facades\Tool; ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">
                    <a href="{{route('news.index')}}"><strong>{{trans('message.danh_sach_tin_tuc')}}</strong></a>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h3><i class="fa fa-unlock-alt"></i> {{trans('message.danh_sach_tin_tuc')}}</h3>
                        <a href="{{ route('news.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-square fa-fw"></i> {{ trans('message.them_moi') }}</a>
                    </div>
                    <div class="ibox-content">
                        <form name="form1" action="{{route('news.index')}}" method="GET">
                            <div class="row">
                                <div class="col-lg-3 col-sm-6">
                                    @include('partials.limit')
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div class="input-group">
                                        <select class="form-control auto-submit" name="type">
                                            <option value="" {{$type == '' ? 'selected' : ''}}>{{ trans('message.chon_the_loai') }}</option>
                                            <option value="1" {{$type == 1 ? 'selected' : ''}}>{{ trans('message.tin_tuc') }}</option>
                                            <option value="2" {{$type == 2 ? 'selected' : ''}}>{{ trans('message.career') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6 offset-3">
                                    <div class="input-group">
                                        <input placeholder="{{ trans('message.nhap_tieu_de_tin_tuc') }}" type="text" value="{{ Request::get('q', null) }}" name="q" class="form-control">
                                        <span class="input-group-append"> 
                                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i></button> 
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <br>
                        <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>{{trans('message.tieu_de')}}</th>
                                        <th class="text-center">{{ trans('message.hinh_anh') }}</th>
                                        <th>{{trans('message.mo_ta')}}</th>
                                        <th>{{ trans('message.hinh_anh') }}</th>
                                        <th>{{ trans('message.ngon_ngu') }}</th>
                                        <th>{{ trans('message.the_loai') }}</th>
                                        <th>{{ trans('message.trang_thai') }}</th>
                                        <th width="100px" class="text-center">{{ trans('message.thao_tac') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                @if(isset($news) && $news->count() > 0)
                                    @foreach($news as $post)
                                    <?php $userCreate = \App\Models\User::find($post->created_by);?>
                                        <tr>
                                            <td>{{$post->title}}</td>
                                            <td class="feed-element text-center">
                                                <img alt="image" style="max-height: 50px " src="{{ Tool::getImage($post->avatar, \App\Helpers\Facades\Tool::getConst('POST_UPLOAD')) }}">
                                            </td>
                                            <td>{{$post->description}}</td>
                                            <td><?php echo $userCreate ? $userCreate->full_name : '';?></td>
                                            <td>{{$post->lang == 'vi' ? trans('message.tieng_viet') : trans('message.tieng_anh') }}</td>
                                            <td>{{$post->type == 1 ? trans('message.tin_tuc') : trans('message.career') }}</td>
                                            <td>{{$post->status == 1 ? trans('message.hoat_dong') : trans('message.tam_dung')}}</td>
                                            <td class="text-center no-wrap">
                                                <?php if ($post->status) {?>
                                                <form method="post" action="{{ route('news.delete', $post->id ) }}" accept-charset="UTF-8">
                                                    <input name="status" type="hidden" value="0">
                                                    <input name="_method" type="hidden" value="delete">
                                                    <?php if ($post->type == 1) {?>
                                                    <a title="Chi tiết" href="{{route('frontend.news.detail', ['slug' => $post->slug])}}" target="_blank" class="btn btn-white btn-sm"><i class="fa fa-eye"></i></a>
                                                    <?php } else {?>
                                                    <a title="Chi tiết" href="{{route('frontend.news.detailCareer', ['slug' => $post->slug])}}" target="_blank" class="btn btn-white btn-sm"><i class="fa fa-eye"></i></a>
                                                    <?php } ?>
                                                    {{ csrf_field() }}

                                                    @if (Entrust::can('news-edit'))
                                                        <a title="Sửa"
                                                           href="{{ route('news.edit',['id'=>$post->id]) }}"
                                                           class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                                                    @endif
                                                    @if (Entrust::can('news-delete'))
                                                        <button type="button" title="{{trans('message.xoa')}}"
                                                                onclick="event.preventDefault(); commonFunc.confirm(this, 'tin tức', 1, 'vi', 'false', '{{$post->name}}');"
                                                                class="btn btn-danger btn-sm"><i
                                                                    class="fa fa-trash-o"></i>
                                                        </button>
                                                    @endif
                                                </form>
                                                <?php } else {?>
                                                    <form method="post" action="{{ route('news.delete', $post->id ) }}" accept-charset="UTF-8">
                                                    <input name="status" type="hidden" value="1">
                                                    <input name="_method" type="hidden" value="delete">
                                                    {{ csrf_field() }}

                                                    @if (Entrust::can('news-edit'))
                                                        <a title="Sửa"
                                                           href="{{ route('news.edit',['id'=>$post->id]) }}"
                                                           class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                                                    @endif
                                                    @if (Entrust::can('news-create'))
                                                        <button type="submit" title="Add" class="btn btn-info btn-sm"><i class="fa fa-plus-square"></i> </button>
                                                    @endif
                                                </form>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                    </tbody>
                                </table>
                        </div>
                        
                        @if(isset($news) && $news->count() > 0)
                        <div class="row">
                            @includeif('partials.pagination', [
                                'pagination'  => $pagination,
                                'data'   => $news->toArray(),
                            ])
                        </div>
                        @else
                            @includeIf('partials.empty')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('body_script')
    @includeif('partials.message')
@endsection
