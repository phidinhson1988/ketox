@extends('layouts.master')
@section('title',trans('message.cap_nhat_san_pham'))
@section('head_stylesheet')
    <style>
        .table-wrapper-scroll-y {
            display: block;
            max-height: 500px;
            overflow-y: auto;
            -ms-overflow-style: -ms-autohiding-scrollbar;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.Jcrop.min.css') }}"/>
@endsection
@section('content')
<?php use App\Helpers\Facades\Tool;?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('product.index') }}">{{trans('message.danh_sach_san_pham')}}</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>{{trans('message.cap_nhat_san_pham')}}</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h3><i class="fa fa-unlock-alt"></i> {{trans('message.cap_nhat_san_pham')}}</h3>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <form id="form-product" name="customFile6" action="{{route('product.update', ['id' => $product->id])}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">{{trans('message.tieu_de')}} <span class="require"></span></label>
                                    <div class="col-sm-10">
                                        <input id="title" type="text" name="title" class="form-control" required value="{{ old('title', $product->title) }}"
                                               placeholder="{{trans('message.nhap_tieu_de')}} ...">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">{{trans('message.slug')}} <span class="require"></span></label>
                                    <div class="col-sm-10">
                                        <input id="slug" type="text" name="slug" class="form-control" required value="{{ old('slug', $product->slug) }}" placeholder="{{trans('message.nhap_slug')}} ...">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">{{trans('message.gia_ban')}}</label>
                                    <div class="col-sm-10">
                                        <input type="number" min='0' name="price" class="form-control" value="{{ old('price', $product->price) }}"
                                               placeholder="{{trans('message.nhap_gia_ban')}} ...">
                                            <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group row {{ Tool::classError('description', $errors) }}">
                                    <label class="col-sm-2 col-form-label">{{ trans('message.mo_ta') }} </label>
                                    <div class="col-sm-10">
                                        <textarea rows="4" name="description" class="form-control"
                                                  placeholder="{{ trans('message.nhap_mo_ta') }}">{{ old('description', $product->description) }}</textarea>
                                        @if ($errors->has('description'))
                                            <span class="help-block">{{ $errors->first('description') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row {{ Tool::classError('content', $errors) }}">
                                    <label class="col-sm-2 col-form-label">{{trans('message.noi_dung_chinh')}} </label>
                                    <div class="col-sm-10">
                                        <textarea id="messageArea" rows="7" class="form-control ckeditor" name="content">{!! old('content', $product->content) !!}</textarea>
                                        @if ($errors->has('content'))
                                            <span class="help-block">{{ $errors->first('content') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">{{trans('message.ngon_ngu')}} </label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="lang" style="max-width: 200px">
                                            <option value="vi"{{($product->lang == 'vi') ? ' selected' : ''}}>{{trans('message.tieng_viet')}}</option>
                                            <option value="en"{{($product->lang == 'en') ? ' selected' : ''}}>{{trans('message.tieng_anh')}}</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <input id="image-file" type="hidden" name="avatar" value="{{$product->avatar}}" />
                                    <label class="col-sm-2 col-form-label">{{ trans('message.anh_dai_dien') }}</label>
                                    <div class="col-sm-6 box-upload-file">
                                        <div class="custom-file" style="margin-bottom: 20px;">
                                            <input type-file="image" data-name="customFile6" type="file" name="file_upload" class="custom-file-input" id="customFile6">
                                            <label class="custom-file-label" for="customFile6">{{trans('message.chon_file')}}</label>
                                            <div class="invalid-feedback" style="font-size: 14px;"></div>
                                        </div>
                                        <ul class="list-show-image" style="width: 100%;padding-top:0">
                                            <?php if ($product->avatar) {
                                                $cardImage = Tool::getImage($product->avatar, Tool::getConst('PRODUCT_UPLOAD')); ?>
                                                <li class="col-12">
                                                    <span class="delete-image">x</span>
                                                    <img class="new-image-crop" data="{{$product->avatar}}" src="{{$cardImage}}">
                                                    <input class="customFile6" id="customFile6-image-hero" type="hidden" name="" data="{{$cardImage}}" value="{{$product->avatar}}">
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <br/>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">{{trans('message.thu_tu')}}</label>
                                    <div class="col-sm-10">
                                        <input id="sort" type="number" name="sort" class="form-control" required value="{{$product->sort}}">
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <div class="hr-line-dashed"></div>
                                <div class="form-group row">
                                    <div class="col-sm-8 offset-2">
                                        <button class="btn btn-primary btn-lg" type="submit"><i
                                                    class="fa fa-save fa-fw"></i> {{ trans('message.luu_lai') }}</button>
                                        <a href="{{ route('product.create') }}">
                                            <button class="btn btn-danger btn-lg" type="button"><i
                                                        class="fa fa-undo fa-fw"></i> {{ trans('message.nhap_lai') }}</button>
                                        </a>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="viewDetail" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title" id="detail-pallet-qrcode">{{trans('message.cat_anh')}}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 show-image">
                            <img src="{{ asset('upload') }}/no-image.gif" id="cropbox" class="img" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="crop-image" class="btn btn-info">{{trans('message.cat_anh')}}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('message.dong_lai')}}</button>
                </div>

            </div>
        </div>
    </div>
    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
@stop
@section('body_script')
    <script type="text/javascript" src="{{asset('js/plugins/ckeditor/ckeditor.js?v='.\App\Helpers\Facades\Tool::getConst('VERSION_JS'))}}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.Jcrop.min.js?v='.Tool::getConst('VERSION_JS')) }}"></script>
   <script>
    CKEDITOR.replace( 'messageArea', {
     height: 500,
     filebrowserImageUploadUrl: "{{route('setting.uploadEditor', ['_token' => csrf_token()])}}"
    });
</script>
    <script>
        var statusImage = false;
        $('.input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "d-m-yyyy",
        });

        $("input.img-choice").change(function () {
            var self = $(this);
            self.parent().parent().removeClass('has-error');
            self.parent().parent().parent().find('.help-block').hide();
            var size = parseInt(this.files[0].size);
            if (size > (2097152*5)) {    // 15728640 byte ~ 15MB
                var largestImgCapacity = "{{trans('message.dung_luong_anh_khong_duoc_vuot_qua_10mb')}}";
                self.parent().parent().addClass('has-error');
                self.parent().parent().parent().find('.help-block').text(largestImgCapacity).show();
                statusImage = false;
                return false;
            }
            var extension = $(this).val().split('.').pop().toLowerCase();
            if (extension == 'jpg' || extension == 'png' || extension == 'jpeg') {
                readURL(this);
            } else {
                var formatImg = "{{ trans('message.anh_phai_thuoc_mot_trong_cac_dinh_dang_png_jpg_jpeg') }}";
                statusImage = false;
                self.parent().parent().addClass('has-error');
                self.parent().parent().parent().find('.help-block').text(formatImg).show();
            }
            return false;
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var self = $(input);
                var reader = new FileReader();
                reader.onload = e => {
                    self.closest('div.form-group').next('div.form-group').find('img.img-preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
                statusImage = true;
            } else {
                statusImage = false;
            }
        }
        $("#form-product").submit(function(){
            if (document.getElementById("image-file").files.length > 0 && !statusImage) {
                return false;
            }
            return true;
        });
    </script>
    <script>
        var urlCheckSlug = '{{route('product.checkSlug')}}';
        var uploadFileAttact = '{{route('admin.uploadFile')}}';
        var urlCropFileImage = '{{route('admin.cropFileImage')}}';
        var idTarget = '';
        var RATIO_IMAGE_CARD = <?php echo Tool::getConst('RATIO_IMAGE_CARD');?>;
        var RATIO_POSITION_CARD = <?php echo Tool::getConst('RATIO_POSITION_CARD');?>;
    </script>
        <script>
    $.fn.modal.Constructor.prototype.enforceFocus = function() {
	        var $modalElement = this.$element;
	        $(document).on('focusin.modal',function(e) {
	                var $parent = $(e.target.parentNode);
	                if ($modalElement[0] !== e.target
	                                && !$modalElement.has(e.target).length
	                                && $(e.target).parentsUntil('*[role="dialog"]').length === 0) {
	                        $modalElement.focus();
	                }
	        });
	};
</script>
    <script type="text/javascript" src="{{asset('js/dms/product.js?v='.\App\Helpers\Facades\Tool::getConst('VERSION_JS'))}}"></script>
@endsection