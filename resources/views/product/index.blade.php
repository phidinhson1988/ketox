@extends('layouts.master')
@section('title', trans('message.danh_sach_san_pham'))
@section('content')
<?php use App\Helpers\Facades\Tool; ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">
                    <a href="{{route('product.index')}}"><strong>{{trans('message.danh_sach_san_pham')}}</strong></a>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h3><i class="fa fa-unlock-alt"></i> {{trans('message.danh_sach_san_pham')}}</h3>
                        <a href="{{ route('product.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-square fa-fw"></i> {{ trans('message.them_moi') }}</a>
                    </div>
                    <div class="ibox-content">
                        <form name="form1" action="{{route('product.index')}}" method="GET">
                            <div class="row">
                                <div class="col-lg-3 col-sm-6">
                                    @include('partials.limit')
                                </div>
                                <div class="col-lg-3 col-sm-6 offset-6">
                                    <div class="input-group">
                                        <input placeholder="{{trans('message.nhap_ten_san_pham')}}" type="text" value="{{ Request::get('q', null) }}" name="q" class="form-control">
                                        <span class="input-group-append"> 
                                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i></button> 
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <br>
                        <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>{{trans('message.tieu_de')}}</th>
                                        <th class="text-center">{{ trans('message.hinh_anh') }}</th>
                                        <th>{{trans('message.mo_ta')}}</th>
                                        <th>{{trans('message.gia_ban')}}</th>
                                        <th>{{trans('message.nguoi_tao')}}</th>
                                        <th>{{trans('message.ngon_ngu')}}</th>
                                        <th>{{trans('message.trang_thai')}}</th>
                                        <th>{{trans('message.thu_tu')}}</th>
                                        <th width="150px" class="text-center">{{ trans('message.thao_tac') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                @if(isset($products) && $products->count() > 0)
                                    @foreach($products as $product)
                                    <?php $userCreate = \App\Models\User::find($product->created_by);?>
                                        <tr>
                                            <td>{{$product->title}}</td>
                                            <td class="feed-element text-center">
                                                <img alt="image" style="max-height: 50px " src="{{ Tool::getImage($product->avatar, \App\Helpers\Facades\Tool::getConst('PRODUCT_UPLOAD')) }}">
                                            </td>
                                            <td>{{$product->description}}</td>
                                            <td>{{$product->price}} VND</td>
                                            <td><?php echo $userCreate ? $userCreate->full_name : '';?></td>
                                            <td>{{$product->lang == 'vi' ? trans('message.tieng_viet') : trans('message.tieng_anh') }}</td>
                                            <td>{{$product->status == 1 ? trans('message.hoat_dong') : trans('message.tam_dung')}}</td>
                                            <td>{{$product->sort}}</td>
                                            <td class="text-center no-wrap">
                                                <?php if ($product->status) {?>
                                                <form method="post" action="{{ route('product.delete', $product->id ) }}" accept-charset="UTF-8">
                                                    <input name="status" type="hidden" value="0">
                                                    <input name="_method" type="hidden" value="delete">
                                                    <a title="Chi tiết" href="{{route('frontend.service.detail', ['slug' => $product->slug])}}" target="_blank" class="btn btn-white btn-sm"><i class="fa fa-eye"></i></a>
                                                    {{ csrf_field() }}

                                                    @if (Entrust::can('product-edit'))
                                                        <a title="Sửa"
                                                           href="{{ route('product.edit',['id'=>$product->id]) }}"
                                                           class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                                                    @endif
                                                    @if (Entrust::can('product-delete'))
                                                        <button type="button" title="{{trans('message.xoa')}}"
                                                                onclick="event.preventDefault(); commonFunc.confirm(this, '{{trans('message.san_pham')}}', 1, 'vi', 'false', '{{$product->name}}');"
                                                                class="btn btn-danger btn-sm"><i
                                                                    class="fa fa-trash-o"></i>
                                                        </button>
                                                    @endif
                                                </form>
                                                <?php } else {?>
                                                    <form method="post" action="{{ route('product.delete', $product->id ) }}" accept-charset="UTF-8">
                                                    <input name="status" type="hidden" value="1">
                                                    <input name="_method" type="hidden" value="delete">
                                                    <a title="Chi tiết" href="{{route('frontend.service.detail', ['slug' => $product->slug])}}" target="_blank" class="btn btn-white btn-sm"><i class="fa fa-eye"></i></a>
                                                    {{ csrf_field() }}

                                                    @if (Entrust::can('product-edit'))
                                                        <a title="Sửa"
                                                           href="{{ route('product.edit',['id'=>$product->id]) }}"
                                                           class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                                                    @endif
                                                    @if (Entrust::can('product-create'))
                                                        <button type="submit" title="Add" class="btn btn-info btn-sm"><i class="fa fa-plus-square"></i> </button>
                                                    @endif
                                                </form>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                    </tbody>
                                </table>
                        </div>
                        
                        @if(isset($products) && $products->count() > 0)
                        <div class="row">
                            @includeif('partials.pagination', [
                                'pagination'  => $pagination,
                                'data'   => $products->toArray(),
                            ])
                        </div>
                        @else
                            @includeIf('partials.empty')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('body_script')
    @includeif('partials.message')
@endsection
