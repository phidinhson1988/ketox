@extends('layouts.master')
@section('title', trans('message.danh_sach_don_hang'))
@section('content')
<?php use App\Helpers\Facades\Tool;
$dataCart = [];?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">
                    <a href="{{route('product.index')}}"><strong>{{ trans('message.danh_sach_don_hang') }}</strong></a>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h3><i class="fa fa-unlock-alt"></i> {{ trans('message.danh_sach_don_hang') }}</h3>
                    </div>
                    <div class="ibox-content">
                        <form name="form1" action="{{route('cart.index')}}" method="GET">
                            <div class="row">
                                <div class="col-lg-3 col-sm-6">
                                    @include('partials.limit')
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div class="input-group">
                                        <select class="form-control auto-submit" name="status">
                                            <option value="">{{ trans('message.chon_trang_thai') }}</option>
                                            <option value="1">{{ trans('message.chua_su_ly') }}</option>
                                            <option value="2">{{ trans('message.da_su_ly') }}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <br>
                        <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>{{ trans('message.nguoi_dat_hang') }}</th>
                                        <th>{{ trans('message.ngay_dat_hang') }}</th>
                                        <th>{{ trans('message.san_pham') }}</th>
                                        <th>{{ trans('message.trang_thai') }}</th>
                                        <th>{{ trans('message.ngay_xac_nhan') }}</th>
                                        <th width="100px" class="text-center">{{ trans('message.thao_tac') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                @if(isset($carts) && $carts->count() > 0)
                                    @foreach($carts as $cart)
                                    <?php 
                                    $product = \App\Models\Product::select('products.*', 'cart_product.cart_id')
                                            ->join('cart_product', 'cart_product.product_id', '=', 'products.id')
                                            ->where('cart_product.cart_id', $cart->id)->first();
                                    $product->avatar = Tool::getImage($product->avatar, Tool::getConst('PRODUCT_UPLOAD'));
                                    $cart->product = $product;
                                    $dataCart[$cart->id] = $cart;
                                    ?>
                                        <tr>
                                            <td>{{$cart->full_name}}</td>
                                            <td>{{ date_format(date_create($cart->created_time), 'd/m/Y H:i') }}</td>
                                            <td>{{ $cart->product->title }}</td>
                                            <td>{{$cart->status == 2 ? trans('message.da_su_ly') : trans('message.chua_su_ly')}}</td>
                                            <td>{{ $cart->updated_at ? date_format(date_create($cart->updated_at), 'd/m/Y H:i') : '' }}</td>
                                            <td class="text-center no-wrap">
                                                <?php if ($cart->status == 1) {?>
                                                <form class="form-delete-cart" id="form-delete-{{$cart->id}}" method="post" action="{{ route('cart.delete', $cart->id ) }}" accept-charset="UTF-8">
                                                    <input name="status" type="hidden" value="2">
                                                    <input name="_method" type="hidden" value="delete">
                                                    {{ csrf_field() }}
                                                    <button type="button" data="{{$cart->id}}" title="detail" class="btn btn-info btn-sm view-cart"><i class="fa fa-info-circle"></i></button>
                                                    @if (Entrust::can('product-delete'))
                                                        <button data="form-delete-{{$cart->id}}" type="button" title="{{trans('message.xac_nhan')}}" class="btn btn-info btn-sm delete-cart">
                                                            <i class="fa fa-check-square-o"></i>
                                                        </button>
                                                    @endif
                                                </form>
                                                <?php } else {?>
                                                <button type="button" data="{{$cart->id}}" title="detail" class="btn btn-info btn-sm view-cart"><i class="fa fa-info-circle"></i></button>
                                                <?php }?>
                                                    
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                    </tbody>
                                </table>
                        </div>
                        @if(isset($carts) && $carts->count() > 0)
                        <div class="row">
                            @includeif('partials.pagination', [
                                'pagination'  => $pagination,
                                'data'   => $carts->toArray(),
                            ])
                        </div>
                        @else
                            @includeIf('partials.empty')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="detail-modal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{trans('message.chi_tiep_don_hang')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="kt-section kt-section--first">
                                <div class="kt-section__body">
                                    <div class="row" style="margin-bottom: 15px;">
                                        <label class="col-3 col-form-label" style="font-size: 14px;font-weight: 500;color: #464457">{{trans('message.ten_san_pham')}}</label>
                                        <label id="product-name" class="col-9 col-form-label" style="font-size: 14px;font-weight: 500;color: #464457"></label>
                                    </div>
                                    <div class="row" style="margin-bottom: 15px;">
                                        <label class="col-3 col-form-label" style="font-size: 14px;font-weight: 500;color: #464457">{{trans('message.mo_ta')}}</label>
                                        <label id="product-description" class="col-9 col-form-label" style="font-size: 14px;font-weight: 500;color: #464457"></label>
                                    </div>
                                    <div class="row" style="margin-bottom: 15px;">
                                        <label class="col-3 col-form-label" style="font-size: 14px;font-weight: 500;color: #464457">{{trans('message.gia_ban')}}</label>
                                        <label id="product-price" class="col-9 col-form-label" style="font-size: 14px;font-weight: 500;color: #464457"></label>
                                    </div>
                                    <div class="row" style="margin-bottom: 15px;">
                                        <label class="col-3 col-form-label" style="font-size: 14px;font-weight: 500;color: #464457">{{trans('message.nguoi_mua')}}</label>
                                        <label id="product-user-name" class="col-9 col-form-label" style="font-size: 14px;font-weight: 500;color: #464457"></label>
                                    </div>
                                    <div class="row" style="margin-bottom: 15px;">
                                        <label class="col-3 col-form-label" style="font-size: 14px;font-weight: 500;color: #464457">{{trans('message.dia_chi')}}</label>
                                        <label id="product-user-address" class="col-9 col-form-label" style="font-size: 14px;font-weight: 500;color: #464457"></label>
                                    </div>
                                    <div class="row" style="margin-bottom: 15px;">
                                        <label class="col-3 col-form-label" style="font-size: 14px;font-weight: 500;color: #464457">{{trans('message.hom_thu')}}</label>
                                        <label id="product-user-email" class="col-9 col-form-label" style="font-size: 14px;font-weight: 500;color: #464457"></label>
                                    </div>
                                    <div class="row" style="margin-bottom: 15px;">
                                        <label class="col-3 col-form-label" style="font-size: 14px;font-weight: 500;color: #464457">{{trans('message.dien_thoai')}}</label>
                                        <label id="product-user-phone" class="col-9 col-form-label" style="font-size: 14px;font-weight: 500;color: #464457"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="kt-section kt-section--first">
                                <div class="kt-section__body">
                                    <label class="col-12 col-form-label" style="font-size: 14px;font-weight: 500;color: #464457">{{trans('message.anh_san_pham')}}</label>
                                    <label class="col-12 col-form-label">
                                        <img id="product-image" src="" style="max-height: 200px">
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-fw fa-power-off"></i> {{trans('message.dong_lai')}}</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('body_script')
    @includeif('partials.message')
    <script>
        var dataCart = <?php echo json_encode($dataCart);?>;
    $(document).ready(function () {
        $('.auto-submit').change(function(){
            $(this).parents('form').submit();
            return false;
        });
        $('.delete-cart').click(function(){
            var idForm = $(this).attr('data');
           Swal.fire({
                title: '',
                text: "{{trans('message.ban_co_muon_xac_nhan_don_hang_nay_khong')}}",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: "{{trans('message.co')}}",
                cancelButtonText: "{{trans('message.khong')}}",
            }).then((result) => {
                if (result.value) {
                    $('#'+idForm).submit();
                }
            }); 
            return false;
        });
        $('.view-cart').click(function(){
            var idCart = $(this).attr('data');
            $('#product-name').text(dataCart[idCart].product.title);
            $('#product-description').text(dataCart[idCart].product.description);
            $('#product-image').attr('src', dataCart[idCart].product.avatar);
            var price = dataCart[idCart].product.price + ' USD';
            $('#product-price').text(price);
            $('#product-user-name').text(dataCart[idCart].full_name);
            $('#product-user-address').text(dataCart[idCart].address);
            $('#product-user-email').text(dataCart[idCart].email);
            $('#product-user-phone').text(dataCart[idCart].phone);
            $('#detail-modal').modal();
        });
    });
    </script>
@endsection
