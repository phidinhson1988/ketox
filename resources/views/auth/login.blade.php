<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Ketox" />
        <meta name="description" content="Ketox website">
        <title>{{ trans('message.dang_nhap') }} | Ketox</title>

        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css" href="{{ asset('/frontend/css/bootstrap.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/frontend/css/font-web.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/frontend/css/login.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/frontend/font-awesome/css/font-awesome.css') }}">
        <link rel="shortcut icon" type="image/png" href="{{ asset('favicon.png') }}"/>
        
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />
        <!-- END:Place Holder -->
        <script type="text/javascript" src="{{ asset('/frontend/js/jquery-1.11.3.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/frontend/js/bootstrap.min.js') }}"></script>

    </head>
    <body class="login-page">
        <div class="container-fluid">
            <div class="container">
                <div class="row ">
                    <div class="col-sm-12 col-md-1"></div>
                    <div class="col-sm-12 col-md-4 col-md-offset-2">
                        <h1>“ {{ trans('message.title_login') }} ”</h1>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="login-box">
                            <div class="header-login">
                                <!--<p>{{ trans('message.ban_chua_co_tai_khoan') }}? <a href="{{route('frontend.user.register')}}">{{ trans('message.tao_tai_khoan') }}</a></p>-->
                            </div>
                            <div class="content-login">
                                <div class="row">
                                    {!! Form::open(['method' => 'POST', 'route' => 'postLogin', 'class' => 'm-t']) !!}
                                        <div class="col-12 user-login{{ $errors->has('username') ? ' has-error' : '' }}">
                                            <i class="icon-user-login"></i>
                                            <input type="text" name="username" required placeholder="{{ trans('message.ten_dang_nhap') }}" value="{{ old('username') }}" />
                                            @if ($errors->has('username'))
                                                <span class="help-block">{{ $errors->first('username') }}</span>
                                            @endif
                                        </div>
                                        <div class="col-12 pass-login{{ $errors->has('username') ? ' has-error' : '' }}">
                                            <i class="icon-pass-login"></i>
                                            <input type="password" name="password" required placeholder="{{ trans('message.mat_khau') }}" value="{{ old('password') }}" />
                                            @if ($errors->has('password'))
                                                <span class="help-block">{{ $errors->first('password') }}</span>
                                            @endif
                                        </div>
                                        <div class="button-login">
                                            <div class="col-12">
                                                <button type="submit">{{ trans('message.dang_nhap') }}</button>
                                            </div>
                                            <div class="col-12">
                                                <a href="#" data-target="#forgotpwd-modal" data-toggle="modal">{{ trans('message.quen_mat_khau') }}?</a>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                    <div class="col-12 social-login">
                                        <a href="#" class="facebook-login"></a>
                                        <a href="#" class="youtube-login"></a>
                                        <a href="#" class="google-login"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="forgotpwd-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" style="margin-top: 100px;">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                    class="sr-only">{{ trans('message.dong_lai') }}</span></button>
                        <h4 class="modal-title">{{ trans('message.quen_mat_khau') }}</h4>
                    </div>
                    <div class="modal-body">
                        {{ trans('message.de_lay_lai_mat_khau_vui_long_lien_he_voi') }} <strong>{{ trans('message.account_lien_he_quan_tri') }}</strong><br>
                        {{ trans('message.so_dien_thoai') }}: <strong>{{ trans('message.dien_thoai_lien_he_quan_tri') }}</strong>. Email: <strong>{{ trans('message.email_lien_he_quan_tri') }}</strong>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('message.dong_lai') }}</button>
                    </div>
                </div>
            </div>
        </div>
@includeif('layouts.script')
@includeif('partials.message')
    </body>
</html>
