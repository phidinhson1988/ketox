@extends('layouts.master')
@section('title', trans('message.danh_sach_phan_hoi_khach_hang'))
@section('content')
<?php use App\Helpers\Facades\Tool; ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">
                    <a href="{{route('feedback.index')}}"><strong>{{trans('message.danh_sach_phan_hoi_khach_hang')}}</strong></a>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h3><i class="fa fa-unlock-alt"></i> {{trans('message.danh_sach_phan_hoi_khach_hang')}}</h3>
                        <a href="{{ route('feedback.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-square fa-fw"></i> {{ trans('message.them_moi') }}</a>
                    </div>
                    <div class="ibox-content">
                        <form name="form1" action="{{route('feedback.index')}}" method="GET">
                            <div class="row">
                                <div class="col-lg-3 col-sm-6">
                                    @include('partials.limit')
                                </div>
                                <div class="col-lg-3 col-sm-6 offset-6">
                                    <div class="input-group">
                                        <input placeholder="{{trans('message.nhap_ten_san_pham')}}" type="text" value="{{ Request::get('q', null) }}" name="q" class="form-control">
                                        <span class="input-group-append"> 
                                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i></button> 
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <br>
                        <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>{{trans('message.san_pham')}}</th>
                                        <th>{{ trans('message.khach_hang') }}</th>
                                        <th>{{ trans('message.hinh_anh') }}</th>
                                        <th>{{trans('message.noi_dung')}}</th>
                                        <th>{{trans('message.trang_thai')}}</th>
                                        <th width="150px" class="text-center">{{ trans('message.thao_tac') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                @if(isset($feedbacks) && $feedbacks->count() > 0)
                                    @foreach($feedbacks as $feedback)
                                    <?php 
                                    $userCreate = \App\Models\User::find($feedback->user_id);
                                    $productNow = \App\Models\Product::find($feedback->product_id);
                                    ?>
                                        <tr>
                                            <td>{{$productNow ? $productNow->title : ''}}</td>
                                            <td>{{$userCreate ? $userCreate->full_name : ''}}</td>
                                            <td class="feed-element text-center">
                                                <img alt="image" style="max-height: 50px " src="{{ Tool::getImage($feedback->avatar, Tool::getConst('POST_UPLOAD')) }}">
                                            </td>
                                            <td>{!!$feedback->content!!}</td>
                                            <td>{{$feedback->status == 1 ? trans('message.hoat_dong') : trans('message.tam_dung')}}</td>
                                            <td class="text-center no-wrap">
                                                <?php if ($feedback->status) {?>
                                                <form method="post" action="{{ route('feedback.delete', $feedback->id ) }}" accept-charset="UTF-8">
                                                    <input name="status" type="hidden" value="0">
                                                    <input name="_method" type="hidden" value="delete">
                                                    {{ csrf_field() }}

                                                    <a title="Sửa"
                                                       href="{{ route('feedback.edit',['id'=>$feedback->id]) }}"
                                                       class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                                                    <button type="button" title="{{trans('message.xoa')}}"
                                                            onclick="event.preventDefault(); commonFunc.confirm(this, '{{trans('message.phan_hoi')}}', 1, 'vi', 'false', '{{$feedback->name}}');"
                                                            class="btn btn-danger btn-sm"><i
                                                                class="fa fa-trash-o"></i>
                                                    </button>
                                                </form>
                                                <?php } else {?>
                                                    <form method="post" action="{{ route('feedback.delete', $feedback->id ) }}" accept-charset="UTF-8">
                                                    <input name="status" type="hidden" value="1">
                                                    <input name="_method" type="hidden" value="delete">
                                                    {{ csrf_field() }}

                                                    <a title="Sửa"
                                                       href="{{ route('feedback.edit',['id'=>$feedback->id]) }}"
                                                       class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                                                    <button type="submit" title="Add" class="btn btn-info btn-sm"><i class="fa fa-plus-square"></i> </button>
                                                </form>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                    </tbody>
                                </table>
                        </div>
                        
                        @if(isset($feedbacks) && $feedbacks->count() > 0)
                        <div class="row">
                            @includeif('partials.pagination', [
                                'pagination'  => $pagination,
                                'data'   => $feedbacks->toArray(),
                            ])
                        </div>
                        @else
                            @includeIf('partials.empty')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('body_script')
    @includeif('partials.message')
@endsection
