@extends('layouts.master')
@section('title', trans('message.danh_sach_mail_lien_he'))
@section('content')
<?php use App\Helpers\Facades\Tool; ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">
                    <a href="{{route('mail.index')}}"><strong>{{trans('message.danh_sach_mail_lien_he')}}</strong></a>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h3><i class="fa fa-unlock-alt"></i> {{trans('message.danh_sach_mail_lien_he')}}</h3>
                        <a href="{{ route('mail.export') }}" class="btn btn-primary pull-right"><i class="fa fa-download fa-fw"></i> Export mail</a>
                    </div>
                    <div class="ibox-content">
                        <form name="form1" action="{{route('mail.index')}}" method="GET">
                            <div class="row">
                                <div class="col-lg-3 col-sm-6">
                                    @include('partials.limit')
                                </div>
                            </div>
                        </form>
                        <br>
                        <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>{{trans('message.mail')}}</th>
                                        <th>{{ trans('message.ten') }}</th>
                                        <th>{{ trans('message.tieu_de') }}</th>
                                        <th>Ảnh</th>
                                        <th>{{trans('message.noi_dung')}}</th>
                                        <th>{{trans('message.the_loai')}}</th>
                                        <th>{{trans('message.ngay_tao')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                @if(isset($mails) && $mails->count() > 0)
                                    @foreach($mails as $mail)
                                        <tr>
                                            <td>{{$mail->email}}</td>
                                            <td>{{$mail->name}}</td>
                                            <td>{{$mail->subject}}</td>
                                            <?php 
                                            if ($mail->image) {?>
                                            <td style="text-align: center;"><img src="{{route('frontend.home')}}/upload/contacts/{{$mail->image}}" height="50px" /></td>
                                            <?php } else {?>
                                            <td></td>
                                            <?php }?>
                                            <td>{{$mail->message}}</td>
                                            <td>{{$mail->type == 1 ? 'Contact' : 'Subscribe email'}}</td>
                                            <td>{{$mail->created_at}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                    </tbody>
                                </table>
                        </div>
                        
                        @if(isset($mails) && $mails->count() > 0)
                        <div class="row">
                            @includeif('partials.pagination', [
                                'pagination'  => $pagination,
                                'data'   => $mails->toArray(),
                            ])
                        </div>
                        @else
                            @includeIf('partials.empty')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('body_script')
    @includeif('partials.message')
@endsection
