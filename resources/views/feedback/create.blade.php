@extends('layouts.master')
@section('title',trans('message.tao_moi_phan_hoi'))
@section('head_stylesheet')
    <style>
        .table-wrapper-scroll-y {
            display: block;
            max-height: 500px;
            overflow-y: auto;
            -ms-overflow-style: -ms-autohiding-scrollbar;
        }
    </style>
@endsection
@section('content')
<?php use App\Helpers\Facades\Tool;?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('feedback.index') }}">{{trans('message.danh_sach_phan_hoi')}}</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>{{trans('message.tao_moi_phan_hoi')}}</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h3><i class="fa fa-unlock-alt"></i> {{trans('message.tao_moi_phan_hoi')}}</h3>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <form id="form-feedback" name="customFile6" action="{{route('feedback.store')}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">{{trans('message.san_pham')}} <span class="require"></span></label>
                                    <div class="col-sm-10">
                                        <select name="product_id" class='form-control' required>
                                            <option value="">Chọn sản phẩm</option>
                                            <?php if ($products->count() > 0) {
                                            foreach ($products as $product) {?>
                                            <option value="{{$product->id}}">{{$product->title}}</option>
                                            <?php }} ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">{{trans('message.tai_khoan')}} <span class="require"></span></label>
                                    <div class="col-sm-10">
                                        <select name="user_id" class='form-control' required>
                                            <option value="">Chọn tài khoản</option>
                                            <?php if ($users->count() > 0) {
                                            foreach ($users as $user) {?>
                                            <option value="{{$user->id}}">{{$user->full_name}}</option>
                                            <?php }} ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">{{trans('message.noi_dung_chinh')}} </label>
                                    <div class="col-sm-10">
                                        <textarea id="messageArea" rows="7" class="form-control ckeditor" name="content"></textarea>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">{{ trans('message.anh_dai_dien') }}</label>
                                    <div class="col-sm-6 box-upload-file">
                                        <div class="custom-file" style="margin-bottom: 20px;">
                                            <input type="file" name="avatar" accept="image/*" />
                                            <div class="invalid-feedback" style="font-size: 14px;"></div>
                                        </div>
                                    </div>
                                    <br/>
                                </div>

                                <div class="hr-line-dashed"></div>
                                <div class="form-group row">
                                    <div class="col-sm-8 offset-2">
                                        <button class="btn btn-primary btn-lg" type="submit"><i
                                                    class="fa fa-save fa-fw"></i> {{ trans('message.luu_lai') }}</button>
                                        <a href="{{ route('feedback.create') }}">
                                            <button class="btn btn-danger btn-lg" type="button"><i
                                                        class="fa fa-undo fa-fw"></i> {{ trans('message.nhap_lai') }}</button>
                                        </a>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
@stop
@section('body_script')
    <script type="text/javascript" src="{{asset('js/plugins/ckeditor/ckeditor.js?v='.Tool::getConst('VERSION_JS'))}}"></script>
    
   <script>
    CKEDITOR.replace( 'messageArea', {
     height: 500,
     filebrowserImageUploadUrl: "{{route('setting.uploadEditor', ['_token' => csrf_token()])}}"
    });
</script>
    <script>
        var statusImage = false;
        $('.input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "d-m-yyyy",
        });
    </script>
    
@endsection