@extends('layouts.master')
@section('title', 'Danh sách sản phẩm')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">
                    <a href="{{route('product.index')}}"><strong>Danh sách sản phẩm</strong></a>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h3><i class="fa fa-unlock-alt"></i> Danh sách sản phẩm</h3>
                        <a href="{{ route('product.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-square fa-fw"></i> {{ trans('message.them_moi') }}</a>
                    </div>
                    <div class="ibox-content">
                        <form name="form1" action="{{route('product.index')}}" method="GET">
                            <div class="row">
                                <div class="col-lg-3 col-sm-6">
                                    @include('partials.limit')
                                </div>
                                <div class="col-lg-3 col-sm-6 offset-6">
                                    <div class="input-group">
                                        <input placeholder="nhập tên sản phẩm" type="text" value="{{ Request::get('q', null) }}" name="q" class="form-control">
                                        <span class="input-group-append"> 
                                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i></button> 
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <br>
                        <div class="table-responsive">
                            @if(isset($products) && $products->count() > 0)
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>{{ trans('message.ten_quyen') }}</th>
                                        <th>{{ trans('message.mo_ta') }}</th>
                                        <th width="5%" class="text-center">{{ trans('message.thao_tac') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($products as $product)
                                        <tr>
                                            <td>{{$product->name}}</td>
                                            <td>{{$product->description}}</td>
                                            <td class="text-center no-wrap">

                                                <form method="post" action="{{ route('role.delete', $product->id ) }}"
                                                      accept-charset="UTF-8">

                                                    <input name="_method" type="hidden" value="delete">
                                                    <input name="user_id" type="hidden" value="{{$product->id}}">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_token" id="token"
                                                           value="{{ csrf_token() }}">

                                                    @if (Entrust::can('role-edit'))
                                                        <a title="Sửa"
                                                           href="{{ route('role.edit',['id'=>$product->id]) }}"
                                                           class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                                                    @endif
                                                    @if (Entrust::can('role-delete'))
                                                        @if($product->name <>'root')
                                                            <button type="button" title="{{ trans('message.xoa') }}"
                                                                    onclick="event.preventDefault(); commonFunc.confirm(this, '{{ trans('message.quyen_han') }}', 1, 'vi', 'false', '{{$product->name}}');"
                                                                    class="btn btn-danger btn-sm"><i
                                                                        class="fa fa-trash-o"></i>
                                                            </button>
                                                        @endif
                                                    @endif
                                                </form>

                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                        </div>
                        <div class="row">
                            @includeif('partials.pagination', [
                                'pagination'  => $pagination,
                                'data'   => $products->toArray(),
                            ])
                        </div>
                        @else
                            @includeIf('partials.empty')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('body_script')
    @includeif('partials.message')
@endsection
