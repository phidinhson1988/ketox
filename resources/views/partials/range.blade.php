<form action="{{ request()->fullUrl() }}" method="GET">
    <div class="input-group">
        <span class="input-group-addon"><strong>{{ trans('message.hien_thi') }}</strong></span>
        <select name="limit" class="form-control auto-submit">
            @foreach($default_range as $line)
            <option {{ request()->get('limit') == $line ? 'selected' : null }} value="{{ $line }}">{{ $line }} {{ trans('message.pagination.line') }}</option>
            @endforeach
        </select>
        @isset($hidden)
        @foreach($hidden as $field => $value)
            <input type="hidden" name="{{ $field }}" value="{{ $value }}" />
        @endforeach
        @else
            <input type="hidden" name="q" value="{{ request()->get('q', null) }}">
        @endif
    </div>
</form>
