@if (Session::has('message'))
    <script type="text/javascript">
        swal({
            title: "{{ trans('message.thanh_cong') }}",
            text: "{{ Session::get('message') }}",
            type: 'success'
        });
    </script>
@endif
@if (Session::has('error'))
    <script type="text/javascript">
        swal({
            title: "{{ trans('message.that_bai') }}",
            text: "{{ Session::get('error') }}",
            type: 'error'
        });
    </script>
@endif
@if (Session::has('warning'))
    <script type="text/javascript">
        swal({
            title: "{{ trans('message.canh_bao') }}",
            text: "{{ Session::get('warning') }}",
            type: 'warning'
        });
    </script>
@endif
@if (Session::has('info'))
    <script type="text/javascript">
        swal({
            title: "{{ trans('message.thong_bao') }}",
            text: "{{ Session::get('info') }}",
            type: 'question'
        });
    </script>
@endif

