@extends('layouts.master')
@section('title', trans('message.cap_nhap_phan_quyen'))
@section('head_stylesheet')
    <style>
        .table-wrapper-scroll-y {
            display: block;
            max-height: 500px;
            overflow-y: auto;
            -ms-overflow-style: -ms-autohiding-scrollbar;
        }
    </style>
@endsection
@section('content')
<?php use App\Helpers\Facades\Tool;?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}"><i class="fa fa-home"></i> {{ trans('message.trang_chu') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('role.index') }}">{{ trans('message.danh_sach_phan_quyen') }}</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>{{ trans('message.cap_nhap_phan_quyen') }}</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h3><i class="fa fa-unlock-alt"></i> {{ trans('message.cap_nhap_phan_quyen') }} <span
                                    class="text-info">{{$role->name}}</span></h3>
                    </div>
                    <div class="ibox-content">
                        {!! Form::model($role,['method' => 'PATCH','route' => array('role.update', $role->id)]) !!}
                        <div class="form-group row {{ Tool::classError('name', $errors) }}">
                            <label class="col-sm-2 col-form-label">{{ trans('message.nhap_ten_quyen') }} <span
                                        class="require"></span></label>
                            <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control"
                                           value="{{ old('name', $role->name) }}"
                                           placeholder="{{ trans('message.nhap_ten_phan_quyen') }}">
                                    @if ($errors->has('name'))
                                        <span class="help-block">{{ $errors->first('name') }}</span>
                                    @endif
                            </div>
                        </div>
                        <div class="form-group row {{ Tool::classError('description', $errors) }}">
                            <label class="col-sm-2 col-form-label">{{ trans('message.mo_ta') }} </label>
                            <div class="col-sm-10">
                                <textarea rows="4" name="description" class="form-control"
                                          placeholder="{{ trans('message.nhap_mo_ta') }}">{{ old('description', $role->description) }}</textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">{{ $errors->first('description') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">{{ trans('message.quyen_han') }} </label>
                            <div class="col-sm-10">
                                <input type="hidden" name="permissions" value=""/>
                                @if(isset($permissions) && count($permissions) >0)
                                    <div class="table-responsive table-wrapper-scroll-y">
                                        <table class="table table-bordered">
                                            <tbody>
                                            @foreach($permissions as $key => $permission)
                                                <tr>
                                                    @php
                                                        $levelMax = 3;
                                                    @endphp
                                                    @for ($i = 1; $i <= $levelMax; $i++)
                                                        @php
                                                            if (old('permission')) {
                                                                $checked = (in_array($permission['id'], old('permission'))) ? true : false;
                                                            }else {
                                                                $checked = (in_array($permission['id'], $rolePermissions)) ? true : false;
                                                            }
                                                        @endphp
                                                        @if ($i == $permission['level'])
                                                            <td class="table-light">
                                                                <input id="permission-{{ $permission['order'] }}"
                                                                       {{ $checked ? 'checked' : null }} data="{{$permission['order']}}"
                                                                       type="checkbox" class="i-checks check-permission"
                                                                       name="permission[]"
                                                                       value="{{$permission['id']}}">
                                                                <span class="m-l-xs">{{ trans('message.'.$permission['display_name']) }}</span>
                                                            </td>
                                                        @else
                                                            <td class="table-active"></td>
                                                        @endif
                                                    @endfor
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @else
                                    @includeif('partials.empty')
                                @endif
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row">
                            <div class="col-sm-8 offset-2">
                                @if($role->name <>'root')
                                    <button class="btn btn-primary btn-lg" type="submit"><i
                                                class="fa fa-save fa-fw"></i> {{ trans('message.luu_lai') }}</button>
                                @endif
                                <a href="{{ route('role.edit',['id'=>$role->id]) }}">
                                    <button class="btn btn-danger btn-lg" type="button"><i
                                                class="fa fa-undo fa-fw"></i> {{ trans('message.nhap_lai') }}</button>
                                </a>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('body_script')
    <script>
        var dataPermission = <?php echo json_encode($permissions); ?>;
    </script>
    <script type="text/javascript" src="{{asset('js/role.js?v='.\App\Helpers\Facades\Tool::getConst('VERSION_JS'))}}"></script>
@endsection

