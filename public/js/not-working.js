$(document).ready(function () {
    // Set idle time // 1 minutes = 60000
    $(document).idleTimer(timeout * 60000);
});

$(document).on("idle.idleTimer", function () {
    $.ajax({
        url: urlTimeOut,
        type: 'GET',
        success: function () {
            window.location.replace(LogOut + "?status=4");
        },
        error: function (xhr) {
        }
    })
});
