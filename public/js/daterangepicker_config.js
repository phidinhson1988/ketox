function DateRange(selector) {
    this.selector = selector;
    this.keepDate = function () {
        var oldFrom = $(this.selector).find('input[type=text][name=from]').val();
        var oldTo = $(this.selector).find('input[type=text][name=to]').val();
        $(this.selector).on('change', function () { //.input-daterange
            var self = $(this);
            var from = self.find('input[type=text][name=from]');
            var to = self.find('input[type=text][name=to]')
            if (from.val() == '' || to.val() == '') {
                from.val(oldFrom);
                to.val(oldTo);
            }
        });
    }
    this.setup = function () {
        $(this.selector).datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: 'dd-mm-yyyy'
        });
    }
}
var daterange = new DateRange('.input-daterange');
daterange.setup();
daterange.keepDate();

