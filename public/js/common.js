CommonFunction = (function () {
    function Common_Function() {
        //IE8 support
        if (!String.prototype.trim) {
            String.prototype.trim = (function () {
                return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
            });
        }
        if (!Array.isArray) {
            Array.prototype.isArray = (function (x) {
                return x.constructor.toString().indexOf('Array') > -1;
            });
        }
        if (!String.prototype.isNumber) {
            String.prototype.isNumber = (function () {
                return /^\d+$/.test(this);
            });
        }
        this.alertSomething = (function (msg = '') {
            var conf = confirm(msg);
            if (conf) {
                location.reload();
            }
        });
        this.replaceString = (function (find, replace, str) {
            var str = str.toString();
            return str.replaceArray(find, replace);
        });
        this.commonMessage = (function (title, text, type) {

        });
        String.prototype.replaceArray = (function (find, replace) {
            var str = this;
            var regex = null;
            for (var i = 0; i < find.length; i++) {
                regex = new RegExp(find[i], 'g');
                str = str.replace(regex, typeof(replace[i]) == 'undefined' ? replace[0] : replace[i]);
            }
            return str;
        });
        this.checkOne = (function (obj) {
            var self = $(obj);
            if (self.is(':checked')) {
                self.closest('tr').addClass('row-selected');
            } else {
                self.closest('tr').removeClass('row-selected');
            }
        });
        this.confirm = function (elem, module, type, lang, multi = false, text = null) {
            var self = $(elem);
            if (lang == 'vi') {
                var check = messages.canh_bao;
                var action = type == 1 ? messages.dung_hoat_dong : messages.cap_nhat;
                var prefix = multi == false ? '' : '';
                var title = messages.ban_co_muon + " " + action + " " + prefix + " " + module + " " + messages.nay_khong;
                if (text != '') {
                    var text = text;
                }
            } else {
                var check = 'Are you sure';
                var action = type == 1 ? 'delete' : 'update';
                var prefix = multi == false ? '' : 'multi';
                var title = "Do you want to " + action + " " + prefix + " " + module + " there?";
            }
            swal({
                title: check,
                text: title,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: window.DMS.lang == 'en' ? 'OK' : 'Có',
                cancelButtonText: window.DMS.lang == 'en' ? 'Cancel' : 'Không',
            }).then(function (isConfirm) {
                if (isConfirm.value) {
                    if (self.attr('id') == 'remove-checked') {
                        var checked = [];
                        $('input[name="select[]"]').each(function (elem) {
                            if ($(this).is(':checked')) {
                                checked.push($(this).val());
                            }
                            $('input[name=selected]').val(checked);
                        })
                    }
                    self.closest('form').submit();
                } else {
                    $('input[name="select[]"]').each(function (elem) {
                        $(this).prop('checked', false);
                        $(this).closest('tr').removeAttr('class');
                        $('button#remove-checked').prop('disabled', true);
                    });
                    $('input#check-all').prop('checked', false);
                }
            }).catch(swal.noop);
        }
        $('.auto-submit').change(function () {
            $(this).closest('form').submit();
        });
    }

    return new Common_Function();
});
commonFunc = CommonFunction();

$('input[name="input[]"]').change(function () {
    commonFunc.checkOne(this);
});

$('.auto-submit').on('change', function () {
    $(this).closest('form').submit();
});

//Check file type
(function ($) {
    $.fn.checkFileType = function (options) {
        var defaults = {
            allowedExtensions: [],
            success: function () {
            },
            error: function () {
            }
        };
        options = $.extend(defaults, options);

        return this.each(function () {

            $(this).on('change', function () {
                var value = $(this).val(),
                    file = value.toLowerCase(),
                    extension = file.substring(file.lastIndexOf('.') + 1);

                if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else {
                    options.success();

                }

            });

        });
    };

})(jQuery);

// check onchange input
(function ($) {
    $.fn.extend({
        donetyping: function (callback, timeout) {
            timeout = timeout || 1e3; // 1 second default timeout
            var timeoutReference,
                doneTyping = function (el) {
                    if (!timeoutReference) return;
                    timeoutReference = null;
                    callback.call(el);
                };
            return this.each(function (i, el) {
                var $el = $(el);
                // Chrome Fix (Use keyup over keypress to detect backspace)
                // thank you @palerdot
                $el.is(':input') && $el.on('keyup keypress paste change', function (e) {
                    // This catches the backspace button in chrome, but also prevents
                    // the event from triggering too preemptively. Without this line,
                    // using tab/shift+tab will make the focused element fire the callback.
                    if (e.which == 13) {
                        return false;
                    }
                    if (e.type == 'keyup' && e.keyCode != 8) return;

                    // Check if timeout has been set. If it has, "reset" the clock and
                    // start over again.
                    if (timeoutReference) clearTimeout(timeoutReference);
                    timeoutReference = setTimeout(function () {
                        // if we made it here, our timeout has elapsed. Fire the
                        // callback
                        doneTyping(el);
                    }, timeout);
                }).on('blur', function () {
                    // If we can, fire the event since we're leaving the field
                    doneTyping(el);
                });
            });
        }
    });
})(jQuery);

$('.navbar-minimalize').click(function () {
    if ($('body').hasClass('mini-navbar')) {
        $('nav.navbar-static-side').css('z-index', 0);
    } else {
        $('nav.navbar-static-side').css('z-index', 20);
    }
});
