$(function() {
    Morris.Bar({
        element: 'morris-bar-chart1',
        data: [{ y: '2006', a: 60},
            { y: '2007', a: 75},
            { y: '2008', a: 50},
            { y: '2009', a: 75},
            { y: '2010', a: 50},
            { y: '2011', a: 75},
            { y: '2012', a: 100} ],
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Số lượng'],
        hideHover: 'auto',
        resize: true,
        barColors: ['#F2594A'],
    });

    Morris.Bar({
        element: 'morris-bar-chart2',
        data: [{ y: 'Ph', a: 60},
            { y: 'Chất LUONG1231231231323', a: 50},
            { y: 'Tài chính', a: 75},
            { y: 'K', a: 100} ],
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Số lượng'],
        hideHover: 'auto',
        resize: true,
        barColors: ['#1ab394'],
    });
});
