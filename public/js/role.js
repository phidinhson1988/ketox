$(document).ready(function () {
    $("body").on("ifChanged", '.check-permission', function () {
        var self = $(this);
        var idPer = self.attr('data');
        perCurrent = dataPermission[idPer];
        if (!self.parent().hasClass('checked')) {
            self.parent().addClass('checked');
            self.prop("checked", true);
            if (Object.keys(perCurrent.parents).length > 0) {
                Object.keys(perCurrent.parents).forEach(function (keyCheck) {
                    $("#permission-" + perCurrent.parents[keyCheck]).prop("checked", true);
                    $("#permission-" + perCurrent.parents[keyCheck]).parent().addClass('checked');
                })
            }
            if (Object.keys(perCurrent.childs).length > 0) {
                Object.keys(perCurrent.childs).forEach(function (keyCheck) {
                    $("#permission-" + perCurrent.childs[keyCheck]).prop("checked", true);
                    $("#permission-" + perCurrent.childs[keyCheck]).parent().addClass('checked');
                })
            }
        } else {
            self.parent().removeClass('checked');
            self.prop("checked", false);
            if (Object.keys(perCurrent.childs).length > 0) {
                Object.keys(perCurrent.childs).forEach(function (keyCheck) {
                    $("#permission-" + perCurrent.childs[keyCheck]).prop("checked", false);
                    $("#permission-" + perCurrent.childs[keyCheck]).parent().removeClass('checked');
                })
            }
            if (Object.keys(perCurrent.parents).length > 0) {
                Object.keys(perCurrent.parents).forEach(function (keyCheck) {
                    var parentCurrent = dataPermission[perCurrent.parents[keyCheck]];
                    var statusCheck = false;
                    if (Object.keys(parentCurrent.childs).length > 0) {
                        Object.keys(parentCurrent.childs).forEach(function (keyParent) {
                            if ($("#permission-" + parentCurrent.childs[keyParent]).parent().hasClass('checked')) {
                                statusCheck = true;
                            }
                        });
                    }
                    if (!statusCheck) {
                        $("#permission-" + perCurrent.parents[keyCheck]).prop("checked", false);
                        $("#permission-" + perCurrent.parents[keyCheck]).parent().removeClass('checked');
                    }
                })
            }
        }

    });
});
