var statusSubmit = false;
var dataReport = {};
var avatarUpload = '';
var imageUpload = '';
var target = '';
var size;
var ratio = RATIO_USER_AVATAR;
var ratioPosition = RATIO_POSITION_USER_AVATAR;
$('.next-step1').click(function(){
    $('#report-page1').show();
    $('#report-page2').hide();
    $('#report-page3').hide();
    $('#report-page4').hide();
});
$('.next-step2').click(function(){
    var status = checkValidate();
    if (status) {
        $('#report-page1').hide();
        $('#report-page2').show();
        $('#report-page3').hide();
        $('#report-page4').hide();
        var username = $('#username').val();
        $('#id-user span').text(username);
    } else {
        swal.fire(mesageError.loi, mesageError.ban_chua_nhap_du_du_lieu, "error");
        return false;
    }
});
$('.next-step3').click(function(){
    var status = checkValidate();
    if (status) {
        $('#report-page1').hide();
        $('#report-page2').hide();
        $('#report-page3').show();
        $('#report-page4').hide();
    } else {
        swal.fire(mesageError.loi, mesageError.ban_chua_nhap_du_du_lieu, "error");
        return false;
    }
});
$('.next-step4').click(function(){
    $('#report-page1').hide();
    $('#report-page2').hide();
    $('#report-page3').hide();
    $('#report-page4').show();
});
$('#username').donetyping(function() {
    var user = $(this).val();
    if ($.inArray(user, usernames) !== -1) {
        statusSubmit = false;
        $('#username').addClass('has-error');
        $('#username').parent().find('.help-block').text(mesageError.ten_dang_nhap_da_ton_tai).show();
    } else{
        statusSubmit = true;
        $('#username').removeClass('has-error');
        $('#username').parent().find('.help-block').text('').hide();
    }
}, 700);
$('.select-city').click(function(){
    var cityNow = $(this).attr('data');
    $('#input-city').val(cityNow);
    $('#btnGroupDrop1').text(dataCity[cityNow]);
});
$('#gender-male').click(function(){
    $('#select-gender').val(1);
    $('#gender-male').addClass('active');
    $('#gender-female').removeClass('active');
});
$('#gender-female').click(function(){
    $('#select-gender').val(2);
    $('#gender-female').addClass('active');
    $('#gender-male').removeClass('active');
});
$('body').on('keyup keypress paste change', '.get-data', function(){
    var name = $(this).attr('data');
    var report = $(this).val();
    dataReport[name] = report;
    $('#result-report').val(JSON.stringify(dataReport));
    $('.get-data').each(function(){
        var data = $(this).attr('data');
        if (data == name) {
            $(this).val(report);
        }
    });
});
function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
function checkValidate(){
    var status = true;
    $('body').find('.has-error').removeClass('has-error');
    $('body').find('.help-block').text('').hide();
    var username = $('#username').val().trim();
    var password = $('#password').val().trim();
    var full_name = $('#full_name').val().trim();
    var email = $('#email').val().trim();
    if (username == '') {
        status = false;
        $('#username').addClass('has-error');
        $('#username').parent().find('.help-block').text(mesageError.ten_dang_nhap_khong_duoc_de_trong).show();
    }
    if (password == '') {
        status = false;
        $('#password').addClass('has-error');
        $('#password').parent().find('.help-block').text(mesageError.mat_khau_khong_duoc_de_trong).show();
    }
    if (full_name == '') {
        status = false;
        $('#full_name').addClass('has-error');
        $('#full_name').parent().find('.help-block').text(mesageError.ho_va_ten_khong_duoc_de_trong).show();
    }
    if (email !='' && !isEmail(email)) {
        status = false;
        $('#email').addClass('has-error');
        $('#email').parent().find('.help-block').text(mesageError.hom_thu_khong_dung).show();
    }
    if (username != '' && !statusSubmit) {
        status = false;
        $('#username').addClass('has-error');
        $('#username').parent().find('.help-block').text(mesageError.ten_dang_nhap_da_ton_tai).show();
    }
    return status;
}
$('.button-submit-form').click(function(){
    var status = checkValidate();
    if (status) {
        $('#form-data-register').submit();
    } else {
        swal.fire(mesageError.loi, mesageError.ban_chua_nhap_du_du_lieu, "error");
        return false;
    }
});
$('#upload-avatar').click(function(){
    if (avatarUpload != '') {
        $('#cropbox').attr('src', avatarUpload);
        $('.show-image').show();
    } else {
        $('.show-image').hide();
    }
    $('#upload-file-product').modal();
    $('#upload-file-product .show-image').hide();
    target = 'image-detail-user';
    ratio = RATIO_USER_AVATAR;
    ratioPosition = RATIO_POSITION_USER_AVATAR;
});
$('#upload-image').click(function(){
    if (imageUpload != '') {
        $('#cropbox').attr('src', imageUpload);
        $('.show-image').show();
    } else {
        $('.show-image').hide();
    }
    $('#upload-file-product').modal();
    $('#upload-file-product .show-image').hide();
    target = 'image-other';
    ratio = RATIO_USER_IMAGE;
    ratioPosition = RATIO_POSITION_USER_IMAGE;
});

$(document).ready(function() {
    $("#upload_file_image").on('change', function() {
        $("#upload_file_image").parent().find('.help-block').text('').hide();
        var typeFile = ['jpeg', 'JPEG', 'png', 'PNG', 'jpg', 'JPG'];
        var value = $(this).val();
        var file = value.toLowerCase();
        var extension = file.substring(file.lastIndexOf('.') + 1);
        if ($.inArray(extension, typeFile) == -1) {
            $("#upload_file_image").parent().find('.help-block').text(mesageError.dinh_dang_file_anh_khong_dung).show();
            $(this).focus();
        } else {
            //options.success();
            var form = document.forms.namedItem('uploadfileimage');
            var files = document.getElementById('upload_file_image').files;
            var names = $.map(files, function(val) {
                return val.name;
            });
            if (files.length > 0) {
                $('body .loading-page').show();
                oData = new FormData(form);
                oData.append("_token", $("#_token").val());
                var oReq = new XMLHttpRequest();
                oReq.open("POST", uploadFileAttact, true);
                oReq.onload = function(oEvent) {
                    $('#customFile6').val(null);
                    $('body .loading-page').hide();
                    var responseText = jQuery.parseJSON(oReq.responseText);
                    if (responseText.status) {
                            $('#'+target).attr('src', responseText.path);
                            $('#'+target).attr('data', responseText.fileName);
                            $('#'+target+'-input').val(responseText.fileName);
                            $('#'+target+'-input').attr('data', responseText.path);
                            
                            $('#upload-file-product .show-image #cropbox').remove();
                            $('#upload-file-product .show-image').html('<img src="" id="cropbox" class="img" />');
                            $('#upload-file-product .show-image').show();
                            $('#cropbox').attr('src', responseText.path);
                            $('#cropbox').attr('data', responseText.fileName);
                            // crop image
                            setTimeout(function() {
                                $('#cropbox').Jcrop({
                                    setSelect: ratioPosition,
                                    aspectRatio: ratio,
                                    onSelect: function(c) {
                                        size = {x: c.x, y: c.y, w: c.w, h: c.h};
                                        $("#crop").css("visibility", "visible");
                                    }
                                });
                              }, 500);
                        
                        return true;
                    } else {
                        $("#upload_file_image").parent().find('.help-block').text(responseText.error).show();
                        return false;
                    }
                };
                oReq.send(oData);
            }
            return false;
        }
    });
    
    $("#crop-image").click(function() {
        var img = $("#cropbox").attr('src');
        var fileName = $("#cropbox").attr('data');
        var url = urlCropFileImage + '?x=' + Math.round(size.x) + '&y=' + Math.round(size.y) + '&w=' + Math.round(size.w) + '&h=' + Math.round(size.h) + '&img=' + img + '&fileName=' + fileName;
        $.ajax({
            url: url,
            method: 'get',
            success: function(response) {
                var data = jQuery.parseJSON(response);
                if (data.status) {
                    var srcImage = data.path + '?x=' + Math.round(size.x) + '&y=' + Math.round(size.y) + '&w=' + Math.round(size.w) + '&h=' + Math.round(size.h);
                    $('#' + target).attr('src', srcImage);
                    $('#' + target+'-input').val(data.nameFile);
                }
                $('#upload-file-product').modal('toggle');
            }, error: function(){
                swal.fire(mesageError.loi, mesageError.co_loi_xay_ra_thu_lai_sau, "error");
            }
        });
    });
    $('#image-detail-user').click(function() {
        target = 'image-detail-user';
        ratio = RATIO_USER_AVATAR;
        ratioPosition = RATIO_POSITION_USER_AVATAR;
        var path = $('#image-detail-user-input').attr('data');
        var fileName = $(this).attr('data');
        $('#upload-file-product .show-image #cropbox').remove();
        $('#upload-file-product .show-image').html('<img src="" id="cropbox" class="img" />');
        $('#cropbox').attr('src', path);
        $('#cropbox').attr('data', fileName);
        // crop image
        setTimeout(function() {
            $('#cropbox').Jcrop({
                setSelect: ratioPosition,
                aspectRatio: ratio,
                onSelect: function(c) {
                    size = {x: c.x, y: c.y, w: c.w, h: c.h};
                    $("#crop").css("visibility", "visible");
                }
            });
          }, 500);
        $('#upload-file-product').modal({backdrop: 'static', keyboard: false});
    });
    $('#image-other').click(function() {
        target = 'image-other';
        ratio = RATIO_USER_IMAGE;
        ratioPosition = RATIO_POSITION_USER_IMAGE;
        var path = $('#image-other-input').attr('data');
        var fileName = $(this).attr('data');
        $('#upload-file-product .show-image #cropbox').remove();
        $('#upload-file-product .show-image').html('<img src="" id="cropbox" class="img" />');
        $('#cropbox').attr('src', path);
        $('#cropbox').attr('data', fileName);
        // crop image
        setTimeout(function() {
            $('#cropbox').Jcrop({
                setSelect: ratioPosition,
                aspectRatio: ratio,
                onSelect: function(c) {
                    size = {x: c.x, y: c.y, w: c.w, h: c.h};
                    $("#crop").css("visibility", "visible");
                }
            });
          }, 500);
        $('#upload-file-product').modal({backdrop: 'static', keyboard: false});
    });
});