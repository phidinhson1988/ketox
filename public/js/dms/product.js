var slugOld = '';
var imageUpdate = [];
var statusSubmit = true;

function getRatioSetting(type) {
    var ratio = 1;
    var ratioSelect = [0, 0, 200, 200];
    switch (type) {
        case '1':
            ratio = RATIO_IMAGE_HERO;
            ratioSelect = RATIO_POSITION_HERO;
            break;
        case '2':
            ratio = RATIO_IMAGE_DETAIL;
            ratioSelect = RATIO_POSITION_DETAIL;
            break;
        case '3':
            ratio = RATIO_IMAGE_FULL;
            ratioSelect = RATIO_POSITION_FULL;
            break;
        case '6':
            ratio = RATIO_IMAGE_CARD;
            ratioSelect = RATIO_POSITION_CARD;
            break;
        default:
            ratio = RATIO_IMAGE_CARD;
            ratioSelect = RATIO_POSITION_CARD;
            break;
    }
    return {ratio: ratio, ratioSelect: ratioSelect};
}
$(document).ready(function() {
    $("#customFile1,#customFile2,#customFile3,#customFile6").on('change', function() {
        var idDiv = $(this).attr('id');
        $("#" + idDiv).parent().find('.help-block').text('').hide();
        var type = $(this).attr('type-file');
        var nameForm = $(this).attr('data-name');
        var typeFile = [];
        switch (type) {
            case 'image':
                typeFile = ['jpeg', 'JPEG', 'png', 'PNG', 'jpg', 'JPG'];
                break;
            case 'zip':
                typeFile = ['zip'];
                break;
            default:
                break;
        }
        var value = $(this).val();
        var file = value.toLowerCase();
        var extension = file.substring(file.lastIndexOf('.') + 1);
        if ($.inArray(extension, typeFile) == -1) {
            $("#" + idDiv).parent().find('.help-block').text('Incorrect image file format (.png, .jpg, jpeg, .gif)').show();
            $(this).focus();
        } else {
            //options.success();
            var form = document.forms.namedItem(nameForm);
            var files = document.getElementById(idDiv).files;
            var names = $.map(files, function(val) {
                return val.name;
            });
            if (files.length > 0) {
                $('body .loading-page').show();
                oData = new FormData(form);
                oData.append("_token", $("#_token").val());
                var oReq = new XMLHttpRequest();
                oReq.open("POST", uploadFileAttact, true);
                oReq.onload = function(oEvent) {
                    $('#customFile6').val(null);
                    $('body .loading-page').hide();
                    var responseText = jQuery.parseJSON(oReq.responseText);
                    if (responseText.status) {
                        if (type == 'image') {
                            var typeImage = 1;
                            switch (idDiv) {
                                case 'customFile1':
                                    typeImage = '1';
                                    break;
                                case 'customFile2':
                                    typeImage = '2';
                                    break;
                                case 'customFile3':
                                    typeImage = '3';
                                    break;
                                case 'customFile6':
                                    typeImage = '6';
                                    break;
                                default:
                                    break;
                            }
                            var dataRatio = getRatioSetting(typeImage);
                            if (idDiv == 'customFile1' || idDiv == 'customFile6') {
                                idTarget = idDiv + '-image-hero';
                                imageNew = '<li class="col-12">';
                                imageNew += '<img class="new-image-crop" data-ratio="' + typeImage + '" data="' + responseText.fileName + '" src="' + responseText.path + '" />';
                                imageNew += '<input class="' + idDiv + '" id="' + idTarget + '" type="hidden" data="' + responseText.path + '" value="' + responseText.fileName + '" />';
                                imageNew += '</li>';
                                var listImage = $("#" + idDiv).parents('.box-upload-file').find('.list-show-image');
                                listImage.html(imageNew);
                                $('#image-file').val(responseText.fileName);
                            } else {
                                var stt = $("#" + idDiv).parents('.box-upload-file').find('.list-show-image').attr('data');
                                if (stt == '' || typeof stt === 'undefined') {
                                    stt = 1;
                                }
                                idTarget = idDiv + '-image-' + stt;
                                imageNew = '<li class="col-4">';
                                imageNew += '<span class="delete-image">x</span>';
                                imageNew += '<img class="new-image-crop" data="' + responseText.fileName + '" src="' + responseText.path + '" />';
                                imageNew += '<input class="' + idDiv + '" id="' + idTarget + '" type="hidden" name="" data="' + responseText.path + '" value="' + responseText.fileName + '" />';
                                imageNew += '</li>';
                                var listImage = $("#" + idDiv).parents('.custom-file').parent().find('.list-show-image');
                                listImage.append(imageNew);
                                stt = parseInt(stt) + 1;
                                $("#" + idDiv).parents('.box-upload-file').find('.list-show-image').attr('data', stt);
                            }
                            $('#viewDetail .show-image #cropbox').remove();
                            $('#viewDetail .show-image').html('<img src="" id="cropbox" class="img" />');
                            $('#cropbox').attr('src', responseText.path);
                            $('#cropbox').attr('data', responseText.fileName);
                            // crop image
                            setTimeout(function() {
                                $('#cropbox').Jcrop({
                                    setSelect: dataRatio.ratioSelect,
                                    aspectRatio: dataRatio.ratio,
                                    onSelect: function(c) {
                                        size = {x: c.x, y: c.y, w: c.w, h: c.h};
                                        $("#crop").css("visibility", "visible");
                                    }
                                });
                            }, 500);
                            $('#viewDetail').modal({backdrop: 'static', keyboard: false});
                        } else if (type == 'zip') {
                            var newFile = '<li>';
                            newFile += '<span class="delete-file">x</span>';
                            newFile += '<input class="' + idDiv + '" data-size="' + responseText.size + '" type="hidden" name="" value="' + responseText.fileName + '" />';
                            newFile += '<span class="file-name">' + responseText.fileName + ' - ('+responseText.size+')</span>';
                            newFile += '</li>';
                            var listFile = $("#" + idDiv).parents('.box-upload-file').find('.list-file-product');
                            listFile.append(newFile);
                        }
                        return true;
                    } else {
                        $("#" + idDiv).parent().find('.help-block').text(responseText.error).show();
                        return false;
                    }
                };
                oReq.send(oData);
            }
            return false;
        }
    });
    
    $("#crop-image").click(function() {
        $('body .loading-page').show();
        var img = $("#cropbox").attr('src');
        var fileName = $("#cropbox").attr('data');
        var url = urlCropFileImage + '?x=' + Math.round(size.x) + '&y=' + Math.round(size.y) + '&w=' + Math.round(size.w) + '&h=' + Math.round(size.h) + '&img=' + img + '&fileName=' + fileName;
        $.ajax({
            url: url,
            method: 'get',
            success: function(response) {
                $('body .loading-page').hide();
                var data = jQuery.parseJSON(response);
                if (data.status) {
                    var srcImage = data.path + '?x=' + Math.round(size.x) + '&y=' + Math.round(size.y) + '&w=' + Math.round(size.w) + '&h=' + Math.round(size.h);
                    $('#' + idTarget).parent().find('img').attr('src', srcImage);
                    $('#' + idTarget).val(data.nameFile);
                    $('#image-file').val(data.nameFile);

                    imageUpdate.push(fileName);
                }
                $('#viewDetail').modal('toggle');
            }, error: function(){
                $('body .loading-page').hide();
                swal.fire("Error!", 'An error occurred. try later.', "error");
            }
        });
    });
    $('body').on('click', '.delete-image', function() {
        var self = $(this);
        Swal.fire({
            title: 'Are you sure?',
            text: "Delete image, You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete this!'
        }).then((result) => {
            if (result.value) {
                self.parent().remove();
            }
        });
    });
    $('body').on('click', '.new-image-crop', function() {
        var typeImage = $(this).attr('data-ratio');
        var dataRatio = getRatioSetting(typeImage);
        var path = $(this).parent().find('input').attr('data');
        var fileName = $(this).attr('data');
        idTarget = $(this).parent().find('input').attr('id');
        $('#viewDetail .show-image #cropbox').remove();
        $('#viewDetail .show-image').html('<img src="" id="cropbox" class="img" />');
        $('#cropbox').attr('src', path);
        $('#cropbox').attr('data', fileName);
        // crop image
        setTimeout(function() {
            $('#cropbox').Jcrop({
                setSelect: dataRatio.ratioSelect,
                aspectRatio: dataRatio.ratio,
                onSelect: function(c) {
                    size = {x: c.x, y: c.y, w: c.w, h: c.h};
                    $("#crop").css("visibility", "visible");
                }
            });
        }, 500);
        $('#viewDetail').modal({backdrop: 'static', keyboard: false});
    });
    $('#title').change(function(){
        $('#slug').val($('#title').val());
        checkSlug();
    });
    function checkSlug(){
        var slug = $('#slug').val().trim();
        slugCheck = slug.replace(slugOld, '');
        if (slugCheck.length > 0) {
            $('#slug').parent().removeClass('has-error');
            $('#slug').parent().find('.help-block').text('').hide();
            slugOld = slug;
            $.ajax({
                url: urlCheckSlug,
                method: 'post',
                data: {
                    _token: $("#_token").val(),
                    slug: slug,
                    productId: $('#productId').val()
                },
                success: function(response) {
                    response = JSON.parse(response);
                    if (response.status) {
                        $('#slug').val(response.slug);
                        statusSubmit = true;
                    } else {
                        statusSubmit = false;
                        $('#slug').val(response.slug);
                        $('#slug').parent().addClass('has-error');
                        $('#slug').parent().find('.help-block').text(response.message).show();
                    }
                }
            });
        }
    }
    $('#slug').donetyping(function() {
        checkSlug();
    }, 700);
    
    $("#form-product").submit(function(){
        if (!statusSubmit) {
            return false;
        }
        return true;
    });
});

