$(document).ready(function () {
    checkShowTime();
});
$('select[name=frequency]').change(function () {
    checkShowTime();
});

function checkShowTime() {
    var elem = $('select[name=frequency]').val();
    if (elem == 'daily') {
        $('#day-choice').addClass('show_screen');
        $('#hour-choice').removeClass('show_screen')
    }
    if (elem == 'weekly') {
        $('#day-choice').removeClass('show_screen');
        $('#hour-choice').removeClass('show_screen')
    }
    if (elem == 'monthly') {
        $('#day-choice').addClass('show_screen');
        $('#hour-choice').addClass('show_screen');
    }
}
