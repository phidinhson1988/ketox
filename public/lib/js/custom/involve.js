/**
 * Involve JS
 *
 * @package  DMS - Sumitomo
 * @author   SonPD1
 * @license  FPT
 */

$('#filter-date .date-from').datepicker({
    todayBtn: "linked",
    format: "dd-mm-yyyy",
    keyboardNavigation: false,
    forceParse: false,
    calendarWeeks: true,
    autoclose: true
});
$('#filter-date .date-to').datepicker({
    todayBtn: "linked",
    format: "dd-mm-yyyy",
    keyboardNavigation: false,
    forceParse: false,
    calendarWeeks: true,
    autoclose: true
});
function approvedDocument(ids, text) {
    swal({
        buttons: {
            confirm: {
                text: 'OK',
                value: true,
                visible: true,
                className: "",
                closeModal: true,
            },
            cancel: {
                text: 'Cancel',
                value: null,
                visible: true,
                className: "",
                closeModal: true,
                buttons: true,
            },
        },
        dangerMode: true,
        title: 'Cảnh báo',
        text: text,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: returnMsg.yes,
        cancelButtonText: returnMsg.no
    }).then(function (isConfirm) {
        if (isConfirm.value) {
            $.ajax({
                url: urlApprovedDocument,
                method: 'post',
                data: {ids: ids, _token: $("#_token").val()},
                success: function (response) {
                    response = JSON.parse(response);
                    if (response.status) {
                        swal(returnMsg.thanh_cong, returnMsg.phe_duyet_thanh_cong, "success");
                        location.reload();
                    } else {
                        swal(returnMsg.that_bai, returnMsg.phe_duyet_khong_thanh_cong, "error");
                    }
                }
            });
            return true;
        } else {
            return false;
        }
    }).catch(swal.noop);


}
;
function controlIcheck() {
    $("body").find("input[name=job_close]").each(function () {
        if ($("#job_close_all").is(':checked')) {
            $(this).prop('checked', true);
            $(this).parent().addClass("checked");
        } else {
            $(this).prop('checked', false);
            $(this).parent().removeClass("checked");
        }
    });
}
// event nhận sự kiện click vao nut Icheck
$("#job_close_all").on("ifChanged", controlIcheck);
$("#approved-all-checkbox").click(function () {
    var docIds = [];
    $("body").find("input[name=job_close]").each(function () {
        var self = $(this);
        var idCurrent = self.val();
        if (self.is(':checked')) {
            docIds.push(idCurrent);
        }
    });
    if (docIds.length > 0) {
        approvedDocument(docIds, returnMsg.co_muon_duyet_toan_bo_tai_lieu);
    } else {
        swal(returnMsg.that_bai, returnMsg.khong_co_tai_lieu_duoc_chon, "error");
    }
    
    return false;
});
$(".approved-document").click(function () {
    var docIds = [];
    var self = $(this);
    docIds.push(self.attr("data"));
    approvedDocument(docIds, returnMsg.co_muon_duyet_tai_lieu_khong);
    return false;
});