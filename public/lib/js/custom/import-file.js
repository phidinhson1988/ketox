/**
* Import file excel
*
* @package  PTMS - Sumitomo
* @author   SonPD1
* @license  FPT
*/

function resetImport() {
    $('#uploadfile').val('');
    $('#path-import').val('');
    $("#message-error-import").hide();
    $("#total-error").hide();
    $("#response-import table thead").html('');
    $("#response-import table tbody").html('');
    $('#save-import-file').prop('disabled', true);
    fileOld = '';
    return true;
}
var fileOld = '';
// Import file
$('#uploadfile').checkFileType({
    allowedExtensions: ['xlsx', 'xls'],
    success: function () {
        $("#uploadfile").parent().removeClass('has-error');
        $("#message-error-import").hide();
        $("#total-error").hide();
        $("#response-import").hide();
        $("#response-import table thead").html('');
        $("#response-import table tbody").html('');
        var form = document.forms.namedItem("fileinfo");
        var files = document.getElementById("uploadfile").files;
        var names = $.map(files, function(val) { return val.name; });
        if (files.length > 0 && fileOld != names[0]) {
            fileOld = names[0];
            oData = new FormData(form);
            oData.append("_token", $("#_token").val());
            var oReq = new XMLHttpRequest();
            oReq.open("POST", urlImportFile, true);
            oReq.onload = function (oEvent) {
                var responseText = jQuery.parseJSON(oReq.responseText);
                if (responseText.status) {
                    // get label table
                    var htmlImportThead = '<tr>';
                    htmlImportThead += '<th>1</th>';
                    var data = responseText.data;
                    for (var k in data[0]) {
                        if (typeof data[0][k] !== 'function') {
                            htmlImportThead += '<th>' + k + '</th>';
                        }
                    }
                    htmlImportThead += '</tr>';
                    // get content table
                    var htmlImportTbody = '';
                    for (var i = 0; i < data.length; i++) {
                        htmlImportTbody += '<tr>';
                        htmlImportTbody += '<td>' + (i + 2) + '</td>';
                        for (var j in data[i]) {
                            var value = (data[i][j] || data[i][j] === 0) ? data[i][j] : '&nbsp';
                            if (typeof value.date != 'undefined') {
                                var msec = Date.parse(value.date);
                                var d = new Date(msec);
                                var day = d.getDate();
                                var month = d.getMonth() + 1;
                                var year = d.getFullYear();
                                value = day+'/'+month+'/'+year;
                            }
                            htmlImportTbody += '<td>' + value + '</td>';
                        }
                        htmlImportTbody += '</tr>';
                    }
                    // show data import
                    $("#response-import").show();
                    $("#response-import table thead").html(htmlImportThead);
                    $("#response-import table tbody").html(htmlImportTbody);
                    $("#path-import").val(responseText.path);
                    $('#save-import-file').prop('disabled', false);
                } else {
                    $("#uploadfile").parent().addClass('has-error');
                    $('#save-import-file').prop('disabled', true);
                    swal(responseText.error, "", "error");
                }
                return false;
            };
            oReq.send(oData);
        }
        return false;
    },
    error: function () {
        $("#uploadfile").parent().addClass('has-error');
        $("#response-import").hide();
        resetImport();
        swal(returnMsg.that_bai, returnMsg.can_import_dung_file_excel, "error");
    }
});
$(document).ready(function () {
    $("#save-import-file").click(function () {
        $("#message-error-import").hide();
        $("#total-error").hide();
        var fileName = $("#path-import").val();
        if (fileName) {
            $.ajax({
                url: urlSaveFile,
                method: 'post',
                data: {fileName: fileName, _token: $("#_token").val()},
                success: function (response) {
                    response = JSON.parse(response);
                    var htmlError = '';
                    if (response.status) {
                        $("#response-import").hide();
                        swal(returnMsg.thanh_cong, returnMsg.import_file_thanh_cong, "success");
                        resetImport();
                    } else {
                        swal(returnMsg.that_bai, returnMsg.import_file_khong_thanh_cong, "error");
                        if (typeof response.errorHeader !== 'undefined' && response.errorHeader.length > 0) {
                            $("#response-import").hide();
                            $("#response-import table thead").html('');
                            $("#response-import table tbody").html('');
                            var htmlImportThead = '<tr>';
                            htmlImportThead += '<th>1</th>';
                            var dataHeader = response.dataHeader;
                            for (var k in dataHeader) {
                                if (typeof dataHeader[k] !== 'function') {
                                    htmlImportThead += '<th>' + dataHeader[k].data + '</th>';
                                }
                            }
                            htmlImportThead += '</tr>';
                            // get content table
                            var htmlImportTbody = '';
                            var dataContent = response.dataContent;
                            for (var i = 0; i < dataContent.length; i++) {
                                htmlImportTbody += '<tr>';
                                htmlImportTbody += '<td>' + (i + 2) + '</td>';
                                for (var j in dataContent[i]) {
                                    var value = (dataContent[i][j].data || dataContent[i][j].data === 0) ? dataContent[i][j].data : '&nbsp';
                                    htmlImportTbody += '<td >' + value + '</td>';
                                }
                                htmlImportTbody += '</tr>';
                            }
                            // show data import
                            $("#response-import").show();
                            $("#response-import table thead").html(htmlImportThead);
                            $("#response-import table tbody").html(htmlImportTbody);
                            htmlError += '<tr>';
                                htmlError += '<td><span class="label label-danger">Bug</span></td>';
                                htmlError += '<td class="issue-info">';
                                    htmlError += '<b>' + returnMsg.import_loi + ':</b>';
                                    htmlError += '<small>'+response.errorHeader+'</small>';
                                htmlError += '</td>';
                            htmlError += '</tr>';
                        } else if (Object.keys(response.errors).length > 0) {
                            $("#response-import").hide();
                            $("#response-import table thead").html('');
                            $("#response-import table tbody").html('');
                            // get label table
                            var htmlImportThead = '<tr>';
                            htmlImportThead += '<th>1</th>';
                            var dataHeader = response.dataHeader;
                            for (var k in dataHeader) {
                                if (typeof dataHeader[k] !== 'function') {
                                    var classHead = dataHeader[k].status ? '' : 'border-danger';
                                    htmlImportThead += '<th class="' + classHead + '">' + dataHeader[k].data + '</th>';
                                }
                            }
                            htmlImportThead += '</tr>';
                            // get content table
                            var htmlImportTbody = '';
                            var dataContent = response.dataContent;
                            for (var i = 0; i < dataContent.length; i++) {
                                htmlImportTbody += '<tr>';
                                htmlImportTbody += '<td>' + (i + 2) + '</td>';
                                for (var j in dataContent[i]) {
                                    var classContent = dataContent[i][j].status ? '' : 'border-danger';
                                    var value = (dataContent[i][j].data || dataContent[i][j].data === 0) ? dataContent[i][j].data : '&nbsp';
                                    if (typeof value.date != 'undefined') {
                                        var msec = Date.parse(value.date);
                                        var d = new Date(msec);
                                        var day = d.getDate();
                                        var month = d.getMonth() + 1;
                                        var year = d.getFullYear();
                                        value = month+'/'+day+'/'+year;
                                    }
                                    htmlImportTbody += '<td class="' + classContent + '">' + value + '</td>';
                                }
                                htmlImportTbody += '</tr>';
                            }
                            // show data import
                            $("#response-import").show();
                            $("#response-import table thead").html(htmlImportThead);
                            $("#response-import table tbody").html(htmlImportTbody);
                            // show error
                            var totalError = 0;
                            Object.keys(response.errors).forEach(function (key, index) {
                                for (var j = 0; j < response.errors[key].length; j++) {
                                    htmlError += '<tr>';
                                        htmlError += '<td width="5%" class="text-center">';
                                            htmlError += '<span class="label label-danger text-center text-uppercase">Lỗi dòng ' + key + ' cột ' + response.errors[key][j].key + '</span>';
                                        htmlError += '</td>';
                                        htmlError += '<td class="project-title">';
                                            htmlError += '<span> '+response.errors[key][j].error+'</span>';
                                        htmlError += '</td>';
                                    htmlError += '</tr>';
                                    totalError++;
                                }
                            });
                            $("#total-error").text(returnMsg.tim_thay + ' '+totalError+' ' + returnMsg.error + '.').show();
                        }
                    }
                    htmlError += '</div>';
                    $("#message-error-import table tbody").html(htmlError);
                    $("#message-error-import").show();
                }
            });
        } else {
            swal(returnMsg.that_bai, returnMsg.import_file_that_bai, "error");
        }
        return false;
    });
    $("#reset-import").click(function () {
        resetImport();
        return false;
    });
});
