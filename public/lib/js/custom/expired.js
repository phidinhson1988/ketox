$(document).ready(function () {
    
    c3.generate({
        bindto: '#stocked1',
        data: {
            columns: [
                quantities
            ],
            colors: {
                'Chờ phê duyệt': '#F2594A',
            },
            type: 'bar',
            labels: true
        },
        bar: {
          width: 50,  
        },
        axis: {
            x: {
                label: returnMsg.phong_ban,
                type: 'category',
                categories: docTypeNames
            },
            y: {
                label: returnMsg.so_luong,
            },
        }
    });

    // get test settings
    var byRow = $('body').hasClass('test-rows');

    // apply matchHeight to each item container's items
    $('.items-container').each(function () {
        $(this).find('.item-child').matchHeight({
            byRow: byRow
        });
    });
});
