/**
 * System JS
 *
 * @package  DMS - Sumitomo
 * @author   SonPD1
 * @license  FPT
 */

function showTactTime(){
    var html = "";
    var htmlTraining = "";
    if (Object.keys(tactTimeData.approve).length > 0) {
        var i = 1;
        Object.keys(tactTimeData.approve).forEach(function(key){
            html += '<tr data="cap_'+i+'">';
                html += '<td style="width: 150px;padding-top: 15px;">' + returnMsg.phe_duyet_cap + ' '+i+'</td>';
                html += '<td><input type="number" step="any" min="1" class="form-control add-date-tact-time" placeholder="' + returnMsg.nhap_so_gio_phe_duyet + '" value="'+tactTimeData.approve[key]+'"></td>';
                html += '<td style="width: 68px">';
                html += '<button class="btn btn-danger btn-lg remove-tact-time" type="button"><i class="fa fa-trash fa-fw"></i></button>';
                html += '</td>';
            html += '</tr>';
            i++;
        });
    }
    htmlCreate = "";
    htmlCreate += '<tr id="box-create">';
        htmlCreate += '<td style="width: 150px;padding-top: 15px;">' + returnMsg.tao_moi_tai_lieu + '</td>';
        htmlCreate += '<td><input type="number" step="any" min="1" class="form-control add-date-create" placeholder="' + returnMsg.nhap_so_gio_tao_moi_tai_lieu + '" value="'+tactTimeData.create+'"></td>';
        htmlCreate += '<td style="width: 68px"></td>';
    htmlCreate += '</tr>';
    
    htmlTraining += '<tr id="box-training">';
        htmlTraining += '<td style="width: 150px;padding-top: 15px;">Training</td>';
        htmlTraining += '<td><input type="number" step="any" min="1" class="form-control add-date-training" placeholder="' + returnMsg.nhap_so_gio_phe_duyet + '" value="'+tactTimeData.training+'"></td>';
        htmlTraining += '<td style="width: 68px"></td>';
    htmlTraining += '</tr>';
    htmlTraining += '<tr id="box-jobclose">';
        htmlTraining += '<td style="width: 150px;padding-top: 15px;">Job Close</td>';
        htmlTraining += '<td><input type="number" step="any" min="1" class="form-control add-date-jobclose" placeholder="' + returnMsg.nhap_so_gio_phe_duyet + '" value="'+tactTimeData.jobclose+'"></td>';
        htmlTraining += '<td style="width: 68px"></td>';
    htmlTraining += '</tr>';
    $("#tact-time tbody").html(html);
    $("#tact-time-training tbody").html(htmlTraining);
    $("#tact-time-create tbody").html(htmlCreate);
};

$(document).ready(function () {
    showTactTime();
    $("#add-tact-time").click(function(){
        var key = 'cap_'+(Object.keys(tactTimeData.approve).length + 1);
        tactTimeData.approve[key] = '';
        showTactTime();
    });
    $("body").on("click", ".remove-tact-time", function(){
        var keyDelete = $(this).parent().parent().attr("data");
        var dataCurrent = {};
        if (Object.keys(tactTimeData.approve).length > 1) {
            var i = 1;
            Object.keys(tactTimeData.approve).forEach(function(key){
                if (keyDelete != key) {
                    var keyNow = 'cap_'+i;
                    dataCurrent[keyNow] = tactTimeData.approve[key];
                    i++;
                }
            });
            tactTimeData.approve = dataCurrent;
            showTactTime();
        } else {
            swal(returnMsg.that_bai, returnMsg.phai_co_it_nhat_1_cap_phe_duyet, "error");
        }
    });
    $("body").on("change", ".add-date-tact-time", function(){
        var keyAdd = $(this).parent().parent().attr("data");
        var value = $(this).val();
        value = parseFloat(value);
        if (value <= 0) {
            value = 1;
        }
        $(this).val(value);
        tactTimeData.approve[keyAdd] = value;
    });
    $("body").on("change", ".add-date-training", function(){
        var value = $(this).val();
        value = parseFloat(value);
        if (value <= 0) {
            value = 1;
        }
        $(this).val(value);
        tactTimeData.training = value;
    });
    $("body").on("change", ".add-date-create", function(){
        var value = $(this).val();
        value = parseFloat(value);
        if (value <= 0) {
            value = 1;
        }
        $(this).val(value);
        tactTimeData.create = value;
    });
    $("body").on("change", ".add-date-jobclose", function(){
        var value = $(this).val();
        value = parseFloat(value);
        if (value <= 0) {
            value = 1;
        }
        $(this).val(value);
        tactTimeData.jobclose = value;
    });
    
    $("#submit-tact-time").click(function(){
        $.ajax({
            url: urlUpdateTactTime,
            method: 'post',
            data: {tactTimeData: JSON.stringify(tactTimeData), _token: $("#_token").val()},
            success: function (response) {
                response = JSON.parse(response);
                if (response.status) {
                    swal(returnMsg.thanh_cong, returnMsg.cap_nhat_thanh_cong, "success");
                } else {
                    swal(returnMsg.that_bai, returnMsg.co_loi_xay_ra_thu_lai_sau, "error");
                }
            }
        });
    });
});
