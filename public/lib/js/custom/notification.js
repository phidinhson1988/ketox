function approvedDocument(ids, text) {
    var query = $('input[name=query]').val();
    var currentPage = $('input[name=currentPage]').val();
    var lastPage = $('input[name=lastPage]').val();
    var total = $('.i-check-item').length;
    var choiced = $('.i-check-item:checked').length;
    var back = 'false';
    if (total == 1 || (choiced == total && currentPage == lastPage)) {
        back = 'true';
    }
    swal({
        title: returnMsg.canh_bao,
        text: text,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: returnMsg.yes,
        cancelButtonText: returnMsg.no
    }).then(function (isConfirm) {
        if (isConfirm.value) {
            $.ajax({
                url: deleteAll,
                method: 'POST',
                data: {
                    ids: ids,
                    _token: $("#_token").val(),
                    url: location.href,
                    query: query, back: back
                },
                success: function (response) {
                    response = JSON.parse(response);
                    if (response.status) {
                        setTimeout(function () {
                            swal({
                                title: returnMsg.thanh_cong,
                                text: returnMsg.xoa_thong_bao_thanh_cong,
                                type: "success"
                            }).then(function () {
                                location.href = response.url;
                            }).catch(swal.noop);
                        }, 100);
                    } else {
                        swal(returnMsg.that_bai, returnMsg.xoa_thong_bao_khong_thanh_cong, "error");
                    }
                }
            });
            return true;
        } else {
            return false;
        }
    }).catch(swal.noop);
}

function controlIcheck() {
    $("body").find("input[name=job_close]").each(function () {
        if ($("#job_close_all").is(':checked')) {
            $(this).prop('checked', true);
            $(this).parent().addClass("checked");
        } else {
            $(this).prop('checked', false);
            $(this).parent().removeClass("checked");
        }
    });
}

function controlIcheckOnly() {

    if ($('.i-check-item:checked').length == $('.i-check-item').length) {
        $("#job_close_all").prop('checked', true);
        $("#job_close_all").parent().addClass("checked");
    } else {
        $("#job_close_all").prop('checked', false);
        $("#job_close_all").parent().removeClass("checked");
    }
}

// event nhận sự kiện click vao nut Icheck
$("#job_close_all").on("ifChanged", controlIcheck);
$(".i-check-item").on("ifChanged", controlIcheckOnly);
$("#approved-all-checkbox").click(function () {
    var docIds = [];
    $("body").find("input[name=job_close]").each(function () {
        var self = $(this);
        var idCurrent = self.val();
        if (self.is(':checked')) {
            docIds.push(idCurrent);
        }
    });
    if (docIds.length > 0) {
        approvedDocument(docIds, returnMsg.co_muon_xoa_toan_bo_thong_bao_khong);
    } else {
        swal(returnMsg.that_bai, returnMsg.khong_co_thong_bao_duoc_chon, "error");
    }

    return false;
});
