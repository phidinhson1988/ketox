/**
 * Document type JS
 *
 * @package  DMS - Sumitomo
 * @author   SonPD1
 * @license  FPT
 */

function showTactTime(status){
    var html = "";
    var htmlTraining = "";
    if (Object.keys(tactTimeData.approve).length > 0) {
        var i = 1;
        Object.keys(tactTimeData.approve).forEach(function(key){
            html += '<tr data="cap_'+i+'">';
                html += '<td style="width: 150px;padding-top: 15px;">Phê duyệt cấp '+i+'</td>';
                html += '<td style="width: 150px;padding-top: 15px;">Số giờ phê duyệt:</td>';
                html += '<td><input type="number" step="any" class="form-control add-date-tact-time" placeholder="Nhập số giờ phê duyệt" value="'+tactTimeData.approve[key]+'"></td>';
                html += '<td style="width: 68px">';
                html += '<button class="btn btn-danger btn-lg remove-tact-time" type="button"><i class="fa fa-trash fa-fw"></i></button>';
                html += '</td>';
            html += '</tr>';
            i++;
        });
    }
    if (status) {
        var checked = tactTimeData.training.status ? ' checked' : '';
        htmlTraining += '<tr id="box-training">';
            htmlTraining += '<td style="width: 150px;padding-top: 15px;"><input id="add-training" type="checkbox" class="i-checks" name=""'+checked+'> Training</td>';
            htmlTraining += '<td style="width: 150px;padding-top: 15px;">Số giờ phê duyệt:</td>';
            htmlTraining += '<td><input type="number" step="any" class="form-control add-date-training" placeholder="Nhập số giờ phê duyệt" value="'+tactTimeData.training.value+'"></td>';
            htmlTraining += '<td style="width: 68px"></td>';
        htmlTraining += '</tr>';
        htmlTraining += '<tr id="box-jobclose">';
            htmlTraining += '<td style="width: 150px;padding-top: 15px;"><input id="add-jobclose" type="checkbox" class="i-checks" name=""'+checked+'> Job Close</td>';
            htmlTraining += '<td style="width: 150px;padding-top: 15px;">Số giờ phê duyệt:</td>';
            htmlTraining += '<td><input type="number" step="any" class="form-control add-date-jobclose" placeholder="Nhập số giờ phê duyệt" value="'+tactTimeData.jobclose.value+'"></td>';
            htmlTraining += '<td style="width: 68px"></td>';
        htmlTraining += '</tr>';
        $("#tact-time-training tbody").html(htmlTraining);

        var htmlCreate = '';
        htmlCreate += '<tr id="box-create">';
            htmlCreate += '<td style="width: 150px;padding-top: 15px;"> Tạo mới</td>';
            htmlCreate += '<td style="width: 150px;padding-top: 15px;">Số giờ phê duyệt:</td>';
            htmlCreate += '<td><input type="number" step="any" class="form-control add-date-create" placeholder="Nhập số giờ phê duyệt" value="'+tactTimeData.create.value+'"></td>';
            htmlCreate += '<td style="width: 68px"></td>';
        htmlCreate += '</tr>';
        $("#tact-time-create tbody").html(htmlCreate);
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    }
    $("#tact-time tbody").html(html);
};
showTactTime(true);
$(document).ready(function () {
    var data = $(".add_level").attr('data-value');
    $('input[name=approve_level]').change(function () {
        var val = $(this).val();
        if (val < 1 || val > 10) {
            $('input[name=approve_level]').val(1);
        }
    });

    $(".ibox-content").on('click', '.add_level', function () {
        var users = JSON.parse(data);
        var no = parseInt($(this).attr('data-no')) + 1;
        var html = '<div class=\"form-group row\" id="add_no_' + no + '">';
        html += '<label class=\"col-sm-2 col-form-label\">Cấp ' + no + '</label>';
        html += '<div class=\"col-sm-6\">';
        html += '<div class=\"input-group\">';
        html += '<select class=\"select2 form-control\" multiple name=\"level[]\">';
        $.each(users, function (index, val) {
            html += '<option value=\" ' + val.id + ' \">' + val.name + '</option>';
        });
        html += '</select>';
        html += '</div>';
        html += '</div>';
        html += '<div class=\"col-sm-2\">';
        html += '<button class=\"btn btn-danger\" data-no = \"' + no + ' \" type=\"button\" tabindex=\"-1\">';
        html += '<span class=\"fa fa-trash\" aria-hidden=\"true\"></span>';
        html += '</button>';
        html += '<button class=\"btn btn-success add_level\" data-no = \"' + no + ' \"  style =\" margin-left: 3px; \" type=\"button\" ><i class=\"fa fa-plus\"></i></button>';
        html += '</div>';
        html += '</div>';

        $('#more_level').append(html);
        $(this).siblings('button').addClass('remove_level');
        $(this).remove();
        $('.select2').select2();
    });

    $(".ibox-content").on('click', '.remove_level', function () {
        var data = $(this).attr('data-no');
        $(this).closest("#add_no_" + data).remove();
    });
    $("#add-tact-time").click(function(){
        var key = 'cap_'+(Object.keys(tactTimeData.approve).length + 1);
        tactTimeData.approve[key] = (typeof tactTimeSetting.approve[key] !== "undefined") ? tactTimeSetting.approve[key] : 1;
        showTactTime(false);
    });
    $("body").on("click", ".remove-tact-time", function(){
        var keyDelete = $(this).parent().parent().attr("data");
        var dataCurrent = {};
        if (Object.keys(tactTimeData.approve).length > 1) {
            var i = 1;
            Object.keys(tactTimeData.approve).forEach(function(key){
                if (keyDelete != key) {
                    var keyNow = 'cap_'+i;
                    dataCurrent[keyNow] = tactTimeData.approve[key];
                    i++;
                }
            });
            tactTimeData.approve = dataCurrent;
            showTactTime(false);
        } else {
            swal(returnMsg.that_bai, returnMsg.phai_co_it_nhat_1_cap_phe_duyet, "error");
        }
    });
    $("body").on("change", ".add-date-tact-time", function(){
        var valueDate = $(this).val();
        var keyAdd = $(this).parent().parent().attr("data");
        if (valueDate == "" || parseFloat(valueDate) <= 0) {
            valueDate = 1;
        }
        $(this).val(valueDate);
        tactTimeData.approve[keyAdd] = valueDate;
    });
    $("body").on("change", ".add-date-training", function(){
        var valueDate = $(this).val();
        var keyAdd = $(this).parent().parent().attr("data");
        if (valueDate == "" || parseFloat(valueDate) <= 0) {
            valueDate = 1;
        }
        $(this).val(valueDate);
        tactTimeData.training.value = valueDate;
    });
    $("body").on("change", ".add-date-create", function(){
        var valueDate = $(this).val();
        var keyAdd = $(this).parent().parent().attr("data");
        if (valueDate == "" || parseFloat(valueDate) <= 0) {
            valueDate = 1;
        }
        $(this).val(valueDate);
        tactTimeData.create.value = valueDate;
    });
    $("body").on("change", ".add-date-jobclose", function(){
        var valueDate = $(this).val();
        var keyAdd = $(this).parent().parent().attr("data");
        if (valueDate == "" || parseFloat(valueDate) <= 0) {
            valueDate = 1;
        }
        $(this).val(valueDate);
        tactTimeData.jobclose.value = valueDate;
    });
    function updateTraining(){
        var status = ($("#add-training").is(':checked')) ? true : false;
        if (status) {
            $("#add-training").prop('checked', true);
            $("#add-training").parent().addClass("checked");
            $("#add-jobclose").prop('checked', true);
            $("#add-jobclose").parent().addClass("checked");
            $("#add-frequency").prop('checked', true);
            $("#add-frequency").parent().addClass("checked");
        } else {
            $("#add-training").prop('checked', false);
            $("#add-training").parent().removeClass("checked");
            $("#add-jobclose").prop('checked', false);
            $("#add-jobclose").parent().removeClass("checked");
            $("#add-frequency").prop('checked', false);
            $("#add-frequency").parent().removeClass("checked");
        }
        tactTimeData.training.status = status;
        tactTimeData.jobclose.status = status;
    }
    function updateJobclose(){
        var status = ($("#add-jobclose").is(':checked')) ? true : false;
        if (status) {
            $("#add-training").prop('checked', true);
            $("#add-training").parent().addClass("checked");
            $("#add-jobclose").prop('checked', true);
            $("#add-jobclose").parent().addClass("checked");
        } else {
            $("#add-training").prop('checked', false);
            $("#add-training").parent().removeClass("checked");
            $("#add-jobclose").prop('checked', false);
            $("#add-jobclose").parent().removeClass("checked");
        }
        tactTimeData.training.status = status;
        tactTimeData.jobclose.status = status;
    }
    $("#add-training").on("ifChanged", updateTraining);
    $("#add-jobclose").on("ifChanged", updateJobclose);

    $("#form-doc-type").submit(function(){
        $("#tact-time-input").val(JSON.stringify(tactTimeData));
        return true;
    });
});
